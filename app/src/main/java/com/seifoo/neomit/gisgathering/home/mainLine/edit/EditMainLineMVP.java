package com.seifoo.neomit.gisgathering.home.mainLine.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface EditMainLineMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent(MainLineObject mainLine);
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> diameter, ArrayList<Integer> diameterIds, int diameterIndex,
                              ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex,
                              ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex,
                              ArrayList<String> material, ArrayList<Integer> materialIds, int materialIndex);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator);

        void setSerialNumber(String data);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);

        void setData(String streetName, String streetNumber);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex,
                              ArrayList<String> subDistrict, ArrayList<Integer> subDistrictIds, int subDistrictIndex,
                              ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds, int waterScheduleIndex,
                              ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds, int assetStatusIndex,
                              ArrayList<String> remarks, ArrayList<Integer> remarksIds, int remarksIndex);

        void setLastUpdated(String date);

        void setAssigned(String date);

        void setData(String dmaZone, String subName, String sectorName);
    }

    interface Presenter {
        void requestFirstSpinnersData(MainLineObject mainLine);

        void requestSecondSpinnersData(MainLineObject mainLine);

        void requestPassFirstStepDataToActivity(MainLineObject mainLine);

        void requestPassSecondStepDataToActivity(MainLineObject mainLine);

        void requestEditMainLine(MainLineObject mainLine, String createdAt, long id);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString, int index);
    }
}
