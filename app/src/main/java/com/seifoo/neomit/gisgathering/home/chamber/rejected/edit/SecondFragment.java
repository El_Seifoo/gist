package com.seifoo.neomit.gisgathering.home.chamber.rejected.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, EditRejectedChambersMVP.View, EditRejectedChambersMVP.SecondView {
    private Spinner subDistrictNameSpinner, diameterAirValveSpinner, diameterWAValveSpinner, diameterISOValveSpinner, diameterFlowMeterSpinner, typeSpinner, remarksSpinner;
    private EditText streetNameEditText, streetNumberEditText, subNameEditText, sectorNameEditText, imageNumberEditText, updatedDateEditText;

    private EditRejectedChambersMVP.Presenter presenter;

    public static SecondFragment newInstance(ChamberObject chamber) {
        SecondFragment fragment = new SecondFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("ChamberObj", chamber);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chamber_second_edit, container, false);

        presenter = new EditRejectedChambersPresenter(this, this, new EditRejectedChambersModel());

        subDistrictNameSpinner = (Spinner) view.findViewById(R.id.sub_district_spinner);
        diameterAirValveSpinner = (Spinner) view.findViewById(R.id.diameter_air_valve_spinner);
        diameterWAValveSpinner = (Spinner) view.findViewById(R.id.diameter_wa_valve_spinner);
        diameterISOValveSpinner = (Spinner) view.findViewById(R.id.diameter_iso_valve_spinner);
        diameterFlowMeterSpinner = (Spinner) view.findViewById(R.id.diameter_flow_meter_spinner);
        typeSpinner = (Spinner) view.findViewById(R.id.type_spinner);
        remarksSpinner = (Spinner) view.findViewById(R.id.remarks_spinner);


        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        streetNumberEditText = (EditText) view.findViewById(R.id.street_num);
        subNameEditText = (EditText) view.findViewById(R.id.sub_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);
        imageNumberEditText = (EditText) view.findViewById(R.id.image_num);
        updatedDateEditText = (EditText) view.findViewById(R.id.last_updated);

        updatedDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestDatePickerDialog(updatedDateEditText.getText().toString().trim());
            }
        });

        presenter.requestSecondSpinnersData((ChamberObject) getArguments().getSerializable("ChamberObj"));
        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new ChamberObject(subDistrictNameIds.get(subDistrictNameSpinner.getSelectedItemPosition()),
                streetNameEditText.getText().toString().trim(), streetNumberEditText.getText().toString().trim(), subNameEditText.getText().toString().trim(),
                sectorNameEditText.getText().toString().trim(),
                diameterAirValveIds.get(diameterAirValveSpinner.getSelectedItemPosition()), diameterWAValveIds.get(diameterWAValveSpinner.getSelectedItemPosition()),
                diameterISOValveIds.get(diameterISOValveSpinner.getSelectedItemPosition()), diameterFlowMeterIds.get(diameterFlowMeterSpinner.getSelectedItemPosition()),
                imageNumberEditText.getText().toString().trim(), typeIds.get(typeSpinner.getSelectedItemPosition()), updatedDateEditText.getText().toString().trim(),
                remarksIds.get(remarksSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> subDistrictNameIds, diameterAirValveIds, diameterWAValveIds, diameterISOValveIds, diameterFlowMeterIds, typeIds, remarksIds;

    @Override
    public void loadSpinnersData(ArrayList<String> subDistrictName, ArrayList<Integer> subDistrictNameIds, int subDistrictNameIndex, ArrayList<String> diameterAirValve, ArrayList<Integer> diameterAirValveIds, int diameterAirValveIndex, ArrayList<String> diameterWAValve, ArrayList<Integer> diameterWAValveIds, int diameterWAValveIndex, ArrayList<String> diameterISOValve, ArrayList<Integer> diameterISOValveIds, int diameterISOValveIndex, ArrayList<String> diameterFlowMeter, ArrayList<Integer> diameterFlowMeterIds, int diameterFlowMeterIndex, ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex, ArrayList<String> remarks, ArrayList<Integer> remarksIds, int remarksIndex) {
        this.subDistrictNameIds = subDistrictNameIds;
        this.diameterAirValveIds = diameterAirValveIds;
        this.diameterWAValveIds = diameterWAValveIds;
        this.diameterISOValveIds = diameterISOValveIds;
        this.diameterFlowMeterIds = diameterFlowMeterIds;
        this.typeIds = typeIds;
        this.remarksIds = remarksIds;


        subDistrictNameSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, subDistrictName));
        subDistrictNameSpinner.setSelection(subDistrictNameIndex);
        diameterAirValveSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameterAirValve));
        diameterAirValveSpinner.setSelection(diameterAirValveIndex);
        diameterWAValveSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameterWAValve));
        diameterWAValveSpinner.setSelection(diameterWAValveIndex);
        diameterISOValveSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameterISOValve));
        diameterISOValveSpinner.setSelection(diameterISOValveIndex);
        diameterFlowMeterSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameterFlowMeter));
        diameterFlowMeterSpinner.setSelection(diameterFlowMeterIndex);
        typeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, type));
        typeSpinner.setSelection(typeIndex);
        remarksSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, remarks));
        remarksSpinner.setSelection(remarksIndex);

    }


    @Override
    public void setLastUpdated(String date) {
        updatedDateEditText.setText(date);
    }

    @Override
    public void setData(String streetName, String streetNumber, String subName, String sectorName, String imageNumber) {
        streetNameEditText.setText(streetName);
        streetNumberEditText.setText(streetNumber);
        subNameEditText.setText(subName);
        sectorNameEditText.setText(sectorName);
        imageNumberEditText.setText(imageNumber);
    }
}
