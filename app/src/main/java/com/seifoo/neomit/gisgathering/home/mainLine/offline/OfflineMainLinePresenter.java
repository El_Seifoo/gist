package com.seifoo.neomit.gisgathering.home.mainLine.offline;

import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.home.mainLine.edit.EditMainLineActivity;
import com.seifoo.neomit.gisgathering.home.mainLine.offline.details.OfflineMainLineDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class OfflineMainLinePresenter implements OfflineMainLineMVP.Presenter, OfflineMainLineModel.VolleyCallback, OfflineMainLineModel.DBCallback {
    private OfflineMainLineMVP.View view;
    private OfflineMainLineModel model;

    public OfflineMainLinePresenter(OfflineMainLineMVP.View view, OfflineMainLineModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestMainLinesData(int type) {
        model.getMainLinesList(view.getAppContext(), this, type, 0);
    }

    @Override
    public void requestRemoveMainLineObjById(long id, int position) {
        model.removeLineObject(view.getActContext(), this, id, position);
    }

    private static final int SHOW_ALL_IN_MAP_REQUEST = 10;

    @Override
    public void OnListItemClickListener(int viewId, int position, MainLineObject mainLine) {
        switch (viewId) {
            case R.id.more_details:
                view.navigateDestination("OfflineMainLineObj", mainLine, OfflineMainLineDetailsActivity.class);
                break;
            case R.id.remove_item:
                this.requestRemoveMainLineObjById(mainLine.getId(), position);
                break;
            case R.id.edit_item:
                this.requestEditMainLineData(position, mainLine);
                break;
            case R.id.map:
                view.navigateToMap(SHOW_ALL_IN_MAP_REQUEST, mainLine, position);
        }
    }

    private static final int EDIT_MAIN_LINE_OBJECT_REQUEST = 1;
    @Override
    public void requestEditMainLineData(int position, MainLineObject mainLine) {
        Intent intent = new Intent(view.getAppContext(), EditMainLineActivity.class);
        intent.putExtra("MainLineObj", mainLine);
        intent.putExtra("position", position);
        view.navigateToEditMainLine(intent, EDIT_MAIN_LINE_OBJECT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_MAIN_LINE_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            view.updateListItem(data.getExtras().getInt("position"), (MainLineObject) data.getSerializableExtra("editedMainLineObj"));
            return;
        }

        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            this.requestMainLinesData(view.getSpinnerSelectedPosition());
        }
    }

    @Override
    public void requestUploadData(int type) {
        model.getMainLinesList(view.getAppContext(), this, type, 1);
    }

    @Override
    public void showLocations(ArrayList<MainLineObject> mainLines) {
        view.navigateToMap2(SHOW_ALL_IN_MAP_REQUEST,mainLines);
    }

    @Override
    public void onSyncingResponse(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        ArrayList<Long> accepted = new ArrayList<>(), rejected = new ArrayList<>();
        for (int i = 0; i < responseArray.length(); i++) {
            if (responseArray.getJSONObject(i).getString("status").equals("success"))
                accepted.add(responseArray.getJSONObject(i).getLong("id"));
            else rejected.add(responseArray.getJSONObject(i).getLong("id"));
        }

        if (!accepted.isEmpty()) {
            for (int i = 0; i < accepted.size(); i++) {
                model.removeLineObject(view.getActContext(), this, accepted.get(i), -1);
            }
        }
    }

    @Override
    public void onSyncingError(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onGetMainLinesListCalled(ArrayList<MainLineObject> mainLines, int index) {
        if (index == 0) {
            if (mainLines.isEmpty())
                view.showEmptyListText();
            else {
                boolean isVisible = false;
                for (int i = 0; i < mainLines.size(); i++) {
                    if (mainLines.get(i).getLatitude() != null && mainLines.get(i).getLongitude() != null) {
                        if (!mainLines.get(i).getLatitude().isEmpty() && !mainLines.get(i).getLongitude().isEmpty()) {
                            isVisible = true;
                            break;
                        }
                    }
                }
                view.loadMainLinesData(mainLines,isVisible);
            }
        } else {
            if (mainLines.isEmpty())
                view.showToastMessage(view.getActContext().getString(R.string.no_main_lines_to_synchronize));
            else {
                view.showProgress();
                model.uploadValvesData(view.getActContext(), this, mainLines);
            }
        }
    }

    @Override
    public void onRemoveMainLineObjectCalled(int flag, int position, long id) {
        if (flag > 0) {
            if (position == -1) {
                view.removeListItem(id);
            } else
                view.removeListItem(position);
        } else view.showToastMessage(view.getActContext().getString(R.string.failed_to_delete));
    }
}
