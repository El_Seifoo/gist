package com.seifoo.neomit.gisgathering.home.valve.info.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class GetValveInfoDetailsModel {

    public void returnValidGeoMaster(Context context,DBCallback callback, ValveObject valve, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds(ids, types, lang), valve);
    }

    protected interface DBCallback {
        void onConvertingIdsCalled(ArrayList<String> strings, ValveObject valve);
    }
}
