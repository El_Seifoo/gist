package com.seifoo.neomit.gisgathering.home.meter.rejected.edit;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class EditRejectedMeterPresenter implements EditRejectedMeterMVP.Presenter, EditRejectedMeterModel.DBCallback, EditRejectedMeterModel.VolleyCallback {
    private static final int PICK_LOCATION_REQUEST = 5050;
    private EditRejectedMeterMVP.MainView mainView;
    private EditRejectedMeterMVP.View view;
    private EditRejectedMeterMVP.FirstView firstView;
    private EditRejectedMeterMVP.SecondView secondView;
    private EditRejectedMeterMVP.ThirdView thirdView;
    private EditRejectedMeterMVP.FourthView fourthView;
    private EditRejectedMeterMVP.FifthView fifthView;
    private EditRejectedMeterModel model;


    @Override
    public void requestFirstStepData(MeterObject meterObject) {
        //meterDiameterSpinner 0, meterBrandSpinner 1, meterTypeSpinner 2, meterStatusSpinner 3, meterMaterialSpinner 4, meterRemarksSpinner 5, readTypeSpinner 6
        model.getGeoMasters(mainView.getAppContext(), this, new int[]{0, 1, 2, 3, 4, 5, 6},
                new int[]{meterObject.getMeterMeterDiameterId(), meterObject.getMeterMeterBrandId(),
                        meterObject.getMeterMeterTypeId(), meterObject.getMeterMeterStatusId(),
                        meterObject.getMeterMeterMaterialId(), meterObject.getMeterMeterRemarksId(), meterObject.getMeterReadTypeId()}
                , 1);

        String location = "";
        if (meterObject.getMeterLatitude() != null && meterObject.getMeterLongitude() != null) {
            if (!meterObject.getMeterLatitude().toLowerCase().equals("null") && !meterObject.getMeterLongitude().trim().equals("null")) {
                location = meterObject.getMeterLatitude().concat(",").concat(meterObject.getMeterLongitude());
            }
        }
        firstView.setLocation(location);
        firstView.setSerialNumber(meterObject.getMeterSerialNumber());
        firstView.setPlateNumber(meterObject.getMeterPlateNumber());
    }

    @Override
    public void requestSecondStepData(MeterObject meterObject) {
        //pipeAfterMeterSpinner 7, pipeSizeSpinner 8, meterMaintenanceAreaSpinner 9, meterBoxPositionSpinner 10, meterBoxTypeSpinner 11,
        //            coverStatusSpinner 12, locationSpinner 13, buildingUsageSpinner 14, waterConnectionTypeSpinner 17, pipeMaterialSpinner 18
        model.getGeoMasters(mainView.getAppContext(), this, new int[]{7, 8, 9, 10, 11, 12, 13, 14, 17, 18},
                new int[]{meterObject.getMeterPipeAfterMeterId(), meterObject.getMeterPipeSizeId(),
                        meterObject.getMeterMaintenanceAreaId(), meterObject.getMeterMeterBoxPositionId(),
                        meterObject.getMeterMeterBoxTypeId(), meterObject.getMeterCoverStatusId(),
                        meterObject.getMeterLocationId(), meterObject.getMeterBuildingUsageId(),
                        meterObject.getMeterWaterConnectionTypeId(), meterObject.getMeterPipeMaterialId()}
                , 2);
    }

    @Override
    public void requestThirdStepData(MeterObject meterObject) {
        //valveTypeSpinner 20, valveStatusSpinner 21, enabledSpinner 22, reducerDiameterSpinner 23
        model.getGeoMasters(mainView.getAppContext(), this, new int[]{20, 21, 22, 23},
                new int[]{meterObject.getMeterValveTypeId(), meterObject.getMeterValveStatusId(),
                        meterObject.getMeterEnabledId(), meterObject.getMeterReducerDiameterId()}
                , 3);

        thirdView.setDate(meterObject.getMeterLastUpdated());
        thirdView.setData(meterObject.getMeterPostCode(), meterObject.getMeterScecoNumber(),
                meterObject.getMeterNumberOfElectricMeters(), meterObject.getMeterLastReading(), meterObject.getMeterNumberOfFloors());
    }

    @Override
    public void requestFourthStepData(MeterObject meterObject) {
        //subBuildingTypeSpinner 15, waterScheduleSpinner 16, newMeterBrandSpinner 19, districtNameSpinner 25
        model.getGeoMasters(mainView.getAppContext(), this, new int[]{15, 16, 19, 25},
                new int[]{meterObject.getMeterSubBuildingTypeId(), meterObject.getMeterWaterScheduleId(),
                        meterObject.getMeterBrandNewMeterId(), meterObject.getMeterDistrictName()}
                , 4);

        fourthView.setMeterData(meterObject.getMeterStreetName(), meterObject.getMeterSectorName(), meterObject.getMeterIsSewerConnectionExist().equals("1") ? 0 : 1,
                meterObject.getGroundElevation(), meterObject.getElevation(), meterObject.getMeterHcn(), meterObject.getMeterMeterAddress());
    }

    @Override
    public void requestFifthStepData(MeterObject meterObject) {

        fifthView.setMeterData(meterObject.getMeterDmaZone(), meterObject.getMeterLocationNumber(),
                meterObject.getMeterBuildingNumber(), meterObject.getMeterBuildingDuplication(),
                meterObject.getMeterBuildingNumberM(), meterObject.getMeterBuildingDescription(),
                meterObject.getMeterStreetNumber(), meterObject.getMeterStreetNumberM(),
                meterObject.getMeterSubName(), meterObject.getMeterArabicName(),
                meterObject.getMeterCustomerActive().equals("1") ? 0 : 1);
    }

    @Override
    public void requestPassFirstStepDataToActivity(MeterObject meterObject) {
        if (meterObject.getMeterLatitude() == null) {
            firstView.showToastMessage(mainView.getAppContext().getString(R.string.location_is_required));
            firstView.showLocationError(mainView.getAppContext().getString(R.string.location_is_required));
            return;
        }
        if (mainView.getAppContext() instanceof EditRejectedMeterActivity) {
            ((EditRejectedMeterActivity) mainView.getAppContext()).passFirstStepData(meterObject);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(MeterObject meterObject) {
        if (mainView.getAppContext() instanceof EditRejectedMeterActivity) {
            ((EditRejectedMeterActivity) mainView.getAppContext()).passSecondStepData(meterObject);
        }
    }

    @Override
    public void requestPassThirdStepDataToActivity(MeterObject meterObject) {
        if (mainView.getAppContext() instanceof EditRejectedMeterActivity) {
            ((EditRejectedMeterActivity) mainView.getAppContext()).passThirdStepData(meterObject);
        }
    }

    @Override
    public void requestPassFourthStepDataToActivity(MeterObject meterObject) {
        if (mainView.getAppContext() instanceof EditRejectedMeterActivity) {
            ((EditRejectedMeterActivity) mainView.getAppContext()).passFourthStepData(meterObject);
        }
    }

    @Override
    public void requestPassFifthStepDataToActivity(MeterObject meterObject) {
        if (mainView.getAppContext() instanceof EditRejectedMeterActivity) {
            ((EditRejectedMeterActivity) mainView.getAppContext()).passFifthStepData(meterObject);
        }
    }

    @Override
    public void requestUploadRejectedMeter(MeterObject editedMeterObject) {
        view.showProgress();
        editedMeterObject.setMeterId(view.getMeterId());
        editedMeterObject.setCreatedAt(returnValidNumbers(view.getCreatedAt()));
//        model.uploadMeterData(view.getActContext(), this, editedMeterObject);
        model.insertMeterObj(view.getActContext(), this, editedMeterObject);
    }

    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.serial_num_qr:
                        requestQrCode(((EditRejectedMeterActivity) mainView.getAppContext()), mainView.getAppContext().getString(R.string.serial_number));
                        break;
                    case R.id.hcn_qr:
                        requestQrCode(((EditRejectedMeterActivity) mainView.getAppContext()), mainView.getAppContext().getString(R.string.hcn));
                        break;
                    case R.id.meter_address_qr:
                        requestQrCode(((EditRejectedMeterActivity) mainView.getAppContext()), mainView.getAppContext().getString(R.string.meter_address));
                        break;
                    case R.id.plate_num_qr:
                        requestQrCode(((EditRejectedMeterActivity) mainView.getAppContext()), mainView.getAppContext().getString(R.string.plate_number));
                        break;
                }
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext, String flagString) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (flagString.equals(mainView.getAppContext().getString(R.string.hcn))) {
            flag = 1;
        } else if (flagString.equals(mainView.getAppContext().getString(R.string.meter_address))) {
            flag = 2;
        } else if (flagString.equals(mainView.getAppContext().getString(R.string.plate_number))) {
            flag = 3;
        }
        if (flag == 0 || flag == 3){
            firstView.initializeScanner(integrator, flag);
        }else {
            fourthView.initializeScanner(integrator, flag);
        }
    }

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void onFourthViewActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void handleQRScannerResult(String contents, int flag) {
        contents = contents.replaceAll("[^0-9]", "");
        if (flag == 0) {
            firstView.setSerialNumber(contents);
        } else if (flag == 1) {
            fourthView.setHCN(contents);
        } else if (flag == 2) {
            fourthView.setMeterAddress(contents);
        } else {
            firstView.setPlateNumber(contents);
        }
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", PICK_LOCATION_REQUEST);
        }
    }


    @Override
    public void requestDatePickerDialog(String dateString) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        mainView.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        thirdView.setDate(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();
    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> geoMastersList, int[] selectedIds, int index) {
        if (index == 1)
            this.handleFirstView(geoMastersList, selectedIds);
        else if (index == 2)
            this.handleSecondView(geoMastersList, selectedIds);
        else if (index == 3)
            this.handleThirdView(geoMastersList, selectedIds);
        else if (index == 4)
            this.handleFourthView(geoMastersList, selectedIds);
    }


    private void handleFirstView(ArrayList<GeoMasterObj> geoMastersList, int[] selectedIds) {
        ArrayList<String>
                meterDiameter = new ArrayList<>(),
                meterBrand = new ArrayList<>(),
                meterType = new ArrayList<>(),
                meterStatus = new ArrayList<>(),
                meterMaterial = new ArrayList<>(),
                meterRemarks = new ArrayList<>(),
                readType = new ArrayList<>();

        ArrayList<Integer>
                meterDiameterIds = new ArrayList<>(),
                meterBrandIds = new ArrayList<>(),
                meterTypeIds = new ArrayList<>(),
                meterStatusIds = new ArrayList<>(),
                meterMaterialIds = new ArrayList<>(),
                meterRemarksIds = new ArrayList<>(),
                readTypeIds = new ArrayList<>();

        //meterDiameterSpinner 0, meterBrandSpinner 1, meterTypeSpinner 2, meterStatusSpinner 3,
        // meterMaterialSpinner 4, meterRemarksSpinner 5, readTypeSpinner 6
        for (int i = 0; i < geoMastersList.size(); i++) {
            if (geoMastersList.get(i).getType() == 0) {
                meterDiameter.add(geoMastersList.get(i).getName());
                meterDiameterIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 1) {
                meterBrand.add(geoMastersList.get(i).getName());
                meterBrandIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 2) {
                meterType.add(geoMastersList.get(i).getName());
                meterTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 3) {
                meterStatus.add(geoMastersList.get(i).getName());
                meterStatusIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 4) {
                meterMaterial.add(geoMastersList.get(i).getName());
                meterMaterialIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 5) {
                meterRemarks.add(geoMastersList.get(i).getName());
                meterRemarksIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 6) {
                readType.add(geoMastersList.get(i).getName());
                readTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            }
        }

        int meterDiameterIndex = returnIndex(meterDiameterIds, selectedIds[0]);
        int meterBrandIndex = returnIndex(meterBrandIds, selectedIds[1]);
        int meterTypeIndex = returnIndex(meterTypeIds, selectedIds[2]);
        int meterStatusIndex = returnIndex(meterStatusIds, selectedIds[3]);
        int meterMaterialIndex = returnIndex(meterMaterialIds, selectedIds[4]);
        int meterRemarksIndex = returnIndex(meterRemarksIds, selectedIds[5]);
        int readTypeIndex = returnIndex(readTypeIds, selectedIds[6]);

        firstView.loadSpinnersData(
                meterDiameter, meterDiameterIds, meterDiameterIndex,
                meterBrand, meterBrandIds, meterBrandIndex,
                meterType, meterTypeIds, meterTypeIndex,
                meterStatus, meterStatusIds, meterStatusIndex,
                meterMaterial, meterMaterialIds, meterMaterialIndex,
                meterRemarks, meterRemarksIds, meterRemarksIndex,
                readType, readTypeIds, readTypeIndex);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> geoMastersList, int[] selectedIds) {

        ArrayList<String>
                pipeAfterMeter = new ArrayList<>(),
                pipeSize = new ArrayList<>(),
                meterMaintenanceArea = new ArrayList<>(),
                meterBoxPosition = new ArrayList<>(),
                meterBoxType = new ArrayList<>(),
                coverStatus = new ArrayList<>(),
                location = new ArrayList<>(),
                buildingUsage = new ArrayList<>(),
                waterConnectionType = new ArrayList<>(),
                pipeMaterial = new ArrayList<>();
        ArrayList<Integer>
                pipeAfterMeterIds = new ArrayList<>(),
                pipeSizeIds = new ArrayList<>(),
                meterMaintenanceAreaIds = new ArrayList<>(),
                meterBoxPositionIds = new ArrayList<>(),
                meterBoxTypeIds = new ArrayList<>(),
                coverStatusIds = new ArrayList<>(),
                locationIds = new ArrayList<>(),
                buildingUsageIds = new ArrayList<>(),
                waterConnectionTypeIds = new ArrayList<>(),
                pipeMaterialIds = new ArrayList<>();

        //pipeAfterMeterSpinner 7, pipeSizeSpinner 8, meterMaintenanceAreaSpinner 9, meterBoxPositionSpinner 10, meterBoxTypeSpinner 11,
        //            coverStatusSpinner 12, locationSpinner 13, buildingUsageSpinner 14, waterConnectionTypeSpinner 17, pipeMaterialSpinner 18
        for (int i = 0; i < geoMastersList.size(); i++) {
            if (geoMastersList.get(i).getType() == 7) {
                pipeAfterMeter.add(geoMastersList.get(i).getName());
                pipeAfterMeterIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 8) {
                pipeSize.add(geoMastersList.get(i).getName());
                pipeSizeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 9) {
                meterMaintenanceArea.add(geoMastersList.get(i).getName());
                meterMaintenanceAreaIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 10) {
                meterBoxPosition.add(geoMastersList.get(i).getName());
                meterBoxPositionIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 11) {
                meterBoxType.add(geoMastersList.get(i).getName());
                meterBoxTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 12) {
                coverStatus.add(geoMastersList.get(i).getName());
                coverStatusIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 13) {
                location.add(geoMastersList.get(i).getName());
                locationIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 14) {
                buildingUsage.add(geoMastersList.get(i).getName());
                buildingUsageIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 17) {
                waterConnectionType.add(geoMastersList.get(i).getName());
                waterConnectionTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 18) {
                pipeMaterial.add(geoMastersList.get(i).getName());
                pipeMaterialIds.add(geoMastersList.get(i).getGeoMasterId());
            }

        }
        int pipeAfterMeterIndex = returnIndex(pipeAfterMeterIds, selectedIds[0]);
        int pipeSizeIndex = returnIndex(pipeSizeIds, selectedIds[1]);
        int meterMaintenanceAreaIndex = returnIndex(meterMaintenanceAreaIds, selectedIds[2]);
        int meterBoxPositionIndex = returnIndex(meterBoxPositionIds, selectedIds[3]);
        int meterBoxTypeIndex = returnIndex(meterBoxTypeIds, selectedIds[4]);
        int coverStatusIndex = returnIndex(coverStatusIds, selectedIds[5]);
        int locationIndex = returnIndex(locationIds, selectedIds[6]);
        int buildingUsageIndex = returnIndex(buildingUsageIds, selectedIds[7]);
        int waterConnectionTypeIndex = returnIndex(waterConnectionTypeIds, selectedIds[8]);
        int pipeMaterialIndex = returnIndex(pipeMaterialIds, selectedIds[9]);

        secondView.loadSpinnersData(
                pipeAfterMeter, pipeAfterMeterIds, pipeAfterMeterIndex,
                pipeSize, pipeSizeIds, pipeSizeIndex,
                meterMaintenanceArea, meterMaintenanceAreaIds, meterMaintenanceAreaIndex,
                meterBoxPosition, meterBoxPositionIds, meterBoxPositionIndex,
                meterBoxType, meterBoxTypeIds, meterBoxTypeIndex,
                coverStatus, coverStatusIds, coverStatusIndex,
                location, locationIds, locationIndex,
                buildingUsage, buildingUsageIds, buildingUsageIndex,
                waterConnectionType, waterConnectionTypeIds, waterConnectionTypeIndex,
                pipeMaterial, pipeMaterialIds, pipeMaterialIndex);
    }

    private void handleThirdView(ArrayList<GeoMasterObj> geoMastersList, int[] selectedIds) {
        ArrayList<String>
                valveType = new ArrayList<>(),
                valveStatus = new ArrayList<>(),
                enabled = new ArrayList<>(),
                reducerDiameter = new ArrayList<>();
        ArrayList<Integer>
                valveTypeIds = new ArrayList<>(),
                valveStatusIds = new ArrayList<>(),
                enabledIds = new ArrayList<>(),
                reducerDiameterIds = new ArrayList<>();


        //valveTypeSpinner 20, valveStatusSpinner 21, enabledSpinner 22, reducerDiameterSpinner 23
        for (int i = 0; i < geoMastersList.size(); i++) {
            if (geoMastersList.get(i).getType() == 20) {
                valveType.add(geoMastersList.get(i).getName());
                valveTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 21) {
                valveStatus.add(geoMastersList.get(i).getName());
                valveStatusIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 22) {
                enabled.add(geoMastersList.get(i).getName());
                enabledIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 23) {
                reducerDiameter.add(geoMastersList.get(i).getName());
                reducerDiameterIds.add(geoMastersList.get(i).getGeoMasterId());
            }
        }


        int valveTypeIndex = returnIndex(valveTypeIds, selectedIds[0]);
        int valveStatusIndex = returnIndex(valveStatusIds, selectedIds[1]);
        int enabledIndex = returnIndex(enabledIds, selectedIds[2]);
        int reducerDiameterIndex = returnIndex(reducerDiameterIds, selectedIds[3]);

        thirdView.loadSpinnersData(valveType, valveTypeIds, valveTypeIndex,
                valveStatus, valveStatusIds, valveStatusIndex,
                enabled, enabledIds, enabledIndex,
                reducerDiameter, reducerDiameterIds, reducerDiameterIndex);
    }

    private void handleFourthView(ArrayList<GeoMasterObj> geoMastersList, int[] selectedIds) {
        ArrayList<String>
                subBuildingType = new ArrayList<>(),
                waterSchedule = new ArrayList<>(),
                newMeterBrand = new ArrayList<>(),
                districtName = new ArrayList<>();
        ArrayList<Integer>
                subBuildingTypeIds = new ArrayList<>(),
                waterScheduleIds = new ArrayList<>(),
                newMeterBrandIds = new ArrayList<>(),
                districtNameIds = new ArrayList<>();

        //subBuildingTypeSpinner 15, waterScheduleSpinner 16, newMeterBrandSpinner 19, districtNameSpinner 25
        for (int i = 0; i < geoMastersList.size(); i++) {
            if (geoMastersList.get(i).getType() == 15) {
                subBuildingType.add(geoMastersList.get(i).getName());
                subBuildingTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 16) {
                waterSchedule.add(geoMastersList.get(i).getName());
                waterScheduleIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 19) {
                newMeterBrand.add(geoMastersList.get(i).getName());
                newMeterBrandIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 25) {
                districtName.add(geoMastersList.get(i).getName());
                districtNameIds.add(geoMastersList.get(i).getGeoMasterId());
            }
        }

        int subBuildingTypeIndex = returnIndex(subBuildingTypeIds, selectedIds[0]);
        int waterScheduleIndex = returnIndex(waterScheduleIds, selectedIds[1]);
        int newMeterBrandIndex = returnIndex(newMeterBrandIds, selectedIds[2]);
        int districtNameIndex = returnIndex(districtNameIds, selectedIds[3]);

        fourthView.loadSpinnersData(subBuildingType, subBuildingTypeIds, subBuildingTypeIndex,
                waterSchedule, waterScheduleIds, waterScheduleIndex,
                newMeterBrand, newMeterBrandIds, newMeterBrandIndex,
                districtName, districtNameIds, districtNameIndex);
    }

    private int returnIndex(ArrayList<Integer> ids, int selectedId) {
        int index = 0;
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == selectedId)
                index = i;
        }
        return index;
    }

    @Override
    public void onMeterInsertionCalled(long flag) {
        if (flag > 0) {
            view.showToastMessage(mainView.getAppContext().getString(R.string.done_successfully));
            view.backToParent();
        } else {
            view.showToastMessage(mainView.getAppContext().getString(R.string.failed_to_save));
        }
    }


    @Override
    public void onSyncingResponse(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        if (responseArray.getJSONObject(0).getString("status").equals("success"))
            view.backToParent();
        else view.showToastMessage(view.getActContext().getString(R.string.failed_to_sync));

    }

    @Override
    public void onSyncingError(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? view.getActContext().getString(R.string.invalid_username_or_password) :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }


    /*






















     */

    public EditRejectedMeterPresenter(EditRejectedMeterMVP.MainView mainView, EditRejectedMeterMVP.View view, EditRejectedMeterModel model) {
        this.mainView = mainView;
        this.view = view;
        this.model = model;
    }

    public EditRejectedMeterPresenter(EditRejectedMeterMVP.MainView mainView, EditRejectedMeterMVP.FirstView firstView, EditRejectedMeterModel model) {
        this.mainView = mainView;
        this.firstView = firstView;
        this.model = model;
    }

    public EditRejectedMeterPresenter(EditRejectedMeterMVP.MainView mainView, EditRejectedMeterMVP.SecondView secondView, EditRejectedMeterModel model) {
        this.mainView = mainView;
        this.secondView = secondView;
        this.model = model;
    }

    public EditRejectedMeterPresenter(EditRejectedMeterMVP.MainView mainView, EditRejectedMeterMVP.ThirdView thirdView, EditRejectedMeterModel model) {

        this.mainView = mainView;
        this.thirdView = thirdView;
        this.model = model;
    }

    public EditRejectedMeterPresenter(EditRejectedMeterMVP.MainView mainView, EditRejectedMeterMVP.FourthView fourthView, EditRejectedMeterModel model) {

        this.mainView = mainView;
        this.fourthView = fourthView;
        this.model = model;
    }

    public EditRejectedMeterPresenter(EditRejectedMeterMVP.MainView mainView, EditRejectedMeterMVP.FifthView fifthView, EditRejectedMeterModel model) {
        this.mainView = mainView;
        this.fifthView = fifthView;
        this.model = model;
    }


}
