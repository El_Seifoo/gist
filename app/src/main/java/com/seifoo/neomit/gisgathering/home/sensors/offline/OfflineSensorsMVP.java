package com.seifoo.neomit.gisgathering.home.sensors.offline;

import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;

import java.util.ArrayList;

public interface OfflineSensorsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void showToastMessage(String message);

        void loadSensorsData(ArrayList<SensorObject> sensor, boolean isVisible);

        void removeListItem(int position);

        void navigateDestination(String key, SensorObject sensor, Class destination);

        void navigateToEditSensor(Intent intent, int requestCode);

        void updateListItem(int position, SensorObject sensor);

        void removeListItem(long id);

        void navigateToMap(int RequestCode, SensorObject sensor, int position);

        int getSpinnerSelectedPosition();

        void navigateToMap2(int requestCode, ArrayList<SensorObject> sensors);
    }

    interface Presenter {
        void requestSensorsData(int type);

        void requestRemoveSensorObjById(long id, int position);

        void OnListItemClickListener(int viewId, int position, SensorObject sensor);

        void requestEditSensorData(int position, SensorObject sensor);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestUploadData(int type);

        void showLocations(ArrayList<SensorObject> sensors);
    }
}
