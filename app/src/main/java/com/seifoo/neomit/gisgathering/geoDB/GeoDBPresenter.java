package com.seifoo.neomit.gisgathering.geoDB;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GeoDBPresenter implements GeoDBMVP.Presenter, GeoDBModel.VolleyCallback {
    private GeoDBMVP.View view;
    private GeoDBModel model;

    public GeoDBPresenter(GeoDBMVP.View view, GeoDBModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestGeoDB() {
        view.showProgress();
        model.getGeoDB(view.getActContext(), this);
    }

    @Override
    public void onItemSelectedListener(String geoDB) {
        MySingleton.getmInstance(view.getAppContext()).saveStringSharedPref(Constants.API_DB_NAME, geoDB);
        view.navigateHome();
    }

    @Override
    public void logout() {
        String username = MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.USER_NAME, "");
        String ip = MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.USER_IP, "");
        DataBaseHelper.getmInstance(view.getActContext()).deleteAllGeoMasters();
        MySingleton.getmInstance(view.getActContext()).logout();
        MySingleton.getmInstance(view.getActContext()).saveStringSharedPref(Constants.USER_IP, ip);
        MySingleton.getmInstance(view.getActContext()).saveStringSharedPref(Constants.USER_NAME, username);
        view.navigateLogin();
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            ArrayList<String> geoDBS = new ArrayList<>();
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                for (int i = 0; i < data.length(); i++) {
                    geoDBS.add(data.getJSONObject(i).getString("Database").trim());
                }
                view.loadGeoDB(geoDBS);
            } else
                logOut();
        } else
            logOut();

    }

    private void logOut() {
        String username = MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.USER_NAME, "");
        String ip = MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.USER_IP, "");
        DataBaseHelper.getmInstance(view.getActContext()).deleteAllGeoMasters();
        MySingleton.getmInstance(view.getActContext()).logout();
        MySingleton.getmInstance(view.getActContext()).saveStringSharedPref(Constants.USER_IP, ip);
        MySingleton.getmInstance(view.getActContext()).saveStringSharedPref(Constants.USER_NAME, username);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? view.getActContext().getString(R.string.invalid_username_or_password) :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
