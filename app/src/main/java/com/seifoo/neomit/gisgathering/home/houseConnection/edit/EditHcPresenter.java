package com.seifoo.neomit.gisgathering.home.houseConnection.edit;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

public class EditHcPresenter implements EditHcMVP.Presenter, EditHcModel.DBCallback {
    private EditHcMVP.View view;
    private EditHcMVP.MainView mainView;
    private EditHcMVP.FirstView firstView;
    private EditHcMVP.SecondView secondView;
    private EditHcModel model;

    public EditHcPresenter(EditHcMVP.View view, EditHcMVP.MainView mainView, EditHcModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public EditHcPresenter(EditHcMVP.View view, EditHcMVP.FirstView firstView, EditHcModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public EditHcPresenter(EditHcMVP.View view, EditHcMVP.SecondView secondView, EditHcModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    @Override
    public void requestFirstSpinnersData(HouseConnectionObject houseConnection) {
        // 26,0,4,3 houseConnectionType,meter diameter,meter material,meter status
        model.getGeoMasters(view.getAppContext(), this, new int[]{26, 0, 4, 3},
                new int[]{houseConnection.getType(), houseConnection.getDiameter(), houseConnection.getMaterial(), houseConnection.getAssetStatus()}, 1);
        firstView.setSerialNumber(houseConnection.getSerialNumber());
        firstView.setHCN(houseConnection.getHCN());
        firstView.setLocation(houseConnection.getLatitude() + "," + houseConnection.getLongitude());
    }

    @Override
    public void requestSecondSpinnersData(HouseConnectionObject houseConnection) {
        // 25,16,5 districts,water schedule,meter remarks
        model.getGeoMasters(view.getAppContext(), this, new int[]{25, 16, 5},
                new int[]{houseConnection.getDistrictName(), houseConnection.getWaterSchedule(), houseConnection.getRemarks()}, 2);
        //String subDistrict, String streetName, String sectorName
        secondView.setData(houseConnection.getSubDistrict(), houseConnection.getStreetName(), houseConnection.getSectorName());
    }

    @Override
    public void requestPassFirstStepDataToActivity(HouseConnectionObject houseConnection) {
        if (!houseConnection.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        houseConnection.setLatitude(houseConnection.getLatitude().split(",")[0]);
        houseConnection.setLongitude(houseConnection.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof EditHCActivity) {
            ((EditHCActivity) view.getAppContext()).passFirstObj(houseConnection);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(HouseConnectionObject houseConnection) {
        if (view.getAppContext() instanceof EditHCActivity) {
            ((EditHCActivity) view.getAppContext()).passSecondObj(houseConnection);
        }
    }

    @Override
    public void requestEditHC(HouseConnectionObject houseConnection, String createdAt, long id) {
        houseConnection.setId(id);
        houseConnection.setCreatedAt(createdAt);
        model.updateHcObj(view.getAppContext(), this, houseConnection);
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View clickedView) {
                switch (clickedView.getId()) {
                    case R.id.serial_num_qr:
                        requestQrCode(((EditHCActivity) view.getAppContext()), view.getAppContext().getString(R.string.serial_number));
                        break;
                    case R.id.hcn_qr:
                        requestQrCode(((EditHCActivity) view.getAppContext()), view.getAppContext().getString(R.string.hcn));
                        break;
                }
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext, String flagString) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (flagString.equals(view.getAppContext().getString(R.string.hcn))) {
            flag = 1;
        }

        firstView.initializeScanner(integrator, flag);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void handleQRScannerResult(String contents, int flag) {
        contents = contents.replaceAll("[^0-9]", "");
        if (flag == 0) {
            firstView.setSerialNumber(contents);
        } else if (flag == 1) {
            firstView.setHCN(contents);
        }
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getHCsLocation(view.getAppContext(), this, prevLatLng);
    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index) {
        if (index == 1) handleFirstView(masters, selectedIds);
        else if (index == 2) handleSecondView(masters, selectedIds);
    }

    // 26,0,4,3 houseConnectionType,meter diameter,meter material,meter status
    private void handleFirstView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String> type = new ArrayList<>(), diameter = new ArrayList<>(), material = new ArrayList<>(), status = new ArrayList<>();
        ArrayList<Integer> typeIds = new ArrayList<>(), diameterIds = new ArrayList<>(), materialIds = new ArrayList<>(), statusIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 26) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                diameter.add(masters.get(i).getName());
                diameterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 4) {
                material.add(masters.get(i).getName());
                materialIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 3) {
                status.add(masters.get(i).getName());
                statusIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int typeIndex = returnIndex(typeIds, selectedIds[0]);
        int diameterIndex = returnIndex(diameterIds, selectedIds[1]);
        int materialIndex = returnIndex(materialIds, selectedIds[2]);
        int statusIndex = returnIndex(statusIds, selectedIds[3]);

        firstView.loadSpinnersData(type, typeIds, typeIndex,
                diameter, diameterIds, diameterIndex,
                material, materialIds, materialIndex,
                status, statusIds, statusIndex);
    }

    // 25,16,5 districts,water schedule,meter remarks
    private void handleSecondView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String> districts = new ArrayList<>(), waterSchedule = new ArrayList<>(), remarks = new ArrayList<>();
        ArrayList<Integer> districtsIds = new ArrayList<>(), waterScheduleIds = new ArrayList<>(), remarksIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 25) {
                districts.add(masters.get(i).getName());
                districtsIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 16) {
                waterSchedule.add(masters.get(i).getName());
                waterScheduleIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 5) {
                remarks.add(masters.get(i).getName());
                remarksIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int districtsIndex = returnIndex(districtsIds, selectedIds[0]);
        int waterScheduleIndex = returnIndex(waterScheduleIds, selectedIds[1]);
        int remarksIndex = returnIndex(remarksIds, selectedIds[2]);


        secondView.loadSpinnersData(districts, districtsIds, districtsIndex,
                waterSchedule, waterScheduleIds, waterScheduleIndex,
                remarks, remarksIds, remarksIndex);
    }

    private int returnIndex(ArrayList<Integer> ids, int selectedId) {
        int index = 0;
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == selectedId)
                index = i;
        }

        return index;
    }

    @Override
    public void onHcUpdatingCalled(int flag, HouseConnectionObject houseConnection) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.hc_edited_successfully));
            mainView.backToParent(houseConnection);
        } else
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_edit));
    }

    @Override
    public void onGetHCsLocationCalled(ArrayList<HashMap<String, String>> hCsLocation, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], hCsLocation, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", hCsLocation, PICK_LOCATION_REQUEST);
        }
    }
}
