package com.seifoo.neomit.gisgathering.home.tank.rejected.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class FirstFragment extends Fragment implements Step, EditRejectedTanksMVP.View, EditRejectedTanksMVP.FirstView {
    private EditText latLngEditText, serialNumberEditText, tankNameEditText, commissionDateEditText, activeVolumeEditText;
    private Spinner typeSpinner, enabledSpinner, tankDiameterSpinner;

    private EditRejectedTanksMVP.Presenter presenter;

    public static FirstFragment newInstance(TankObject tank) {
        FirstFragment fragment = new FirstFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("TankObj", tank);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tank_first_edit, container, false);

        presenter = new EditRejectedTanksPresenter(this, this, new EditRejectedTanksModel());

        typeSpinner = (Spinner) view.findViewById(R.id.type_spinner);
        enabledSpinner = (Spinner) view.findViewById(R.id.enabled_spinner);
        tankDiameterSpinner = (Spinner) view.findViewById(R.id.tank_diameter_spinner);

        latLngEditText = (EditText) view.findViewById(R.id.location);
        serialNumberEditText = (EditText) view.findViewById(R.id.serial_num);
        tankNameEditText = (EditText) view.findViewById(R.id.tank_name);
        commissionDateEditText = (EditText) view.findViewById(R.id.commission_date);
        activeVolumeEditText = (EditText) view.findViewById(R.id.active_volume);

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);

        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(latLngEditText.getText().toString().trim());
            }
        });

        commissionDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(commissionDateEditText.getText().toString().trim(), 0);
            }
        });

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.serial_num_qr)));

        presenter.requestFirstSpinnersData((TankObject) getArguments().getSerializable("TankObj"));

        return view;

    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFirstStepDataToActivity(
                new TankObject(latLngEditText.getText().toString().trim(), latLngEditText.getText().toString().trim(),
                        serialNumberEditText.getText().toString().trim(), tankNameEditText.getText().toString().trim(),
                        typeIds.get(typeSpinner.getSelectedItemPosition()), enabledIds.get(enabledSpinner.getSelectedItemPosition()),
                        commissionDateEditText.getText().toString().trim(), tankDiameterIds.get(tankDiameterSpinner.getSelectedItemPosition()),
                        activeVolumeEditText.getText().toString().trim()));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> typeIds, enabledIds, tankDiameterIds;

    @Override
    public void loadSpinnersData(ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex, ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex, ArrayList<String> tankDiameter, ArrayList<Integer> tankDiameterIds, int tankDiameterIndex) {
        this.typeIds = typeIds;
        this.enabledIds = enabledIds;
        this.tankDiameterIds = tankDiameterIds;

        typeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, type));
        typeSpinner.setSelection(typeIndex);
        enabledSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, enabled));
        enabledSpinner.setSelection(enabledIndex);
        tankDiameterSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, tankDiameter));
        tankDiameterSpinner.setSelection(tankDiameterIndex);
    }

    @Override
    public void showLocationError(String errorMessage) {
        latLngEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((EditRejectedTanksActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, 1);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }

    @Override
    public void setCommissionDate(String commissionDate) {
        commissionDateEditText.setText(commissionDate);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void setLocation(String location) {
        latLngEditText.setText(location);
    }

    @Override
    public void setData(String tankName, String activeVolume) {
        tankNameEditText.setText(tankName);
        activeVolumeEditText.setText(activeVolume);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFirstViewActivityResult(requestCode, resultCode, data);
    }
}
