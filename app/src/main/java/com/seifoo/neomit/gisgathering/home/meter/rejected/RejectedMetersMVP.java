package com.seifoo.neomit.gisgathering.home.meter.rejected;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.meter.MeterObject;

import java.util.ArrayList;

public interface RejectedMetersMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showEmptyListText();

        void loadRejectedMeters(ArrayList<MeterObject> rejectedMeters);
    }

    interface Presenter {
        void requestRejected();
    }
}
