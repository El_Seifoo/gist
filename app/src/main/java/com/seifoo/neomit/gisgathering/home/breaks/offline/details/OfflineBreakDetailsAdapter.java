package com.seifoo.neomit.gisgathering.home.breaks.offline.details;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public class OfflineBreakDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_TEXT = 0;
    private static final int VIEW_TYPE_IMAGE = 1;

    private ArrayList<MoreDetails> details;
    private boolean isOffline;
    private OnListItemClicked onListItemClicked;
    private Context context;

    public void setDetailsList(ArrayList<MoreDetails> details) {
        this.details = details;
        notifyDataSetChanged();
    }

    public OfflineBreakDetailsAdapter(Context context, boolean isOffline, OnListItemClicked onListItemClicked) {
        this.context = context;
        this.isOffline = isOffline;
        this.onListItemClicked = onListItemClicked;

    }

    public OfflineBreakDetailsAdapter(Context context, boolean isOffline) {
        this.context = context;
        this.isOffline = isOffline;
    }


    public interface OnListItemClicked {
        void onItemClickListener(String latitude, String longitude);
    }


    @Override
    public int getItemViewType(int position) {
        if (details.get(position).getKey().equals(context.getString(R.string.before_breaking)) || details.get(position).getKey().equals(context.getString(R.string.after_breaking)))
            return VIEW_TYPE_IMAGE;
        return VIEW_TYPE_TEXT;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_TEXT:
                return new TextHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offline_meter_details_list_item, viewGroup, false));
            case VIEW_TYPE_IMAGE:
            default:
                return new ImageHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offline_break_details_images_list_item, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_TEXT:
                ((TextHolder) holder).key.setText(details.get(position).getKey());
                ((TextHolder) holder).value.setText(details.get(position).getValue());
                if (!isOffline) {
                    if (position == 0) {
                        ((TextHolder) holder).location.setVisibility(View.VISIBLE);
                    } else {
                        ((TextHolder) holder).location.setVisibility(View.GONE);
                    }
                }
                break;
            case VIEW_TYPE_IMAGE:
                ((ImageHolder) holder).label.setText(details.get(position).getKey());
                Glide.with(((ImageHolder) holder).itemView.getContext())
                        .asBitmap()
                        .load(stringToBitmap(details.get(position).getValue().split("//////")[0]))
                        .apply(new RequestOptions().error(R.mipmap.ic_launcher))
                        .into(((ImageHolder) holder).imageView);
        }

    }

    public Bitmap stringToBitmap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return details != null ? details.size() : 0;
    }

    public class TextHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView key, value;
        Button location;

        public TextHolder(@NonNull View itemView) {
            super(itemView);
            key = (TextView) itemView.findViewById(R.id.key);
            value = (TextView) itemView.findViewById(R.id.value);

            if (!isOffline) {
                location = (Button) itemView.findViewById(R.id.location_btn);
                location.setOnClickListener(this);
            }
        }


        @Override
        public void onClick(View view) {
            onListItemClicked.onItemClickListener(details.get(getAdapterPosition()).getValue().split(",")[0], details.get(getAdapterPosition()).getValue().split(",")[1]);
        }
    }

    public class ImageHolder extends RecyclerView.ViewHolder {
        TextView label;
        ImageView imageView;

        public ImageHolder(View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.label);
            imageView = (ImageView) itemView.findViewById(R.id.value);
        }
    }

}
