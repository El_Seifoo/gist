package com.seifoo.neomit.gisgathering.home.sensors.info.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class GetSensorInfoDetailsModel {
    public void returnValidGeoMaster(Context context, DBCallback callback, SensorObject sensor, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds1(ids, types, lang), sensor);
    }

    protected interface DBCallback {
        void onConvertingIdsCalled(ArrayList<String> strings, SensorObject sensor);
    }
}
