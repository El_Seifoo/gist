package com.seifoo.neomit.gisgathering.home.houseConnection.rejected;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;

import java.util.ArrayList;

public interface RejectedHcsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showEmptyListText();

        void loadRejectedHCs(ArrayList<HouseConnectionObject> HCs);
    }

    interface Presenter {
        void requestHCs();
    }
}
