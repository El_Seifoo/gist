package com.seifoo.neomit.gisgathering.home.houseConnection.rejected;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RejectedHcsPresenter implements RejectedHcsMVP.Presenter, RejectedHcsModel.VolleyCallback, RejectedHcsModel.DBCallback {
    private RejectedHcsMVP.View view;
    private RejectedHcsModel model;

    public RejectedHcsPresenter(RejectedHcsMVP.View view, RejectedHcsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestHCs() {
        view.showProgress();
        model.getRejected(view.getAppContext(), this);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ArrayList<HouseConnectionObject> rejectedHCs = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {

                    rejectedHCs.add(new HouseConnectionObject(data.getJSONObject(i).getLong("Corrected_Id"),
                            returnValidString(data.getJSONObject(i).getString("SERIAL_NO")),
                            returnValidString(data.getJSONObject(i).getString("HCN")),
                            data.getJSONObject(i).getString("F_TYPE_ID").isEmpty() || data.getJSONObject(i).getString("F_TYPE_ID").toLowerCase().equals("null") ? 1 : data.getJSONObject(i).getInt("F_TYPE_ID"),
                            returnValidInt(data.getJSONObject(i).getString("DIAMETER_ID")),
                            returnValidInt(data.getJSONObject(i).getString("MATERIAL_ID")),
                            returnValidInt(data.getJSONObject(i).getString("ASSET_STATUS_ID")),
                            returnValidInt(data.getJSONObject(i).getString("DISTRICT_NAME_ID")),
                            returnValidString(data.getJSONObject(i).getString("SUP_DISTRICT_ID")),
                            returnValidString(data.getJSONObject(i).getString("STREET_NAME")),
                            returnValidString(data.getJSONObject(i).getString("SECTOR_NAME")),
                            returnValidInt(data.getJSONObject(i).getString("WATER_SCHEDULE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("REMARKS")),
                            returnValidString(data.getJSONObject(i).getString("Y_Map")),
                            returnValidString(data.getJSONObject(i).getString("X_Map")),
                            returnValidString(data.getJSONObject(i).getString("Reason")),
                            returnValidDate1(data.getJSONObject(i).getString("CreatedDate"))));
//x -> longitude , y -> latitude
                }
                model.checkDatabaseHCs(view.getAppContext(), this, rejectedHCs);
            } else {
                view.hideProgress();
                view.showEmptyListText();
            }
        } else {
            view.hideProgress();
            view.showEmptyListText();
        }
    }

    private String returnValidString(String data) {
        return data.isEmpty() || data.toLowerCase().equals("null") ? "" : data;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }


    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onCheckingHCsCalled(ArrayList<HouseConnectionObject> HCs) {
        view.hideProgress();
        if (HCs.isEmpty())
            view.showEmptyListText();
        else
            view.loadRejectedHCs(HCs);
    }
}
