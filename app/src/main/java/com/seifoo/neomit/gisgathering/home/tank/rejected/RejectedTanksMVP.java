package com.seifoo.neomit.gisgathering.home.tank.rejected;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.tank.TankObject;

import java.util.ArrayList;

public interface RejectedTanksMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showEmptyListText();

        void loadRejectedTanks(ArrayList<TankObject> tanks);
    }

    interface Presenter {
        void requestTanks();
    }
}
