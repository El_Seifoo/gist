package com.seifoo.neomit.gisgathering.home.tank.offline;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.home.tank.map.GetTankMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.tank.rejected.map.RejectedTanksMapActivity;

import java.util.ArrayList;

public class OfflineTanksActivity extends AppCompatActivity implements OfflineTanksMVP.View, OfflineTanksAdapter.TankObjectListItemListener {
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private OfflineTanksAdapter adapter;
    private Button uploadButton;
    private ProgressBar progressBar;
    private Spinner spinner;
    private Button showLocations;

    private OfflineTanksMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_tanks);

        getSupportActionBar().setTitle(getString(R.string.upload));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new OfflineTanksPresenter(this, new OfflineTanksModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        uploadButton = (Button) findViewById(R.id.upload_button);
        showLocations = (Button) findViewById(R.id.show_locations);
        showLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.showLocations(tanks);
            }
        });

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.offline_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineTanksAdapter(this, true);

        spinner = (Spinner) findViewById(R.id.type_spinner);
        spinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.offline_types)));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                presenter.requestTanksData(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestUploadData(spinner.getSelectedItemPosition());
            }
        });

    }


    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        uploadButton.setClickable(false);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        uploadButton.setClickable(true);
    }

    @Override
    public void showEmptyListText() {
        emptyListTextView.setText(getString(R.string.no_tanks_available));
        adapter.clear();
        showLocations.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private ArrayList<TankObject> tanks;

    @Override
    public void loadTanksData(ArrayList<TankObject> tanks, boolean isVisible) {
        this.tanks = tanks;
        emptyListTextView.setText("");
        adapter.setList(tanks);
        recyclerView.setAdapter(adapter);
        showLocations.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void removeListItem(int position) {
        adapter.removeItem(this, position);
    }

    @Override
    public void navigateDestination(String key, TankObject tank, Class destination) {
        Intent intent = new Intent(this, destination);
        intent.putExtra(key, tank);
        startActivity(intent);
    }

    @Override
    public void navigateToEditTank(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void updateListItem(int position, TankObject tank) {
        adapter.updateItem(position, tank);
    }

    @Override
    public void navigateToMap(int requestCode, TankObject tank, int position) {
        Intent intent1 = new Intent(this, GetTankMapDetailsActivity.class);
        intent1.putExtra("Tank", tank);
        intent1.putExtra("position", position);
        intent1.putExtra("OfflineEdit", "");
        startActivityForResult(intent1, requestCode);
    }

    @Override
    public int getSpinnerSelectedPosition() {
        return spinner.getSelectedItemPosition();
    }

    @Override
    public void navigateToMap2(int requestCode, ArrayList<TankObject> tanks) {
        Intent intent = new Intent(OfflineTanksActivity.this, RejectedTanksMapActivity.class);
        intent.putExtra("Tanks", tanks);
        intent.putExtra("OfflineEdit", "");
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void removeListItem(long id) {
        adapter.updateItem(this, id);
    }

    @Override
    public void onListItemClickListener(int viewId, int position, TankObject tank) {
        presenter.OnListItemClickListener(viewId, position, tank);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
