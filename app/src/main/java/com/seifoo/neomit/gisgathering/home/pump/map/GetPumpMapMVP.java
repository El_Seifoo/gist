package com.seifoo.neomit.gisgathering.home.pump.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;

public interface GetPumpMapMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void initializeScanner(IntentIntegrator integrator);

        void setCode(String code);

        void loadPumpMap(String latitude, String longitude);
    }

    interface Presenter {
        void requestQrCode(Activity activity);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestGetMap(String code);
    }
}
