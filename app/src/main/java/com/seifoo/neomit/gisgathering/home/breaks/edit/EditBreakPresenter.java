package com.seifoo.neomit.gisgathering.home.breaks.edit;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.widget.DatePicker;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class EditBreakPresenter implements EditBreakMVP.Presenter, EditBreakModel.DBCallback {
    private EditBreakMVP.View view;
    private EditBreakMVP.MainView mainView;
    private EditBreakMVP.FirstView firstView;
    private EditBreakMVP.SecondView secondView;
    private EditBreakMVP.ThirdView thirdView;
    private EditBreakModel model;

    public EditBreakPresenter(EditBreakMVP.View view, EditBreakMVP.MainView mainView, EditBreakModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public EditBreakPresenter(EditBreakMVP.View view, EditBreakMVP.FirstView firstView, EditBreakModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public EditBreakPresenter(EditBreakMVP.View view, EditBreakMVP.SecondView secondView, EditBreakModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    public EditBreakPresenter(EditBreakMVP.View view, EditBreakMVP.ThirdView thirdView, EditBreakModel model) {
        this.view = view;
        this.thirdView = thirdView;
        this.model = model;
    }

    /*
        0 -> meter diameter , 1 -> meter brand , 2 - > meter type , 3 -> meter status , 4 -> meter material , 5 -> meter remarks
        6 -> read type , 7 -> pipe after meter , 8 -> pipe size , 9 -> maintenance area , 10 -> meter box Position , 11 -> meter box type
        12 -> cover status , 13 -> location , 14 -> building usage , 15 -> sub-building type , 16 -> water schedule , 17 -> water connection type
        18 -> pipe material , 19 -> new meter brand , 20 ->break type  , 21 -> break status , 22 -> yes_no , 23 -> reducer diameter
        24 -> FireType , 25 -> districts , 26 -> houseConnectionType , 27 -> breakJob , 28 -> sub-district , 29 -> tankType
        30 -> pipeType , 31 -> maintenanceType , 32 -> soilType ,  33 -> maintenanceDeviceType , 34 -> procedure
        35 -> maintenanceCompany , 36 -> asphalt
     */
    @Override
    public void requestFirstSpinnersData(BreakObject breakObj) {
        // pipeType 30,  districtName 25 ,  subZone 28 ,  diameter 0
        model.getGeoMasters(view.getAppContext(), this, new int[]{30, 25, 28, 0},
                new int[]{breakObj.getPipeType(), breakObj.getDistrictName(), breakObj.getSubZone(), breakObj.getDiameter()},
                1);
        firstView.setLocation(breakObj.getLatitude() + "," + breakObj.getLongitude());
        firstView.setBreakDate(breakObj.getBreakDate());
        firstView.setData(breakObj.getBreakNumber(), breakObj.getBuildingNumber(), breakObj.getPipeNumber(), breakObj.getStreetName());
    }

    @Override
    public void requestSecondSpinnersData(BreakObject breakObj) {
        // pipeMaterial 18,  maintenanceCompany 35,  maintenanceType 31 , soilType 32 ,  asphalt 36,  maintenanceDeviceType 33, procedure 34,
        model.getGeoMasters(view.getAppContext(), this, new int[]{18, 35, 31, 32, 36, 33, 34},
                new int[]{breakObj.getPipeMaterial(), breakObj.getMaintenanceCompany(), breakObj.getMaintenanceType(),
                        breakObj.getSoilType(), breakObj.getAsphalt(), breakObj.getMaintenanceDeviceType(), breakObj.getProcedure()},
                2);
        secondView.setData(breakObj.getBreakDetails(), breakObj.getUsedEquipment(), breakObj.getDimensionDrilling());
    }

    @Override
    public void requestThirdStepData(BreakObject breakObj) {
        if (breakObj.getPhotos() != null) {
            if (breakObj.getPhotos().containsKey("beforeImage") && breakObj.getPhotos().get("beforeImage") != null){
                paths.put("beforeImage", breakObj.getPhotos().get("beforeImage"));
                thirdView.loadBeforeImage(stringToBitmap(breakObj.getPhotos().get("beforeImage").split("//////")[0]));
            }
            if (breakObj.getPhotos().containsKey("afterImage") && breakObj.getPhotos().get("afterImage") != null){
                paths.put("afterImage", breakObj.getPhotos().get("afterImage"));
                thirdView.loadAfterImage(stringToBitmap(breakObj.getPhotos().get("afterImage").split("//////")[0]));
            }
        }

    }

    public Bitmap stringToBitmap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    @Override
    public void requestPassFirstStepDataToActivity(BreakObject breakObject) {
        if (!breakObject.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        breakObject.setLatitude(breakObject.getLatitude().split(",")[0]);
        breakObject.setLongitude(breakObject.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof EditBreakActivity) {
            ((EditBreakActivity) view.getAppContext()).passFirstObj(breakObject);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(BreakObject breakObject) {
        if (view.getAppContext() instanceof EditBreakActivity) {
            ((EditBreakActivity) view.getAppContext()).passSecondObj(breakObject);
        }
    }

    @Override
    public void requestPassThirdStepDataToActivity() {
        ((EditBreakActivity) view.getAppContext()).passThirdObj(new BreakObject(paths));
    }

    @Override
    public void requestEditBreak(BreakObject breakObject, String createdAt, long id) {
        breakObject.setId(id);
        breakObject.setCreatedAt(createdAt);
        model.updateBreakObj(view.getAppContext(), this, breakObject);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    Bitmap bitmap;
    Uri filePath;
    Map<String, String> paths = new HashMap<>(2);

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        if (requestCode == SELECT_AFTER_IMG_REQUEST || requestCode == SELECT_BEFORE_IMG_REQUEST) {
            if (resultCode == RESULT_OK && data != null) {
                bitmap = (Bitmap) data.getExtras().get("data");
                try {
                    filePath = createImageFile(bitmapToByte(bitmap));
                    Log.e("filepath", filePath.toString());
                    long length = bitmapToByte(bitmap).length;
                    if ((length / 1024) > 1024) {
                        thirdView.showToastMessage(view.getAppContext().getString(R.string.cant_save_more_than_1_mega));
                        return;
                    }

                    switch (requestCode) {
                        case SELECT_BEFORE_IMG_REQUEST:
                            paths.put("beforeImage", bitmapToString(bitmap) + "//////" + getPath(view.getAppContext(), filePath));
                            thirdView.loadBeforeImage(bitmapToByte(bitmap));
                            break;
                        default:
                            paths.put("afterImage", bitmapToString(bitmap) + "//////" + getPath(view.getAppContext(), filePath));
                            thirdView.loadAfterImage(bitmapToByte(bitmap));
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Uri createImageFile(byte[] imageBytes) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File imageFolder = new File(Environment.getExternalStorageDirectory(), "GIST");
        imageFolder.mkdir();
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                imageFolder      // directory
        );

        FileOutputStream fileOutputStream = new FileOutputStream(image);
        fileOutputStream.write(imageBytes);
        fileOutputStream.close();
        return Uri.fromFile(image);
//        return Uri.parse(image.getAbsolutePath());
    }

    private byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public String bitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public String getPath(Context context, Uri uri) {
        if (uri == null) return "";
        String result = "";
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            result = uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getBreaksLocation(view.getAppContext(), this, prevLatLng);
    }

    @Override
    public void requestDatePickerDialog(String dateString) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        view.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        firstView.setBreakDate(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();
    }

    private final static int SELECT_BEFORE_IMG_REQUEST = 5;
    private final static int SELECT_AFTER_IMG_REQUEST = 6;

    @Override
    public void onButtonsClickListener(int id) {
        thirdView.getFragment().requestPermissions(
                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                id == R.id.before ? SELECT_BEFORE_IMG_REQUEST : SELECT_AFTER_IMG_REQUEST
        );
    }

    @Override
    public void onRequestPermissionsResult(Activity activity, int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == SELECT_BEFORE_IMG_REQUEST || requestCode == SELECT_AFTER_IMG_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent capturePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (capturePhoto.resolveActivity(activity.getApplicationContext().getPackageManager()) != null) {
                    thirdView.pickImage(capturePhoto, requestCode);
                }
            }
        }
    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index) {
        if (index == 1) handleFirstView(masters, selectedIds);
        else if (index == 2) handleSecondView(masters, selectedIds);
    }

    private void handleFirstView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                pipeType = new ArrayList<>(),
                districtName = new ArrayList<>(),
                subZone = new ArrayList<>(),
                diameter = new ArrayList<>();

        ArrayList<Integer>
                pipeTypeIds = new ArrayList<>(),
                districtNameIds = new ArrayList<>(),
                subZoneIds = new ArrayList<>(),
                diameterIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 30) {
                pipeType.add(masters.get(i).getName());
                pipeTypeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 25) {
                districtName.add(masters.get(i).getName());
                districtNameIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 28) {
                subZone.add(masters.get(i).getName());
                subZoneIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                diameter.add(masters.get(i).getName());
                diameterIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int pipeTypeIndex = returnIndex(pipeTypeIds, selectedIds[0]);
        int districtNameIndex = returnIndex(districtNameIds, selectedIds[1]);
        int subZoneIndex = returnIndex(subZoneIds, selectedIds[2]);
        int diameterIndex = returnIndex(diameterIds, selectedIds[3]);
        firstView.loadSpinnersData(pipeType, pipeTypeIds, pipeTypeIndex,
                districtName, districtNameIds, districtNameIndex,
                subZone, subZoneIds, subZoneIndex,
                diameter, diameterIds, diameterIndex);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                pipeMaterial = new ArrayList<>(),
                maintenanceCompany = new ArrayList<>(),
                maintenanceType = new ArrayList<>(),
                soilType = new ArrayList<>(),
                asphalt = new ArrayList<>(),
                maintenanceDeviceType = new ArrayList<>(),
                procedure = new ArrayList<>();

        ArrayList<Integer>
                pipeMaterialIds = new ArrayList<>(),
                maintenanceCompanyIds = new ArrayList<>(),
                maintenanceTypeIds = new ArrayList<>(),
                soilTypeIds = new ArrayList<>(),
                asphaltIds = new ArrayList<>(),
                maintenanceDeviceTypeIds = new ArrayList<>(),
                procedureIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 18) {
                pipeMaterial.add(masters.get(i).getName());
                pipeMaterialIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 35) {
                maintenanceCompany.add(masters.get(i).getName());
                maintenanceCompanyIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 31) {
                maintenanceType.add(masters.get(i).getName());
                maintenanceTypeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 32) {
                soilType.add(masters.get(i).getName());
                soilTypeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 36) {
                asphalt.add(masters.get(i).getName());
                asphaltIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 33) {
                maintenanceDeviceType.add(masters.get(i).getName());
                maintenanceDeviceTypeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 34) {
                procedure.add(masters.get(i).getName());
                procedureIds.add(masters.get(i).getGeoMasterId());
            }
        }


        int pipeMaterialIndex = returnIndex(pipeMaterialIds, selectedIds[0]);
        int maintenanceCompanyIndex = returnIndex(maintenanceCompanyIds, selectedIds[1]);
        int maintenanceTypeIndex = returnIndex(maintenanceTypeIds, selectedIds[2]);
        int soilTypeIndex = returnIndex(soilTypeIds, selectedIds[3]);
        int asphaltIndex = returnIndex(asphaltIds, selectedIds[4]);
        int maintenanceDeviceTypeIndex = returnIndex(maintenanceDeviceTypeIds, selectedIds[5]);
        int procedureIndex = returnIndex(procedureIds, selectedIds[6]);

        secondView.loadSpinnersData(pipeMaterial, pipeMaterialIds, pipeMaterialIndex,
                maintenanceCompany, maintenanceCompanyIds, maintenanceCompanyIndex,
                maintenanceType, maintenanceTypeIds, maintenanceTypeIndex,
                soilType, soilTypeIds, soilTypeIndex,
                asphalt, asphaltIds, asphaltIndex,
                maintenanceDeviceType, maintenanceDeviceTypeIds, maintenanceDeviceTypeIndex,
                procedure, procedureIds, procedureIndex);
    }


    private int returnIndex(ArrayList<Integer> ids, int selectedId) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == selectedId)
                return i;
        }
        return 0;
    }

    @Override
    public void onBreakUpdatingCalled(int flag, BreakObject breakObj) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.edit_break_done_successfully));
            mainView.backToParent(breakObj);
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_edit));
        }
    }

    @Override
    public void onGetBreaksLocationCalled(ArrayList<HashMap<String, String>> breaksLocation, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], breaksLocation, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", breaksLocation, PICK_LOCATION_REQUEST);
        }
    }
}
