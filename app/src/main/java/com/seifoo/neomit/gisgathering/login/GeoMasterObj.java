package com.seifoo.neomit.gisgathering.login;

public class GeoMasterObj {
    private int id;
    private int geoMasterId;
    private String arName;
    private String enName;
    private String name;
    private int type;

    /*
        0 -> meter diameter , 1 -> meter brand , 2 - > meter type , 3 -> meter status , 4 -> meter material , 5 -> meter remarks
        6 -> read type , 7 -> pipe after meter , 8 -> pipe size , 9 -> maintenance area , 10 -> meter box Position , 11 -> meter box type
        12 -> cover status , 13 -> location , 14 -> building usage , 15 -> sub-building type , 16 -> water schedule , 17 -> water connection type
        18 -> pipe material , 19 -> new meter brand , 20 ->sensor type  , 21 -> sensor status , 22 -> yes_no , 23 -> reducer diameter
        24 -> FireType , 25 -> districts , 26 -> houseConnectionType , 27 -> sensorJob , 28 -> sub-district , 29 -> tankType
        30 -> pipeType , 31 -> maintenanceType , 32 -> soilType ,  33 -> maintenanceDeviceType , 34 -> procedure
        35 -> maintenanceCompany , 36 -> asphalt , 37 -> deviceType , 38 -> sensorType
     */

    public GeoMasterObj(int id, int geoMasterId, String arName, String enName, int type) {
        this.id = id;
        this.geoMasterId = geoMasterId;
        this.arName = arName;
        this.enName = enName;
        this.type = type;
    }

    public GeoMasterObj(int geoMasterId, String arName, String enName, int type) {
        this.geoMasterId = geoMasterId;
        this.arName = arName;
        this.enName = enName;
        this.type = type;
    }

    public GeoMasterObj(int geoMasterId, String name, int type) {
        this.geoMasterId = geoMasterId;
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getGeoMasterId() {
        return geoMasterId;
    }

    public String getArName() {
        return arName;
    }

    public String getEnName() {
        return enName;
    }

    public int getType() {
        return type;
    }
}
