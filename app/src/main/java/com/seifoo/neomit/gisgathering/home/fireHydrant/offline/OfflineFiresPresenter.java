package com.seifoo.neomit.gisgathering.home.fireHydrant.offline;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.fireHydrant.edit.EditFireHydrantActivity;
import com.seifoo.neomit.gisgathering.home.fireHydrant.offline.offlineFiresDetails.OfflineFiresDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class OfflineFiresPresenter implements OfflineFiresMVP.Presenter, OfflineFiresModel.VolleyCallback, OfflineFiresModel.DBCallback {
    private OfflineFiresMVP.View view;
    private OfflineFiresModel model;

    public OfflineFiresPresenter(OfflineFiresMVP.View view, OfflineFiresModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestFiresData(int type) {
        model.getFiresList(view.getAppContext(), this, type, 0);
    }

    @Override
    public void requestRemoveFireObjById(long id, int position) {
        model.removeFireObject(view.getActContext(), this, id, position);
    }

    private static final int SHOW_ALL_IN_MAP_REQUEST = 10;

    @Override
    public void OnListItemClickListener(int viewId, int position, FireHydrantObject fireHydrantObject) {
        switch (viewId) {
            case R.id.more_details:
                view.navigateDestination("OfflineFireObj", fireHydrantObject, OfflineFiresDetailsActivity.class);
                break;
            case R.id.remove_item:
                this.requestRemoveFireObjById(fireHydrantObject.getId(), position);
                break;
            case R.id.edit_item:
                this.requestEditFireData(position, fireHydrantObject);
                break;
            case R.id.map:
                view.navigateToMap(SHOW_ALL_IN_MAP_REQUEST, fireHydrantObject, position);
        }
    }

    private static final int EDIT_FIRE_OBJECT_REQUEST = 1;

    @Override
    public void requestEditFireData(int position, FireHydrantObject fireHydrantObject) {
        Intent intent = new Intent(view.getAppContext(), EditFireHydrantActivity.class);
        intent.putExtra("fireObj", fireHydrantObject);
        intent.putExtra("position", position);
        view.navigateToEditFire(intent, EDIT_FIRE_OBJECT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_FIRE_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            view.updateListItem(data.getExtras().getInt("position"), (FireHydrantObject) data.getSerializableExtra("editedFireObj"));
            return;
        }
        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            this.requestFiresData(view.getSpinnerSelectedPosition());
        }
    }

    @Override
    public void requestUploadData(int type) {
        model.getFiresList(view.getAppContext(), this, type, 1);
    }

    @Override
    public void showLocations(ArrayList<FireHydrantObject> fires) {
        view.navigateToMap2(SHOW_ALL_IN_MAP_REQUEST,fires);
    }

    @Override
    public void onSyncingResponse(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        ArrayList<Long> accepted = new ArrayList<>(), rejected = new ArrayList<>();
        for (int i = 0; i < responseArray.length(); i++) {
            if (responseArray.getJSONObject(i).getString("status").equals("success"))
                accepted.add(responseArray.getJSONObject(i).getLong("id"));
            else rejected.add(responseArray.getJSONObject(i).getLong("id"));
        }

        if (!accepted.isEmpty()) {
            for (int i = 0; i < accepted.size(); i++) {
                model.removeFireObject(view.getActContext(), this, accepted.get(i), -1);
            }
        }
    }

    @Override
    public void onSyncingError(VolleyError error) {
//        String body;
//        try {
//            //get status code here
//            String statusCode = String.valueOf(error.networkResponse.statusCode);
//            //get response body and parse with appropriate encoding
//            if (error.networkResponse.data != null) {
//                try {
//                    body = new String(error.networkResponse.data, "UTF-8");
//                    Log.e("body", body);
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                    Log.e("aaaaaaaa", e.toString());
//                }
//            }
//        } catch (Exception e) {
//            Log.e("aaaaaaaa 1", e.toString());
//        }

        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onGetFiresListCalled(ArrayList<FireHydrantObject> fires, int index) {
        if (index == 0) {
            if (fires.isEmpty())
                view.showEmptyListText();
            else {
                boolean isVisible = false;
                for (int i = 0; i < fires.size(); i++) {
                    if (fires.get(i).getFireLatitude() != null && fires.get(i).getFireLongitude() != null) {
                        if (!fires.get(i).getFireLatitude().isEmpty() && !fires.get(i).getFireLongitude().isEmpty()) {
                            isVisible = true;
                            break;
                        }
                    }
                }
                view.loadFiresData(fires,isVisible);
            }
        } else {
            if (fires.isEmpty())
                view.showToastMessage(view.getActContext().getString(R.string.no_fires_to_synchronize));
            else {
                view.showProgress();
                model.uploadFiresData(view.getActContext(), this, fires);
            }
        }
    }

    @Override
    public void onRemoveFireObjectCalled(int flag, int position, long id) {
        if (flag > 0) {
            if (position == -1) {
                view.removeListItem(id);
            } else
                view.removeListItem(position);
        } else view.showToastMessage(view.getActContext().getString(R.string.failed_to_delete));
    }
}
