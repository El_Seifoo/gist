package com.seifoo.neomit.gisgathering.home.sensors.edit;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class EditSensorPresenter implements EditSensorMVP.Presenter, EditSensorModel.DBCallback {
    private EditSensorMVP.View view;
    private EditSensorMVP.MainView mainView;
    private EditSensorMVP.FirstView firstView;
    private EditSensorMVP.SecondView secondView;
    private EditSensorModel model;

    public EditSensorPresenter(EditSensorMVP.View view, EditSensorMVP.MainView mainView, EditSensorModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public EditSensorPresenter(EditSensorMVP.View view, EditSensorMVP.FirstView firstView, EditSensorModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public EditSensorPresenter(EditSensorMVP.View view, EditSensorMVP.SecondView secondView, EditSensorModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    /*
        0 -> meter diameter , 1 -> meter brand , 2 - > meter type , 3 -> meter status , 4 -> meter material , 5 -> meter remarks
        6 -> read type , 7 -> pipe after meter , 8 -> pipe size , 9 -> maintenance area , 10 -> meter box Position , 11 -> meter box type
        12 -> cover status , 13 -> location , 14 -> building usage , 15 -> sub-building type , 16 -> water schedule , 17 -> water connection type
        18 -> pipe material , 19 -> new meter brand , 20 ->sensor type  , 21 -> sensor status , 22 -> yes_no , 23 -> reducer diameter
        24 -> FireType , 25 -> districts , 26 -> houseConnectionType , 27 -> sensorJob , 28 -> sub-district , 29 -> tankType
        30 -> pipeType , 31 -> maintenanceType , 32 -> soilType ,  33 -> maintenanceDeviceType , 34 -> procedure
        35 -> maintenanceCompany , 36 -> asphalt
     */
    @Override
    public void requestFirstSpinnersData(SensorObject sensor) {
        // deviceType 37, assetStatus 3, type 38 , maintenanceArea 9
        model.getGeoMasters(view.getAppContext(), this, new int[]{37, 3, 38, 9},
                new int[]{sensor.getDeviceType(), sensor.getAssetStatus(), sensor.getType(), sensor.getMaintenanceArea()},
                1);
        firstView.setLocation(sensor.getLatitude() + "," + sensor.getLongitude());
        firstView.setAssetNumber(sensor.getAssetNumber());
        firstView.setSerialNumber(sensor.getSerialNumber());
        firstView.setData(sensor.getElevation(), sensor.getChamberStatus(), sensor.getLineNumber());
    }

    @Override
    public void requestSecondSpinnersData(SensorObject sensor) {
        // enabled 22, districtName 25, diameter 0, pipeMaterial 18
        model.getGeoMasters(view.getAppContext(), this, new int[]{22, 25, 0, 18},
                new int[]{sensor.getEnabled(), sensor.getDistrictName(), sensor.getDiameter(), sensor.getPipeMaterial()},
                2);
        secondView.setCommissionDate(sensor.getCommissionDate());
        secondView.setLastUpdate(sensor.getLastUpdate());
        secondView.setData(sensor.getStreetName(), sensor.getDmaZone(), sensor.getSectorName());
    }

    @Override
    public void requestPassFirstStepDataToActivity(SensorObject sensor) {
        if (!sensor.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        sensor.setLatitude(sensor.getLatitude().split(",")[0]);
        sensor.setLongitude(sensor.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof EditSensorActivity) {
            ((EditSensorActivity) view.getAppContext()).passFirstObj(sensor);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(SensorObject sensor) {
        if (view.getAppContext() instanceof EditSensorActivity) {
            ((EditSensorActivity) view.getAppContext()).passSecondObj(sensor);
        }
    }

    @Override
    public void requestEditSensor(SensorObject sensor, String createdAt, long id) {
        sensor.setId(id);
        sensor.setCreatedAt(createdAt);
        model.updateSensorObj(view.getAppContext(), this, sensor);
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View clickedView) {
                switch (clickedView.getId()) {
                    case R.id.serial_num_qr:
                        requestQrCode(((EditSensorActivity) view.getAppContext()), view.getAppContext().getString(R.string.serial_number));
                        break;
                    case R.id.asset_num_qr:
                        requestQrCode(((EditSensorActivity) view.getAppContext()), view.getAppContext().getString(R.string.asset_number));
                        break;
                }
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext, String flagString) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (flagString.equals(view.getAppContext().getString(R.string.asset_number))) {
            flag = 1;
        }

        firstView.initializeScanner(integrator, flag);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void handleQRScannerResult(String contents, int flag) {
        contents = contents.replaceAll("[^0-9]", "");
        if (flag == 0) {
            firstView.setSerialNumber(contents);
        } else if (flag == 1) {
            firstView.setAssetNumber(contents);
        }
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getSensorsLocation(view.getAppContext(), this, prevLatLng);
    }

    @Override
    public void requestDatePickerDialog(String dateString, final int index) {

        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        view.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        if (index == 0)
                            secondView.setCommissionDate(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                        else
                            secondView.setLastUpdate(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();
    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index) {
        if (index == 1) handleFirstView(masters, selectedIds);
        else if (index == 2) handleSecondView(masters, selectedIds);
    }

    //{37, 3, 38, 9}
    private void handleFirstView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                deviceType = new ArrayList<>(),
                assetStatus = new ArrayList<>(),
                type = new ArrayList<>(),
                maintenanceArea = new ArrayList<>();

        ArrayList<Integer>
                deviceTypeIds = new ArrayList<>(),
                assetStatusIds = new ArrayList<>(),
                typeIds = new ArrayList<>(),
                maintenanceAreaIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 37) {
                deviceType.add(masters.get(i).getName());
                deviceTypeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 3) {
                assetStatus.add(masters.get(i).getName());
                assetStatusIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 38) {//////////// not completely done
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 9) {
                maintenanceArea.add(masters.get(i).getName());
                maintenanceAreaIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int deviceTypeIndex = returnIndex(deviceTypeIds, selectedIds[0]);
        int assetStatusIndex = returnIndex(assetStatusIds, selectedIds[1]);
        int typeIndex = returnIndex(typeIds, selectedIds[2]);
        int maintenanceAreaIndex = returnIndex(maintenanceAreaIds, selectedIds[3]);

        firstView.loadSpinnersData(deviceType, deviceTypeIds, deviceTypeIndex,
                assetStatus, assetStatusIds, assetStatusIndex,
                type, typeIds, typeIndex,
                maintenanceArea, maintenanceAreaIds, maintenanceAreaIndex);
    }

    //{22, 25, 0, 18}
    private void handleSecondView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                enabled = new ArrayList<>(),
                districtName = new ArrayList<>(),
                diameter = new ArrayList<>(),
                pipeMaterial = new ArrayList<>();

        ArrayList<Integer>
                enabledIds = new ArrayList<>(),
                districtNameIds = new ArrayList<>(),
                diameterIds = new ArrayList<>(),
                pipeMaterialIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 22) {
                enabled.add(masters.get(i).getName());
                enabledIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 25) {
                districtName.add(masters.get(i).getName());
                districtNameIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                diameter.add(masters.get(i).getName());
                diameterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 18) {
                pipeMaterial.add(masters.get(i).getName());
                pipeMaterialIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int enabledIndex = returnIndex(enabledIds, selectedIds[0]);
        int districtNameIndex = returnIndex(districtNameIds, selectedIds[1]);
        int diameterIndex = returnIndex(diameterIds, selectedIds[2]);
        int pipeMaterialIndex = returnIndex(pipeMaterialIds, selectedIds[3]);

        secondView.loadSpinnersData(enabled, enabledIds, enabledIndex,
                districtName, districtNameIds, districtNameIndex,
                diameter, diameterIds, diameterIndex,
                pipeMaterial, pipeMaterialIds, pipeMaterialIndex);
    }

    private int returnIndex(ArrayList<Integer> ids, int selectedId) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == selectedId)
                return i;
        }
        return 0;
    }

    @Override
    public void onSensorUpdatingCalled(int flag, SensorObject sensor) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.edit_sensor_done_successfully));
            mainView.backToParent(sensor);
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_edit));
        }
    }

    @Override
    public void onGetSensorsLocationCalled(ArrayList<HashMap<String, String>> sensorsLocation, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], sensorsLocation, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", sensorsLocation, PICK_LOCATION_REQUEST);
        }
    }
}
