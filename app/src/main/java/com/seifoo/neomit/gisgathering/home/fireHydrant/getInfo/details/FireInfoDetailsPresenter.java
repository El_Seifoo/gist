package com.seifoo.neomit.gisgathering.home.fireHydrant.getInfo.details;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class FireInfoDetailsPresenter implements FireInfoDetailsMVP.Presenter, FireInfoDetailsModel.DBCallback {
    private FireInfoDetailsMVP.View view;
    private FireInfoDetailsModel model;

    public FireInfoDetailsPresenter(FireInfoDetailsMVP.View view, FireInfoDetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestFireData(FireHydrantObject fireObject) {
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();
        ids.add(fireObject.getFireBarrelDiameter());
        types.add(0);
        ids.add(fireObject.getFireMaterialId());
        types.add(4);
        ids.add(fireObject.getFireTypeId());
        types.add(24);
        ids.add(fireObject.getFireStatus());
        types.add(3);
        ids.add(fireObject.getFireRemarks());
        types.add(5);
        ids.add(fireObject.getFireValveType());
        types.add(20);
        ids.add(fireObject.getFireBrand());
        types.add(1);
        ids.add(fireObject.getFireWaterSchedule());
        types.add(16);
        ids.add(fireObject.getFireMaintenanceArea());
        types.add(9);
        ids.add(fireObject.getFireDistrictName());
        types.add(25);

        model.returnValidGeoMaster(view.getAppContext(), this, fireObject, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));
    }

    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, FireHydrantObject fireObject) {

        ArrayList<MoreDetails> moreDetails = new ArrayList<>();
        // 0 , 1 , 3 , 4 , 5 , 9 , 16 , 20 , 24
        if (!fireObject.getFireLatitude().isEmpty() && !fireObject.getFireLongitude().isEmpty())
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), fireObject.getFireLatitude() + "," + fireObject.getFireLongitude()));
        else
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), ""));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.serial_number), fireObject.getFireSerialNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.fh_number), fireObject.getFireNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.height), fireObject.getFireHeight()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.barrel_diameter), strings.get(0)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.material), strings.get(3)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.type), strings.get(8)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.commission_date), fireObject.getFireCommissionDate()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(9)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_name), fireObject.getFireStreetName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sector_name), fireObject.getFireSectorName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.fire_hydrant_status), strings.get(2)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.remarks), strings.get(4)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.with_water), fireObject.getFireWithWater()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.fire_hydrant_valve_type), strings.get(7)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.fire_hydrant_brand), strings.get(1)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.water_schedule), strings.get(6)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.meter_maintenance_area), strings.get(5)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.created_at), fireObject.getCreatedAt()));

        view.loadFireMoreDetails(moreDetails);
    }
}
