package com.seifoo.neomit.gisgathering.home.meter.offline;

import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.meter.MeterObject;

import java.util.ArrayList;

public interface OfflineMeterMVP {
    interface View {

        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void showToastMessage(String message);

        void loadMetersData(ArrayList<MeterObject> meterObjectList, boolean isVisible);


        void removeListItem(int position);

        void navigateDestination(String key, MeterObject meterObject, Class destination);

        void navigateToEditMeter(Intent intent, int requestCode);

        void updateListItem(int position, MeterObject editedMeterObj);

        void removeListItem(long id);

        void navigateToMap(int requestCode, MeterObject meterObject, int position);

        int getSpinnerSelectedPosition();

        void navigateToMap2(int requestCode, ArrayList<MeterObject> meters);
    }

    interface Presenter {
        void requestMetersData(int type);

        void requestRemoveMeterObjById(long id, int position);

        void OnListItemClickListener(int viewId, int position, MeterObject meterObject);

        void requestEditMeterData(int position, MeterObject meterObject);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestUploadData(int type);

        void showLocations(ArrayList<MeterObject> meters);
    }
}
