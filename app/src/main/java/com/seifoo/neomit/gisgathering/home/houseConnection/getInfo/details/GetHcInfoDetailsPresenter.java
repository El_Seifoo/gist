package com.seifoo.neomit.gisgathering.home.houseConnection.getInfo.details;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class GetHcInfoDetailsPresenter implements GetHcInfoDetailsMVP.Presenter, GetHcInfoDetailsModel.DBCallback {
    private GetHcInfoDetailsMVP.View view;
    private GetHcInfoDetailsModel model;

    public GetHcInfoDetailsPresenter(GetHcInfoDetailsMVP.View view, GetHcInfoDetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestHcData(HouseConnectionObject HcObject) {
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();
        // 26,0,4,3 houseConnectionType,meter diameter,meter material,meter status
        // 25,16,5 districts,water schedule,meter remarks
        ids.add(HcObject.getDiameter());
        types.add(0);
        ids.add(HcObject.getAssetStatus());
        types.add(3);
        ids.add(HcObject.getMaterial());
        types.add(4);
        ids.add(HcObject.getRemarks());
        types.add(5);
        ids.add(HcObject.getWaterSchedule());
        types.add(16);
        ids.add(HcObject.getDistrictName());
        types.add(25);
        ids.add(HcObject.getType());
        types.add(26);


        model.returnValidGeoMaster(view.getAppContext(), this, HcObject, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));
    }

    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, HouseConnectionObject HcObject) {
        ArrayList<MoreDetails> moreDetails = new ArrayList<>();
        //  0,  3,  4,   5,  16,   25,   26
        //  0   1   2    3   4      5    6


        if (!HcObject.getLatitude().isEmpty() && !HcObject.getLongitude().isEmpty())
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), HcObject.getLatitude() + "," + HcObject.getLongitude()));
        else
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), ""));

        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.serial_number), HcObject.getSerialNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.hcn), HcObject.getHCN()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.type), strings.get(6)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.diameter), strings.get(0)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.material), strings.get(2)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.asset_status), strings.get(1)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(5)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sub_district), HcObject.getSubDistrict()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_name), HcObject.getStreetName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sector_name), HcObject.getSectorName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.water_schedule), strings.get(4)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.remarks), strings.get(3)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.created_at), HcObject.getCreatedAt()));

        view.loadHcMoreDetails(moreDetails);
    }
}
