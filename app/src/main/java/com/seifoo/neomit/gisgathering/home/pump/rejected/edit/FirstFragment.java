package com.seifoo.neomit.gisgathering.home.pump.rejected.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class FirstFragment extends Fragment implements Step, EditRejectedPumpsMVP.View, EditRejectedPumpsMVP.FirstView {
    private EditText latLngEditText, codeEditText, stationNameEditText;
    private Spinner enabledSpinner, typeSpinner, assetStatusSpinner;

    private EditRejectedPumpsMVP.Presenter presenter;

    public static FirstFragment newInstance(PumpObject pump) {
        FirstFragment fragment = new FirstFragment();

        Bundle args = new Bundle();
        args.putSerializable("PumpObj", pump);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pump_first_edit, container, false);

        presenter = new EditRejectedPumpsPresenter(this, this, new EditRejectedPumpsModel());


        enabledSpinner = (Spinner) view.findViewById(R.id.enabled_spinner);
        typeSpinner = (Spinner) view.findViewById(R.id.type_spinner);
        assetStatusSpinner = (Spinner) view.findViewById(R.id.asset_status_spinner);


        latLngEditText = (EditText) view.findViewById(R.id.location);
        codeEditText = (EditText) view.findViewById(R.id.station_code);
        stationNameEditText = (EditText) view.findViewById(R.id.station_name);


        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(latLngEditText.getText().toString().trim());
            }
        });

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.station_code_qr)));


        presenter.requestFirstSpinnersData((PumpObject) getArguments().getSerializable("PumpObj"));

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFirstStepDataToActivity(new PumpObject(
                latLngEditText.getText().toString().trim(), latLngEditText.getText().toString().trim(),
                codeEditText.getText().toString().trim(), stationNameEditText.getText().toString().trim(),
                enabledIds.get(enabledSpinner.getSelectedItemPosition()), typeIds.get(typeSpinner.getSelectedItemPosition()),
                assetStatusIds.get(assetStatusSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    ArrayList<Integer> enabledIds, typeIds, assetStatusIds;

    @Override
    public void loadSpinnersData(ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex,
                                 ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex,
                                 ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds, int assetStatusIndex) {
        this.enabledIds = enabledIds;
        this.typeIds = typeIds;
        this.assetStatusIds = assetStatusIds;

        enabledSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, enabled));
        enabledSpinner.setSelection(enabledIndex);
        typeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, type));
        typeSpinner.setSelection(typeIndex);
        assetStatusSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, assetStatus));
        assetStatusSpinner.setSelection(assetStatusIndex);

    }

    @Override
    public void showLocationError(String errorMessage) {
        latLngEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((EditRejectedPumpsActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, 1);
    }

    @Override
    public void setStationCode(String data) {
        codeEditText.setText(data);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivityForResult(intent, requestCode);
    }


    @Override
    public void setLocation(String location) {
        latLngEditText.setText(location);
    }

    @Override
    public void setStationName(String stationName) {
        stationNameEditText.setText(stationName);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFirstViewActivityResult(requestCode, resultCode, data);
    }
}
