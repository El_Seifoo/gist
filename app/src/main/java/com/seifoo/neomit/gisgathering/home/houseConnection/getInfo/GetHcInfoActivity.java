package com.seifoo.neomit.gisgathering.home.houseConnection.getInfo;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.home.houseConnection.getInfo.details.GetHcInfoDetailsActivity;

public class GetHcInfoActivity extends AppCompatActivity implements GetHcInfoMVP.View {
    private EditText serialNumberEditText, HCNEditText;
    private Button getInfoButton;
    private ProgressBar progressBar;

    private GetHcInfoMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_hc_info);

        getSupportActionBar().setTitle(getString(R.string.get_info));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new GetHcInfoPresenter(this,new GetHcInfoModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        serialNumberEditText = (EditText) findViewById(R.id.serial_num);
        HCNEditText = (EditText) findViewById(R.id.hcn);

        getInfoButton = (Button) findViewById(R.id.get_info_button);
        getInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestGetInfo(serialNumberEditText.getText().toString().trim(),
                        HCNEditText.getText().toString().trim());
            }
        });

    }

    public void scanQR(View view) {
        presenter.whichQRClicked(view.getId());
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator, int flag) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }

    @Override
    public void setHCN(String data) {
        HCNEditText.setText(data);
    }


    @Override
    public void loadHcInfo(HouseConnectionObject moreDetails) {
        Intent intent = new Intent(this, GetHcInfoDetailsActivity.class);
        intent.putExtra("GetInfo", moreDetails);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
