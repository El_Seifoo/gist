package com.seifoo.neomit.gisgathering.home.breaks.map;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;

public class GetBreakMapActivity extends AppCompatActivity implements GetBreakMapMVP.View {
    private EditText breakNumberEditText;
    private Button getMapButton;
    private ProgressBar progressBar;

    private GetBreakMapMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_break_map);

        getSupportActionBar().setTitle(getString(R.string.get_map));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new GetBreakMapPresenter(this, new GetBreakMapModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        breakNumberEditText = (EditText) findViewById(R.id.break_number);

        getMapButton = (Button) findViewById(R.id.get_info_button);
        getMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestGetMap(breakNumberEditText.getText().toString().trim());
            }
        });
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void loadBreakMap(String latitude, String longitude) {
        Intent intent = new Intent(this, GetBreakMapDetailsActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
