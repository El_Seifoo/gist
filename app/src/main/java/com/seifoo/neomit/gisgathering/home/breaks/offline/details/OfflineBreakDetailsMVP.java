package com.seifoo.neomit.gisgathering.home.breaks.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface OfflineBreakDetailsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void loadBreakMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestBreakDetails(BreakObject breakObj);
    }
}
