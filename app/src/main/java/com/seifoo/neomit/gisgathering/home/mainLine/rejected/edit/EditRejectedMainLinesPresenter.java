package com.seifoo.neomit.gisgathering.home.mainLine.rejected.edit;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class EditRejectedMainLinesPresenter implements EditRejectedMainLinesMVP.Presenter,EditRejectedMainLinesModel.DBCallback {
    private EditRejectedMainLinesMVP.View view;
    private EditRejectedMainLinesMVP.MainView mainView;
    private EditRejectedMainLinesMVP.FirstView firstView;
    private EditRejectedMainLinesMVP.SecondView secondView;
    private EditRejectedMainLinesModel model;

    public EditRejectedMainLinesPresenter(EditRejectedMainLinesMVP.View view, EditRejectedMainLinesMVP.MainView mainView, EditRejectedMainLinesModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public EditRejectedMainLinesPresenter(EditRejectedMainLinesMVP.View view, EditRejectedMainLinesMVP.FirstView firstView, EditRejectedMainLinesModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public EditRejectedMainLinesPresenter(EditRejectedMainLinesMVP.View view, EditRejectedMainLinesMVP.SecondView secondView, EditRejectedMainLinesModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    @Override
    public void requestFirstSpinnersData(MainLineObject mainLine) {
        //  diameter 0, type 29, enabled 22, material 4
        model.getGeoMasters(view.getAppContext(), this, new int[]{0, 29, 22, 4},
                new int[]{mainLine.getDiameter(), mainLine.getType(), mainLine.getEnabled(), mainLine.getMaterial()},
                1);

        firstView.setLocation(mainLine.getLatitude() + "," + mainLine.getLongitude());
        firstView.setSerialNumber(mainLine.getSerialNumber());
        firstView.setData(mainLine.getStreetName(), mainLine.getStreetNumber());
    }

    @Override
    public void requestSecondSpinnersData(MainLineObject mainLine) {
//  districtName 25, subDistrict 28, waterSchedule 16, assetStatus 3 , remarks 5
        model.getGeoMasters(view.getAppContext(), this, new int[]{25, 28, 16, 3, 5},
                new int[]{mainLine.getDistrictName(), mainLine.getSubDistrict(), mainLine.getWaterSchedule(),
                        mainLine.getAssetStatus(), mainLine.getRemarks()},
                2);

        secondView.setLastUpdated(mainLine.getLastUpdated());
        secondView.setAssigned(mainLine.getAssigned());
        secondView.setData(mainLine.getDmaZone(), mainLine.getSubName(), mainLine.getSectorName());
    }

    @Override
    public void requestPassFirstStepDataToActivity(MainLineObject mainLine) {
        if (!mainLine.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        mainLine.setLatitude(mainLine.getLatitude().split(",")[0]);
        mainLine.setLongitude(mainLine.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof EditRejectedMainLinesActivity) {
            ((EditRejectedMainLinesActivity) view.getAppContext()).passFirstObj(mainLine);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(MainLineObject mainLine) {
        if (view.getAppContext() instanceof EditRejectedMainLinesActivity) {
            ((EditRejectedMainLinesActivity) view.getAppContext()).passSecondObj(mainLine);
        }
    }

    @Override
    public void requestEditMainLine(MainLineObject mainLine, String createdAt, long id) {
        mainLine.setMainLineId(id);
        mainLine.setCreatedAt(returnValidNumbers(createdAt));
        model.insertMainLineObj(view.getAppContext(), this, mainLine);
    }

    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQrCode(((EditRejectedMainLinesActivity) view.getAppContext()));
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        firstView.initializeScanner(integrator);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents());
        }
    }

    public void handleQRScannerResult(String contents) {
        firstView.setSerialNumber(contents.replaceAll("[^0-9]", ""));
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", PICK_LOCATION_REQUEST);
        }
    }

    @Override
    public void requestDatePickerDialog(String dateString, final int index) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        view.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        if (index == 0)
                            secondView.setLastUpdated(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                        else
                            secondView.setAssigned(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();
    }


    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index) {
        if (index == 1) handleFirstView(masters, selectedIds);
        else if (index == 2) handleSecondView(masters, selectedIds);
    }

    private void handleFirstView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                diameter = new ArrayList<>(),
                type = new ArrayList<>(),
                enabled = new ArrayList<>(),
                material = new ArrayList<>();


        ArrayList<Integer>
                diameterIds = new ArrayList<>(),
                typeIds = new ArrayList<>(),
                enabledIds = new ArrayList<>(),
                materialIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 0) {
                diameter.add(masters.get(i).getName());
                diameterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 28) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 22) {
                enabled.add(masters.get(i).getName());
                enabledIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 4) {
                material.add(masters.get(i).getName());
                materialIds.add(masters.get(i).getGeoMasterId());
            }
        }


        int diameterIndex = returnIndex(diameterIds, selectedIds[0]);
        int typeIndex = returnIndex(typeIds, selectedIds[1]);
        int enabledIndex = returnIndex(enabledIds, selectedIds[2]);
        int materialIndex = returnIndex(materialIds, selectedIds[3]);

        firstView.loadSpinnersData(diameter, diameterIds, diameterIndex,
                type, typeIds, typeIndex,
                enabled, enabledIds, enabledIndex,
                material, materialIds, materialIndex);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                districtName = new ArrayList<>(),
                subDistrict = new ArrayList<>(),
                waterSchedule = new ArrayList<>(),
                assetStatus = new ArrayList<>(),
                remarks = new ArrayList<>();

        ArrayList<Integer>
                districtNameIds = new ArrayList<>(),
                subDistrictIds = new ArrayList<>(),
                waterScheduleIds = new ArrayList<>(),
                assetStatusIds = new ArrayList<>(),
                remarksIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 25) {
                districtName.add(masters.get(i).getName());
                districtNameIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 28) {
                subDistrict.add(masters.get(i).getName());
                subDistrictIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 16) {
                waterSchedule.add(masters.get(i).getName());
                waterScheduleIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 3) {
                assetStatus.add(masters.get(i).getName());
                assetStatusIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 5) {
                remarks.add(masters.get(i).getName());
                remarksIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int districtNameIndex = returnIndex(districtNameIds, selectedIds[0]);
        int subDistrictIndex = returnIndex(subDistrictIds, selectedIds[1]);
        int waterScheduleIndex = returnIndex(waterScheduleIds, selectedIds[2]);
        int assetStatusIndex = returnIndex(assetStatusIds, selectedIds[3]);
        int remarksIndex = returnIndex(remarksIds, selectedIds[4]);

        secondView.loadSpinnersData(districtName, districtNameIds, districtNameIndex,
                subDistrict, subDistrictIds, subDistrictIndex,
                waterSchedule, waterScheduleIds, waterScheduleIndex,
                assetStatus, assetStatusIds, assetStatusIndex,
                remarks, remarksIds, remarksIndex);
    }

    private int returnIndex(ArrayList<Integer> ids, int selectedId) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == selectedId)
                return i;
        }
        return 0;
    }

    @Override
    public void onMainLineInsertionCalled(long flag) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.edit_main_line_done_successfully));
            mainView.backToParent();
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_save_main_line));
        }
    }
}
