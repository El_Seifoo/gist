package com.seifoo.neomit.gisgathering.home.chamber;

import java.io.Serializable;

public class ChamberObject implements Serializable {
    private long id, chamberId;
    private String latitude, longitude;
    private int pipeMaterial;
    private String chNumber, length, width, depth;
    private int diameter, districtName;
    private String depthUnderPipe, depthAbovePipe, dmaZone;
    private int subDistrictName;
    private String streetName, streetNumber, subName, sectorName;
    private int diameterAirValve, diameterWAValve, diameterISOValve, diameterFlowMeter;
    private String imageNumber;
    private int type;
    private String updatedDate;
    private int remarks;
    private String reason, createdAt;

    // first cont.
    public ChamberObject(String latitude, String longitude, int pipeMaterial, String chNumber, String length, String width,
                         String depth, int diameter, String depthUnderPipe, String depthAbovePipe, int districtName, String dmaZone) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.pipeMaterial = pipeMaterial;
        this.chNumber = chNumber;
        this.length = length;
        this.width = width;
        this.depth = depth;
        this.diameter = diameter;
        this.depthUnderPipe = depthUnderPipe;
        this.depthAbovePipe = depthAbovePipe;
        this.districtName = districtName;
        this.dmaZone = dmaZone;
    }

    public ChamberObject getFirstObject() {
        return new ChamberObject(latitude, longitude, pipeMaterial, chNumber, length, width,
                depth, diameter, depthUnderPipe, depthAbovePipe, districtName, dmaZone);
    }

    // second const.
    public ChamberObject(int subDistrictName, String streetName, String streetNumber, String subName, String sectorName, int diameterAirValve,
                         int diameterWAValve, int diameterISOValve, int diameterFlowMeter, String imageNumber, int type, String updatedDate, int remarks) {
        this.subDistrictName = subDistrictName;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.subName = subName;
        this.sectorName = sectorName;
        this.diameterAirValve = diameterAirValve;
        this.diameterWAValve = diameterWAValve;
        this.diameterISOValve = diameterISOValve;
        this.diameterFlowMeter = diameterFlowMeter;
        this.imageNumber = imageNumber;
        this.type = type;
        this.updatedDate = updatedDate;
        this.remarks = remarks;
    }

    public ChamberObject getSecondObject() {
        return new ChamberObject(subDistrictName, streetName, streetNumber, subName, sectorName, diameterAirValve,
                diameterWAValve, diameterISOValve, diameterFlowMeter, imageNumber, type, updatedDate, remarks);
    }

    public ChamberObject(ChamberObject firstObj, ChamberObject secondObj) {
        this.latitude = firstObj.getLatitude();
        this.longitude = firstObj.getLongitude();
        this.pipeMaterial = firstObj.getPipeMaterial();
        this.chNumber = firstObj.getChNumber();
        this.length = firstObj.getLength();
        this.width = firstObj.getWidth();
        this.depth = firstObj.getDepth();
        this.diameter = firstObj.getDiameter();
        this.depthUnderPipe = firstObj.getDepthUnderPipe();
        this.depthAbovePipe = firstObj.getDepthAbovePipe();
        this.districtName = firstObj.getDistrictName();
        this.dmaZone = firstObj.getDmaZone();

        this.subDistrictName = secondObj.getSubDistrictName();
        this.streetName = secondObj.getStreetName();
        this.streetNumber = secondObj.getStreetNumber();
        this.subName = secondObj.getSubName();
        this.sectorName = secondObj.getSectorName();
        this.diameterAirValve = secondObj.getDiameterAirValve();
        this.diameterWAValve = secondObj.getDiameterWAValve();
        this.diameterISOValve = secondObj.getDiameterISOValve();
        this.diameterFlowMeter = secondObj.getDiameterFlowMeter();
        this.imageNumber = secondObj.getImageNumber();
        this.type = secondObj.getType();
        this.updatedDate = secondObj.getUpdatedDate();
        this.remarks = secondObj.getRemarks();

    }

    public ChamberObject(long id, long chamberId, String latitude, String longitude, int pipeMaterial, String chNumber, String length, String width,
                         String depth, int diameter, String depthUnderPipe, String depthAbovePipe, int districtName, String dmaZone, int subDistrictName,
                         String streetName, String streetNumber, String subName, String sectorName, int diameterAirValve, int diameterWAValve, int diameterISOValve,
                         int diameterFlowMeter, String imageNumber, int type, String updatedDate, int remarks, String createdAt) {
        this.id = id;
        this.chamberId = chamberId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.pipeMaterial = pipeMaterial;
        this.chNumber = chNumber;
        this.length = length;
        this.width = width;
        this.depth = depth;
        this.diameter = diameter;
        this.depthUnderPipe = depthUnderPipe;
        this.depthAbovePipe = depthAbovePipe;
        this.districtName = districtName;
        this.dmaZone = dmaZone;
        this.subDistrictName = subDistrictName;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.subName = subName;
        this.sectorName = sectorName;
        this.diameterAirValve = diameterAirValve;
        this.diameterWAValve = diameterWAValve;
        this.diameterISOValve = diameterISOValve;
        this.diameterFlowMeter = diameterFlowMeter;
        this.imageNumber = imageNumber;
        this.type = type;
        this.updatedDate = updatedDate;
        this.remarks = remarks;
        this.createdAt = createdAt;
    }

    public ChamberObject(String latitude, String longitude, int pipeMaterial, String chNumber, String length, String width, String depth, int diameter,
                         String depthUnderPipe, String depthAbovePipe, int districtName, String dmaZone, int subDistrictName, String streetName,
                         String streetNumber, String subName, String sectorName, int diameterAirValve, int diameterWAValve, int diameterISOValve, int diameterFlowMeter,
                         String imageNumber, int type, String updatedDate, int remarks, String createdAt) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.pipeMaterial = pipeMaterial;
        this.chNumber = chNumber;
        this.length = length;
        this.width = width;
        this.depth = depth;
        this.diameter = diameter;
        this.depthUnderPipe = depthUnderPipe;
        this.depthAbovePipe = depthAbovePipe;
        this.districtName = districtName;
        this.dmaZone = dmaZone;
        this.subDistrictName = subDistrictName;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.subName = subName;
        this.sectorName = sectorName;
        this.diameterAirValve = diameterAirValve;
        this.diameterWAValve = diameterWAValve;
        this.diameterISOValve = diameterISOValve;
        this.diameterFlowMeter = diameterFlowMeter;
        this.imageNumber = imageNumber;
        this.type = type;
        this.updatedDate = updatedDate;
        this.remarks = remarks;
        this.createdAt = createdAt;
    }

    public ChamberObject(long chamberId, String latitude, String longitude, int pipeMaterial, String chNumber, String length, String width, String depth,
                         int diameter, String depthUnderPipe, String depthAbovePipe, int districtName, String dmaZone, int subDistrictName, String streetName,
                         String streetNumber, String subName, String sectorName, int diameterAirValve, int diameterWAValve, int diameterISOValve, int diameterFlowMeter,
                         String imageNumber, int type, String updatedDate, int remarks, String reason, String createdAt) {
        this.chamberId = chamberId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.pipeMaterial = pipeMaterial;
        this.chNumber = chNumber;
        this.length = length;
        this.width = width;
        this.depth = depth;
        this.diameter = diameter;
        this.depthUnderPipe = depthUnderPipe;
        this.depthAbovePipe = depthAbovePipe;
        this.districtName = districtName;
        this.dmaZone = dmaZone;
        this.subDistrictName = subDistrictName;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.subName = subName;
        this.sectorName = sectorName;
        this.diameterAirValve = diameterAirValve;
        this.diameterWAValve = diameterWAValve;
        this.diameterISOValve = diameterISOValve;
        this.diameterFlowMeter = diameterFlowMeter;
        this.imageNumber = imageNumber;
        this.type = type;
        this.updatedDate = updatedDate;
        this.remarks = remarks;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public long getChamberId() {
        return chamberId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public int getPipeMaterial() {
        return pipeMaterial;
    }

    public String getChNumber() {
        return chNumber;
    }

    public String getLength() {
        return length;
    }

    public String getWidth() {
        return width;
    }

    public String getDepth() {
        return depth;
    }

    public int getDiameter() {
        return diameter;
    }

    public String getDepthUnderPipe() {
        return depthUnderPipe;
    }

    public String getDepthAbovePipe() {
        return depthAbovePipe;
    }

    public int getDistrictName() {
        return districtName;
    }

    public String getDmaZone() {
        return dmaZone;
    }

    public int getSubDistrictName() {
        return subDistrictName;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getSubName() {
        return subName;
    }

    public String getSectorName() {
        return sectorName;
    }

    public int getDiameterAirValve() {
        return diameterAirValve;
    }

    public int getDiameterWAValve() {
        return diameterWAValve;
    }

    public int getDiameterISOValve() {
        return diameterISOValve;
    }

    public int getDiameterFlowMeter() {
        return diameterFlowMeter;
    }

    public String getImageNumber() {
        return imageNumber;
    }

    public int getType() {
        return type;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public int getRemarks() {
        return remarks;
    }

    public String getReason() {
        return reason;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setChamberId(long chamberId) {
        this.chamberId = chamberId;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
