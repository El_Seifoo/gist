package com.seifoo.neomit.gisgathering.home.meter.getMap;


import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.getInfo.GetMeterInfoActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class GetMeterMapPresenter implements GetMeterMapMVP.Presenter, GetMeterMapModel.VolleyCallback {
    private GetMeterMapMVP.View view;
    private GetMeterMapModel model;

    public GetMeterMapPresenter(GetMeterMapMVP.View view, GetMeterMapModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void whichQRClicked(int id) {
        switch (id) {
            case R.id.serial_num_qr:
                requestQrCode(((GetMeterMapActivity) view.getAppContext()), view.getAppContext().getString(R.string.serial_number));
                break;
            case R.id.hcn_qr:
                requestQrCode(((GetMeterMapActivity) view.getAppContext()), view.getAppContext().getString(R.string.hcn));
                break;
            case R.id.meter_address_qr:
                requestQrCode(((GetMeterMapActivity) view.getAppContext()), view.getAppContext().getString(R.string.meter_address));
                break;
            case R.id.plate_num_qr:
                requestQrCode(((GetMeterMapActivity) view.getAppContext()), view.getAppContext().getString(R.string.plate_number));
                break;
        }
    }

    @Override
    public void requestQrCode(Activity activity, String flagString) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (flagString.equals(view.getAppContext().getString(R.string.serial_number))) {
            flag = 0;
        } else if (flagString.equals(view.getAppContext().getString(R.string.hcn))) {
            flag = 1;
        } else if (flagString.equals(view.getAppContext().getString(R.string.meter_address))) {
            flag = 2;
        } else if (flagString.equals(view.getAppContext().getString(R.string.plate_number))) {
            flag = 3;
        }
        view.initializeScanner(integrator, flag);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void handleQRScannerResult(String contents, int flag) {
        contents = contents.replaceAll("[^0-9]", "");
        if (flag == 0) {
            view.setSerialNumber(contents);
        } else if (flag == 1) {
            view.setHCN(contents);
        } else if (flag == 2) {
            view.setMeterAddress(contents);
        } else {
            view.setPlateNumber(contents);
        }
    }

    @Override
    public void requestGetMap(String serialNumber, String hcn, String meterAddress, String plateNumber) {
        if (serialNumber.isEmpty() && hcn.isEmpty() && meterAddress.isEmpty() && plateNumber.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.fill_at_least_one_field_to_continue));
            return;
        }
        view.showProgress();
        model.getMap(view.getActContext(), this, serialNumber, hcn, meterAddress, plateNumber);
    }

    //x -> longitude , y -> latitude
    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                if (returnValidString(data.getJSONObject(0).getString("X_Map")).isEmpty() || returnValidString(data.getJSONObject(0).getString("Y_Map")).isEmpty())
                    view.showToastMessage(view.getActContext().getString(R.string.this_meter_has_no_location));
                else
                    view.loadMeterMap(returnValidString(data.getJSONObject(0).getString("Y_Map")), returnValidString(data.getJSONObject(0).getString("X_Map")));
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_meter_has_no_location));

        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_meter_has_no_location));

    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
