package com.seifoo.neomit.gisgathering.home.houseConnection.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class OfflineHCsDetailsModel {
    public void returnValidGeoMaster(Context context, DBCallback callback, HouseConnectionObject object, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds(ids, types, lang), object);
    }

    protected interface DBCallback {
        void onConvertingIdsCalled(ArrayList<String> strings, HouseConnectionObject object);
    }
}
