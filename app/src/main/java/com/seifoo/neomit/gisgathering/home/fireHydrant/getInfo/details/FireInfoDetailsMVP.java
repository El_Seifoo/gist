package com.seifoo.neomit.gisgathering.home.fireHydrant.getInfo.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface FireInfoDetailsMVP {
    interface View {
        Context getAppContext();

        Context getActContext();

        void loadFireMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {
        void requestFireData(FireHydrantObject fireObject);
    }
}
