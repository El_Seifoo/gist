package com.seifoo.neomit.gisgathering.home.sensors.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;

public interface GetSensorMapMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void initializeScanner(IntentIntegrator integrator);

        void setSerialNumber(String data);

        void loadSensorMap(String latitude, String longitude);
    }

    interface Presenter {
        void requestQrCode(Activity activity);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestGetMap(String serialNumber);
    }
}
