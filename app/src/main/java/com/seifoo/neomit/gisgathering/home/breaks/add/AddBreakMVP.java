package com.seifoo.neomit.gisgathering.home.breaks.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface AddBreakMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();

        String getCurrentDate();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> pipeType, ArrayList<Integer> pipeTypeIds,
                              ArrayList<String> districtName, ArrayList<Integer> districtNameIds,
                              ArrayList<String> subZone, ArrayList<Integer> subZoneIds,
                              ArrayList<String> diameter, ArrayList<Integer> diameterIds);

        void showLocationError(String errorMessage);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);

        void setBreakDate(String breakDate);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds,
                              ArrayList<String> maintenanceCompany, ArrayList<Integer> maintenanceCompanyIds,
                              ArrayList<String> maintenanceType, ArrayList<Integer> maintenanceTypeIds,
                              ArrayList<String> soilType, ArrayList<Integer> soilTypeIds,
                              ArrayList<String> asphalt, ArrayList<Integer> asphaltIds,
                              ArrayList<String> maintenanceDeviceType, ArrayList<Integer> maintenanceDeviceTypeIds,
                              ArrayList<String> procedure, ArrayList<Integer> procedureIds);
    }

    interface ThirdView {
        void showToastMessage(String message);

        void pickImage(Intent intent, int requestCode);

        void loadBeforeImage(byte[] image);

        void loadAfterImage(byte[] image);


        Fragment getFragment();

        void loadBeforeImage(Bitmap img);

        void loadAfterImage(Bitmap img);
    }

    interface Presenter {
        void requestFirstSpinnersData();

        void requestSecondSpinnersData();

        void requestPassFirstStepDataToActivity(BreakObject breakObject);

        void requestPassSecondStepDataToActivity(BreakObject breakObject);

        void requestPassThirdStepDataToActivity();

        void requestAddBreak(BreakObject breakObject);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString);

        void onButtonsClickListener(int id);

        void onRequestPermissionsResult(Activity activity, int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults);

    }
}
