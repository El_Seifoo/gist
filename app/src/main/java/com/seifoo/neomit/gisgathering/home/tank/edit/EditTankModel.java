package com.seifoo.neomit.gisgathering.home.tank.edit;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class EditTankModel {
    public void getGeoMasters(Context context, DBCallback callback, int[] types, int[] selectedIds, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE, context.getString(R.string.default_language_value))), selectedIds, index);
    }

    public void updateTank(Context context, DBCallback callback, TankObject tank) {
        callback.onTankUpdateCalled(DataBaseHelper.getmInstance(context).updateTank(tank), tank);
    }

    protected void getTanksLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetTanksLocationCalled(DataBaseHelper.getmInstance(context).getTanksLocation(), prevLatLng);
    }
    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index);

        void onTankUpdateCalled(long flag, TankObject tank);

        void onGetTanksLocationCalled(ArrayList<HashMap<String,String>> tanksLocation, String prevLatLng);
    }

}
