package com.seifoo.neomit.gisgathering.home.fireHydrant.add;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class AddFiresModel {

    public void getGeoMasters(Context context, DBCallback callback, int[] types, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context)
                .getGeoMastersByType(types,
                        MySingleton.getmInstance(context).
                                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))),
                index);
    }

    public void insertFireObj(Context context, DBCallback callback, FireHydrantObject meterObject) {
        callback.onFiresInsertionCalled(DataBaseHelper.getmInstance(context).insertFireHydrant(meterObject));
    }

    protected void getFiresLocation(Context context, DBCallback callback, String latLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetFiresLocationCalled(DataBaseHelper.getmInstance(context).getFireHydrantsLocation(), latLng);
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index);

        void onFiresInsertionCalled(long flag);

        void onGetFiresLocationCalled(ArrayList<HashMap<String,String>> fires, String prevLatLng);
    }
}
