package com.seifoo.neomit.gisgathering.home.pump.info;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.home.valve.info.GetValveInfoMVP;
import com.seifoo.neomit.gisgathering.home.valve.info.GetValveInfoModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class GetPumpInfoPresenter implements GetPumpInfoMVP.Presenter, GetPumpInfoModel.VolleyCallback {
    private GetPumpInfoMVP.View view;
    private GetPumpInfoModel model;

    public GetPumpInfoPresenter(GetPumpInfoMVP.View view, GetPumpInfoModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestQrCode(Activity activity) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        view.initializeScanner(integrator);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                view.setCode(result.getContents().replaceAll("[^0-9]", ""));
        }
    }

    @Override
    public void requestGetInfo(String code) {
        if (code.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.station_code_is_required));
            return;
        }
        view.showProgress();
        model.getInfo(view.getActContext(), this, code);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                PumpObject valve = new PumpObject(
                        returnValidString(data.getJSONObject(0).getString("Y_Map")),
                        returnValidString(data.getJSONObject(0).getString("X_Map")),
                        returnValidString(data.getJSONObject(0).getString("CODE")),
                        returnValidString(data.getJSONObject(0).getString("STATION_NAME")),
                        returnValidInt(data.getJSONObject(0).getString("ENABLED_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("ENABLED_ID")),
                        returnValidInt(data.getJSONObject(0).getString("F_TYPE")),
                        returnValidInt(data.getJSONObject(0).getString("ASSET_STATUS_ID")),
                        returnValidString(data.getJSONObject(0).getString("STREET_NAME")),
                        returnValidInt(data.getJSONObject(0).getString("DISTRICT_NAME_ID")),
                        returnValidInt(data.getJSONObject(0).getString("SUP_DISTRICT_ID")),
                        returnValidString(data.getJSONObject(0).getString("DMA_ZONE_ID")),
                        returnValidString(data.getJSONObject(0).getString("SECTOR_NAME")),
                        returnValidDate(data.getJSONObject(0).getString("LAST_UPDATED")),
                        returnValidString(data.getJSONObject(0).getString("REMARKS")),
                        returnValidDate1(data.getJSONObject(0).getString("CreatedDate")));
                view.loadPumpInfo(valve);
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_pump_not_exist));
        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_pump_not_exist));
    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
