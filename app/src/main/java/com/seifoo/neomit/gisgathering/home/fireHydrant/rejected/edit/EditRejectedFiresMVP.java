package com.seifoo.neomit.gisgathering.home.fireHydrant.rejected.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;

import java.util.ArrayList;

public interface EditRejectedFiresMVP {
    // common
    interface View {
        Context getAppContext();

    }

    // activity
    interface MainView {
        void showToastMessage(String message);

        void backToParent();

        Context getActContext();

    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> barrelDiameter, ArrayList<Integer> barrelDiameterIds, int barrelDiameterIndex,
                              ArrayList<String> material, ArrayList<Integer> materialIds, int materialIndex,
                              ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex,
                              ArrayList<String> districtsNames, ArrayList<Integer> districtsNamesIds, int districtsNamesIndex);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator, int flag);

        void setSerialNumber(String data);

        void setFHNumber(String data);

        void setCommissionDate(String date);

        void setHeight(String height);


        void navigateToTheMap(String latitude, String longitude, int requestCode);

        void setLocation(String location);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> fireHydrateStatus, ArrayList<Integer> fireHydrateStatusIds,
                              int fireHydrateStatusIndex, ArrayList<String> remarks, ArrayList<Integer> remarksIds,
                              int remarksIndex, ArrayList<String> fireValveType, ArrayList<Integer> fireValveTypeIds,
                              int fireValveTypeIndex, ArrayList<String> fireHydrateBrand, ArrayList<Integer> fireHydrateBrandIds,
                              int fireHydrateBrandIndex, ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,
                              int waterScheduleIndex, ArrayList<String> maintenanceArea, ArrayList<Integer> maintenanceAreaIds, int maintenanceAreaIndex);

        void setData(String streetName, String sectorName, String withWater);
    }

    interface Presenter {
        void requestFirstStepData(FireHydrantObject fireObject);

        void requestSecondStepData(FireHydrantObject fireObject);

        void requestPassFirstStepDataToActivity(FireHydrantObject fireObject);

        void requestPassSecondStepDataToActivity(FireHydrantObject fireObject);

        void requestEditRejectedFireObject(FireHydrantObject editedFireObject, String createdAt, long id);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext, String flagString);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents, int flag);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String date);
    }
}
