package com.seifoo.neomit.gisgathering.home.meter.getMap;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;

public class GetMeterMapActivity extends AppCompatActivity implements GetMeterMapMVP.View {
    private EditText serialNumberEditText, hcnEditText, meterAddressEditText, plateNumberEditText;
    private Button getInfoButton;
    private ProgressBar progressBar;

    private GetMeterMapPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_map);

        getSupportActionBar().setTitle(getString(R.string.get_map));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new GetMeterMapPresenter(this, new GetMeterMapModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        serialNumberEditText = (EditText) findViewById(R.id.serial_num);
        hcnEditText = (EditText) findViewById(R.id.hcn);
        meterAddressEditText = (EditText) findViewById(R.id.meter_address);
        plateNumberEditText = (EditText) findViewById(R.id.plate_num);

        getInfoButton = (Button) findViewById(R.id.get_map_button);
        getInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestGetMap(serialNumberEditText.getText().toString().trim(),
                        hcnEditText.getText().toString().trim(),
                        meterAddressEditText.getText().toString().trim(),
                        plateNumberEditText.getText().toString().trim());
            }
        });
    }

    public void scanQR(View view) {
        presenter.whichQRClicked(view.getId());
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator, int flag) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }

    @Override
    public void setHCN(String data) {
        hcnEditText.setText(data);
    }

    @Override
    public void setMeterAddress(String data) {
        meterAddressEditText.setText(data);
    }

    @Override
    public void setPlateNumber(String data) {
        plateNumberEditText.setText(data);
    }


    @Override
    public void loadMeterMap(String latitude, String longitude) {
        Intent intent = new Intent(this, GetMeterMapDetailsActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
