package com.seifoo.neomit.gisgathering.home.meter.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, EditMeterMVP.MainView, EditMeterMVP.SecondView {
    private Spinner pipeAfterMeterSpinner, pipeSizeSpinner, meterMaintenanceAreaSpinner, meterBoxPositionSpinner, meterBoxTypeSpinner,
            coverStatusSpinner, locationSpinner, buildingUsageSpinner, waterConnectionTypeSpinner, pipeMaterialSpinner;

    private EditMeterMVP.Presenter presenter;

    public static SecondFragment newInstance(MeterObject meterObj) {
        SecondFragment fragment = new SecondFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("meterObj", meterObj);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second_edit, container, false);
        presenter = new EditMeterPresenter(this, this, new EditMeterModel());

        pipeAfterMeterSpinner = (Spinner) view.findViewById(R.id.pipe_after_meter_spinner);
        pipeSizeSpinner = (Spinner) view.findViewById(R.id.pipe_size_spinner);
        meterMaintenanceAreaSpinner = (Spinner) view.findViewById(R.id.meter_maintenance_area_spinner);
        meterBoxPositionSpinner = (Spinner) view.findViewById(R.id.meter_box_position_spinner);
        meterBoxTypeSpinner = (Spinner) view.findViewById(R.id.meter_box_type_spinner);
        coverStatusSpinner = (Spinner) view.findViewById(R.id.cover_status_spinner);
        locationSpinner = (Spinner) view.findViewById(R.id.location_spinner);
        buildingUsageSpinner = (Spinner) view.findViewById(R.id.building_usage_spinner);

        waterConnectionTypeSpinner = (Spinner) view.findViewById(R.id.water_connection_type_spinner);
        pipeMaterialSpinner = (Spinner) view.findViewById(R.id.pipe_material_spinner);

        presenter.requestSecondStepData((MeterObject) getArguments().getSerializable("meterObj"));

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new MeterObject(pipeAfterMeterIds.get(pipeAfterMeterSpinner.getSelectedItemPosition()), pipeSizeIds.get(pipeSizeSpinner.getSelectedItemPosition()), meterMaintenanceAreaIds.get(meterMaintenanceAreaSpinner.getSelectedItemPosition()), meterBoxPositionIds.get(meterBoxPositionSpinner.getSelectedItemPosition()), meterBoxTypeIds.get(meterBoxTypeSpinner.getSelectedItemPosition()),
                coverStatusIds.get(coverStatusSpinner.getSelectedItemPosition()), locationIds.get(locationSpinner.getSelectedItemPosition()), buildingUsageIds.get(buildingUsageSpinner.getSelectedItemPosition()), waterConnectionTypeIds.get(waterConnectionTypeSpinner.getSelectedItemPosition()),
                pipeMaterialIds.get(pipeMaterialSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void showToastMessage(String message) {

    }

    @Override
    public void backToParent(MeterObject meterObject) {

    }

    private ArrayList<Integer> pipeAfterMeterIds, pipeSizeIds, meterMaintenanceAreaIds, meterBoxPositionIds,
            meterBoxTypeIds, coverStatusIds, locationIds, buildingUsageIds, waterConnectionTypeIds, pipeMaterialIds;

    @Override
    public void loadSpinnersData(ArrayList<String> pipeAfterMeter, ArrayList<Integer> pipeAfterMeterIds,int pipeAfterMeterIndex,
                                 ArrayList<String> pipeSize, ArrayList<Integer> pipeSizeIds,int pipeSizeIndex,
                                 ArrayList<String> meterMaintenanceArea, ArrayList<Integer> meterMaintenanceAreaIds,int meterMaintenanceAreaIndex,
                                 ArrayList<String> meterBoxPosition, ArrayList<Integer> meterBoxPositionIds,int meterBoxPositionIndex,
                                 ArrayList<String> meterBoxType, ArrayList<Integer> meterBoxTypeIds,int meterBoxTypeIndex,
                                 ArrayList<String> coverStatus, ArrayList<Integer> coverStatusIds,int coverStatusIndex,
                                 ArrayList<String> location, ArrayList<Integer> locationIds,int locationIndex,
                                 ArrayList<String> buildingUsage, ArrayList<Integer> buildingUsageIds,int buildingUsageIndex,
                                 ArrayList<String> waterConnectionType, ArrayList<Integer> waterConnectionTypeIds,int waterConnectionTypeIndex,
                                 ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds,int pipeMaterialIndex) {

        this.pipeAfterMeterIds = pipeAfterMeterIds;
        this.pipeSizeIds = pipeSizeIds;
        this.meterMaintenanceAreaIds = meterMaintenanceAreaIds;
        this.meterBoxPositionIds = meterBoxPositionIds;
        this.meterBoxTypeIds = meterBoxTypeIds;
        this.coverStatusIds = coverStatusIds;
        this.locationIds = locationIds;
        this.buildingUsageIds = buildingUsageIds;
        this.waterConnectionTypeIds = waterConnectionTypeIds;
        this.pipeMaterialIds = pipeMaterialIds;

        pipeAfterMeterSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, pipeAfterMeter));
        pipeAfterMeterSpinner.setSelection(pipeAfterMeterIndex);
        pipeSizeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, pipeSize));
        pipeSizeSpinner.setSelection(pipeSizeIndex);
        meterMaintenanceAreaSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterMaintenanceArea));
        meterMaintenanceAreaSpinner.setSelection(meterMaintenanceAreaIndex);
        meterBoxPositionSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterBoxPosition));
        meterBoxPositionSpinner.setSelection(meterBoxPositionIndex);
        meterBoxTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterBoxType));
        meterBoxTypeSpinner.setSelection(meterBoxTypeIndex);
        coverStatusSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, coverStatus));
        coverStatusSpinner.setSelection(coverStatusIndex);
        locationSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, location));
        locationSpinner.setSelection(locationIndex);
        buildingUsageSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, buildingUsage));
        buildingUsageSpinner.setSelection(buildingUsageIndex);
        waterConnectionTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, waterConnectionType));
        waterConnectionTypeSpinner.setSelection(waterConnectionTypeIndex);
        pipeMaterialSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, pipeMaterial));
        pipeMaterialSpinner.setSelection(pipeMaterialIndex);
    }
}
