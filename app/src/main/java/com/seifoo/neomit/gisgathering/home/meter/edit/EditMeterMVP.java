package com.seifoo.neomit.gisgathering.home.meter.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface EditMeterMVP {
    interface MainView {
        Context getAppContext();

        void showToastMessage(String message);

        void backToParent(MeterObject meterObject);
    }

    interface FirstView {

        void loadSpinnersData(ArrayList<String> meterDiameter, ArrayList<Integer> meterDiameterIds, int meterDiameterIndex,
                              ArrayList<String> meterBrand, ArrayList<Integer> meterBrandIds, int meterBrandIndex,
                              ArrayList<String> meterType, ArrayList<Integer> meterTypeIds, int meterTypeIndex,
                              ArrayList<String> meterStatus, ArrayList<Integer> meterStatusIds, int meterStatusIndex,
                              ArrayList<String> meterMaterial, ArrayList<Integer> meterMaterialIds, int meterMaterialIndex,
                              ArrayList<String> meterRemarks, ArrayList<Integer> meterRemarksIds, int meterRemarksIndex,
                              ArrayList<String> readType, ArrayList<Integer> readTypeIds, int readTypeIndex);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator, int flag);

        void setLocation(String location);

        void setSerialNumber(String data);

        void setPlateNumber(String data);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);
    }

    interface SecondView {

        void loadSpinnersData(ArrayList<String> pipeAfterMeter, ArrayList<Integer> pipeAfterMeterIds, int pipeAfterMeterIndex,
                              ArrayList<String> pipeSize, ArrayList<Integer> pipeSizeIds, int pipeSizeIndex,
                              ArrayList<String> meterMaintenanceArea, ArrayList<Integer> meterMaintenanceAreaIds, int meterMaintenanceAreaIndex,
                              ArrayList<String> meterBoxPosition, ArrayList<Integer> meterBoxPositionIds, int meterBoxPositionIndex,
                              ArrayList<String> meterBoxType, ArrayList<Integer> meterBoxTypeIds, int meterBoxTypeIndex,
                              ArrayList<String> coverStatus, ArrayList<Integer> coverStatusIds, int coverStatusIndex,
                              ArrayList<String> location, ArrayList<Integer> locationIds, int locationIndex,
                              ArrayList<String> buildingUsage, ArrayList<Integer> buildingUsageIds, int buildingUsageIndex,
                              ArrayList<String> waterConnectionType, ArrayList<Integer> waterConnectionTypeIds, int waterConnectionTypeIndex,
                              ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds, int pipeMaterialIndex);
    }

    interface ThirdView {

        void loadSpinnersData(ArrayList<String> valveType, ArrayList<Integer> valveTypeIds, int valveTypeIndex,
                              ArrayList<String> valveStatus, ArrayList<Integer> valveStatusIds, int valveStatusIndex,
                              ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex,
                              ArrayList<String> reducerDiameter, ArrayList<Integer> reducerDiameterIds, int reducerDiameterIndex);

        void setDate(String date);

        void setData(String postCode, String scecoNumber, String numberOfElectricMeters, String lastReading, String numberOfFloors);
    }

    interface FourthView {

        void loadSpinnersData(ArrayList<String> subBuildingType, ArrayList<Integer> subBuildingTypeIds,int subBuildingTypeIndex,
                              ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,int waterScheduleIndex,
                              ArrayList<String> newMeterBrand, ArrayList<Integer> newMeterBrandIds,int newMeterBrandIndex,
                              ArrayList<String> districtName, ArrayList<Integer> districtNameIds,int districtNameIndex);

        void setMeterAddress(String address);

        void setHCN(String hcn);

        void setMeterData(String streetName, String sectorName,int sewerConnection ,String groundElevation,
                          String elevation, String hcn, String meterAddress);

        void initializeScanner(IntentIntegrator integrator, int flag);
    }

    interface FifthView {

        void setMeterData(String dmaZoneString, String locationNumber, String buildingNumber, String buildingDuplication, String buildingNumberM, String buildingDescription,
                          String streetNumber, String streetNumberM, String subName, String arabicName, int customerActiveIndex);

    }

    interface Presenter {

        void requestFirstStepData(MeterObject meterObject);

        void requestSecondStepData(MeterObject meterObject);

        void requestThirdStepData(MeterObject meterObject);

        void requestFourthStepData(MeterObject meterObject);

        void requestFifthStepData(MeterObject meterObject);

        void requestPassFirstStepDataToActivity(MeterObject meterObject);

        void requestPassSecondStepDataToActivity(MeterObject meterObject);

        void requestPassThirdStepDataToActivity(MeterObject meterObject);

        void requestPassFourthStepDataToActivity(MeterObject meterObject);

        void requestPassFifthStepDataToActivity(MeterObject meterObject);

        void requestEditMeterObject(MeterObject editedMeterObject, MeterObject mainMeterObj);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext, String flagString);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void onFourthViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents, int flag);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String date);
    }
}
