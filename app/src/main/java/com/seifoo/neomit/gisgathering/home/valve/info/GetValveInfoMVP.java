package com.seifoo.neomit.gisgathering.home.valve.info;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

public interface GetValveInfoMVP {

    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void initializeScanner(IntentIntegrator integrator);

        void setSerialNumber(String data);

        void loadValveInfo(ValveObject valveObject);
    }

    interface Presenter {

        void requestQrCode(Activity activity);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestGetInfo(String serialNumber);
    }
}
