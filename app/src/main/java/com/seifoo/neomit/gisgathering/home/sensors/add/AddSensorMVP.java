package com.seifoo.neomit.gisgathering.home.sensors.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface AddSensorMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();

        String getCurrentDate();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> deviceType, ArrayList<Integer> deviceTypeIds,
                              ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds,
                              ArrayList<String> type, ArrayList<Integer> typeIds,
                              ArrayList<String> maintenanceArea, ArrayList<Integer> maintenanceAreaIds);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator,int flag);

        void setSerialNumber(String serialNumber);

        void setAssetNumber(String assetNumber);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> enabled, ArrayList<Integer> enabledIds,
                              ArrayList<String> districtName, ArrayList<Integer> districtNameIds,
                              ArrayList<String> diameter, ArrayList<Integer> diameterIds,
                              ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds);

        void setCommissionDate(String commissionDate);

        void setLastUpdate(String lastUpdate);
    }

    interface Presenter {
        void requestFirstSpinnersData();

        void requestSecondSpinnersData();

        void requestPassFirstStepDataToActivity(SensorObject sensor);

        void requestPassSecondStepDataToActivity(SensorObject sensor);

        void requestAddSensor(SensorObject sensor);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext,String flagString);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents,int flag);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString, int index);

    }
}
