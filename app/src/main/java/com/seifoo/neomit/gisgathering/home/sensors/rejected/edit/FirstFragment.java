package com.seifoo.neomit.gisgathering.home.sensors.rejected.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class FirstFragment extends Fragment implements Step, EditRejectedSensorsMVP.View, EditRejectedSensorsMVP.FirstView {
    private EditText latLngEditText, serialNumberEditText, assetNumberEditText, elevationEditText, chamberStatusEditText, lineNumberEditText;
    private Spinner deviceTypeSpinner, assetStatusSpinner, typeSpinner, maintenanceAreaSpinner;

    private EditRejectedSensorsMVP.Presenter presenter;

    public static FirstFragment newInstance(SensorObject sensor) {
        FirstFragment fragment = new FirstFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("SensorObj", sensor);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sensor_first_edit, container, false);

        presenter = new EditRejectedSensorsPresenter(this, this, new EditRejectedSensorsModel());


        deviceTypeSpinner = (Spinner) view.findViewById(R.id.device_type_spinner);
        assetStatusSpinner = (Spinner) view.findViewById(R.id.asset_status_spinner);
        typeSpinner = (Spinner) view.findViewById(R.id.type_spinner);
        maintenanceAreaSpinner = (Spinner) view.findViewById(R.id.maintenance_area_spinner);


        latLngEditText = (EditText) view.findViewById(R.id.location);
        serialNumberEditText = (EditText) view.findViewById(R.id.serial_num);
        assetNumberEditText = (EditText) view.findViewById(R.id.asset_num);
        elevationEditText = (EditText) view.findViewById(R.id.elevation);
        chamberStatusEditText = (EditText) view.findViewById(R.id.chamber_status);
        lineNumberEditText = (EditText) view.findViewById(R.id.line_number);

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(latLngEditText.getText().toString().trim());
            }
        });

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.serial_num_qr)));
        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.asset_num_qr)));


        presenter.requestFirstSpinnersData((SensorObject) getArguments().getSerializable("SensorObj"));

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFirstStepDataToActivity(new SensorObject(latLngEditText.getText().toString().trim(),
                latLngEditText.getText().toString().trim(), serialNumberEditText.getText().toString().trim(),
                assetNumberEditText.getText().toString().trim(), deviceTypeIds.get(deviceTypeSpinner.getSelectedItemPosition()),
                assetStatusIds.get(assetStatusSpinner.getSelectedItemPosition()), elevationEditText.getText().toString().trim(),
                chamberStatusEditText.getText().toString().trim(), typeIds.get(typeSpinner.getSelectedItemPosition()),
                lineNumberEditText.getText().toString().trim(), maintenanceAreaIds.get(maintenanceAreaSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> deviceTypeIds, assetStatusIds, typeIds, maintenanceAreaIds;

    @Override
    public void loadSpinnersData(ArrayList<String> deviceType, ArrayList<Integer> deviceTypeIds, int deviceTypeIndex, ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds, int assetStatusIndex, ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex, ArrayList<String> maintenanceArea, ArrayList<Integer> maintenanceAreaIds, int maintenanceAreaIndex) {
        this.deviceTypeIds = deviceTypeIds;
        this.assetStatusIds = assetStatusIds;
        this.typeIds = typeIds;
        this.maintenanceAreaIds = maintenanceAreaIds;

        deviceTypeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, deviceType));
        deviceTypeSpinner.setSelection(deviceTypeIndex);
        assetStatusSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, assetStatus));
        assetStatusSpinner.setSelection(assetStatusIndex);
        typeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, type));
        typeSpinner.setSelection(typeIndex);
        maintenanceAreaSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, maintenanceArea));
        maintenanceAreaSpinner.setSelection(maintenanceAreaIndex);
    }

    @Override
    public void showLocationError(String errorMessage) {
        latLngEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((EditRejectedSensorsActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator, int flag) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void setSerialNumber(String serialNumber) {
        serialNumberEditText.setText(serialNumber);
    }

    @Override
    public void setAssetNumber(String assetNumber) {
        assetNumberEditText.setText(assetNumber);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void setLocation(String location) {
        latLngEditText.setText(location);
    }


    @Override
    public void setData(String elevation, String chamberStatus, String lineNumber) {
        elevationEditText.setText(elevation);
        chamberStatusEditText.setText(chamberStatus);
        lineNumberEditText.setText(lineNumber);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFirstViewActivityResult(requestCode, resultCode, data);
    }
}
