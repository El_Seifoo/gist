package com.seifoo.neomit.gisgathering.home.fireHydrant.offline;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;
import com.seifoo.neomit.gisgathering.utils.Urls;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class OfflineFiresModel {
    protected void uploadFiresData(final Context context, final VolleyCallback callback, final ArrayList<FireHydrantObject> fires) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSyncingResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("parsing error ", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onSyncingError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""));
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                for (int i = 0; i < fires.size(); i++) {
                    if (fires.get(i).getFireHydrantId() != 0.0d)
                        params.put("Json[data][" + i + "][Corrected_Id]", fires.get(i).getFireHydrantId() + "");
                    params.put("Json[data][" + i + "][RECORD_ID]", fires.get(i).getId() + "");
                    params.put("Json[data][" + i + "][SERIAL_NO]", fires.get(i).getFireSerialNumber());
                    params.put("Json[data][" + i + "][FH_NUMBER]", fires.get(i).getFireNumber());
                    params.put("Json[data][" + i + "][HEIGHT]", fires.get(i).getFireHeight());
                    params.put("Json[data][" + i + "][BARREL_DIAMETER_ID]", fires.get(i).getFireBarrelDiameter() + "");
                    params.put("Json[data][" + i + "][MATERIAL_ID]", fires.get(i).getFireMaterialId() + "");
                    params.put("Json[data][" + i + "][F_TYPE_ID]", fires.get(i).getFireTypeId() + "");
                    params.put("Json[data][" + i + "][COMMISSION_DATE]", fires.get(i).getFireCommissionDate());
                    params.put("Json[data][" + i + "][DISTRICT_NAME_ID]", fires.get(i).getFireDistrictName() + "");
                    params.put("Json[data][" + i + "][STREET_NAME]", fires.get(i).getFireStreetName());
                    params.put("Json[data][" + i + "][SECTOR_NAME]", fires.get(i).getFireSectorName());
                    params.put("Json[data][" + i + "][FIREHYDRANT_STATUS_ID]", fires.get(i).getFireStatus() + "");
                    params.put("Json[data][" + i + "][REMARKS]", fires.get(i).getFireRemarks() + "");
                    params.put("Json[data][" + i + "][WITH_WATER]", fires.get(i).getFireWithWater());
                    params.put("Json[data][" + i + "][FIREHYDRANT_BRAND]", fires.get(i).getFireBrand() + "");
                    params.put("Json[data][" + i + "][FEATURE_TYPE_VALVE_ID]", fires.get(i).getFireValveType() + "");
                    params.put("Json[data][" + i + "][WATER_SCHEDULE_ID]", fires.get(i).getFireWaterSchedule() + "");
                    params.put("Json[data][" + i + "][MAINTENANCE_AREA_ID]", fires.get(i).getFireMaintenanceArea() + "");
                    params.put("Json[data][" + i + "][CreatedDate]", fires.get(i).getCreatedAt());
                    params.put("Json[data][" + i + "][X_MAP]", !fires.get(i).getFireLongitude().isEmpty() ? fires.get(i).getFireLongitude() : "360");
                    params.put("Json[data][" + i + "][Y_MAP]", !fires.get(i).getFireLatitude().isEmpty() ? fires.get(i).getFireLatitude() : "360");
                }
                params.put("Json[tableName]", context.getString(R.string.api_fire_hydrant_table_name));
                params.put("Json[databaseName]", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""));
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected void getFiresList(Context context, DBCallback callback, int type, int index) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetFiresListCalled(DataBaseHelper.getmInstance(context).getAllFireHydrants(type), index);
    }

    protected void removeFireObject(Context context, DBCallback callback, long id, int position) {
        callback.onRemoveFireObjectCalled(DataBaseHelper.getmInstance(context).deleteFireHydrateById(id), position, id);
    }

    protected interface VolleyCallback {
        void onSyncingResponse(String response) throws JSONException;

        void onSyncingError(VolleyError error);
    }

    protected interface DBCallback {

        void onGetFiresListCalled(ArrayList<FireHydrantObject> fires, int index);

        void onRemoveFireObjectCalled(int flag, int position, long id);
    }
}
