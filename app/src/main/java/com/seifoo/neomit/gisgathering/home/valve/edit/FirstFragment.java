package com.seifoo.neomit.gisgathering.home.valve.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.HashMap;

public class FirstFragment extends Fragment implements Step, EditValveMVP.View, EditValveMVP.FirstView {
    private Spinner valveJobSpinner, materialSpinner, diameterSpinner, typeSpinner, lockSpinner, statusSpinner;
    private EditText latLngEditText, serialNumberEditText, fullNumberOfTurnsEditText, numberOfTurnsEditText, elevationEditText, groundElevationEditText;

    private EditValveMVP.Presenter presenter;

    public static FirstFragment newInstance(ValveObject valveObject) {
        FirstFragment fragment = new FirstFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("ValveObj", valveObject);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_valve_first_edit, container, false);

        presenter = new EditValvePresenter(this, this, new EditValveModel());

        valveJobSpinner = (Spinner) view.findViewById(R.id.valve_job_spinner);
        materialSpinner = (Spinner) view.findViewById(R.id.material_spinner);
        diameterSpinner = (Spinner) view.findViewById(R.id.diameter_spinner);
        typeSpinner = (Spinner) view.findViewById(R.id.valve_type_spinner);
        lockSpinner = (Spinner) view.findViewById(R.id.lock_spinner);
        statusSpinner = (Spinner) view.findViewById(R.id.valve_status_spinner);


        latLngEditText = (EditText) view.findViewById(R.id.location);
        serialNumberEditText = (EditText) view.findViewById(R.id.serial_num);
        fullNumberOfTurnsEditText = (EditText) view.findViewById(R.id.full_number_of_turns);
        numberOfTurnsEditText = (EditText) view.findViewById(R.id.number_of_turns);
        elevationEditText = (EditText) view.findViewById(R.id.elevation);
        groundElevationEditText = (EditText) view.findViewById(R.id.ground_elevation);

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(latLngEditText.getText().toString().trim());
            }
        });

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.serial_num_qr)));

        presenter.requestFirstSpinnersData((ValveObject) getArguments().getSerializable("ValveObj"));

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFirstStepDataToActivity(new ValveObject(latLngEditText.getText().toString().trim(), latLngEditText.getText().toString().trim(),
                valveJobIds.get(valveJobSpinner.getSelectedItemPosition()), serialNumberEditText.getText().toString().trim(),
                materialIds.get(materialSpinner.getSelectedItemPosition()), diameterIds.get(diameterSpinner.getSelectedItemPosition()),
                typeIds.get(typeSpinner.getSelectedItemPosition()), fullNumberOfTurnsEditText.getText().toString().trim(),
                numberOfTurnsEditText.getText().toString().trim(), lockIds.get(lockSpinner.getSelectedItemPosition()),
                statusIds.get(statusSpinner.getSelectedItemPosition()), elevationEditText.getText().toString().trim(), groundElevationEditText.getText().toString().trim()));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    ArrayList<Integer> valveJobIds, materialIds, diameterIds, typeIds, lockIds, statusIds;

    @Override
    public void loadSpinnersData(ArrayList<String> valveJob, ArrayList<Integer> valveJobIds, int valveJobIndex,
                                 ArrayList<String> material, ArrayList<Integer> materialIds, int materialIndex,
                                 ArrayList<String> diameter, ArrayList<Integer> diameterIds, int diameterIndex,
                                 ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex,
                                 ArrayList<String> lock, ArrayList<Integer> lockIds, int lockIndex,
                                 ArrayList<String> status, ArrayList<Integer> statusIds, int statusIndex) {
        this.valveJobIds = valveJobIds;
        this.materialIds = materialIds;
        this.diameterIds = diameterIds;
        this.typeIds = typeIds;
        this.lockIds = lockIds;
        this.statusIds = statusIds;


        valveJobSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, valveJob));
        valveJobSpinner.setSelection(valveJobIndex);
        materialSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, material));
        materialSpinner.setSelection(materialIndex);
        diameterSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameter));
        diameterSpinner.setSelection(diameterIndex);
        typeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, type));
        typeSpinner.setSelection(typeIndex);
        lockSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, lock));
        lockSpinner.setSelection(lockIndex);
        statusSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, status));
        statusSpinner.setSelection(statusIndex);

    }

    @Override
    public void showLocationError(String errorMessage) {
        latLngEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((EditValveActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, 1);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("latLng", latLngs);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void setLocation(String location) {
        latLngEditText.setText(location);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFirstViewActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setData(String fullNumberOfTurns, String numberOfTurns, String elevation, String groundElevation) {
        fullNumberOfTurnsEditText.setText(fullNumberOfTurns);
        numberOfTurnsEditText.setText(numberOfTurns);
        elevationEditText.setText(elevation);
        groundElevationEditText.setText(groundElevation);
    }

}
