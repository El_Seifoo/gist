package com.seifoo.neomit.gisgathering.home.tank.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, EditTankMVP.View, EditTankMVP.SecondView {
    private EditText inActiveVolumeEditText, lowLevelEditText, highLevelEditText, streetNameEditText, regionEditText, lastUpdatedEditText, sectorNameEditText, remarksEditText;
    private Spinner assetStatusSpinner, districtNameSpinner;

    private EditTankMVP.Presenter presenter;

    public static SecondFragment newInstance(TankObject tank) {
        SecondFragment fragment = new SecondFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("TankObj", tank);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tank_second_edit, container, false);

        presenter = new EditTankPresenter(this, this, new EditTankModel());

        assetStatusSpinner = (Spinner) view.findViewById(R.id.asset_status_spinner);
        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);

        inActiveVolumeEditText = (EditText) view.findViewById(R.id.in_active_volume);
        lowLevelEditText = (EditText) view.findViewById(R.id.low_level);
        highLevelEditText = (EditText) view.findViewById(R.id.high_level);
        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);
        regionEditText = (EditText) view.findViewById(R.id.region);
        remarksEditText = (EditText) view.findViewById(R.id.remarks);
        lastUpdatedEditText = (EditText) view.findViewById(R.id.last_updated);

        lastUpdatedEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(lastUpdatedEditText.getText().toString().trim(), 2);
            }
        });

        presenter.requestSecondSpinnersData((TankObject) getArguments().getSerializable("TankObj"));
        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new TankObject(
                inActiveVolumeEditText.getText().toString().trim(), lowLevelEditText.getText().toString().trim(),
                highLevelEditText.getText().toString().trim(), assetStatusIds.get(assetStatusSpinner.getSelectedItemPosition()),
                streetNameEditText.getText().toString().trim(), districtNameIds.get(districtNameSpinner.getSelectedItemPosition()),
                sectorNameEditText.getText().toString().trim(),
                regionEditText.getText().toString().trim(), remarksEditText.getText().toString().trim(),
                lastUpdatedEditText.getText().toString().trim()));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> assetStatusIds, districtNameIds;

    @Override
    public void loadSpinnersData(ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds, int assetStatusIndex,
                                 ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex) {
        this.assetStatusIds = assetStatusIds;
        this.districtNameIds = districtNameIds;

        assetStatusSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, assetStatus));
        assetStatusSpinner.setSelection(assetStatusIndex);
        districtNameSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, districtName));
        districtNameSpinner.setSelection(districtNameIndex);
    }

    @Override
    public void setLastUpdated(String lastUpdated) {
        lastUpdatedEditText.setText(lastUpdated);
    }

    @Override
    public void setData(String inActiveVolume, String lowLevel, String highLevel, String streetName, String region, String sectorName, String remarks) {
        inActiveVolumeEditText.setText(inActiveVolume);
        lowLevelEditText.setText(lowLevel);
        highLevelEditText.setText(highLevel);
        streetNameEditText.setText(streetName);
        regionEditText.setText(region);
        sectorNameEditText.setText(sectorName);
        remarksEditText.setText(remarks);
    }
}
