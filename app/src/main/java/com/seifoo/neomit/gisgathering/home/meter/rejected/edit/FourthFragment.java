package com.seifoo.neomit.gisgathering.home.meter.rejected.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class FourthFragment extends Fragment implements Step, EditRejectedMeterMVP.MainView, EditRejectedMeterMVP.FourthView {
    private EditText streetNameEditText, sectorNameEditText, groundElevationEditText, elevationEditText, hcnEditText, meterAddressEditText;
    private Spinner subBuildingTypeSpinner, waterScheduleSpinner, newMeterBrandSpinner, districtNameSpinner;
    private RadioGroup sewerConnection;

    private EditRejectedMeterMVP.Presenter presenter;

    public static FourthFragment newInstance(MeterObject meterObject) {
        FourthFragment fragment = new FourthFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("meterObj", meterObject);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fourth_edit, container, false);

        presenter = new EditRejectedMeterPresenter(this, this, new EditRejectedMeterModel());

        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);
        groundElevationEditText = (EditText) view.findViewById(R.id.ground_elevation);
        elevationEditText = (EditText) view.findViewById(R.id.elevation);
        hcnEditText = (EditText) view.findViewById(R.id.hcn);
        meterAddressEditText = (EditText) view.findViewById(R.id.meter_address);


        sewerConnection = (RadioGroup) view.findViewById(R.id.sewer_connection_radio_group);
        ((RadioButton) sewerConnection.getChildAt(0)).setChecked(true);

        subBuildingTypeSpinner = (Spinner) view.findViewById(R.id.sub_building_type_spinner);
        waterScheduleSpinner = (Spinner) view.findViewById(R.id.water_schedule_spinner);
        newMeterBrandSpinner = (Spinner) view.findViewById(R.id.new_meter_brand_spinner);
        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.hcn_qr)));
        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.meter_address_qr)));

        presenter.requestFourthStepData((MeterObject) getArguments().getSerializable("meterObj"));
        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFourthStepDataToActivity(new MeterObject(
                streetNameEditText.getText().toString().trim(),
                sectorNameEditText.getText().toString().trim(),
                getCheckedButton(sewerConnection).equals(getContext().getString(R.string.exist))?"1":"0",
                groundElevationEditText.getText().toString().trim(),
                elevationEditText.getText().toString().trim(),
                hcnEditText.getText().toString().trim(),
                meterAddressEditText.getText().toString().trim(),
                subBuildingTypeIds.get(subBuildingTypeSpinner.getSelectedItemPosition()),
                waterScheduleIds.get(waterScheduleSpinner.getSelectedItemPosition()),
                newMeterBrandIds.get(newMeterBrandSpinner.getSelectedItemPosition()),
                districtNameIds.get(districtNameSpinner.getSelectedItemPosition())));
        return null;
    }

    private String getCheckedButton(RadioGroup radioGroup) {
        int checkedButtonId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) getView().findViewById(checkedButtonId);
        return radioButton.getText().toString().trim();
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    ArrayList<Integer> subBuildingTypeIds, waterScheduleIds, newMeterBrandIds, districtNameIds;

    @Override
    public void loadSpinnersData(ArrayList<String> subBuildingType, ArrayList<Integer> subBuildingTypeIds, int subBuildingTypeIndex,
                                 ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds, int waterScheduleIndex,
                                 ArrayList<String> newMeterBrand, ArrayList<Integer> newMeterBrandIds, int newMeterBrandIndex,
                                 ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex) {


        this.subBuildingTypeIds = subBuildingTypeIds;
        this.waterScheduleIds = waterScheduleIds;
        this.newMeterBrandIds = newMeterBrandIds;
        this.districtNameIds = districtNameIds;

        subBuildingTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, subBuildingType));
        subBuildingTypeSpinner.setSelection(subBuildingTypeIndex);
        waterScheduleSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, waterSchedule));
        waterScheduleSpinner.setSelection(waterScheduleIndex);
        newMeterBrandSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, newMeterBrand));
        newMeterBrandSpinner.setSelection(newMeterBrandIndex);
        districtNameSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, districtName));
        districtNameSpinner.setSelection(districtNameIndex);

    }

    @Override
    public void setMeterAddress(String address) {
        meterAddressEditText.setText(address);
    }

    @Override
    public void setHCN(String hcn) {
        hcnEditText.setText(hcn);
    }

    @Override
    public void setMeterData(String streetName, String sectorName, int sewerConnectionIndex, String groundElevation,
                             String elevation, String hcn, String meterAddress) {
        streetNameEditText.setText(streetName);
        sectorNameEditText.setText(sectorName);
        ((RadioButton) sewerConnection.getChildAt(sewerConnectionIndex)).setChecked(true);
        groundElevationEditText.setText(groundElevation);
        elevationEditText.setText(elevation);
        hcnEditText.setText(hcn);
        meterAddressEditText.setText(meterAddress);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator, int flag) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFourthViewActivityResult(requestCode, resultCode, data);
    }

}
