package com.seifoo.neomit.gisgathering.home.sensors.rejected;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;

import java.util.ArrayList;

public interface RejectedSensorsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showEmptyListText();

        void loadRejectedSensors(ArrayList<SensorObject> sensors);
    }

    interface Presenter {
        void requestSensors();
    }
}
