package com.seifoo.neomit.gisgathering.home.meter;

import java.io.Serializable;

public class MeterObject implements Serializable {

    public MeterObject(String meterSerialNumber) {
        this.meterSerialNumber = meterSerialNumber;
    }

    private long id, meterId;
    private String meterSerialNumber, meterHcn, meterMeterAddress, meterPlateNumber, meterDmaZone, meterLastUpdated, meterLocationNumber,
            meterPostCode, meterScecoNumber, meterNumberOfElectricMeters, meterLastReading, meterBuildingNumber, meterNumberOfFloors,
            meterBuildingDuplication, meterBuildingNumberM, meterBuildingDescription, meterStreetNumber, meterStreetName, meterStreetNumberM,
            meterSectorName, meterSubName, meterIsSewerConnectionExist, meterArabicName, meterCustomerActive, meterLatitude, meterLongitude, reason;

    private int meterMeterDiameterId, meterMeterBrandId, meterMeterTypeId, meterMeterStatusId, meterMeterMaterialId, meterMeterRemarksId,
            meterReadTypeId, meterPipeAfterMeterId, meterPipeSizeId, meterMaintenanceAreaId, meterMeterBoxPositionId, meterMeterBoxTypeId,
            meterCoverStatusId, meterLocationId, meterBuildingUsageId, meterSubBuildingTypeId, meterWaterScheduleId, meterDistrictName,
            meterWaterConnectionTypeId, meterPipeMaterialId, meterBrandNewMeterId, meterValveTypeId, meterValveStatusId,
            meterEnabledId, meterReducerDiameterId;

    private String groundElevation, elevation, createdAt;

    //first step const..
    public MeterObject(String latlng, String meterSerialNumber, String meterPlateNumber, int meterMeterDiameterId, int meterMeterBrandId,
                       int meterMeterTypeId, int meterMeterStatusId, int meterMeterMaterialId, int meterMeterRemarksId, int meterReadTypeId) {
        if (!latlng.trim().equals(",") && latlng.contains(",")) {
            this.meterLatitude = latlng.split(",")[0];
            this.meterLongitude = latlng.split(",")[1];
        }
        this.meterSerialNumber = meterSerialNumber;
        this.meterPlateNumber = meterPlateNumber;
        this.meterMeterDiameterId = meterMeterDiameterId;
        this.meterMeterBrandId = meterMeterBrandId;
        this.meterMeterTypeId = meterMeterTypeId;
        this.meterMeterStatusId = meterMeterStatusId;
        this.meterMeterMaterialId = meterMeterMaterialId;
        this.meterMeterRemarksId = meterMeterRemarksId;
        this.meterReadTypeId = meterReadTypeId;

    }

    //second step const..
    public MeterObject(int meterPipeAfterMeterId, int meterPipeSizeId, int meterMaintenanceAreaId, int meterMeterBoxPositionId, int meterMeterBoxTypeId,
                       int meterCoverStatusId, int meterLocationId, int meterBuildingUsageId, int meterWaterConnectionTypeId, int meterPipeMaterialId) {
        this.meterPipeAfterMeterId = meterPipeAfterMeterId;
        this.meterPipeSizeId = meterPipeSizeId;
        this.meterMaintenanceAreaId = meterMaintenanceAreaId;
        this.meterMeterBoxPositionId = meterMeterBoxPositionId;
        this.meterMeterBoxTypeId = meterMeterBoxTypeId;
        this.meterCoverStatusId = meterCoverStatusId;
        this.meterLocationId = meterLocationId;
        this.meterBuildingUsageId = meterBuildingUsageId;
        this.meterWaterConnectionTypeId = meterWaterConnectionTypeId;
        this.meterPipeMaterialId = meterPipeMaterialId;
    }

    //third step const..
    public MeterObject(int meterValveTypeId, int meterValveStatusId, int meterEnabledId, int meterReducerDiameterId,
                       String meterLastUpdated, String meterPostCode, String meterScecoNumber,
                       String meterNumberOfElectricMeters, String meterLastReading, String meterNumberOfFloors) {
        this.meterValveTypeId = meterValveTypeId;
        this.meterValveStatusId = meterValveStatusId;
        this.meterEnabledId = meterEnabledId;
        this.meterReducerDiameterId = meterReducerDiameterId;
        this.meterLastUpdated = meterLastUpdated;
        this.meterPostCode = meterPostCode;
        this.meterScecoNumber = meterScecoNumber;
        this.meterNumberOfElectricMeters = meterNumberOfElectricMeters;
        this.meterLastReading = meterLastReading;
        this.meterNumberOfFloors = meterNumberOfFloors;
    }


    //fourth step const..
    public MeterObject(String meterStreetName, String meterSectorName, String meterIsSewerConnectionExist, String groundElevation, String elevation,
                       String meterHcn, String meterMeterAddress, int meterSubBuildingTypeId, int meterWaterScheduleId, int meterBrandNewMeterId, int meterDistrictName) {
        this.meterStreetName = meterStreetName;
        this.meterSectorName = meterSectorName;
        this.meterIsSewerConnectionExist = meterIsSewerConnectionExist;
        this.groundElevation = groundElevation;
        this.elevation = elevation;
        this.meterHcn = meterHcn;
        this.meterMeterAddress = meterMeterAddress;
        this.meterSubBuildingTypeId = meterSubBuildingTypeId;
        this.meterWaterScheduleId = meterWaterScheduleId;
        this.meterBrandNewMeterId = meterBrandNewMeterId;
        this.meterDistrictName = meterDistrictName;

    }

    //fifth step const..
    public MeterObject(String meterDmaZone, String meterLocationNumber, String meterBuildingNumber, String meterBuildingDuplication,
                       String meterBuildingNumberM, String meterBuildingDescription, String meterStreetNumber, String meterStreetNumberM,
                       String meterSubName, String meterArabicName, String meterCustomerActive) {
        this.meterDmaZone = meterDmaZone;
        this.meterLocationNumber = meterLocationNumber;
        this.meterBuildingNumber = meterBuildingNumber;
        this.meterBuildingDuplication = meterBuildingDuplication;
        this.meterBuildingNumberM = meterBuildingNumberM;
        this.meterBuildingDescription = meterBuildingDescription;
        this.meterStreetNumber = meterStreetNumber;
        this.meterStreetNumberM = meterStreetNumberM;
        this.meterSubName = meterSubName;
        this.meterArabicName = meterArabicName;
        this.meterCustomerActive = meterCustomerActive;
    }

    // return first step obj
    public MeterObject getFirstStepObj() {
        return new MeterObject(meterLatitude + "," + meterLongitude, meterSerialNumber, meterPlateNumber, meterMeterDiameterId,
                meterMeterBrandId, meterMeterTypeId, meterMeterStatusId, meterMeterMaterialId, meterMeterRemarksId, meterReadTypeId);
    }

    // return second step obj
    public MeterObject getSecondStepObj() {
        return new MeterObject(meterPipeAfterMeterId, meterPipeSizeId, meterMaintenanceAreaId, meterMeterBoxPositionId, meterMeterBoxTypeId,
                meterCoverStatusId, meterLocationId, meterBuildingUsageId, meterWaterConnectionTypeId, meterPipeMaterialId);
    }

    // return third step obj
    public MeterObject getThirdStepObj() {
        return new MeterObject(meterValveTypeId, meterValveStatusId, meterEnabledId, meterReducerDiameterId,
                meterLastUpdated, meterPostCode, meterScecoNumber,
                meterNumberOfElectricMeters, meterLastReading, meterNumberOfFloors);
    }

    // return fourth step obj
    public MeterObject getFourthStepObj() {
        return new MeterObject(meterStreetName, meterSectorName, meterIsSewerConnectionExist, groundElevation, elevation,
                meterHcn, meterMeterAddress, meterSubBuildingTypeId, meterWaterScheduleId, meterBrandNewMeterId, meterDistrictName);
    }

    // return fifth step obj
    public MeterObject getFifthStepObj() {
        return new MeterObject(meterDmaZone, meterLocationNumber, meterBuildingNumber, meterBuildingDuplication, meterBuildingNumberM,
                meterBuildingDescription, meterStreetNumber, meterStreetNumberM, meterSubName, meterArabicName, meterCustomerActive);
    }

    //db insertion const..
    public MeterObject(MeterObject firstStepMeterObj, MeterObject secondStepMeterObj, MeterObject thirdStepMeterObj, MeterObject fourthStepMeterObj, MeterObject fifthStepMeterObj) {

        /*
        meterLatitude + "," + meterLongitude, meterSerialNumber, meterPlateNumber, meterMeterDiameterId,
                meterMeterBrandId, meterMeterTypeId, meterMeterStatusId, meterMeterMaterialId, meterMeterRemarksId, meterReadTypeId
         */
        this.meterLatitude = firstStepMeterObj.getMeterLatitude();
        this.meterLongitude = firstStepMeterObj.getMeterLongitude();
        this.meterSerialNumber = firstStepMeterObj.getMeterSerialNumber();
        this.meterPlateNumber = firstStepMeterObj.getMeterPlateNumber();
        this.meterMeterDiameterId = firstStepMeterObj.getMeterMeterDiameterId();
        this.meterMeterBrandId = firstStepMeterObj.getMeterMeterBrandId();
        this.meterMeterTypeId = firstStepMeterObj.getMeterMeterTypeId();
        this.meterMeterStatusId = firstStepMeterObj.getMeterMeterStatusId();
        this.meterMeterMaterialId = firstStepMeterObj.getMeterMeterMaterialId();
        this.meterMeterRemarksId = firstStepMeterObj.getMeterMeterRemarksId();
        this.meterReadTypeId = firstStepMeterObj.getMeterReadTypeId();


        /*
        meterPipeAfterMeterId, meterPipeSizeId, meterMaintenanceAreaId, meterMeterBoxPositionId, meterMeterBoxTypeId,
                meterCoverStatusId, meterLocationId, meterBuildingUsageId, meterWaterConnectionTypeId, meterPipeMaterialId
         */
        this.meterPipeAfterMeterId = secondStepMeterObj.getMeterPipeAfterMeterId();
        this.meterPipeSizeId = secondStepMeterObj.getMeterPipeSizeId();
        this.meterMaintenanceAreaId = secondStepMeterObj.getMeterMaintenanceAreaId();
        this.meterMeterBoxPositionId = secondStepMeterObj.getMeterMeterBoxPositionId();
        this.meterMeterBoxTypeId = secondStepMeterObj.getMeterMeterBoxTypeId();
        this.meterCoverStatusId = secondStepMeterObj.getMeterCoverStatusId();
        this.meterLocationId = secondStepMeterObj.getMeterLocationId();
        this.meterBuildingUsageId = secondStepMeterObj.getMeterBuildingUsageId();
        this.meterWaterConnectionTypeId = secondStepMeterObj.getMeterWaterConnectionTypeId();
        this.meterPipeMaterialId = secondStepMeterObj.getMeterPipeMaterialId();

        /*
         meterValveTypeId, meterValveStatusId, meterEnabledId, meterReducerDiameterId,
                meterLastUpdated, meterPostCode, meterScecoNumber,
                meterNumberOfElectricMeters, meterLastReading, meterNumberOfFloors
         */
        this.meterValveTypeId = thirdStepMeterObj.getMeterValveTypeId();
        this.meterValveStatusId = thirdStepMeterObj.getMeterValveStatusId();
        this.meterEnabledId = thirdStepMeterObj.getMeterEnabledId();
        this.meterReducerDiameterId = thirdStepMeterObj.getMeterReducerDiameterId();
        this.meterLastUpdated = thirdStepMeterObj.getMeterLastUpdated();
        this.meterPostCode = thirdStepMeterObj.getMeterPostCode();
        this.meterScecoNumber = thirdStepMeterObj.getMeterScecoNumber();
        this.meterNumberOfElectricMeters = thirdStepMeterObj.getMeterNumberOfElectricMeters();
        this.meterLastReading = thirdStepMeterObj.getMeterLastReading();
        this.meterNumberOfFloors = thirdStepMeterObj.getMeterNumberOfFloors();

        /*
        meterStreetName, meterSectorName, meterIsSewerConnectionExist, groundElevation, elevation,
                meterHcn, meterMeterAddress, meterSubBuildingTypeId, meterWaterScheduleId, meterBrandNewMeterId, meterDistrictName
         */
        this.meterStreetName = fourthStepMeterObj.getMeterStreetName();
        this.meterSectorName = fourthStepMeterObj.getMeterSectorName();
        this.meterIsSewerConnectionExist = fourthStepMeterObj.getMeterIsSewerConnectionExist();
        this.groundElevation = fourthStepMeterObj.getGroundElevation();
        this.elevation = fourthStepMeterObj.getElevation();
        this.meterHcn = fourthStepMeterObj.getMeterHcn();
        this.meterMeterAddress = fourthStepMeterObj.getMeterMeterAddress();
        this.meterSubBuildingTypeId = fourthStepMeterObj.getMeterSubBuildingTypeId();
        this.meterWaterScheduleId = fourthStepMeterObj.getMeterWaterScheduleId();
        this.meterBrandNewMeterId = fourthStepMeterObj.getMeterBrandNewMeterId();
        this.meterDistrictName = fourthStepMeterObj.getMeterDistrictName();

        /*
        meterDmaZone, meterLocationNumber, meterBuildingNumber, meterBuildingDuplication, meterBuildingNumberM,
                meterBuildingDescription, meterStreetNumber, meterStreetNumberM, meterSubName, meterArabicName, meterCustomerActive
         */
        this.meterDmaZone = fifthStepMeterObj.getMeterDmaZone();
        this.meterLocationNumber = fifthStepMeterObj.getMeterLocationNumber();
        this.meterBuildingNumber = fifthStepMeterObj.getMeterBuildingNumber();
        this.meterBuildingDuplication = fifthStepMeterObj.getMeterBuildingDuplication();
        this.meterBuildingNumberM = fifthStepMeterObj.getMeterBuildingNumberM();
        this.meterBuildingDescription = fifthStepMeterObj.getMeterBuildingDescription();
        this.meterStreetNumber = fifthStepMeterObj.getMeterStreetNumber();
        this.meterStreetNumberM = fifthStepMeterObj.getMeterStreetNumberM();
        this.meterSubName = fifthStepMeterObj.getMeterSubName();
        this.meterArabicName = fifthStepMeterObj.getMeterArabicName();
        this.meterCustomerActive = fifthStepMeterObj.getMeterCustomerActive();

    }

    public MeterObject(long id, long meterId, String meterSerialNumber, String meterHcn, String meterMeterAddress, String meterPlateNumber,
                       int meterMeterDiameterId, int meterMeterBrandId, int meterMeterTypeId, int meterMeterStatusId, int meterMeterMaterialId, int meterMeterRemarksId,
                       int meterReadTypeId, int meterPipeAfterMeterId, int meterPipeSizeId, int meterMaintenanceAreaId, int meterMeterBoxPositionId, int meterMeterBoxTypeId,
                       int meterCoverStatusId, int meterLocationId, int meterBuildingUsageId, int meterSubBuildingTypeId, int meterWaterScheduleId,
                       int meterWaterConnectionTypeId, int meterPipeMaterialId, int meterBrandNewMeterId, int meterValveTypeId, int meterValveStatusId,
                       int meterEnabledId, int meterReducerDiameterId, int meterDistrictName, String meterDmaZone, String meterLastUpdated, String meterLocationNumber,
                       String meterPostCode, String meterScecoNumber, String meterNumberOfElectricMeters, String meterLastReading, String meterBuildingNumber, String meterNumberOfFloors,
                       String meterBuildingDuplication, String meterBuildingNumberM, String meterBuildingDescription, String meterStreetNumber, String meterStreetName, String meterStreetNumberM,
                       String meterSectorName, String meterSubName, String meterIsSewerConnectionExist, String meterArabicName, String meterCustomerActive, String meterLatitude, String meterLongitude, String groundElevation, String elevation, String createdAt) {
        this.id = id;
        this.meterId = meterId;
        this.meterSerialNumber = meterSerialNumber;
        this.meterHcn = meterHcn;
        this.meterMeterAddress = meterMeterAddress;
        this.meterPlateNumber = meterPlateNumber;
        this.meterMeterDiameterId = meterMeterDiameterId;
        this.meterMeterBrandId = meterMeterBrandId;
        this.meterMeterTypeId = meterMeterTypeId;
        this.meterMeterStatusId = meterMeterStatusId;
        this.meterMeterMaterialId = meterMeterMaterialId;
        this.meterMeterRemarksId = meterMeterRemarksId;
        this.meterReadTypeId = meterReadTypeId;
        this.meterPipeAfterMeterId = meterPipeAfterMeterId;
        this.meterPipeSizeId = meterPipeSizeId;
        this.meterMaintenanceAreaId = meterMaintenanceAreaId;
        this.meterMeterBoxPositionId = meterMeterBoxPositionId;
        this.meterMeterBoxTypeId = meterMeterBoxTypeId;
        this.meterCoverStatusId = meterCoverStatusId;
        this.meterLocationId = meterLocationId;
        this.meterBuildingUsageId = meterBuildingUsageId;
        this.meterSubBuildingTypeId = meterSubBuildingTypeId;
        this.meterWaterScheduleId = meterWaterScheduleId;
        this.meterWaterConnectionTypeId = meterWaterConnectionTypeId;
        this.meterPipeMaterialId = meterPipeMaterialId;
        this.meterBrandNewMeterId = meterBrandNewMeterId;
        this.meterValveTypeId = meterValveTypeId;
        this.meterValveStatusId = meterValveStatusId;
        this.meterEnabledId = meterEnabledId;
        this.meterReducerDiameterId = meterReducerDiameterId;
        this.meterDistrictName = meterDistrictName;
        this.meterDmaZone = meterDmaZone;
        this.meterLastUpdated = meterLastUpdated;
        this.meterLocationNumber = meterLocationNumber;
        this.meterPostCode = meterPostCode;
        this.meterScecoNumber = meterScecoNumber;
        this.meterNumberOfElectricMeters = meterNumberOfElectricMeters;
        this.meterLastReading = meterLastReading;
        this.meterBuildingNumber = meterBuildingNumber;
        this.meterNumberOfFloors = meterNumberOfFloors;
        this.meterBuildingDuplication = meterBuildingDuplication;
        this.meterBuildingNumberM = meterBuildingNumberM;
        this.meterBuildingDescription = meterBuildingDescription;
        this.meterStreetNumber = meterStreetNumber;
        this.meterStreetName = meterStreetName;
        this.meterStreetNumberM = meterStreetNumberM;
        this.meterSectorName = meterSectorName;
        this.meterSubName = meterSubName;
        this.meterIsSewerConnectionExist = meterIsSewerConnectionExist;
        this.meterArabicName = meterArabicName;
        this.meterCustomerActive = meterCustomerActive;
        this.meterLatitude = meterLatitude;
        this.meterLongitude = meterLongitude;
        this.groundElevation = groundElevation;
        this.elevation = elevation;
        this.createdAt = createdAt;
    }

    public MeterObject(String meterSerialNumber, String meterHcn, String meterMeterAddress, String meterPlateNumber,
                       int meterMeterDiameterId, int meterMeterBrandId, int meterMeterTypeId, int meterMeterStatusId, int meterMeterMaterialId, int meterMeterRemarksId,
                       int meterReadTypeId, int meterPipeAfterMeterId, int meterPipeSizeId, int meterMaintenanceAreaId, int meterMeterBoxPositionId, int meterMeterBoxTypeId,
                       int meterCoverStatusId, int meterLocationId, int meterBuildingUsageId, int meterSubBuildingTypeId, int meterWaterScheduleId,
                       int meterWaterConnectionTypeId, int meterPipeMaterialId, int meterBrandNewMeterId, int meterValveTypeId, int meterValveStatusId,
                       int meterEnabledId, int meterReducerDiameterId, int meterDistrictName, String meterDmaZone, String meterLastUpdated, String meterLocationNumber,
                       String meterPostCode, String meterScecoNumber, String meterNumberOfElectricMeters, String meterLastReading, String meterBuildingNumber, String meterNumberOfFloors,
                       String meterBuildingDuplication, String meterBuildingNumberM, String meterBuildingDescription, String meterStreetNumber, String meterStreetName, String meterStreetNumberM,
                       String meterSectorName, String meterSubName, String meterIsSewerConnectionExist, String meterArabicName, String meterCustomerActive, String meterLatitude, String meterLongitude, String groundElevation, String elevation, String createdAt) {
        this.meterSerialNumber = meterSerialNumber;
        this.meterHcn = meterHcn;
        this.meterMeterAddress = meterMeterAddress;
        this.meterPlateNumber = meterPlateNumber;
        this.meterMeterDiameterId = meterMeterDiameterId;
        this.meterMeterBrandId = meterMeterBrandId;
        this.meterMeterTypeId = meterMeterTypeId;
        this.meterMeterStatusId = meterMeterStatusId;
        this.meterMeterMaterialId = meterMeterMaterialId;
        this.meterMeterRemarksId = meterMeterRemarksId;
        this.meterReadTypeId = meterReadTypeId;
        this.meterPipeAfterMeterId = meterPipeAfterMeterId;
        this.meterPipeSizeId = meterPipeSizeId;
        this.meterMaintenanceAreaId = meterMaintenanceAreaId;
        this.meterMeterBoxPositionId = meterMeterBoxPositionId;
        this.meterMeterBoxTypeId = meterMeterBoxTypeId;
        this.meterCoverStatusId = meterCoverStatusId;
        this.meterLocationId = meterLocationId;
        this.meterBuildingUsageId = meterBuildingUsageId;
        this.meterSubBuildingTypeId = meterSubBuildingTypeId;
        this.meterWaterScheduleId = meterWaterScheduleId;
        this.meterWaterConnectionTypeId = meterWaterConnectionTypeId;
        this.meterPipeMaterialId = meterPipeMaterialId;
        this.meterBrandNewMeterId = meterBrandNewMeterId;
        this.meterValveTypeId = meterValveTypeId;
        this.meterValveStatusId = meterValveStatusId;
        this.meterEnabledId = meterEnabledId;
        this.meterReducerDiameterId = meterReducerDiameterId;
        this.meterDistrictName = meterDistrictName;
        this.meterDmaZone = meterDmaZone;
        this.meterLastUpdated = meterLastUpdated;
        this.meterLocationNumber = meterLocationNumber;
        this.meterPostCode = meterPostCode;
        this.meterScecoNumber = meterScecoNumber;
        this.meterNumberOfElectricMeters = meterNumberOfElectricMeters;
        this.meterLastReading = meterLastReading;
        this.meterBuildingNumber = meterBuildingNumber;
        this.meterNumberOfFloors = meterNumberOfFloors;
        this.meterBuildingDuplication = meterBuildingDuplication;
        this.meterBuildingNumberM = meterBuildingNumberM;
        this.meterBuildingDescription = meterBuildingDescription;
        this.meterStreetNumber = meterStreetNumber;
        this.meterStreetName = meterStreetName;
        this.meterStreetNumberM = meterStreetNumberM;
        this.meterSectorName = meterSectorName;
        this.meterSubName = meterSubName;
        this.meterIsSewerConnectionExist = meterIsSewerConnectionExist;
        this.meterArabicName = meterArabicName;
        this.meterCustomerActive = meterCustomerActive;
        this.meterLatitude = meterLatitude;
        this.meterLongitude = meterLongitude;
        this.groundElevation = groundElevation;
        this.elevation = elevation;
        this.createdAt = createdAt;
    }


    public long getId() {
        return id;
    }

    public String getMeterSerialNumber() {
        return meterSerialNumber;
    }

    public String getMeterHcn() {
        return meterHcn;
    }

    public String getMeterMeterAddress() {
        return meterMeterAddress;
    }

    public String getMeterPlateNumber() {
        return meterPlateNumber;
    }

    public int getMeterDistrictName() {
        return meterDistrictName;
    }

    public String getMeterDmaZone() {
        return meterDmaZone;
    }

    public String getMeterLastUpdated() {
        return meterLastUpdated;
    }

    public String getMeterLocationNumber() {
        return meterLocationNumber;
    }

    public String getMeterPostCode() {
        return meterPostCode;
    }

    public String getMeterScecoNumber() {
        return meterScecoNumber;
    }

    public String getMeterNumberOfElectricMeters() {
        return meterNumberOfElectricMeters;
    }

    public String getMeterLastReading() {
        return meterLastReading;
    }

    public String getMeterBuildingNumber() {
        return meterBuildingNumber;
    }

    public String getMeterNumberOfFloors() {
        return meterNumberOfFloors;
    }

    public String getMeterBuildingDuplication() {
        return meterBuildingDuplication;
    }

    public String getMeterBuildingNumberM() {
        return meterBuildingNumberM;
    }

    public String getMeterBuildingDescription() {
        return meterBuildingDescription;
    }

    public String getMeterStreetNumber() {
        return meterStreetNumber;
    }

    public String getMeterStreetName() {
        return meterStreetName;
    }

    public String getMeterStreetNumberM() {
        return meterStreetNumberM;
    }

    public String getMeterSectorName() {
        return meterSectorName;
    }

    public String getMeterSubName() {
        return meterSubName;
    }

    public String getMeterIsSewerConnectionExist() {
        return meterIsSewerConnectionExist;
    }

    public String getMeterArabicName() {
        return meterArabicName;
    }

    public String getMeterCustomerActive() {
        return meterCustomerActive;
    }

    public String getMeterLatitude() {
        return meterLatitude;
    }

    public String getMeterLongitude() {
        return meterLongitude;
    }

    public int getMeterMeterDiameterId() {
        return meterMeterDiameterId;
    }

    public int getMeterMeterBrandId() {
        return meterMeterBrandId;
    }

    public int getMeterMeterTypeId() {
        return meterMeterTypeId;
    }

    public int getMeterMeterStatusId() {
        return meterMeterStatusId;
    }

    public int getMeterMeterMaterialId() {
        return meterMeterMaterialId;
    }

    public int getMeterMeterRemarksId() {
        return meterMeterRemarksId;
    }

    public int getMeterReadTypeId() {
        return meterReadTypeId;
    }

    public int getMeterPipeAfterMeterId() {
        return meterPipeAfterMeterId;
    }

    public int getMeterPipeSizeId() {
        return meterPipeSizeId;
    }

    public int getMeterMaintenanceAreaId() {
        return meterMaintenanceAreaId;
    }

    public int getMeterMeterBoxPositionId() {
        return meterMeterBoxPositionId;
    }

    public int getMeterMeterBoxTypeId() {
        return meterMeterBoxTypeId;
    }

    public int getMeterCoverStatusId() {
        return meterCoverStatusId;
    }

    public int getMeterLocationId() {
        return meterLocationId;
    }

    public int getMeterBuildingUsageId() {
        return meterBuildingUsageId;
    }

    public int getMeterSubBuildingTypeId() {
        return meterSubBuildingTypeId;
    }

    public int getMeterWaterScheduleId() {
        return meterWaterScheduleId;
    }

    public int getMeterWaterConnectionTypeId() {
        return meterWaterConnectionTypeId;
    }

    public int getMeterPipeMaterialId() {
        return meterPipeMaterialId;
    }

    public int getMeterBrandNewMeterId() {
        return meterBrandNewMeterId;
    }

    public int getMeterValveTypeId() {
        return meterValveTypeId;
    }

    public int getMeterValveStatusId() {
        return meterValveStatusId;
    }

    public int getMeterEnabledId() {
        return meterEnabledId;
    }

    public int getMeterReducerDiameterId() {
        return meterReducerDiameterId;
    }

    public String getGroundElevation() {
        return groundElevation;
    }

    public String getElevation() {
        return elevation;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMeterId() {
        return meterId;
    }

    public void setMeterId(long meterId) {
        this.meterId = meterId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public MeterObject(long meterId, String meterSerialNumber, String meterHcn, String meterMeterAddress, String meterPlateNumber,
                       int meterMeterDiameterId, int meterMeterBrandId, int meterMeterTypeId, int meterMeterStatusId, int meterMeterMaterialId, int meterMeterRemarksId,
                       int meterReadTypeId, int meterPipeAfterMeterId, int meterPipeSizeId, int meterMaintenanceAreaId, int meterMeterBoxPositionId, int meterMeterBoxTypeId,
                       int meterCoverStatusId, int meterLocationId, int meterBuildingUsageId, int meterSubBuildingTypeId, int meterWaterScheduleId,
                       int meterWaterConnectionTypeId, int meterPipeMaterialId, int meterBrandNewMeterId, int meterValveTypeId, int meterValveStatusId,
                       int meterEnabledId, int meterReducerDiameterId, int meterDistrictName, String meterDmaZone, String meterLastUpdated, String meterLocationNumber,
                       String meterPostCode, String meterScecoNumber, String meterNumberOfElectricMeters, String meterLastReading, String meterBuildingNumber, String meterNumberOfFloors,
                       String meterBuildingDuplication, String meterBuildingNumberM, String meterBuildingDescription, String meterStreetNumber, String meterStreetName, String meterStreetNumberM,
                       String meterSectorName, String meterSubName, String meterIsSewerConnectionExist, String meterArabicName, String meterCustomerActive, String meterLatitude, String meterLongitude,
                       String reason, String groundElevation, String elevation, String createdAt) {
        this.meterId = meterId;
        this.meterSerialNumber = meterSerialNumber;
        this.meterHcn = meterHcn;
        this.meterMeterAddress = meterMeterAddress;
        this.meterPlateNumber = meterPlateNumber;
        this.meterMeterDiameterId = meterMeterDiameterId;
        this.meterMeterBrandId = meterMeterBrandId;
        this.meterMeterTypeId = meterMeterTypeId;
        this.meterMeterStatusId = meterMeterStatusId;
        this.meterMeterMaterialId = meterMeterMaterialId;
        this.meterMeterRemarksId = meterMeterRemarksId;
        this.meterReadTypeId = meterReadTypeId;
        this.meterPipeAfterMeterId = meterPipeAfterMeterId;
        this.meterPipeSizeId = meterPipeSizeId;
        this.meterMaintenanceAreaId = meterMaintenanceAreaId;
        this.meterMeterBoxPositionId = meterMeterBoxPositionId;
        this.meterMeterBoxTypeId = meterMeterBoxTypeId;
        this.meterCoverStatusId = meterCoverStatusId;
        this.meterLocationId = meterLocationId;
        this.meterBuildingUsageId = meterBuildingUsageId;
        this.meterSubBuildingTypeId = meterSubBuildingTypeId;
        this.meterWaterScheduleId = meterWaterScheduleId;
        this.meterWaterConnectionTypeId = meterWaterConnectionTypeId;
        this.meterPipeMaterialId = meterPipeMaterialId;
        this.meterBrandNewMeterId = meterBrandNewMeterId;
        this.meterValveTypeId = meterValveTypeId;
        this.meterValveStatusId = meterValveStatusId;
        this.meterEnabledId = meterEnabledId;
        this.meterReducerDiameterId = meterReducerDiameterId;
        this.meterDistrictName = meterDistrictName;
        this.meterDmaZone = meterDmaZone;
        this.meterLastUpdated = meterLastUpdated;
        this.meterLocationNumber = meterLocationNumber;
        this.meterPostCode = meterPostCode;
        this.meterScecoNumber = meterScecoNumber;
        this.meterNumberOfElectricMeters = meterNumberOfElectricMeters;
        this.meterLastReading = meterLastReading;
        this.meterBuildingNumber = meterBuildingNumber;
        this.meterNumberOfFloors = meterNumberOfFloors;
        this.meterBuildingDuplication = meterBuildingDuplication;
        this.meterBuildingNumberM = meterBuildingNumberM;
        this.meterBuildingDescription = meterBuildingDescription;
        this.meterStreetNumber = meterStreetNumber;
        this.meterStreetName = meterStreetName;
        this.meterStreetNumberM = meterStreetNumberM;
        this.meterSectorName = meterSectorName;
        this.meterSubName = meterSubName;
        this.meterIsSewerConnectionExist = meterIsSewerConnectionExist;
        this.meterArabicName = meterArabicName;
        this.meterCustomerActive = meterCustomerActive;
        this.meterLatitude = meterLatitude;
        this.meterLongitude = meterLongitude;
        this.reason = reason;
        this.groundElevation = groundElevation;
        this.elevation = elevation;
        this.createdAt = createdAt;
    }


    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }


}
