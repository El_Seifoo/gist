package com.seifoo.neomit.gisgathering.home.sensors.add;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, AddSensorMVP.View, AddSensorMVP.SecondView {
    private EditText commissionDateEditText, streetNameEditText, dmaZoneEditText, sectorNameEditText, lastUpdateEditText;
    private Spinner enabledSpinner, districtNameSpinner, diameterSpinner, pipeMaterialSpinner;

    private AddSensorMVP.Presenter presenter;

    public static SecondFragment newInstance() {
        return new SecondFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sensor_second, container, false);

        presenter = new AddSensorPresenter(this, this, new AddSensorModel());

        enabledSpinner = (Spinner) view.findViewById(R.id.enabled_spinner);
        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);
        diameterSpinner = (Spinner) view.findViewById(R.id.diameter_spinner);
        pipeMaterialSpinner = (Spinner) view.findViewById(R.id.pipe_material_spinner);


        commissionDateEditText = (EditText) view.findViewById(R.id.commission_date);
        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        dmaZoneEditText = (EditText) view.findViewById(R.id.dma_zone);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);
        lastUpdateEditText = (EditText) view.findViewById(R.id.last_updated);

        commissionDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(commissionDateEditText.getText().toString().trim(), 0);
            }
        });

        lastUpdateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(lastUpdateEditText.getText().toString().trim(), 1);
            }
        });

        presenter.requestSecondSpinnersData();

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new SensorObject(commissionDateEditText.getText().toString().trim(), enabledIds.get(enabledSpinner.getSelectedItemPosition()),
                districtNameIds.get(districtNameSpinner.getSelectedItemPosition()), streetNameEditText.getText().toString().trim(),
                dmaZoneEditText.getText().toString().trim(), sectorNameEditText.getText().toString().trim(),
                diameterIds.get(diameterSpinner.getSelectedItemPosition()), pipeMaterialIds.get(pipeMaterialSpinner.getSelectedItemPosition()),
                lastUpdateEditText.getText().toString().trim()));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> enabledIds, districtNameIds, diameterIds, pipeMaterialIds;

    @Override
    public void loadSpinnersData(ArrayList<String> enabled, ArrayList<Integer> enabledIds, ArrayList<String> districtName, ArrayList<Integer> districtNameIds, ArrayList<String> diameter, ArrayList<Integer> diameterIds, ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds) {
        this.enabledIds = enabledIds;
        this.districtNameIds = districtNameIds;
        this.diameterIds = diameterIds;
        this.pipeMaterialIds = pipeMaterialIds;

        enabledSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, enabled));
        districtNameSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, districtName));
        diameterSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameter));
        pipeMaterialSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, pipeMaterial));
    }

    @Override
    public void setCommissionDate(String commissionDate) {
        commissionDateEditText.setText(commissionDate);
    }

    @Override
    public void setLastUpdate(String lastUpdate) {
        lastUpdateEditText.setText(lastUpdate);
    }
}
