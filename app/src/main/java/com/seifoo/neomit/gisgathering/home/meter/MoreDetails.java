package com.seifoo.neomit.gisgathering.home.meter;

import java.io.Serializable;

public class MoreDetails implements Serializable {
    private String key;
    private String value;

    public MoreDetails(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}