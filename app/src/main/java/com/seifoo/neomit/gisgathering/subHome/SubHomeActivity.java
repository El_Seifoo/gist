package com.seifoo.neomit.gisgathering.subHome;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.CustomArrayAdapter;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class SubHomeActivity extends AppCompatActivity implements SubHomeMVP.View {

    private GridView gridView;
    private CustomArrayAdapter adapter;

    private SubHomeMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_home);

        getSupportActionBar().setTitle(getResources().getStringArray(R.array.home_list_titles)[getIntent().getExtras().getInt("Position")] + " (" + MySingleton.getmInstance(this).getStringSharedPref(Constants.API_DB_NAME, "") + ")");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new SubHomePresenter(this);

        gridView = (GridView) findViewById(R.id.grid_view);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                presenter.onItemClicked(position, getIntent().getExtras().getInt("Position"));
            }
        });

        presenter.requestListData();
    }

    @Override
    public Context getBContext() {
        return getBaseContext();
    }

    @Override
    public void loadListData(ArrayList<Integer> imgs, String[] titles) {
        adapter = new CustomArrayAdapter(SubHomeActivity.this, imgs, titles);
        gridView.setAdapter(adapter);
    }

    @Override
    public boolean backHome() {
        finish();
        return true;
    }

    @Override
    public void navigateToDestination(Class destination) {
        startActivity(new Intent(SubHomeActivity.this, destination));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return presenter.onOptionsItemSelected(item);

    }
}
