package com.seifoo.neomit.gisgathering.home.sensors.offline.details;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class OfflineSensorsDetailsPresenter implements OfflineSensorsDetailsMVP.Presenter, OfflineSensorsDetailsModel.DBCallback {
    private OfflineSensorsDetailsMVP.View view;
    private OfflineSensorsDetailsModel model;

    public OfflineSensorsDetailsPresenter(OfflineSensorsDetailsMVP.View view, OfflineSensorsDetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestSensorDetails(SensorObject sensor) {
        // deviceType 37, assetStatus 3, type 38 , maintenanceArea 9
        // enabled 22, districtName 25, diameter 0, pipeMaterial 18
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();

        ids.add(sensor.getDeviceType());
        types.add(37);
        ids.add(sensor.getAssetStatus());
        types.add(3);
        ids.add(sensor.getType());
        types.add(38);
        ids.add(sensor.getMaintenanceArea());
        types.add(9);
        ids.add(sensor.getEnabled());
        types.add(22);
        ids.add(sensor.getDistrictName());
        types.add(25);
        ids.add(sensor.getDiameter());
        types.add(0);
        ids.add(sensor.getPipeMaterial());
        types.add(18);

        model.returnValidGeoMaster(view.getAppContext(), this, sensor, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));
    }

    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, SensorObject sensor) {
        // deviceType 33, assetStatus 3, type ((0)) , maintenanceArea 9
        // enabled 22, districtName 25, diameter 0, pipeMaterial 18
        ArrayList<MoreDetails> moreDetails = new ArrayList<>();

        if (!sensor.getLatitude().isEmpty() && !sensor.getLongitude().isEmpty())
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), sensor.getLatitude() + "," + sensor.getLongitude()));
        else
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), ""));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.serial_number), sensor.getSerialNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.asset_number), sensor.getAssetNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.device_type), strings.get(0)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.asset_status), strings.get(1)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.elevation), sensor.getElevation()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.chamber_status), sensor.getChamberStatus()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.type), strings.get(2)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.line_number), sensor.getLineNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.maintenance_area), strings.get(3)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.commission_date), sensor.getCommissionDate()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.enabled), strings.get(4)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(5)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_name), sensor.getStreetName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.dma_zone), sensor.getDmaZone()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sector_name), sensor.getSectorName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.diameter), strings.get(6)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.pipe_material), strings.get(7)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.last_updated), sensor.getLastUpdate()));

        view.loadSensorMoreDetails(moreDetails);

    }
}
