package com.seifoo.neomit.gisgathering.home.breaks.rejected.edit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

public class ThirdFragment extends Fragment implements Step, EditRejectedBreaksMVP.View, EditRejectedBreaksMVP.ThirdView {
    private ImageView beforeImage, afterImage;
    private EditRejectedBreaksMVP.Presenter presenter;

    public static ThirdFragment newInstance(BreakObject breakObj) {
        ThirdFragment fragment = new ThirdFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("BreakObj", breakObj);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_break_third, container, false);

        presenter = new EditRejectedBreaksPresenter(this, this, new EditRejectedBreaksModel());

        Button beforeBtn = (Button) view.findViewById(R.id.before);
        Button afterBtn = (Button) view.findViewById(R.id.after);

        beforeImage = (ImageView) view.findViewById(R.id.before_image);
        afterImage = (ImageView) view.findViewById(R.id.after_image);

        onClick(beforeBtn);
        onClick(afterBtn);

        presenter.requestThirdStepData((BreakObject) getArguments().getSerializable("BreakObj"));
        return view;
    }

    private void onClick(Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                presenter.onButtonsClickListener(view.getId());
            }
        });
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassThirdStepDataToActivity();
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getAppContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void pickImage(Intent intent, int requestCode) {
        startActivityForResult(Intent.createChooser(intent, "Select Image"), requestCode);
    }

    @Override
    public void loadBeforeImage(byte[] image) {
        Glide.with(getAppContext())
                .asBitmap()
                .load(image)
                .apply(new RequestOptions())
                .into(beforeImage);
    }

    @Override
    public void loadAfterImage(byte[] image) {
        Glide.with(getAppContext())
                .asBitmap()
                .load(image)
                .apply(new RequestOptions())
                .into(afterImage);
    }

    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public void loadBeforeImage(Bitmap img) {
        Glide.with(getAppContext())
                .asBitmap()
                .load(img)
                .apply(new RequestOptions()
                        .error(R.mipmap.ic_launcher))
                .into(beforeImage);
    }

    @Override
    public void loadAfterImage(Bitmap img) {
        Glide.with(getAppContext())
                .asBitmap()
                .load(img)
                .apply(new RequestOptions()
                        .error(R.mipmap.ic_launcher))
                .into(afterImage);
    }
    @Override
    public void loadBeforeImage(String img) {
        Glide.with(getAppContext())
                .asBitmap()
                .load(img)
                .apply(new RequestOptions()
                        .error(R.mipmap.ic_launcher))
                .into(beforeImage);
    }

    @Override
    public void loadAfterImage(String img) {
        Glide.with(getAppContext())
                .asBitmap()
                .load(img)
                .apply(new RequestOptions()
                        .error(R.mipmap.ic_launcher))
                .into(afterImage);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        presenter.onRequestPermissionsResult(getActivity(), requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }
}
