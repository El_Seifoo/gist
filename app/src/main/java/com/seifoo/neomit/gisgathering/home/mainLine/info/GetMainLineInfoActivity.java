package com.seifoo.neomit.gisgathering.home.mainLine.info;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.home.mainLine.info.details.GetMainLineInfoDetailsActivity;

public class GetMainLineInfoActivity extends AppCompatActivity implements GetMainLineInfoMVP.View {
    private EditText serialNumberEditText;
    private Button getInfoButton;
    private ProgressBar progressBar;

    private GetMainLineInfoMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_main_line_info);

        getSupportActionBar().setTitle(getString(R.string.get_info));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new GetMainLineInfoPresenter(this, new GetMainLineInfoModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        serialNumberEditText = (EditText) findViewById(R.id.serial_num);

        getInfoButton = (Button) findViewById(R.id.get_info_button);
        getInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestGetInfo(serialNumberEditText.getText().toString().trim());
            }
        });
    }

    public void scanQR(View view) {
        presenter.requestQrCode(this);
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, 1);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }


    @Override
    public void loadMainLineInfo(MainLineObject mainLineObject) {
        Intent intent = new Intent(this, GetMainLineInfoDetailsActivity.class);
        intent.putExtra("GetInfo", mainLineObject);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
