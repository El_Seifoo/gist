package com.seifoo.neomit.gisgathering.geoDB;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.HomeActivity;
import com.seifoo.neomit.gisgathering.login.LoginActivity;

import java.util.ArrayList;

public class GeoDBActivity extends AppCompatActivity implements GeoDBMVP.View {
    private Spinner spinner;
    private ProgressBar progressBar;
    private Button btn, logOutBtn;
    private GeoDBMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_db);


        getSupportActionBar().setTitle("");

        presenter = new GeoDBPresenter(this, new GeoDBModel());

        progressBar = findViewById(R.id.loading_spinner);

        spinner = findViewById(R.id.geo_db_spinner);

        btn = (Button) findViewById(R.id.submit_button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onItemSelectedListener(spinner.getSelectedItem().toString());
            }
        });

        logOutBtn = (Button) findViewById(R.id.log_out_button);
        logOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.logout();
            }
        });

        presenter.requestGeoDB();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateHome() {
        Intent homeIntent = new Intent(this, HomeActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);
    }

    @Override
    public void loadGeoDB(final ArrayList<String> geoDBS) {
        spinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, geoDBS));
    }

    @Override
    public void navigateLogin() {
        Intent loginIntent = new Intent(getAppContext(), LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.geo_data_base_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            presenter.requestGeoDB();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
