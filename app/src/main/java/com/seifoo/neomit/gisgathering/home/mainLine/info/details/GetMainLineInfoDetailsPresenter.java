package com.seifoo.neomit.gisgathering.home.mainLine.info.details;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class GetMainLineInfoDetailsPresenter implements GetMainLineInfoDetailsMVP.Presenter, GetMainLineInfoDetailsModel.DBCallback {
    private GetMainLineInfoDetailsMVP.View view;
    private GetMainLineInfoDetailsModel model;

    public GetMainLineInfoDetailsPresenter(GetMainLineInfoDetailsMVP.View view, GetMainLineInfoDetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestMainLineData(MainLineObject mainLine) {
        //  diameter 0, type 29, enabled 22, material 4
        //  districtName 25, subDistrict 28, waterSchedule 16, assetStatus 3 , remarks 5
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();
        ids.add(mainLine.getDiameter());
        types.add(0);
        ids.add(mainLine.getType());
        types.add(24);
        ids.add(mainLine.getEnabled());
        types.add(22);
        ids.add(mainLine.getMaterial());
        types.add(4);
        ids.add(mainLine.getDistrictName());
        types.add(25);
        ids.add(mainLine.getSubDistrict());
        types.add(28);
        ids.add(mainLine.getWaterSchedule());
        types.add(16);
        ids.add(mainLine.getAssetStatus());
        types.add(3);
        ids.add(mainLine.getRemarks());
        types.add(5);
        model.returnValidGeoMaster(view.getAppContext(), this, mainLine, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));
    }

    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, MainLineObject mainLine) {
        // 0 , 3 , 4 , 5 , 16 , 22 , 24 , 25 , 28 , 29
        // 0 , 1   2   3   4    5    6    7    8

        ArrayList<MoreDetails> moreDetails = new ArrayList<>();
        if (!mainLine.getLatitude().isEmpty() && !mainLine.getLongitude().isEmpty())
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), mainLine.getLatitude() + "," + mainLine.getLongitude()));
        else
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), ""));

        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.serial_number), mainLine.getSerialNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.diameter), strings.get(0)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.type), strings.get(6)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.enabled), strings.get(5)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.material), strings.get(2)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_name), mainLine.getStreetName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_num), mainLine.getStreetNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.last_updated), mainLine.getLastUpdated()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(7)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.assigned), mainLine.getAssigned()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sub_district), strings.get(8)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.dma_zone), mainLine.getDmaZone()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sub_name), mainLine.getSubName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sector_name), mainLine.getSectorName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.water_schedule), strings.get(4)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.asset_status), strings.get(1)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.remarks), strings.get(3)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.created_at), mainLine.getCreatedAt()));

        view.loadMainLineMoreDetails(moreDetails);
    }
}
