package com.seifoo.neomit.gisgathering.home.pump.edit;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.home.pump.add.AddPumpActivity;
import com.seifoo.neomit.gisgathering.home.pump.edit.EditPumpMVP;
import com.seifoo.neomit.gisgathering.home.pump.edit.EditPumpModel;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class EditPumpPresenter implements EditPumpMVP.Presenter, EditPumpModel.DBCallback {
    private EditPumpMVP.View view;
    private EditPumpMVP.MainView mainView;
    private EditPumpMVP.FirstView firstView;
    private EditPumpMVP.SecondView secondView;
    private EditPumpModel model;

    public EditPumpPresenter(EditPumpMVP.View view, EditPumpMVP.MainView mainView, EditPumpModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public EditPumpPresenter(EditPumpMVP.View view, EditPumpMVP.FirstView firstView, EditPumpModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public EditPumpPresenter(EditPumpMVP.View view, EditPumpMVP.SecondView secondView, EditPumpModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    /*
        0 -> meter diameter , 1 -> meter brand , 2 - > meter type , 3 -> meter status , 4 -> meter material , 5 -> meter remarks
        6 -> read type , 7 -> pipe after meter , 8 -> pipe size , 9 -> maintenance area , 10 -> meter box Position , 11 -> meter box type
        12 -> cover status , 13 -> location , 14 -> building usage , 15 -> sub-building type , 16 -> water schedule , 17 -> water connection type
        18 -> pipe material , 19 -> new meter brand , 20 ->pump type  , 21 -> pump status , 22 -> yes_no , 23 -> reducer diameter
        24 -> FireType , 25 -> districts , 26 -> houseConnectionType , 27 -> pumpJob , 28 -> subDistrict , 29 -> mainLineType
     */
    @Override
    public void requestFirstSpinnersData(PumpObject pump) {
        //  enabled 22,   type 24,    assetStatus 3,
        model.getGeoMasters(view.getAppContext(), this, new int[]{22, 24, 3},
                new int[]{pump.getEnabled(), pump.getType(), pump.getAssetStatus()},
                1);

        firstView.setLocation(pump.getLatitude() + "," + pump.getLongitude());
        firstView.setStationCode(pump.getCode());
        firstView.setStationName(pump.getStationName());
    }

    @Override
    public void requestSecondSpinnersData(PumpObject pump) {
        // districtName 25, subDistrict 28,
        model.getGeoMasters(view.getAppContext(), this, new int[]{25, 28},
                new int[]{pump.getDistrictName(), pump.getSubDistrict()},
                2);
        secondView.setLastUpdated(pump.getLastUpdated());
        secondView.setData(pump.getStreetName(), pump.getDmaZone(), pump.getSectorName(), pump.getRemarks());
    }

    @Override
    public void requestPassFirstStepDataToActivity(PumpObject pump) {
        if (!pump.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        pump.setLatitude(pump.getLatitude().split(",")[0]);
        pump.setLongitude(pump.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof EditPumpActivity) {
            ((EditPumpActivity) view.getAppContext()).passFirstObj(pump);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(PumpObject pump) {
        if (view.getAppContext() instanceof EditPumpActivity) {
            ((EditPumpActivity) view.getAppContext()).passSecondObj(pump);
        }
    }

    @Override
    public void requestEditPump(PumpObject pump, String createdAt, long id) {
        pump.setId(id);
        pump.setCreatedAt(createdAt);
        model.updatePumpObj(view.getAppContext(), this, pump);
    }


    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQrCode(((EditPumpActivity) view.getAppContext()));
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        firstView.initializeScanner(integrator);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents());
        }
    }

    @Override
    public void handleQRScannerResult(String contents) {
        firstView.setStationCode(contents.replaceAll("[^0-9]", ""));
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getPumpsLocation(view.getAppContext(), this, prevLatLng);
    }

    @Override
    public void requestDatePickerDialog(String dateString) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        view.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        secondView.setLastUpdated(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();

    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index) {
        if (index == 1) handleFirstView(masters, selectedIds);
        else if (index == 2) handleSecondView(masters, selectedIds);
    }


    private void handleFirstView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                enabled = new ArrayList<>(),
                type = new ArrayList<>(),
                assetStatus = new ArrayList<>();

        ArrayList<Integer>
                enabledIds = new ArrayList<>(),
                typeIds = new ArrayList<>(),
                assetStatusIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 22) {
                enabled.add(masters.get(i).getName());
                enabledIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 24) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 3) {
                assetStatus.add(masters.get(i).getName());
                assetStatusIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int enabledIndex = returnIndex(enabledIds, selectedIds[0]);
        int typeIndex = returnIndex(typeIds, selectedIds[1]);
        int assetStatusIndex = returnIndex(assetStatusIds, selectedIds[2]);

        firstView.loadSpinnersData(enabled, enabledIds, enabledIndex,
                type, typeIds, typeIndex,
                assetStatus, assetStatusIds, assetStatusIndex);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                districtName = new ArrayList<>(),
                subDistrict = new ArrayList<>();

        ArrayList<Integer>
                districtNameIds = new ArrayList<>(),
                subDistrictIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 25) {
                districtName.add(masters.get(i).getName());
                districtNameIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 28) {
                subDistrict.add(masters.get(i).getName());
                subDistrictIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int districtNameIndex = returnIndex(districtNameIds, selectedIds[0]);
        int subDistrictIndex = returnIndex(subDistrictIds, selectedIds[1]);

        secondView.loadSpinnersData(districtName, districtNameIds, districtNameIndex,
                subDistrict, subDistrictIds, subDistrictIndex);
    }

    private int returnIndex(ArrayList<Integer> ids, int selectedId) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == selectedId)
                return i;
        }
        return 0;
    }

    @Override
    public void onPumpUpdatingCalled(int flag, PumpObject pump) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.edit_pump_done_successfully));
            mainView.backToParent(pump);
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_edit));
        }
    }

    @Override
    public void onGetPumpsLocationCalled(ArrayList<HashMap<String, String>> pumpsLocation, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], pumpsLocation, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", pumpsLocation, PICK_LOCATION_REQUEST);
        }
    }
}
