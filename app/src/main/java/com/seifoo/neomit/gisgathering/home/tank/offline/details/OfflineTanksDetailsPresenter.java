package com.seifoo.neomit.gisgathering.home.tank.offline.details;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class OfflineTanksDetailsPresenter implements OfflineTanksDetailsMVP.Presenter, OfflineTanksDetailsModel.DBCallback {
    private OfflineTanksDetailsMVP.View view;
    private OfflineTanksDetailsModel model;

    public OfflineTanksDetailsPresenter(OfflineTanksDetailsMVP.View view, OfflineTanksDetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestTanksDetails(TankObject tank) {
        // type 29 , tankDiameter 0
        // assetStatus 3,districtName 25,remarks 5
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();

        ids.add(tank.getTankDiameter());
        types.add(0);
        ids.add(tank.getAssetStatus());
        types.add(3);
        ids.add(tank.getDistrictName());
        types.add(25);
        ids.add(tank.getType());
        types.add(29);

        model.returnValidGeoMaster(view.getAppContext(), this, tank, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));

    }

    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, TankObject tank) {
        // 0,  3, 25, 29
        // 0,  1, 2,  3
        ArrayList<MoreDetails> moreDetails = new ArrayList<>();

        if (!tank.getLatitude().isEmpty() && !tank.getLongitude().isEmpty())
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), tank.getLatitude() + "," + tank.getLongitude()));
        else
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), ""));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.serial_number), tank.getSerialNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.tank_name), tank.getTankName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.type), strings.get(3)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.enabled), tank.getEnabled() == 1 ? view.getActContext().getString(R.string.yes) : view.getActContext().getString(R.string.no)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.commission_date), tank.getCommissionDate()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.tank_diameter), strings.get(0)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.active_volume), tank.getActiveVolume()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.in_active_volume), tank.getInActiveVolume()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.low_level), tank.getLowLevel()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.high_level), tank.getHighLevel()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.asset_status), strings.get(1)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_name), tank.getStreetName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(2)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sector_name), tank.getSectorName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.region), tank.getRegion()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.remarks), tank.getRemarks()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.last_updated), tank.getLastUpdated()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.created_at), tank.getCreatedAt()));

        view.loadTanksMoreDetails(moreDetails);
    }
}
