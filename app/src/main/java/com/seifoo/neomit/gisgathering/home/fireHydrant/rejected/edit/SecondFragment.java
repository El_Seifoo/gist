package com.seifoo.neomit.gisgathering.home.fireHydrant.rejected.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, EditRejectedFiresMVP.View, EditRejectedFiresMVP.SecondView {
    private EditText streetNameEditText, sectorNameEditText, withWaterEditText;
    private Spinner fireHydrateStatusSpinner, remarksSpinner, valveTypeSpinner, fireBrandSpinner, waterScheduleSpinner, maintenanceAreaSpinner;

    private EditRejectedFiresMVP.Presenter presenter;

    public static SecondFragment newInstance(FireHydrantObject fireObj) {
        SecondFragment fragment = new SecondFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("fireObj", fireObj);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fire_second_edit, container, false);

        presenter = new EditRejectedFiresPresenter(this, this, new EditRejectedFiresModel());

        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);
        withWaterEditText = (EditText) view.findViewById(R.id.with_water);


        fireHydrateStatusSpinner = (Spinner) view.findViewById(R.id.fire_hydrant_status_spinner);
        remarksSpinner = (Spinner) view.findViewById(R.id.remarks_spinner);
        valveTypeSpinner = (Spinner) view.findViewById(R.id.fire_hydrant_valve_type_spinner);
        fireBrandSpinner = (Spinner) view.findViewById(R.id.fire_hydrant_brand_spinner);
        waterScheduleSpinner = (Spinner) view.findViewById(R.id.water_schedule_spinner);
        maintenanceAreaSpinner = (Spinner) view.findViewById(R.id.maintenance_area_spinner);

        presenter.requestSecondStepData((FireHydrantObject) getArguments().getSerializable("fireObj"));


        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new FireHydrantObject(streetNameEditText.getText().toString().trim(), sectorNameEditText.getText().toString().trim(),
                fireHydrateStatusIds.get(fireHydrateStatusSpinner.getSelectedItemPosition()), remarksIds.get(remarksSpinner.getSelectedItemPosition()),
                withWaterEditText.getText().toString().trim(), fireValveTypeIds.get(valveTypeSpinner.getSelectedItemPosition()),
                fireHydrateBrandIds.get(fireBrandSpinner.getSelectedItemPosition()), waterScheduleIds.get(waterScheduleSpinner.getSelectedItemPosition()),
                maintenanceAreaIds.get(maintenanceAreaSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> fireHydrateStatusIds, remarksIds, fireValveTypeIds, fireHydrateBrandIds, waterScheduleIds, maintenanceAreaIds;

    @Override
    public void loadSpinnersData(ArrayList<String> fireHydrateStatus, ArrayList<Integer> fireHydrateStatusIds, int fireHydrateStatusIndex, ArrayList<String> remarks, ArrayList<Integer> remarksIds, int remarksIndex, ArrayList<String> fireValveType, ArrayList<Integer> fireValveTypeIds, int fireValveTypeIndex, ArrayList<String> fireHydrateBrand, ArrayList<Integer> fireHydrateBrandIds, int fireHydrateBrandIndex, ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds, int waterScheduleIndex, ArrayList<String> maintenanceArea, ArrayList<Integer> maintenanceAreaIds, int maintenanceAreaIndex) {
        this.fireHydrateStatusIds = fireHydrateStatusIds;
        this.remarksIds = remarksIds;
        this.fireValveTypeIds = fireValveTypeIds;
        this.fireHydrateBrandIds = fireHydrateBrandIds;
        this.waterScheduleIds = waterScheduleIds;
        this.maintenanceAreaIds = maintenanceAreaIds;

        fireHydrateStatusSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, fireHydrateStatus));
        fireHydrateStatusSpinner.setSelection(fireHydrateStatusIndex);
        remarksSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, remarks));
        remarksSpinner.setSelection(remarksIndex);
        valveTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, fireValveType));
        valveTypeSpinner.setSelection(fireValveTypeIndex);
        fireBrandSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, fireHydrateBrand));
        fireBrandSpinner.setSelection(fireHydrateBrandIndex);
        waterScheduleSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, waterSchedule));
        waterScheduleSpinner.setSelection(waterScheduleIndex);
        maintenanceAreaSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, maintenanceArea));
        maintenanceAreaSpinner.setSelection(maintenanceAreaIndex);
    }

    @Override
    public void setData(String streetName, String sectorName, String withWater) {
        streetNameEditText.setText(streetName);
        sectorNameEditText.setText(sectorName);
        withWaterEditText.setText(withWater);
    }
}
