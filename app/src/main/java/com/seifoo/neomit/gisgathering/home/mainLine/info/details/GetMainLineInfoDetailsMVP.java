package com.seifoo.neomit.gisgathering.home.mainLine.info.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface GetMainLineInfoDetailsMVP {
    interface View {
        Context getAppContext();

        Context getActContext();

        void loadMainLineMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {
        void requestMainLineData(MainLineObject mainLine);
    }
}
