package com.seifoo.neomit.gisgathering.home.fireHydrant.offline.offlineFiresDetails;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class OfflineFiresDetailsPresenter implements OfflineFiresDetailsMVP.Presenter, OfflineFiresDetailsModel.DBCallback {
    private OfflineFiresDetailsMVP.View view;
    private OfflineFiresDetailsModel model;

    public OfflineFiresDetailsPresenter(OfflineFiresDetailsMVP.View view, OfflineFiresDetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestFireDetails(FireHydrantObject offlineFireObj) {
        //barrelDiameterSpinner, materialSpinner, typeSpinner {0, 4, 24}
        //fireHydrateStatusSpinner, remarksSpinner, valveTypeSpinner, fireBrandSpinner, waterScheduleSpinner, maintenanceAreaSpinner {3, 5, 20, 1, 16, 9}
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();
        ids.add(offlineFireObj.getFireBarrelDiameter());
        types.add(0);
        ids.add(offlineFireObj.getFireMaterialId());
        types.add(4);
        ids.add(offlineFireObj.getFireTypeId());
        types.add(24);
        ids.add(offlineFireObj.getFireStatus());
        types.add(3);
        ids.add(offlineFireObj.getFireRemarks());
        types.add(5);
        ids.add(offlineFireObj.getFireValveType());
        types.add(20);
        ids.add(offlineFireObj.getFireBrand());
        types.add(1);
        ids.add(offlineFireObj.getFireWaterSchedule());
        types.add(16);
        ids.add(offlineFireObj.getFireMaintenanceArea());
        types.add(9);
        ids.add(offlineFireObj.getFireDistrictName());
        types.add(25);
        model.returnValidGeoMaster(view.getAppContext(), this, offlineFireObj, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));
    }


    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, FireHydrantObject offlineMeterObj) {
        ArrayList<MoreDetails> moreDetails = new ArrayList<>();
        // 0 , 1 , 3 , 4 , 5 , 9 , 16 , 20 , 24
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), offlineMeterObj.getFireLatitude() + "," + offlineMeterObj.getFireLongitude()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.serial_number), offlineMeterObj.getFireSerialNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.fh_number), offlineMeterObj.getFireNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.height), offlineMeterObj.getFireHeight()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.barrel_diameter), strings.get(0)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.material), strings.get(3)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.type), strings.get(8)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.commission_date), offlineMeterObj.getFireCommissionDate()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(9)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_name), offlineMeterObj.getFireStreetName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sector_name), offlineMeterObj.getFireSectorName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.fire_hydrant_status), strings.get(2)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.remarks), strings.get(4)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.with_water), offlineMeterObj.getFireWithWater()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.fire_hydrant_valve_type), strings.get(7)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.fire_hydrant_brand), strings.get(1)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.water_schedule), strings.get(6)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.meter_maintenance_area), strings.get(5)));

        view.loadFireMoreDetails(moreDetails);
    }
}
