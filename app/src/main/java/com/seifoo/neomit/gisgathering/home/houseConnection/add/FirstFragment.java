package com.seifoo.neomit.gisgathering.home.houseConnection.add;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.HashMap;

public class FirstFragment extends Fragment implements Step, AddHcMVP.View, AddHcMVP.FirstView {
    private EditText locationEditText, serialNumberEditText, HCNEditText;
    private Spinner typeSpinner, diameterSpinner, materialSpinner, assetStatusSpinner;

    private AddHcMVP.Presenter presenter;

    public static FirstFragment newInstance() {
        return new FirstFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hc_first, container, false);

        presenter = new AddHcPresenter(this, this, new AddHcModel());

        locationEditText = (EditText) view.findViewById(R.id.location);
        serialNumberEditText = (EditText) view.findViewById(R.id.serial_num);
        HCNEditText = (EditText) view.findViewById(R.id.hcn);


        typeSpinner = (Spinner) view.findViewById(R.id.type_spinner);
        diameterSpinner = (Spinner) view.findViewById(R.id.diameter_spinner);
        materialSpinner = (Spinner) view.findViewById(R.id.material_spinner);
        assetStatusSpinner = (Spinner) view.findViewById(R.id.asset_status_spinner);

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(locationEditText.getText().toString().trim());
            }
        });

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.serial_num_qr)));
        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.hcn_qr)));

        presenter.requestFirstSpinnersData();

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
//        String latitude, String longitude, String serialNumber,
//                String HCN, int type, int diameter, int material, int assetStatus);

        presenter.requestPassFirstStepDataToActivity(new HouseConnectionObject(locationEditText.getText().toString().trim(), locationEditText.getText().toString().trim(),
                serialNumberEditText.getText().toString().trim(), HCNEditText.getText().toString().trim(),
                typeIds.get(typeSpinner.getSelectedItemPosition()), diameterIds.get(diameterSpinner.getSelectedItemPosition()),
                materialIds.get(materialSpinner.getSelectedItemPosition()), statusIds.get(assetStatusSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> typeIds, diameterIds, materialIds, statusIds;

    @Override
    public void loadSpinnersData(ArrayList<String> type, ArrayList<Integer> typeIds,
                                 ArrayList<String> diameter, ArrayList<Integer> diameterIds,
                                 ArrayList<String> material, ArrayList<Integer> materialIds,
                                 ArrayList<String> status, ArrayList<Integer> statusIds) {
        this.typeIds = typeIds;
        this.diameterIds = diameterIds;
        this.materialIds = materialIds;
        this.statusIds = statusIds;

        typeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, type));
        diameterSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, diameter));
        materialSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, material));
        assetStatusSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, status));

    }

    @Override
    public void showLocationError(String errorMessage) {
        locationEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((AddHCActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator, int flag) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }

    @Override
    public void setHCN(String data) {
        HCNEditText.setText(data);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("latLng", latLngs);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void setLocation(String location) {
        locationEditText.setText(location);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFirstViewActivityResult(requestCode, resultCode, data);
    }
}
