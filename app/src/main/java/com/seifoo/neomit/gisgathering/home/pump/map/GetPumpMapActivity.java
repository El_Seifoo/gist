package com.seifoo.neomit.gisgathering.home.pump.map;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;

public class GetPumpMapActivity extends AppCompatActivity implements GetPumpMapMVP.View {
    private EditText codeEditText;
    private Button getMapButton;
    private ProgressBar progressBar;

    private GetPumpMapMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_pump_map);

        getSupportActionBar().setTitle(getString(R.string.get_map));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new GetPumpMapPresenter(this, new GetPumpMapModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        codeEditText = (EditText) findViewById(R.id.station_code);

        getMapButton = (Button) findViewById(R.id.get_info_button);
        getMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestGetMap(codeEditText.getText().toString().trim());
            }
        });
    }

    public void scanQR(View view) {
        presenter.requestQrCode(this);
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, 1);
    }

    @Override
    public void setCode(String code) {
        codeEditText.setText(code);
    }

    @Override
    public void loadPumpMap(String latitude, String longitude) {
        Intent intent = new Intent(this, GetPumpMapDetailsActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
