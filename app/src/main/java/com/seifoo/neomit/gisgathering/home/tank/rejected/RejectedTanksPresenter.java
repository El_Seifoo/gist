package com.seifoo.neomit.gisgathering.home.tank.rejected;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RejectedTanksPresenter implements RejectedTanksMVP.Presenter, RejectedTanksModel.VolleyCallback, RejectedTanksModel.DBCallback {
    private RejectedTanksMVP.View view;
    private RejectedTanksModel model;

    public RejectedTanksPresenter(RejectedTanksMVP.View view, RejectedTanksModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestTanks() {
        view.showProgress();
        model.getRejected(view.getAppContext(), this);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ArrayList<TankObject> rejectedTanks = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {

                    rejectedTanks.add(new TankObject(data.getJSONObject(i).getLong("Corrected_Id"),
                            returnValidString(data.getJSONObject(i).getString("Y_Map")),
                            returnValidString(data.getJSONObject(i).getString("X_Map")),
                            returnValidString(data.getJSONObject(i).getString("SERIAL_NO")),
                            returnValidString(data.getJSONObject(i).getString("TANK_NAME")),
                            returnValidInt(data.getJSONObject(i).getString("FEATURE_TYPE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("ENABLED_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(i).getString("ENABLED_ID")),
                            returnValidDate(data.getJSONObject(i).getString("COMMISSION_DATE")),
                            returnValidInt(data.getJSONObject(i).getString("TANK_DIAMETER")),
                            returnValidString(data.getJSONObject(i).getString("ACTIVE_VOLUME")),
                            returnValidString(data.getJSONObject(i).getString("INACTIVE_VOLUME")),
                            returnValidString(data.getJSONObject(i).getString("LOW_LEVEL")),
                            returnValidString(data.getJSONObject(i).getString("HIGH_LEVEL")),
                            returnValidInt(data.getJSONObject(i).getString("ASSET_STATUS_ID")),
                            returnValidString(data.getJSONObject(i).getString("STREET_NAME")),
                            returnValidInt(data.getJSONObject(i).getString("DISTRICT_NAME_ID")),
                            returnValidString(data.getJSONObject(i).getString("SECTOR_NAME")),
                            returnValidString(data.getJSONObject(i).getString("REGION_ID")),
                            returnValidString(data.getJSONObject(i).getString("REMARKS")),
                            returnValidDate(data.getJSONObject(i).getString("LAST_UPDATED")),
                            returnValidString(data.getJSONObject(i).getString("Reason")),
                            returnValidDate1(data.getJSONObject(i).getString("CreatedDate"))));
//x -> longitude , y -> latitude
// returnValidString(data.getJSONObject(i).getString("CreatedDate"))
                }
                model.checkDatabaseValves(view.getAppContext(), this, rejectedTanks);
            } else {
                view.hideProgress();
                view.showEmptyListText();
            }
        } else {
            view.hideProgress();
            view.showEmptyListText();
        }
    }

    private String returnValidString(String data) {
        return data.isEmpty() || data.toLowerCase().equals("null") ? "" : data;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onCheckingValvesCalled(ArrayList<TankObject> tanks) {
        view.hideProgress();
        if (tanks.isEmpty())
            view.showEmptyListText();
        else
            view.loadRejectedTanks(tanks);
    }
}
