package com.seifoo.neomit.gisgathering.home.sensors.add;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class AddSensorModel {
    public void getGeoMasters(Context context, DBCallback callback, int[] types, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))), index);
    }

    public void insertSensor(Context context, DBCallback callback, SensorObject sensor) {
        callback.onSensorInsertionCalled(DataBaseHelper.getmInstance(context).insertSensor(sensor));
    }

    protected void getSensorsLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetSensorsLocationCalled(DataBaseHelper.getmInstance(context).getSensorsLocation(), prevLatLng);
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index);

        void onSensorInsertionCalled(long flag);

        void onGetSensorsLocationCalled(ArrayList<HashMap<String,String>> sensorsLocation, String prevLatLng);
    }
}
