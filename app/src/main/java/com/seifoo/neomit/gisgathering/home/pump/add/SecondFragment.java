package com.seifoo.neomit.gisgathering.home.pump.add;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, AddPumpMVP.View, AddPumpMVP.SecondView {
    private EditText streetNameEditText, dmaZoneEditText, sectorNameEditText, lastUpdatedEditText, remarksEditText;
    private Spinner districtNameSpinner, subDistrictSpinner;

    private AddPumpMVP.Presenter presenter;

    public static SecondFragment newInstance() {
        return new SecondFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pump_second, container, false);

        presenter = new AddPumpPresenter(this, this, new AddPumpModel());

        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);
        subDistrictSpinner = (Spinner) view.findViewById(R.id.sub_district_spinner);

        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        dmaZoneEditText = (EditText) view.findViewById(R.id.dma_zone);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);
        lastUpdatedEditText = (EditText) view.findViewById(R.id.last_updated);
        remarksEditText = (EditText) view.findViewById(R.id.remarks);

        lastUpdatedEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(lastUpdatedEditText.getText().toString().trim());
            }
        });

        presenter.requestSecondSpinnersData();

        return view;
    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new PumpObject(
                streetNameEditText.getText().toString().trim(), districtNameIds.get(districtNameSpinner.getSelectedItemPosition()),
                subDistrictIds.get(subDistrictSpinner.getSelectedItemPosition()), dmaZoneEditText.getText().toString().trim(),
                sectorNameEditText.getText().toString().trim(), lastUpdatedEditText.getText().toString().trim(),
                remarksEditText.getText().toString().trim()));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> districtNameIds, subDistrictIds;

    @Override
    public void loadSpinnersData(ArrayList<String> districtName, ArrayList<Integer> districtNameIds, ArrayList<String> subDistrict, ArrayList<Integer> subDistrictIds) {
        this.districtNameIds = districtNameIds;
        this.subDistrictIds = subDistrictIds;

        districtNameSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, districtName));
        subDistrictSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, subDistrict));
    }

    @Override
    public void setLastUpdated(String date) {
        lastUpdatedEditText.setText(date);
    }
}
