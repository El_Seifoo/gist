package com.seifoo.neomit.gisgathering.home.breaks.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface EditBreakMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent(BreakObject breakObj);

        String getCurrentDate();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> pipeType, ArrayList<Integer> pipeTypeIds, int pipeTypeIndex,
                              ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex,
                              ArrayList<String> subZone, ArrayList<Integer> subZoneIds, int subZoneIndex,
                              ArrayList<String> diameter, ArrayList<Integer> diameterIds, int diameterIndex);

        void showLocationError(String errorMessage);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);

        void setBreakDate(String breakDate);

        void setData(String breakNumber, String buildingNumber, String pipeNumber, String streetName);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds, int pipeMaterialIndex,
                              ArrayList<String> maintenanceCompany, ArrayList<Integer> maintenanceCompanyIds, int maintenanceCompanyIndex,
                              ArrayList<String> maintenanceType, ArrayList<Integer> maintenanceTypeIds, int maintenanceTypeIndex,
                              ArrayList<String> soilType, ArrayList<Integer> soilTypeIds, int soilTypeIndex,
                              ArrayList<String> asphalt, ArrayList<Integer> asphaltIds, int asphaltIndex,
                              ArrayList<String> maintenanceDeviceType, ArrayList<Integer> maintenanceDeviceTypeIds, int maintenanceDeviceTypeIndex,
                              ArrayList<String> procedure, ArrayList<Integer> procedureIds, int procedureIndex);

        void setData(String breakDetails, String usedEquipment, String dimensionDrilling);
    }

    interface ThirdView {
        void showToastMessage(String message);

        void pickImage(Intent intent, int requestCode);

        void loadBeforeImage(byte[] image);

        void loadAfterImage(byte[] image);

        Fragment getFragment();

        void loadBeforeImage(Bitmap img);

        void loadAfterImage(Bitmap img);
    }

    interface Presenter {
        void requestFirstSpinnersData(BreakObject breakObj);

        void requestSecondSpinnersData(BreakObject breakObj);

        void requestThirdStepData(BreakObject breakObj);

        void requestPassFirstStepDataToActivity(BreakObject breakObject);

        void requestPassSecondStepDataToActivity(BreakObject breakObject);

        void requestPassThirdStepDataToActivity();

        void requestEditBreak(BreakObject breakObject, String createdAt, long id);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString);

        void onButtonsClickListener(int id);

        void onRequestPermissionsResult(Activity activity, int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults);

    }
}
