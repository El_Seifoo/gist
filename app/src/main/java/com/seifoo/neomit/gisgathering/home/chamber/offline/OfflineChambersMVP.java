package com.seifoo.neomit.gisgathering.home.chamber.offline;

import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;

import java.util.ArrayList;

public interface OfflineChambersMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void showToastMessage(String message);

        void loadChambersData(ArrayList<ChamberObject> chambers, boolean isVisible);

        void removeListItem(int position);

        void navigateDestination(String key, ChamberObject chamber, Class destination);

        void navigateToEditChamber(Intent intent, int requestCode);

        void updateListItem(int position, ChamberObject chamber);

        void removeListItem(long id);

        void navigateToMap(int RequestCode, ChamberObject chamber, int position);

        int getSpinnerSelectedPosition();

        void navigateToMap2(int requestCode, ArrayList<ChamberObject> chambers);
    }

    interface Presenter {
        void requestChambersData(int type);

        void requestRemoveChamberObjById(long id, int position);

        void OnListItemClickListener(int viewId, int position, ChamberObject chamber);

        void requestEditChamberData(int position, ChamberObject chamber);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestUploadData(int type);

        void showLocations(ArrayList<ChamberObject> chambers);
    }
}
