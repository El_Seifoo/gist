package com.seifoo.neomit.gisgathering.home.fireHydrant.getInfo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;

public interface GetFireInfoMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void initializeScanner(IntentIntegrator integrator, int flag);

        void setSerialNumber(String data);

        void setFHNumber(String data);

        void loadFireInfo(FireHydrantObject moreDetails);
    }

    interface Presenter {
        void whichQRClicked(int id);

        void requestQrCode(Activity activity, String flagString);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents, int flag);

        void requestGetInfo(String serialNumber, String fhNumber);

    }
}
