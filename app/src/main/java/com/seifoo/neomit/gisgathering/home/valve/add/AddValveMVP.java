package com.seifoo.neomit.gisgathering.home.valve.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

import java.util.ArrayList;
import java.util.HashMap;

interface AddValveMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();

        String getCurrentDate();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> valveJob, ArrayList<Integer> valveJobIds,
                              ArrayList<String> material, ArrayList<Integer> materialIds,
                              ArrayList<String> diameter, ArrayList<Integer> diameterIds,
                              ArrayList<String> type, ArrayList<Integer> typeIds,
                              ArrayList<String> lock, ArrayList<Integer> lockIds,
                              ArrayList<String> status, ArrayList<Integer> statusIds);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator);

        void setSerialNumber(String data);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);

    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> coverStatus, ArrayList<Integer> coverStatusIds,
                              ArrayList<String> enabled, ArrayList<Integer> enabledIds,
                              ArrayList<String> districts, ArrayList<Integer> districtsIds,
                              ArrayList<String> subDistricts, ArrayList<Integer> subDistrictsIds,
                              ArrayList<String> existInField, ArrayList<Integer> existInFieldIds,
                              ArrayList<String> existInMap, ArrayList<Integer> existInMapIds,
                              ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,
                              ArrayList<String> remarks, ArrayList<Integer> remarksIds);
    }

    interface Presenter {
        void requestFirstSpinnersData();

        void requestSecondSpinnersData();

        void requestPassFirstStepDataToActivity(ValveObject valve);

        void requestPassSecondStepDataToActivity(ValveObject valve);

        void requestAddValve(ValveObject valve);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents);

        void requestPickLocation(String prevLatLng);

    }
}
