package com.seifoo.neomit.gisgathering.home.fireHydrant.offline.offlineFiresDetails;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface OfflineFiresDetailsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void loadFireMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestFireDetails(FireHydrantObject offlineFireObj);
    }
}
