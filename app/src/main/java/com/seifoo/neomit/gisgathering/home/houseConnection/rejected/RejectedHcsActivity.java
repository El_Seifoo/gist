package com.seifoo.neomit.gisgathering.home.houseConnection.rejected;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.home.houseConnection.getMap.GetHcMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.houseConnection.offline.OfflineHcsAdapter;
import com.seifoo.neomit.gisgathering.home.houseConnection.rejected.edit.EditRejectedHCsActivity;
import com.seifoo.neomit.gisgathering.home.houseConnection.rejected.map.RejectedHCsMapActivity;

import java.util.ArrayList;

public class RejectedHcsActivity extends AppCompatActivity implements RejectedHcsMVP.View, OfflineHcsAdapter.HcObjectListItemListener {
    private static final int EDIT_REJECTED_HC_REQUEST = 1;
    private static final int SHOW_ALL_IN_MAP_REQUEST = 2;
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private OfflineHcsAdapter adapter;
    private ProgressBar progressBar;
    private Button showLocations;

    private RejectedHcsMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_hcs);

        getSupportActionBar().setTitle(getString(R.string.rejected_hcs));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new RejectedHcsPresenter(this, new RejectedHcsModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);


        showLocations = (Button) findViewById(R.id.show_locations);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.rejected_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineHcsAdapter(this, false);

        presenter.requestHCs();

    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyListText() {
        showLocations.setVisibility(View.GONE);
        emptyListTextView.setText(getString(R.string.no_hcs_available));
        adapter.clear();
    }

    @Override
    public void loadRejectedHCs(ArrayList<HouseConnectionObject> HCs) {
        emptyListTextView.setText("");
        adapter.setList(HCs);
        recyclerView.setAdapter(adapter);
        handleMapButton(HCs);
    }

    private void handleMapButton(final ArrayList<HouseConnectionObject> HCs) {
        for (int i = 0; i < HCs.size(); i++) {
            if (HCs.get(i).getLatitude() != null && HCs.get(i).getLongitude() != null) {
                if (!HCs.get(i).getLatitude().isEmpty() && !HCs.get(i).getLongitude().isEmpty()) {
                    showLocations.setVisibility(View.VISIBLE);
                    showLocations.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(RejectedHcsActivity.this, RejectedHCsMapActivity.class);
                            intent.putExtra("HCs", HCs);
                            startActivityForResult(intent, SHOW_ALL_IN_MAP_REQUEST);
                        }
                    });
                    break;
                }
            }
        }
    }

    @Override
    public void onListItemClickListener(int viewId, int position, HouseConnectionObject HC) {
        switch (viewId) {
            case R.id.more_details:
                Intent intent = new Intent(this, EditRejectedHCsActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("HcObj", HC);
                startActivityForResult(intent, EDIT_REJECTED_HC_REQUEST);
                break;
            case R.id.map:
                Intent intent1 = new Intent(this, GetHcMapDetailsActivity.class);
                intent1.putExtra("HC", HC);
                startActivityForResult(intent1, SHOW_ALL_IN_MAP_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_REJECTED_HC_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.removeItem(this, data.getExtras().getInt("position"));
            return;
        }
        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            presenter.requestHCs();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
