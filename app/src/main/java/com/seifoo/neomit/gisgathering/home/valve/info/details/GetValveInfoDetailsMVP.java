package com.seifoo.neomit.gisgathering.home.valve.info.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

import java.util.ArrayList;

public interface GetValveInfoDetailsMVP {
    interface View {
        Context getAppContext();

        Context getActContext();

        void loadValveMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {
        void requestValveData(ValveObject valve);
    }
}

