package com.seifoo.neomit.gisgathering.home.fireHydrant.add;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.HashMap;

public class FirstFragment extends Fragment implements Step, AddFiresMVP.View, AddFiresMVP.FirstView {
    private EditText locationEditText, serialNumberEditText, FHEditText, heightEditText, commissionDateEditText;
    private Spinner barrelDiameterSpinner, materialSpinner, typeSpinner, districtNameSpinner;

    private AddFiresMVP.Presenter presenter;

    public static FirstFragment newInstance() {
        return new FirstFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fire_first, container, false);

        presenter = new AddFiresPresenter(this, this, new AddFiresModel());

        locationEditText = (EditText) view.findViewById(R.id.location);
        serialNumberEditText = (EditText) view.findViewById(R.id.serial_num);
        FHEditText = (EditText) view.findViewById(R.id.fh_num);
        heightEditText = (EditText) view.findViewById(R.id.height);
        commissionDateEditText = (EditText) view.findViewById(R.id.commission_date);


        barrelDiameterSpinner = (Spinner) view.findViewById(R.id.barrel_diameter_spinner);
        materialSpinner = (Spinner) view.findViewById(R.id.material_spinner);
        typeSpinner = (Spinner) view.findViewById(R.id.type_spinner);
        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(locationEditText.getText().toString().trim());
            }
        });

        commissionDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestDatePickerDialog(commissionDateEditText.getText().toString().trim());
            }
        });

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.serial_num_qr)));
        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.fh_num_qr)));

        presenter.requestFirstSpinnersData();

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        /*
        String fireLatitude, String fireLongitude, String fireSerialName, String fireNumber, String fireHeight,
                             int fireBarrelDiameter, int fireMaterialId, int fireTypeId, String fireCommissionDate, String fireDistrictName
         */
        presenter.requestPassFirstStepDataToActivity(new FireHydrantObject(locationEditText.getText().toString().trim(), locationEditText.getText().toString().trim(),
                serialNumberEditText.getText().toString().trim(), FHEditText.getText().toString().trim(), heightEditText.getText().toString().trim(),
                barrelDiameterIds.get(barrelDiameterSpinner.getSelectedItemPosition()), materialIds.get(materialSpinner.getSelectedItemPosition()),
                typeIds.get(typeSpinner.getSelectedItemPosition()), commissionDateEditText.getText().toString().trim(), districtsNamesIds.get(districtNameSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> barrelDiameterIds, materialIds, typeIds, districtsNamesIds;

    @Override
    public void loadSpinnersData(ArrayList<String> barrelDiameter, ArrayList<Integer> barrelDiameterIds,
                                 ArrayList<String> material, ArrayList<Integer> materialIds,
                                 ArrayList<String> type, ArrayList<Integer> typeIds,
                                 ArrayList<String> districtsNames, ArrayList<Integer> districtsNamesIds) {
        this.barrelDiameterIds = barrelDiameterIds;
        this.materialIds = materialIds;
        this.typeIds = typeIds;
        this.districtsNamesIds = districtsNamesIds;

        barrelDiameterSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, barrelDiameter));
        materialSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, material));
        typeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, type));
        districtNameSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, districtsNames));
    }

    @Override
    public void showLocationError(String errorMessage) {
        locationEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((AddFireHydrantActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator, int flag) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }

    @Override
    public void setFHNumber(String data) {
        FHEditText.setText(data);
    }

    @Override
    public void setCommissionDate(String date) {
        commissionDateEditText.setText(date);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("latLng", latLngs);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void setLocation(String location) {
        locationEditText.setText(location);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFirstViewActivityResult(requestCode, resultCode, data);
    }
}
