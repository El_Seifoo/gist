package com.seifoo.neomit.gisgathering.home.sensors.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, EditSensorMVP.View, EditSensorMVP.SecondView {
    private EditText commissionDateEditText, streetNameEditText, dmaZoneEditText, sectorNameEditText, lastUpdateEditText;
    private Spinner enabledSpinner, districtNameSpinner, diameterSpinner, pipeMaterialSpinner;

    private EditSensorMVP.Presenter presenter;

    public static SecondFragment newInstance(SensorObject sensor) {
        SecondFragment fragment = new SecondFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("SensorObj", sensor);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sensor_second_edit, container, false);

        presenter = new EditSensorPresenter(this, this, new EditSensorModel());

        enabledSpinner = (Spinner) view.findViewById(R.id.enabled_spinner);
        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);
        diameterSpinner = (Spinner) view.findViewById(R.id.diameter_spinner);
        pipeMaterialSpinner = (Spinner) view.findViewById(R.id.pipe_material_spinner);


        commissionDateEditText = (EditText) view.findViewById(R.id.commission_date);
        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        dmaZoneEditText = (EditText) view.findViewById(R.id.dma_zone);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);
        lastUpdateEditText = (EditText) view.findViewById(R.id.last_updated);

        commissionDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(commissionDateEditText.getText().toString().trim(), 0);
            }
        });

        lastUpdateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(lastUpdateEditText.getText().toString().trim(), 1);
            }
        });

        presenter.requestSecondSpinnersData((SensorObject) getArguments().getSerializable("SensorObj"));

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new SensorObject(commissionDateEditText.getText().toString().trim(), enabledIds.get(enabledSpinner.getSelectedItemPosition()),
                districtNameIds.get(districtNameSpinner.getSelectedItemPosition()), streetNameEditText.getText().toString().trim(),
                dmaZoneEditText.getText().toString().trim(), sectorNameEditText.getText().toString().trim(),
                diameterIds.get(diameterSpinner.getSelectedItemPosition()), pipeMaterialIds.get(pipeMaterialSpinner.getSelectedItemPosition()),
                lastUpdateEditText.getText().toString().trim()));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> enabledIds, districtNameIds, diameterIds, pipeMaterialIds;

    @Override
    public void loadSpinnersData(ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex, ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex, ArrayList<String> diameter, ArrayList<Integer> diameterIds, int diameterIndex, ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds, int pipeMaterialIndex) {
        this.enabledIds = enabledIds;
        this.districtNameIds = districtNameIds;
        this.diameterIds = diameterIds;
        this.pipeMaterialIds = pipeMaterialIds;

        enabledSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, enabled));
        enabledSpinner.setSelection(enabledIndex);
        districtNameSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, districtName));
        districtNameSpinner.setSelection(districtNameIndex);
        diameterSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameter));
        diameterSpinner.setSelection(diameterIndex);
        pipeMaterialSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, pipeMaterial));
        pipeMaterialSpinner.setSelection(pipeMaterialIndex);
    }

    @Override
    public void setCommissionDate(String commissionDate) {
        commissionDateEditText.setText(commissionDate);
    }

    @Override
    public void setLastUpdate(String lastUpdate) {
        lastUpdateEditText.setText(lastUpdate);
    }

    @Override
    public void setData(String streetName, String dmaZone, String sectorName) {
        streetNameEditText.setText(streetName);
        dmaZoneEditText.setText(dmaZone);
        sectorNameEditText.setText(sectorName);
    }

}
