package com.seifoo.neomit.gisgathering.home.meter.offline.offlineMeterDetails;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class OfflineMeterDetailsModel {

    public void returnValidGeoMaster(Context context, DBCallback callback, MeterObject offlineMeterObj, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds(ids, types, lang), offlineMeterObj);
    }

    protected interface DBCallback {

        void onConvertingIdsCalled(ArrayList<String> strings, MeterObject meterObject);
    }
}
