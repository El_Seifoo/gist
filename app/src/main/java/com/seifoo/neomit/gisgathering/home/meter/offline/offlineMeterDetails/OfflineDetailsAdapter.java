package com.seifoo.neomit.gisgathering.home.meter.offline.offlineMeterDetails;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public class OfflineDetailsAdapter extends RecyclerView.Adapter<OfflineDetailsAdapter.Holder> {
    private ArrayList<MoreDetails> details;
    private boolean isOffline;
    private OnListItemClicked onListItemClicked;

    public void setDetailsList(ArrayList<MoreDetails> details) {
        this.details = details;
        notifyDataSetChanged();
    }

    public OfflineDetailsAdapter(boolean isOffline, OnListItemClicked onListItemClicked) {
        this.isOffline = isOffline;
        this.onListItemClicked = onListItemClicked;

    }

    public OfflineDetailsAdapter(boolean isOffline) {
        this.isOffline = isOffline;
    }


    public interface OnListItemClicked {
        void onItemClickListener(String latitude, String longitude);
    }


    @NonNull
    @Override
    public OfflineDetailsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new Holder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offline_meter_details_list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OfflineDetailsAdapter.Holder holder, int position) {
        holder.key.setText(details.get(position).getKey());
        holder.value.setText(details.get(position).getValue());
        if (!isOffline) {
            if (position == 0) {
                holder.location.setVisibility(View.VISIBLE);
            } else {
                holder.location.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return details != null ? details.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView key, value;
        Button location;

        public Holder(@NonNull View itemView) {
            super(itemView);
            key = (TextView) itemView.findViewById(R.id.key);
            value = (TextView) itemView.findViewById(R.id.value);

            if (!isOffline) {
                location = (Button) itemView.findViewById(R.id.location_btn);
                location.setOnClickListener(this);
            }
        }


        @Override
        public void onClick(View view) {
            onListItemClicked.onItemClickListener(details.get(getAdapterPosition()).getValue().split(",")[0], details.get(getAdapterPosition()).getValue().split(",")[1]);
        }
    }
}
