package com.seifoo.neomit.gisgathering.utils;

public class Urls {
    public static final String HTTP = "http://";
    public static final String ROOT_URL = "/api/gis/";
    public static final String LOGIN = ROOT_URL + "login";
    public static final String GEO_DATA_BASE = ROOT_URL + "GetGeoDB";
    public static final String GEO_MASTERS = ROOT_URL + "GetGeoMasters";
    public static final String SYNC_DATA = ROOT_URL + "SyncData";
    public static final String GET_INFO = ROOT_URL + "GetInfo";
    public static final String GET_MAP = ROOT_URL + "Map";
    public static final String GET_REJECTED = ROOT_URL + "GetRejected";
    public static final String SYNC_PHOTOS = ROOT_URL + "SyncImages";
}
