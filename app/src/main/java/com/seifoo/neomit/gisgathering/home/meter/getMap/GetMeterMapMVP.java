package com.seifoo.neomit.gisgathering.home.meter.getMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface GetMeterMapMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void initializeScanner(IntentIntegrator integrator, int flag);

        void setSerialNumber(String data);

        void setHCN(String data);

        void setMeterAddress(String data);

        void setPlateNumber(String data);

        void loadMeterMap(String latitude,String longitude);
    }

    interface Presenter {

        void whichQRClicked(int id);

        void requestQrCode(Activity activity, String flagString);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents, int flag);

        void requestGetMap(String serialNumber, String hcn, String meterAddress, String plateNumber);
    }
}
