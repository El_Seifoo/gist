package com.seifoo.neomit.gisgathering.home.chamber.info;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;

public interface GetChamberInfoMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void initializeScanner(IntentIntegrator integrator);

        void setSerialNumber(String data);

        void loadChamberInfo(ChamberObject chamber);
    }

    interface Presenter {

        void requestQrCode(Activity activity);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestGetInfo(String serialNumber);
    }
}
