package com.seifoo.neomit.gisgathering.geoDB;

import android.content.Context;

import java.util.ArrayList;

public interface GeoDBMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void navigateHome();

        void loadGeoDB(ArrayList<String> geoDBS);

        void navigateLogin();
    }

    interface Presenter {
        void requestGeoDB();

        void onItemSelectedListener(String geoDB);

        void logout();
    }
}
