package com.seifoo.neomit.gisgathering.home.breaks.add;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class AddBreakModel {
    public void getGeoMasters(Context context, DBCallback callback, int[] types, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE, context.getString(R.string.default_language_value))), index);
    }

    public void insertBreak(Context context, DBCallback callback, BreakObject breakObj) {
        callback.onBreakInsertionCalled(DataBaseHelper.getmInstance(context).insertBreak(breakObj));
    }

    protected void getBreaksLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetBreaksLocationCalled(DataBaseHelper.getmInstance(context).getBreaksLocation(), prevLatLng);
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index);

        void onBreakInsertionCalled(long flag);

        void onGetBreaksLocationCalled(ArrayList<HashMap<String, String>> breaksLocation, String prevLatLng);
    }
}
