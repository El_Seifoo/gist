package com.seifoo.neomit.gisgathering.home.sensors.info;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class GetSensorInfoPresenter implements GetSensorInfoMVP.Presenter, GetSensorInfoModel.VolleyCallback {
    private GetSensorInfoMVP.View view;
    private GetSensorInfoModel model;

    public GetSensorInfoPresenter(GetSensorInfoMVP.View view, GetSensorInfoModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestQrCode(Activity activity) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        view.initializeScanner(integrator);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                view.setSerialNumber(result.getContents().replaceAll("[^0-9]", ""));
        }
    }

    @Override
    public void requestGetInfo(String serialNumber) {
        if (serialNumber.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.serial_number_is_required));
            return;
        }
        view.showProgress();
        model.getInfo(view.getActContext(), this, serialNumber);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                /*
                "X_MAP": "42.125165",
            "Y_MAP": "24.1654",
            "SERIAL_NO": "1",
            "ASSET_NO": "1",
            "DEVICE_TYPE_ID": "1",
            "ASSET_STATUS_ID": "1",
            "ELEVATION": "1",
            "CHAMBER_STATUS": "1",
            "F_TYPE_ID": "1",
            "LINE_NO": "1",
            "MAINTENANCE_AREA_ID": "1",
            "COMMISSION_DATE": "2019-03-31T22:20:00",
            "ENABLED_ID": "1",
            "DISTRICTS_NAME": "abc",
            "STREET_NAME": "abc",
            "DMA_ZONE_ID": "1",
            "SECTOR_NAME": "abc",
            "DIAMATER_ID": "1",
            "PIPE_MATERIAL_ID": "1",
            "LAST_UPDATED": "2019-03-31T22:20:00",
            "Reason": null,
            "CreatedDate": "2019-05-01T12:08:00"
                 */
                SensorObject sensor = new SensorObject(
                        returnValidString(data.getJSONObject(0).getString("Y_MAP")),
                        returnValidString(data.getJSONObject(0).getString("X_MAP")),
                        returnValidString(data.getJSONObject(0).getString("SERIAL_NO")),
                        returnValidString(data.getJSONObject(0).getString("ASSET_NO")),
                        returnValidInt(data.getJSONObject(0).getString("DEVICE_TYPE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("ASSET_STATUS_ID")),
                        returnValidString(data.getJSONObject(0).getString("ELEVATION")),
                        returnValidString(data.getJSONObject(0).getString("CHAMBER_STATUS")),
                        returnValidInt(data.getJSONObject(0).getString("F_TYPE_ID")),
                        returnValidString(data.getJSONObject(0).getString("LINE_NO")),
                        returnValidInt(data.getJSONObject(0).getString("MAINTENANCE_AREA_ID")),
                        returnValidDate(data.getJSONObject(0).getString("COMMISSION_DATE")),
                        returnValidInt(data.getJSONObject(0).getString("ENABLED_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("ENABLED_ID")),
                        returnValidInt(data.getJSONObject(0).getString("DISTRICTS_NAME")),
                        returnValidString(data.getJSONObject(0).getString("STREET_NAME")),
                        returnValidString(data.getJSONObject(0).getString("DMA_ZONE_ID")),
                        returnValidString(data.getJSONObject(0).getString("SECTOR_NAME")),
                        returnValidInt(data.getJSONObject(0).getString("DIAMATER_ID")),
                        returnValidInt(data.getJSONObject(0).getString("PIPE_MATERIAL_ID")),
                        returnValidDate(data.getJSONObject(0).getString("LAST_UPDATED")),
                        returnValidDate1(data.getJSONObject(0).getString("CreatedDate")));
                view.loadSensorInfo(sensor);
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_sensor_not_exist));
        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_sensor_not_exist));
    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
