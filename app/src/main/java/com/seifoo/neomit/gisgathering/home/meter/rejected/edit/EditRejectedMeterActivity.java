package com.seifoo.neomit.gisgathering.home.meter.rejected.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class EditRejectedMeterActivity extends AppCompatActivity implements StepperLayout.StepperListener, EditRejectedMeterMVP.MainView, EditRejectedMeterMVP.View {
    private StepperLayout mStepperLayout;
    private EditRejectedMeterMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_meter);
        getSupportActionBar().setTitle(getString(R.string.edit_meter));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new EditRejectedMeterPresenter(this, this, new EditRejectedMeterModel());
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new MyStepperAdapter(getSupportFragmentManager(), this));
        mStepperLayout.setListener(this);

    }

    @Override
    public void onCompleted(View completeButton) {
        presenter.requestUploadRejectedMeter(new MeterObject(firstStepMeterObj, secondStepMeterObj, thirdStepMeterObj, fourthStepMeterObj, fifthStepMeterObj));
    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {

    }

    private MeterObject firstStepMeterObj, secondStepMeterObj, thirdStepMeterObj, fourthStepMeterObj, fifthStepMeterObj;

    protected void passFirstStepData(MeterObject meterObject) {
        firstStepMeterObj = meterObject;
    }

    protected void passSecondStepData(MeterObject meterObject) {
        secondStepMeterObj = meterObject;
    }

    protected void passThirdStepData(MeterObject meterObject) {
        thirdStepMeterObj = meterObject;
    }

    protected void passFourthStepData(MeterObject meterObject) {
        fourthStepMeterObj = meterObject;
    }

    protected void passFifthStepData(MeterObject meterObject) {
        fifthStepMeterObj = meterObject;
    }

    protected void changeStepperPosition(int position) {
        mStepperLayout.setCurrentStepPosition(position);
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void backToParent() {
        Intent intent = getIntent();
        intent.putExtra("position", getIntent().getExtras().getInt("position"));
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public long getMeterId() {
        return ((MeterObject) getIntent().getSerializableExtra("meterObj")).getMeterId();
    }

    @Override
    public String getCreatedAt() {
        return ((MeterObject) getIntent().getSerializableExtra("meterObj")).getCreatedAt();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class MyStepperAdapter extends AbstractFragmentStepAdapter {


        public MyStepperAdapter(FragmentManager fm, Context context) {
            super(fm, context);
        }

        @Override
        public Step createStep(int position) {
            switch (position) {
                case 0:
                    return FirstFragment.newInstance(((MeterObject) ((EditRejectedMeterActivity) context).getIntent().getSerializableExtra("meterObj")).getFirstStepObj());
                case 1:
                    return SecondFragment.newInstance(((MeterObject) ((EditRejectedMeterActivity) context).getIntent().getSerializableExtra("meterObj")).getSecondStepObj());
                case 2:
                    return ThirdFragment.newInstance(((MeterObject) ((EditRejectedMeterActivity) context).getIntent().getSerializableExtra("meterObj")).getThirdStepObj());
                case 3:
                    return FourthFragment.newInstance(((MeterObject) ((EditRejectedMeterActivity) context).getIntent().getSerializableExtra("meterObj")).getFourthStepObj());
                case 4:
                default:
                    return FifthFragment.newInstance(((MeterObject) ((EditRejectedMeterActivity) context).getIntent().getSerializableExtra("meterObj")).getFifthStepObj());
            }
        }

        @Override
        public int getCount() {
            return 5;
        }


        @NonNull
        @Override
        public StepViewModel getViewModel(@IntRange(from = 0) int position) {
            //Override this method to set Step title for the Tabs, not necessary for other stepper types
            return new StepViewModel.Builder(context)
                    .setTitle("") //can be a CharSequence instead
                    .create();
        }
    }
}
