package com.seifoo.neomit.gisgathering.home.chamber.rejected;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.home.chamber.map.GetChamberMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.chamber.offline.OfflineChambersAdapter;
import com.seifoo.neomit.gisgathering.home.chamber.rejected.edit.EditRejectedChambersActivity;
import com.seifoo.neomit.gisgathering.home.chamber.rejected.map.RejectedChambersMapActivity;

import java.util.ArrayList;

public class RejectedChambersActivity extends AppCompatActivity implements RejectedChambersMVP.View, OfflineChambersAdapter.ChamberObjectListItemListener {
    private static final int EDIT_REJECTED_VALVE_REQUEST = 1;
    private static final int SHOW_ALL_IN_MAP_REQUEST = 2;
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private OfflineChambersAdapter adapter;
    private ProgressBar progressBar;
    private Button showLocations;

    private RejectedChambersMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_chambers);

        getSupportActionBar().setTitle(getString(R.string.rejected_chambers));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new RejectedChambersPresenter(this, new RejectedChambersModel());
        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);


        showLocations = (Button) findViewById(R.id.show_locations);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.rejected_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineChambersAdapter(this, false);

        presenter.requestChambers();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyListText() {
        showLocations.setVisibility(View.GONE);
        emptyListTextView.setText(getString(R.string.no_chambers_available));
        adapter.clear();
    }

    @Override
    public void loadRejectedChambers(ArrayList<ChamberObject> chambers) {
        emptyListTextView.setText("");
        adapter.setList(chambers);
        recyclerView.setAdapter(adapter);
        handleMapButton(chambers);
    }

    private void handleMapButton(final ArrayList<ChamberObject> chambers) {
        for (int i = 0; i < chambers.size(); i++) {
            if (chambers.get(i).getLatitude() != null && chambers.get(i).getLongitude() != null) {
                if (!chambers.get(i).getLatitude().isEmpty() && !chambers.get(i).getLongitude().isEmpty()) {
                    showLocations.setVisibility(View.VISIBLE);
                    showLocations.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(RejectedChambersActivity.this, RejectedChambersMapActivity.class);
                            intent.putExtra("Chambers", chambers);
                            startActivityForResult(intent, SHOW_ALL_IN_MAP_REQUEST);
                        }
                    });
                    break;
                }
            }
        }
    }

    @Override
    public void onListItemClickListener(int viewId, int position, ChamberObject chamber) {
        switch (viewId) {
            case R.id.more_details:
                Intent intent = new Intent(this, EditRejectedChambersActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("ChamberObj", chamber);
                startActivityForResult(intent, EDIT_REJECTED_VALVE_REQUEST);
                break;
            case R.id.map:
                Intent intent1 = new Intent(this, GetChamberMapDetailsActivity.class);
                intent1.putExtra("Chamber", chamber);
                startActivityForResult(intent1, SHOW_ALL_IN_MAP_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_REJECTED_VALVE_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.removeItem(this, data.getExtras().getInt("position"));
            return;
        }
        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            presenter.requestChambers();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
