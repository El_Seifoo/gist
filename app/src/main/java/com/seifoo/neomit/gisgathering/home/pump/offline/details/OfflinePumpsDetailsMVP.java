package com.seifoo.neomit.gisgathering.home.pump.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;

import java.util.ArrayList;

public interface OfflinePumpsDetailsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void loadPumpMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestPumpDetails(PumpObject pump);
    }
}
