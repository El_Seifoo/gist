package com.seifoo.neomit.gisgathering.home.chamber.map;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;

public class GetChamberMapActivity extends AppCompatActivity implements GetChamberMapMVP.View {
    private EditText CHNumberEditText;
    private Button getMapButton;
    private ProgressBar progressBar;

    private GetChamberMapMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_chamber_map);

        getSupportActionBar().setTitle(getString(R.string.get_map));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new GetChamberMapPresenter(this, new GetChamberMapModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        CHNumberEditText = (EditText) findViewById(R.id.ch_num);

        getMapButton = (Button) findViewById(R.id.get_info_button);
        getMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestGetMap(CHNumberEditText.getText().toString().trim());
            }
        });
    }

    public void scanQR(View view) {
        presenter.requestQrCode(this);
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, 1);
    }

    @Override
    public void setCHN(String data) {
        CHNumberEditText.setText(data);
    }

    @Override
    public void loadChamberMap(String latitude, String longitude) {
        Intent intent = new Intent(this, GetChamberMapDetailsActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
