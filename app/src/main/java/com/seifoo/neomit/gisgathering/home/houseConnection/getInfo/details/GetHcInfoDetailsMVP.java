package com.seifoo.neomit.gisgathering.home.houseConnection.getInfo.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface GetHcInfoDetailsMVP {
    interface View {
        Context getAppContext();

        Context getActContext();

        void loadHcMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {
        void requestHcData(HouseConnectionObject HcObject);
    }
}
