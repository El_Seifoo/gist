package com.seifoo.neomit.gisgathering.home.chamber.add;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class AddChamberPresenter implements AddChamberMVP.Presenter, AddChamberModel.DBCallback {
    private AddChamberMVP.View view;
    private AddChamberMVP.MainView mainView;
    private AddChamberMVP.FirstView firstView;
    private AddChamberMVP.SecondView secondView;
    private AddChamberModel model;

    public AddChamberPresenter(AddChamberMVP.View view, AddChamberMVP.MainView mainView, AddChamberModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public AddChamberPresenter(AddChamberMVP.View view, AddChamberMVP.FirstView firstView, AddChamberModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public AddChamberPresenter(AddChamberMVP.View view, AddChamberMVP.SecondView secondView, AddChamberModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    /*
        0 -> meter diameter , 1 -> meter brand , 2 - > meter type , 3 -> meter status , 4 -> meter material , 5 -> meter remarks
        6 -> read type , 7 -> pipe after meter , 8 -> pipe size , 9 -> maintenance area , 10 -> meter box Position , 11 -> meter box type
        12 -> cover status , 13 -> location , 14 -> building usage , 15 -> sub-building type , 16 -> water schedule , 17 -> water connection type
        18 -> pipe material , 19 -> new meter brand , 20 ->chamber type  , 21 -> chamber status , 22 -> yes_no , 23 -> reducer diameter
        24 -> FireType , 25 -> districts , 26 -> houseConnectionType , 27 -> chamberJob , 28 -> subDistrict , 29 -> mainLineType
     */
    @Override
    public void requestFirstSpinnersData() {
        // pipeMaterial 4, diameter  0 ,  districtName 25
        model.getGeoMasters(view.getAppContext(), this, new int[]{4, 0, 25}, 1);

    }

    @Override
    public void requestSecondSpinnersData() {
        //  subDistrictName 28 , diameterAirValve 0 ,  diameterWAValve 0 , diameterISOValve 0 ,  diameterFlowMeter 0 , type 24,  remarks 5
        model.getGeoMasters(view.getAppContext(), this, new int[]{28, 0, 0, 0, 0, 24, 5}, 2);
    }

    @Override
    public void requestPassFirstStepDataToActivity(ChamberObject chamber) {
        if (!chamber.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        chamber.setLatitude(chamber.getLatitude().split(",")[0]);
        chamber.setLongitude(chamber.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof AddChamberActivity) {
            ((AddChamberActivity) view.getAppContext()).passFirstObj(chamber);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(ChamberObject chamber) {
        if (view.getAppContext() instanceof AddChamberActivity) {
            ((AddChamberActivity) view.getAppContext()).passSecondObj(chamber);
        }
    }

    @Override
    public void requestAddChamber(ChamberObject chamber) {
        chamber.setCreatedAt(returnValidNumbers(mainView.getCurrentDate()));
        model.insertChamber(view.getAppContext(), this, chamber);
    }


    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQrCode(((AddChamberActivity) view.getAppContext()));
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        firstView.initializeScanner(integrator);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents());
        }
    }

    @Override
    public void handleQRScannerResult(String contents) {
        firstView.setCHN(contents.replaceAll("[^0-9]", ""));
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getChambersLocation(view.getAppContext(), this, prevLatLng);
    }

    public void requestDatePickerDialog(String dateString) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        view.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        secondView.setLastUpdated(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();

    }


    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index) {
        if (index == 1) handleFirstView(masters);
        else if (index == 2) handleSecondView(masters);
    }

    private void handleFirstView(ArrayList<GeoMasterObj> masters) {
        ArrayList<String>
                pipeMaterial = new ArrayList<>(),
                diameter = new ArrayList<>(),
                districtName = new ArrayList<>();


        ArrayList<Integer>
                pipeMaterialIds = new ArrayList<>(),
                diameterIds = new ArrayList<>(),
                districtNameIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 4) {
                pipeMaterial.add(masters.get(i).getName());
                pipeMaterialIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                diameter.add(masters.get(i).getName());
                diameterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 25) {
                districtName.add(masters.get(i).getName());
                districtNameIds.add(masters.get(i).getGeoMasterId());
            }
        }

        firstView.loadSpinnersData(pipeMaterial, pipeMaterialIds,
                diameter, diameterIds,
                districtName, districtNameIds);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> masters) {
        ArrayList<String>
                subDistrictName = new ArrayList<>(),
                diameterAirValve = new ArrayList<>(),
                diameterWAValve = new ArrayList<>(),
                diameterISOValve = new ArrayList<>(),
                diameterFlowMeter = new ArrayList<>(),
                type = new ArrayList<>(),
                remarks = new ArrayList<>();


        ArrayList<Integer>
                subDistrictNameIds = new ArrayList<>(),
                diameterAirValveIds = new ArrayList<>(),
                diameterWAValveIds = new ArrayList<>(),
                diameterISOValveIds = new ArrayList<>(),
                diameterFlowMeterIds = new ArrayList<>(),
                typeIds = new ArrayList<>(),
                remarksIds = new ArrayList<>();


        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 28) {
                subDistrictName.add(masters.get(i).getName());
                subDistrictNameIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                diameterAirValve.add(masters.get(i).getName());
                diameterAirValveIds.add(masters.get(i).getGeoMasterId());

                diameterWAValve.add(masters.get(i).getName());
                diameterWAValveIds.add(masters.get(i).getGeoMasterId());

                diameterISOValve.add(masters.get(i).getName());
                diameterISOValveIds.add(masters.get(i).getGeoMasterId());

                diameterFlowMeter.add(masters.get(i).getName());
                diameterFlowMeterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 24) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 5) {
                remarks.add(masters.get(i).getName());
                remarksIds.add(masters.get(i).getGeoMasterId());
            }
        }

        secondView.loadSpinnersData(subDistrictName, subDistrictNameIds,
                diameterAirValve, diameterAirValveIds,
                diameterWAValve, diameterWAValveIds,
                diameterISOValve, diameterISOValveIds,
                diameterFlowMeter, diameterFlowMeterIds,
                type, typeIds,
                remarks, remarksIds);
    }

    @Override
    public void onChamberInsertionCalled(long flag) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.add_chamber_done_successfully));
            mainView.backToParent();
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_save_chamber));
        }
    }

    @Override
    public void onGetChambersLocationCalled(ArrayList<HashMap<String, String>> latLngs, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], latLngs, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", latLngs, PICK_LOCATION_REQUEST);
        }
    }
}
