package com.seifoo.neomit.gisgathering.home.houseConnection.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface AddHcMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();

        String getCurrentDate();
    }

    interface FirstView {//houseConnectionType,meter diameter,meter material,meter status

        void loadSpinnersData(ArrayList<String> type, ArrayList<Integer> typeIds,
                              ArrayList<String> diameter, ArrayList<Integer> diameterIds,
                              ArrayList<String> material, ArrayList<Integer> materialIds,
                              ArrayList<String> status, ArrayList<Integer> statusIds);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator, int flag);

        void setSerialNumber(String data);

        void setHCN(String data);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);

    }

    interface SecondView {//districts,waterSchedule,meter remarks

        void loadSpinnersData(ArrayList<String> districts, ArrayList<Integer> districtsIds,
                              ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,
                              ArrayList<String> remarks, ArrayList<Integer> remarksIds);
    }

    interface Presenter {
        void requestFirstSpinnersData();

        void requestSecondSpinnersData();

        void requestPassFirstStepDataToActivity(HouseConnectionObject houseConnection);

        void requestPassSecondStepDataToActivity(HouseConnectionObject houseConnection);

        void requestAddHC(HouseConnectionObject houseConnection);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext, String flagString);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents, int flag);

        void requestPickLocation(String prevLatLng);

    }
}
