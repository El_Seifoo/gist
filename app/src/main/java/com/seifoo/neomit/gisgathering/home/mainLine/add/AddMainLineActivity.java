package com.seifoo.neomit.gisgathering.home.mainLine.add;

import android.content.Context;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddMainLineActivity extends AppCompatActivity implements StepperLayout.StepperListener, AddMainLineMVP.View, AddMainLineMVP.MainView {
    private StepperLayout mStepperLayout;
    private AddMainLineMVP.Presenter presenter;

    /*
        serial 123 , diameter 32mm , type UnColl , enabled no , material FRP , streetname qwe, streetnumber 159
        dma 124 , subName 235 , sectorname 020 , waterSch Sat , asset notWorking , remarks underCar
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_main_line);

        getSupportActionBar().setTitle(getString(R.string.add_main_line));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new AddMainLinePresenter(this, this, new AddMainLineModel());

        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new MyStepperAdapter(getSupportFragmentManager(), this));
        mStepperLayout.setListener(this);
    }

    private MainLineObject firstStepObj, secondStepObj;

    protected void passFirstObj(MainLineObject firstStepObj) {
        this.firstStepObj = firstStepObj;
    }

    protected void passSecondObj(MainLineObject secondStepObj) {
        this.secondStepObj = secondStepObj;
    }

    protected void changeStepperPosition(int position) {
        mStepperLayout.setCurrentStepPosition(position);
    }

    @Override
    public void onCompleted(View completeButton) {
        presenter.requestAddMainLine(new MainLineObject(firstStepObj, secondStepObj));
    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {

    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void backToParent() {
        finish();
    }

    @Override
    public String getCurrentDate() {
        Calendar today = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return sdf.format(today.getTime());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class MyStepperAdapter extends AbstractFragmentStepAdapter {


        public MyStepperAdapter(FragmentManager fm, Context context) {
            super(fm, context);
        }

        @Override
        public Step createStep(int position) {
            switch (position) {
                case 0:
                    return FirstFragment.newInstance();
                case 1:
                default:
                    return SecondFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }


        @NonNull
        @Override
        public StepViewModel getViewModel(@IntRange(from = 0) int position) {
            //Override this method to set Step title for the Tabs, not necessary for other stepper types
            return new StepViewModel.Builder(context)
                    .setTitle("") //can be a CharSequence instead
                    .create();
        }
    }


}
