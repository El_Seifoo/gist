package com.seifoo.neomit.gisgathering.home.mainLine;

import java.io.Serializable;

public class MainLineObject implements Serializable {
    private long id, mainLineId;
    private String latitude, longitude, serialNumber;
    private int diameter, type, enabled, material;
    private String streetName, streetNumber, lastUpdated;
    private int districtName;
    private String assigned;
    private int subDistrict;
    private String dmaZone, subName, sectorName;
    private int waterSchedule, assetStatus, remarks;
    private String reason, createdAt;

    // first step const.
    public MainLineObject(String latitude, String longitude, String serialNumber, int diameter,
                          int type, int enabled, int material, String streetName, String streetNumber) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.diameter = diameter;
        this.type = type;
        this.enabled = enabled;
        this.material = material;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
    }

    public MainLineObject getFirstObject() {
        return new MainLineObject(latitude, longitude, serialNumber, diameter, type, enabled, material, streetName, streetNumber);
    }

    // second step const.
    public MainLineObject(String lastUpdated, int districtName, String assigned, int subDistrict, String dmaZone,
                          String subName, String sectorName, int waterSchedule, int assetStatus, int remarks) {
        this.lastUpdated = lastUpdated;
        this.districtName = districtName;
        this.assigned = assigned;
        this.subDistrict = subDistrict;
        this.dmaZone = dmaZone;
        this.subName = subName;
        this.sectorName = sectorName;
        this.waterSchedule = waterSchedule;
        this.assetStatus = assetStatus;
        this.remarks = remarks;
    }

    public MainLineObject getSecondObject() {
        return new MainLineObject(lastUpdated, districtName, assigned, subDistrict, dmaZone, subName, sectorName, waterSchedule, assetStatus, remarks);
    }

    public MainLineObject(MainLineObject firstObj, MainLineObject secondObj) {
        this.latitude = firstObj.getLatitude();
        this.longitude = firstObj.getLongitude();
        this.serialNumber = firstObj.getSerialNumber();
        this.diameter = firstObj.getDiameter();
        this.type = firstObj.getType();
        this.enabled = firstObj.getEnabled();
        this.material = firstObj.getMaterial();
        this.streetName = firstObj.getStreetName();
        this.streetNumber = firstObj.getStreetNumber();

        this.lastUpdated = secondObj.getLastUpdated();
        this.districtName = secondObj.getDistrictName();
        this.assigned = secondObj.getAssigned();
        this.subDistrict = secondObj.getSubDistrict();
        this.dmaZone = secondObj.getDmaZone();
        this.subName = secondObj.getSubName();
        this.sectorName = secondObj.getSectorName();
        this.waterSchedule = secondObj.getWaterSchedule();
        this.assetStatus = secondObj.getAssetStatus();
        this.remarks = secondObj.getRemarks();
    }


    public MainLineObject(long id, long mainLineId, String latitude, String longitude, String serialNumber,
                          int diameter, int type, int enabled, int material, String streetName, String streetNumber,
                          String lastUpdated, int districtName, String assigned, int subDistrict, String dmaZone,
                          String subName, String sectorName, int waterSchedule, int assetStatus, int remarks, String createdAt) {
        this.id = id;
        this.mainLineId = mainLineId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.diameter = diameter;
        this.type = type;
        this.enabled = enabled;
        this.material = material;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.lastUpdated = lastUpdated;
        this.districtName = districtName;
        this.assigned = assigned;
        this.subDistrict = subDistrict;
        this.dmaZone = dmaZone;
        this.subName = subName;
        this.sectorName = sectorName;
        this.waterSchedule = waterSchedule;
        this.assetStatus = assetStatus;
        this.remarks = remarks;
        this.createdAt = createdAt;
    }

    public MainLineObject(String latitude, String longitude, String serialNumber, int diameter, int type,
                          int enabled, int material, String streetName, String streetNumber, String lastUpdated,
                          int districtName, String assigned, int subDistrict, String dmaZone, String subName, String sectorName,
                          int waterSchedule, int assetStatus, int remarks, String createdAt) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.diameter = diameter;
        this.type = type;
        this.enabled = enabled;
        this.material = material;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.lastUpdated = lastUpdated;
        this.districtName = districtName;
        this.assigned = assigned;
        this.subDistrict = subDistrict;
        this.dmaZone = dmaZone;
        this.subName = subName;
        this.sectorName = sectorName;
        this.waterSchedule = waterSchedule;
        this.assetStatus = assetStatus;
        this.remarks = remarks;
        this.createdAt = createdAt;
    }


    public MainLineObject(long mainLineId, String latitude, String longitude, String serialNumber, int diameter,
                          int type, int enabled, int material, String streetName, String streetNumber, String lastUpdated,
                          int districtName, String assigned, int subDistrict, String dmaZone, String subName, String sectorName,
                          int waterSchedule, int assetStatus, int remarks, String reason, String createdAt) {
        this.mainLineId = mainLineId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.diameter = diameter;
        this.type = type;
        this.enabled = enabled;
        this.material = material;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.lastUpdated = lastUpdated;
        this.districtName = districtName;
        this.assigned = assigned;
        this.subDistrict = subDistrict;
        this.dmaZone = dmaZone;
        this.subName = subName;
        this.sectorName = sectorName;
        this.waterSchedule = waterSchedule;
        this.assetStatus = assetStatus;
        this.remarks = remarks;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public long getMainLineId() {
        return mainLineId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public int getDiameter() {
        return diameter;
    }

    public int getType() {
        return type;
    }

    public int getEnabled() {
        return enabled;
    }

    public int getMaterial() {
        return material;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public int getDistrictName() {
        return districtName;
    }

    public String getAssigned() {
        return assigned;
    }

    public int getSubDistrict() {
        return subDistrict;
    }

    public String getDmaZone() {
        return dmaZone;
    }

    public String getSubName() {
        return subName;
    }

    public String getSectorName() {
        return sectorName;
    }

    public int getWaterSchedule() {
        return waterSchedule;
    }

    public int getAssetStatus() {
        return assetStatus;
    }

    public int getRemarks() {
        return remarks;
    }

    public String getReason() {
        return reason;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setMainLineId(long mainLineId) {
        this.mainLineId = mainLineId;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
