package com.seifoo.neomit.gisgathering.home.fireHydrant;

import java.io.Serializable;

public class FireHydrantObject implements Serializable {

    private long id, fireHydrantId;
    private String fireSerialNumber, fireNumber, fireHeight, fireCommissionDate,
            fireStreetName, fireSectorName, fireWithWater, fireLatitude, fireLongitude, reason;
    private int fireBarrelDiameter, fireMaterialId, fireTypeId, fireDistrictName,
            fireStatus, fireRemarks, fireValveType, fireBrand, fireWaterSchedule, fireMaintenanceArea;
    private String createdAt;


    public FireHydrantObject(String fireLatitude, String fireLongitude, String fireSerialNumber, String fireNumber, String fireHeight,
                             int fireBarrelDiameter, int fireMaterialId, int fireTypeId, String fireCommissionDate, int fireDistrictName) {
        this.fireLatitude = fireLatitude;
        this.fireLongitude = fireLongitude;
        this.fireSerialNumber = fireSerialNumber;
        this.fireNumber = fireNumber;
        this.fireHeight = fireHeight;
        this.fireBarrelDiameter = fireBarrelDiameter;
        this.fireMaterialId = fireMaterialId;
        this.fireTypeId = fireTypeId;
        this.fireCommissionDate = fireCommissionDate;
        this.fireDistrictName = fireDistrictName;
    }

    public FireHydrantObject(String fireStreetName, String fireSectorName, int fireStatus, int fireRemarks,
                             String fireWithWater, int fireValveType, int fireBrand, int fireWaterSchedule, int fireMaintenanceArea) {
        this.fireStreetName = fireStreetName;
        this.fireSectorName = fireSectorName;
        this.fireStatus = fireStatus;
        this.fireRemarks = fireRemarks;
        this.fireWithWater = fireWithWater;
        this.fireValveType = fireValveType;
        this.fireBrand = fireBrand;
        this.fireWaterSchedule = fireWaterSchedule;
        this.fireMaintenanceArea = fireMaintenanceArea;
    }

    public FireHydrantObject getFirstObject() {
        return new FireHydrantObject(fireLatitude, fireLongitude, fireSerialNumber, fireNumber, fireHeight,
                fireBarrelDiameter, fireMaterialId, fireTypeId, fireCommissionDate, fireDistrictName);
    }

    public FireHydrantObject getSecondObject() {
        return new FireHydrantObject(fireStreetName, fireSectorName, fireStatus, fireRemarks,
                fireWithWater, fireValveType, fireBrand, fireWaterSchedule, fireMaintenanceArea);
    }

    public FireHydrantObject(FireHydrantObject firstObj, FireHydrantObject secondObj) {
        this.fireLatitude = firstObj.getFireLatitude();
        this.fireLongitude = firstObj.getFireLongitude();
        this.fireSerialNumber = firstObj.getFireSerialNumber();
        this.fireNumber = firstObj.getFireNumber();
        this.fireHeight = firstObj.getFireHeight();
        this.fireBarrelDiameter = firstObj.getFireBarrelDiameter();
        this.fireMaterialId = firstObj.getFireMaterialId();
        this.fireTypeId = firstObj.getFireTypeId();
        this.fireCommissionDate = firstObj.getFireCommissionDate();
        this.fireDistrictName = firstObj.getFireDistrictName();

        this.fireStreetName = secondObj.getFireStreetName();
        this.fireSectorName = secondObj.getFireSectorName();
        this.fireStatus = secondObj.getFireStatus();
        this.fireRemarks = secondObj.getFireRemarks();
        this.fireWithWater = secondObj.getFireWithWater();
        this.fireValveType = secondObj.getFireValveType();
        this.fireBrand = secondObj.getFireBrand();
        this.fireWaterSchedule = secondObj.getFireWaterSchedule();
        this.fireMaintenanceArea = secondObj.getFireMaintenanceArea();
    }

    public FireHydrantObject(long id, long fireHydrantId, String fireSerialNumber, String fireNumber, String fireHeight, String fireCommissionDate,
                             int fireDistrictName, String fireStreetName, String fireSectorName, int fireRemarks, String fireWithWater, int fireValveType,
                             int fireWaterSchedule, String fireLatitude, String fireLongitude, int fireBarrelDiameter, int fireMaterialId, int fireTypeId,
                             int fireStatus, int fireBrand, int fireMaintenanceArea, String createdAt) {
        this.id = id;
        this.fireHydrantId = fireHydrantId;
        this.fireSerialNumber = fireSerialNumber;
        this.fireNumber = fireNumber;
        this.fireHeight = fireHeight;
        this.fireCommissionDate = fireCommissionDate;
        this.fireDistrictName = fireDistrictName;
        this.fireStreetName = fireStreetName;
        this.fireSectorName = fireSectorName;
        this.fireRemarks = fireRemarks;
        this.fireWithWater = fireWithWater;
        this.fireValveType = fireValveType;
        this.fireWaterSchedule = fireWaterSchedule;
        this.fireLatitude = fireLatitude;
        this.fireLongitude = fireLongitude;
        this.fireBarrelDiameter = fireBarrelDiameter;
        this.fireMaterialId = fireMaterialId;
        this.fireTypeId = fireTypeId;
        this.fireStatus = fireStatus;
        this.fireBrand = fireBrand;
        this.fireMaintenanceArea = fireMaintenanceArea;
        this.createdAt = createdAt;
    }

    public FireHydrantObject(String fireSerialNumber, String fireNumber, String fireHeight, String fireCommissionDate, int fireDistrictName,
                             String fireStreetName, String fireSectorName, int fireRemarks, String fireWithWater, int fireValveType, int fireWaterSchedule,
                             String fireLatitude, String fireLongitude, int fireBarrelDiameter, int fireMaterialId, int fireTypeId, int fireStatus,
                             int fireBrand, int fireMaintenanceArea, String createdAt) {
        this.fireSerialNumber = fireSerialNumber;
        this.fireNumber = fireNumber;
        this.fireHeight = fireHeight;
        this.fireCommissionDate = fireCommissionDate;
        this.fireDistrictName = fireDistrictName;
        this.fireStreetName = fireStreetName;
        this.fireSectorName = fireSectorName;
        this.fireRemarks = fireRemarks;
        this.fireWithWater = fireWithWater;
        this.fireValveType = fireValveType;
        this.fireWaterSchedule = fireWaterSchedule;
        this.fireLatitude = fireLatitude;
        this.fireLongitude = fireLongitude;
        this.fireBarrelDiameter = fireBarrelDiameter;
        this.fireMaterialId = fireMaterialId;
        this.fireTypeId = fireTypeId;
        this.fireStatus = fireStatus;
        this.fireBrand = fireBrand;
        this.fireMaintenanceArea = fireMaintenanceArea;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public long getFireHydrantId() {
        return fireHydrantId;
    }

    public String getFireLatitude() {
        return fireLatitude;
    }

    public String getFireLongitude() {
        return fireLongitude;
    }

    public String getFireSerialNumber() {
        return fireSerialNumber;
    }

    public String getFireNumber() {
        return fireNumber;
    }

    public String getFireHeight() {
        return fireHeight;
    }

    public int getFireBarrelDiameter() {
        return fireBarrelDiameter;
    }

    public int getFireMaterialId() {
        return fireMaterialId;
    }

    public int getFireTypeId() {
        return fireTypeId;
    }

    public String getFireCommissionDate() {
        return fireCommissionDate;
    }

    public int getFireDistrictName() {
        return fireDistrictName;
    }

    public String getFireStreetName() {
        return fireStreetName;
    }

    public String getFireSectorName() {
        return fireSectorName;
    }

    public int getFireStatus() {
        return fireStatus;
    }

    public int getFireRemarks() {
        return fireRemarks;
    }

    public String getFireWithWater() {
        return fireWithWater;
    }

    public int getFireValveType() {
        return fireValveType;
    }

    public int getFireBrand() {
        return fireBrand;
    }

    public int getFireWaterSchedule() {
        return fireWaterSchedule;
    }

    public int getFireMaintenanceArea() {
        return fireMaintenanceArea;
    }

    public String getReason() {
        return reason;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFireHydrantId(long fireHydrantId) {
        this.fireHydrantId = fireHydrantId;
    }

    public void setFireLatitude(String fireLatitude) {
        this.fireLatitude = fireLatitude;
    }

    public void setFireLongitude(String fireLongitude) {
        this.fireLongitude = fireLongitude;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public FireHydrantObject(long fireHydrantId, String fireSerialNumber, String fireNumber, String fireHeight, String fireCommissionDate,
                             int fireDistrictName, String fireStreetName, String fireSectorName, int fireRemarks, String fireWithWater, int fireValveType,
                             int fireWaterSchedule, String fireLatitude, String fireLongitude, String reason, int fireBarrelDiameter, int fireMaterialId,
                             int fireTypeId, int fireStatus, int fireBrand, int fireMaintenanceArea, String createdAt) {
        this.fireHydrantId = fireHydrantId;
        this.fireSerialNumber = fireSerialNumber;
        this.fireNumber = fireNumber;
        this.fireHeight = fireHeight;
        this.fireCommissionDate = fireCommissionDate;
        this.fireDistrictName = fireDistrictName;
        this.fireStreetName = fireStreetName;
        this.fireSectorName = fireSectorName;
        this.fireRemarks = fireRemarks;
        this.fireWithWater = fireWithWater;
        this.fireValveType = fireValveType;
        this.fireWaterSchedule = fireWaterSchedule;
        this.fireLatitude = fireLatitude;
        this.fireLongitude = fireLongitude;
        this.reason = reason;
        this.fireBarrelDiameter = fireBarrelDiameter;
        this.fireMaterialId = fireMaterialId;
        this.fireTypeId = fireTypeId;
        this.fireStatus = fireStatus;
        this.fireBrand = fireBrand;
        this.fireMaintenanceArea = fireMaintenanceArea;
        this.createdAt = createdAt;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
