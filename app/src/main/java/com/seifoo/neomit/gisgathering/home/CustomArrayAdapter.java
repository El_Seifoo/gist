package com.seifoo.neomit.gisgathering.home;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.seifoo.neomit.gisgathering.R;

import java.util.ArrayList;

public class CustomArrayAdapter extends ArrayAdapter {
    private Context context;
    private ArrayList<Integer> imgs;
    private String[] titles;
    private ImageView imageView;
    private TextView title;

    public CustomArrayAdapter(Context context, ArrayList<Integer> imgs, String[] titles) {
        super(context, R.layout.grid_view_items, imgs);
        this.context = context;
        this.imgs = imgs;
        this.titles = titles;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        View newView = inflater.inflate(R.layout.grid_view_items, null);

        imageView = (ImageView) newView.findViewById(R.id.image);
        title = (TextView) newView.findViewById(R.id.title);

        imageView.setImageResource(imgs.get(position));
        title.setText(titles[position]);

        return newView;
    }
}
