package com.seifoo.neomit.gisgathering.home.fireHydrant.offline.offlineFiresDetails;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class OfflineFiresDetailsModel {
    public void returnValidGeoMaster(Context context, DBCallback callback, FireHydrantObject offlineMeterObj, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds(ids, types, lang), offlineMeterObj);
    }

    protected interface DBCallback {

        void onConvertingIdsCalled(ArrayList<String> strings, FireHydrantObject offlineMeterObj);
    }
}
