package com.seifoo.neomit.gisgathering.home.breaks.rejected;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.breaks.map.GetBreakMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.breaks.offline.OfflineBreaksAdapter;
import com.seifoo.neomit.gisgathering.home.breaks.rejected.edit.EditRejectedBreaksActivity;
import com.seifoo.neomit.gisgathering.home.breaks.rejected.map.RejectedBreaksMapActivity;

import java.util.ArrayList;

public class RejectedBreaksActivity extends AppCompatActivity implements RejectedBreaksMVP.View, OfflineBreaksAdapter.BreakObjectListItemListener {
    private static final int EDIT_REJECTED_BREAK_REQUEST = 1;
    private static final int SHOW_ALL_IN_MAP_REQUEST = 2;
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private OfflineBreaksAdapter adapter;
    private ProgressBar progressBar;
    private Button showLocations;

    private RejectedBreaksMVP.Presenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_breaks);

        getSupportActionBar().setTitle(getString(R.string.rejected_breaks));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new RejectedBreaksPresenter(this, new RejectedBreaksModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        showLocations = (Button) findViewById(R.id.show_locations);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.rejected_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineBreaksAdapter(this, false);

        presenter.requestBreaks();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyListText() {
        showLocations.setVisibility(View.GONE);
        emptyListTextView.setText(getString(R.string.no_valves_available));
        adapter.clear();
    }

    @Override
    public void loadRejectedBreaks(ArrayList<BreakObject> breaks) {
        emptyListTextView.setText("");
        adapter.setList(breaks);
        recyclerView.setAdapter(adapter);
        handleMapButton(breaks);
    }

    private void handleMapButton(final ArrayList<BreakObject> breaks) {
        for (int i = 0; i < breaks.size(); i++) {
            if (breaks.get(i).getLatitude() != null && breaks.get(i).getLongitude() != null) {
                if (!breaks.get(i).getLatitude().isEmpty() && !breaks.get(i).getLongitude().isEmpty()) {
                    showLocations.setVisibility(View.VISIBLE);
                    showLocations.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(RejectedBreaksActivity.this, RejectedBreaksMapActivity.class);
                            intent.putExtra("Breaks", breaks);
                            startActivityForResult(intent, SHOW_ALL_IN_MAP_REQUEST);
                        }
                    });
                    break;
                }
            }
        }
    }

    @Override
    public void onListItemClickListener(int viewId, int position, BreakObject breakObj) {
        switch (viewId) {
            case R.id.more_details:
                Intent intent = new Intent(this, EditRejectedBreaksActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("BreakObj", breakObj);
                startActivityForResult(intent, EDIT_REJECTED_BREAK_REQUEST);
                break;
            case R.id.map:
                Intent intent1 = new Intent(this, GetBreakMapDetailsActivity.class);
                intent1.putExtra("Break", breakObj);
                startActivityForResult(intent1, SHOW_ALL_IN_MAP_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_REJECTED_BREAK_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.removeItem(this, data.getExtras().getInt("position"));
            return;
        }
        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            presenter.requestBreaks();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
