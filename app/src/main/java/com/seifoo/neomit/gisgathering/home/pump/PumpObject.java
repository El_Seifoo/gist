package com.seifoo.neomit.gisgathering.home.pump;

import java.io.Serializable;

public class PumpObject implements Serializable {
    private long id, pumpId;
    private String latitude, longitude, code, stationName;
    private int enabled, type, assetStatus;
    private String streetName;
    private int districtName, subDistrict;
    private String dmaZone, sectorName, lastUpdated, remarks, reason, createdAt;

    public PumpObject(String latitude, String longitude, String code, String stationName, int enabled, int type, int assetStatus) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.code = code;
        this.stationName = stationName;
        this.enabled = enabled;
        this.type = type;
        this.assetStatus = assetStatus;
    }


    public PumpObject getFirstObject() {
        return new PumpObject(latitude, longitude, code, stationName, enabled, type, assetStatus);
    }

    public PumpObject(String streetName, int districtName, int subDistrict, String dmaZone,
                      String sectorName, String lastUpdated, String remarks) {
        this.streetName = streetName;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.dmaZone = dmaZone;
        this.sectorName = sectorName;
        this.lastUpdated = lastUpdated;
        this.remarks = remarks;
    }

    public PumpObject getSecondObject() {
        return new PumpObject(streetName, districtName, subDistrict, dmaZone,
                sectorName, lastUpdated, remarks);
    }


    public PumpObject(PumpObject firstStepObj, PumpObject secondStepObj) {
        this.latitude = firstStepObj.getLatitude();
        this.longitude = firstStepObj.getLongitude();
        this.code = firstStepObj.getCode();
        this.stationName = firstStepObj.getStationName();
        this.enabled = firstStepObj.getEnabled();
        this.type = firstStepObj.getType();
        this.assetStatus = firstStepObj.getAssetStatus();


        this.streetName = secondStepObj.getStreetName();
        this.districtName = secondStepObj.getDistrictName();
        this.subDistrict = secondStepObj.getSubDistrict();
        this.dmaZone = secondStepObj.getDmaZone();
        this.sectorName = secondStepObj.getSectorName();
        this.lastUpdated = secondStepObj.getLastUpdated();
        this.remarks = secondStepObj.getRemarks();


    }

    public PumpObject(long id, long pumpId, String latitude, String longitude, String code, String stationName,
                      int enabled, int type, int assetStatus, String streetName, int districtName, int subDistrict,
                      String dmaZone, String sectorName, String lastUpdated, String remarks, String createdAt) {
        this.id = id;
        this.pumpId = pumpId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.code = code;
        this.stationName = stationName;
        this.enabled = enabled;
        this.type = type;
        this.assetStatus = assetStatus;
        this.streetName = streetName;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.dmaZone = dmaZone;
        this.sectorName = sectorName;
        this.lastUpdated = lastUpdated;
        this.remarks = remarks;
        this.createdAt = createdAt;
    }

    public PumpObject(String latitude, String longitude, String code, String stationName, int enabled, int type,
                      int assetStatus, String streetName, int districtName, int subDistrict, String dmaZone,
                      String sectorName, String lastUpdated, String remarks, String createdAt) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.code = code;
        this.stationName = stationName;
        this.enabled = enabled;
        this.type = type;
        this.assetStatus = assetStatus;
        this.streetName = streetName;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.dmaZone = dmaZone;
        this.sectorName = sectorName;
        this.lastUpdated = lastUpdated;
        this.remarks = remarks;
        this.createdAt = createdAt;
    }

    public PumpObject(long pumpId, String latitude, String longitude, String code, String stationName,
                      int enabled, int type, int assetStatus, String streetName, int districtName,
                      int subDistrict, String dmaZone, String sectorName,
                      String lastUpdated, String remarks, String reason, String createdAt) {
        this.pumpId = pumpId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.code = code;
        this.stationName = stationName;
        this.enabled = enabled;
        this.type = type;
        this.assetStatus = assetStatus;
        this.streetName = streetName;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.dmaZone = dmaZone;
        this.sectorName = sectorName;
        this.lastUpdated = lastUpdated;
        this.remarks = remarks;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public long getPumpId() {
        return pumpId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getCode() {
        return code;
    }

    public String getStationName() {
        return stationName;
    }

    public int getEnabled() {
        return enabled;
    }

    public int getType() {
        return type;
    }

    public int getAssetStatus() {
        return assetStatus;
    }

    public String getStreetName() {
        return streetName;
    }

    public int getDistrictName() {
        return districtName;
    }

    public int getSubDistrict() {
        return subDistrict;
    }

    public String getDmaZone() {
        return dmaZone;
    }

    public String getSectorName() {
        return sectorName;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getReason() {
        return reason;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPumpId(long pumpId) {
        this.pumpId = pumpId;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
