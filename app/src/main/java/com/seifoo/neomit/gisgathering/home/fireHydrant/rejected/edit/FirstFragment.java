package com.seifoo.neomit.gisgathering.home.fireHydrant.rejected.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class FirstFragment extends Fragment implements Step, EditRejectedFiresMVP.View, EditRejectedFiresMVP.FirstView {

    private EditText locationEditText, serialNumberEditText, FHEditText, heightEditText, commissionDateEditText;
    private Spinner barrelDiameterSpinner, materialSpinner, typeSpinner, districtNameSpinner;

    private EditRejectedFiresMVP.Presenter presenter;

    public static FirstFragment newInstance(FireHydrantObject fireObj) {
        FirstFragment fragment = new FirstFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("fireObj", fireObj);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fire_first_edit, container, false);

        presenter = new EditRejectedFiresPresenter(this, this, new EditRejectedFiresModel());

        locationEditText = (EditText) view.findViewById(R.id.location);
        serialNumberEditText = (EditText) view.findViewById(R.id.serial_num);
        FHEditText = (EditText) view.findViewById(R.id.fh_num);
        heightEditText = (EditText) view.findViewById(R.id.height);
        commissionDateEditText = (EditText) view.findViewById(R.id.commission_date);

        barrelDiameterSpinner = (Spinner) view.findViewById(R.id.barrel_diameter_spinner);
        materialSpinner = (Spinner) view.findViewById(R.id.material_spinner);
        typeSpinner = (Spinner) view.findViewById(R.id.type_spinner);
        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(locationEditText.getText().toString().trim());
            }
        });

        commissionDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestDatePickerDialog(commissionDateEditText.getText().toString().trim());
            }
        });

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.serial_num_qr)));
        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.fh_num_qr)));

        presenter.requestFirstStepData((FireHydrantObject) getArguments().getSerializable("fireObj"));


        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFirstStepDataToActivity(new FireHydrantObject(locationEditText.getText().toString().trim(), locationEditText.getText().toString().trim(),
                serialNumberEditText.getText().toString().trim(), FHEditText.getText().toString().trim(), heightEditText.getText().toString().trim(),
                barrelDiameterIds.get(barrelDiameterSpinner.getSelectedItemPosition()), materialIds.get(materialSpinner.getSelectedItemPosition()),
                typeIds.get(typeSpinner.getSelectedItemPosition()), commissionDateEditText.getText().toString().trim(), districtsNamesIds.get(districtNameSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> barrelDiameterIds, materialIds, typeIds, districtsNamesIds;

    @Override
    public void loadSpinnersData(ArrayList<String> barrelDiameter, ArrayList<Integer> barrelDiameterIds, int barrelDiameterIndex, ArrayList<String> material, ArrayList<Integer> materialIds, int materialIndex, ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex, ArrayList<String> districtsNames, ArrayList<Integer> districtsNamesIds, int districtsNamesIndex) {
        this.barrelDiameterIds = barrelDiameterIds;
        this.materialIds = materialIds;
        this.typeIds = typeIds;
        this.districtsNamesIds = districtsNamesIds;

        barrelDiameterSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, barrelDiameter));
        barrelDiameterSpinner.setSelection(barrelDiameterIndex);
        materialSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, material));
        materialSpinner.setSelection(materialIndex);
        typeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, type));
        typeSpinner.setSelection(typeIndex);
        districtNameSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, districtsNames));
        districtNameSpinner.setSelection(districtsNamesIndex);
    }

    @Override
    public void showLocationError(String errorMessage) {
        locationEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((EditRejectedFiresActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator, int flag) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }

    @Override
    public void setFHNumber(String data) {
        FHEditText.setText(data);
    }

    @Override
    public void setCommissionDate(String date) {
        commissionDateEditText.setText(date);
    }

    @Override
    public void setHeight(String height) {
        heightEditText.setText(height);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void setLocation(String location) {
        locationEditText.setText(location);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFirstViewActivityResult(requestCode, resultCode, data);
    }
}
