package com.seifoo.neomit.gisgathering.home.sensors.offline.details;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.meter.offline.offlineMeterDetails.OfflineDetailsAdapter;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;

import java.util.ArrayList;

public class OfflineSensorsDetailsActivity extends AppCompatActivity implements OfflineSensorsDetailsMVP.View {
    private RecyclerView recyclerView;
    private OfflineDetailsAdapter adapter;

    private OfflineSensorsDetailsMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_sensors_details);

        getSupportActionBar().setTitle(getString(R.string.details));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new OfflineSensorsDetailsPresenter(this, new OfflineSensorsDetailsModel());

        recyclerView = (RecyclerView) findViewById(R.id.offline_details_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineDetailsAdapter(true);

        presenter.requestSensorDetails((SensorObject) getIntent().getExtras().getSerializable("OfflineSensorObj"));
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void loadSensorMoreDetails(ArrayList<MoreDetails> list) {
        adapter.setDetailsList(list);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
