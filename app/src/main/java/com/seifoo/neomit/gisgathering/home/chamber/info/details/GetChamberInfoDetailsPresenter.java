package com.seifoo.neomit.gisgathering.home.chamber.info.details;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class GetChamberInfoDetailsPresenter implements GetChamberInfoDetailsMVP.Presenter, GetChamberInfoDetailsModel.DBCallback {
    private GetChamberInfoDetailsMVP.View view;
    private GetChamberInfoDetailsModel model;

    public GetChamberInfoDetailsPresenter(GetChamberInfoDetailsMVP.View view, GetChamberInfoDetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestChamberData(ChamberObject chamber) {
        // pipeMaterial 4, length 0 ,  width 0 ,  depth 0 , diameter 0 ,  depthUnderPipe 0 , depthAbovePipe 0 ,  districtName 25
        //  subDistrictName 28 , diameterAirValve 0 ,  diameterWAValve 0 , diameterISOValve 0 ,  diameterFlowMeter 0 , type 24,  remarks 5
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();


        ids.add(chamber.getPipeMaterial());
        types.add(4);
        ids.add(chamber.getDiameter());
        types.add(0);
        ids.add(chamber.getDistrictName());
        types.add(25);
        ids.add(chamber.getSubDistrictName());
        types.add(28);
        ids.add(chamber.getDiameterAirValve());
        types.add(0);
        ids.add(chamber.getDiameterWAValve());
        types.add(0);
        ids.add(chamber.getDiameterISOValve());
        types.add(0);
        ids.add(chamber.getDiameterFlowMeter());
        types.add(0);
        ids.add(chamber.getType());
        types.add(24);
        ids.add(chamber.getRemarks());
        types.add(5);

        model.returnValidGeoMaster(view.getAppContext(), this, chamber, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));
    }

    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, ChamberObject chamber) {
        // 0...0 , 4  ,  5 , 24 , 25 , 28
        //  0-9  , 10 , 11 , 12 , 13 , 14
        ArrayList<MoreDetails> moreDetails = new ArrayList<>();

        if (!chamber.getLatitude().isEmpty() && !chamber.getLongitude().isEmpty())
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), chamber.getLatitude() + "," + chamber.getLongitude()));
        else
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), ""));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.pipe_material), strings.get(0)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.ch_num), chamber.getChNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.length), chamber.getLength()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.width), chamber.getWidth()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.depth), chamber.getDepth()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.diameter), strings.get(1)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.depth_under_pipe), chamber.getDepthUnderPipe()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.depth_above_pipe), chamber.getDepthAbovePipe()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(2)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.dma_zone), chamber.getDmaZone()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sub_district), strings.get(3)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_name), chamber.getStreetName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_num), chamber.getStreetNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sub_name), chamber.getSubName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sector_name), chamber.getSectorName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.diameter_air_valve), strings.get(4)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.diameter_wa_valve), strings.get(5)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.diameter_iso_valve), strings.get(6)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.diameter_flow_meter), strings.get(7)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.image_num), chamber.getImageNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.type), strings.get(8)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.last_updated), chamber.getUpdatedDate()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.remarks), strings.get(9)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.created_at), chamber.getCreatedAt()));

        view.loadChamberMoreDetails(moreDetails);
    }
}
