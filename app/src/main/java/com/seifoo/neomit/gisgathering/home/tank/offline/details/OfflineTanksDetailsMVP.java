package com.seifoo.neomit.gisgathering.home.tank.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;

import java.util.ArrayList;

public interface OfflineTanksDetailsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();


        void loadTanksMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestTanksDetails(TankObject tank);
    }
}
