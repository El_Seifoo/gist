package com.seifoo.neomit.gisgathering.home.pump.add;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class AddPumpPresenter implements AddPumpMVP.Presenter, AddPumpModel.DBCallback {
    private AddPumpMVP.View view;
    private AddPumpMVP.MainView mainView;
    private AddPumpMVP.FirstView firstView;
    private AddPumpMVP.SecondView secondView;
    private AddPumpModel model;

    public AddPumpPresenter(AddPumpMVP.View view, AddPumpMVP.MainView mainView, AddPumpModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public AddPumpPresenter(AddPumpMVP.View view, AddPumpMVP.FirstView firstView, AddPumpModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public AddPumpPresenter(AddPumpMVP.View view, AddPumpMVP.SecondView secondView, AddPumpModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    /*
        0 -> meter diameter , 1 -> meter brand , 2 - > meter type , 3 -> meter status , 4 -> meter material , 5 -> meter remarks
        6 -> read type , 7 -> pipe after meter , 8 -> pipe size , 9 -> maintenance area , 10 -> meter box Position , 11 -> meter box type
        12 -> cover status , 13 -> location , 14 -> building usage , 15 -> sub-building type , 16 -> water schedule , 17 -> water connection type
        18 -> pipe material , 19 -> new meter brand , 20 ->pump type  , 21 -> pump status , 22 -> yes_no , 23 -> reducer diameter
        24 -> FireType , 25 -> districts , 26 -> houseConnectionType , 27 -> pumpJob , 28 -> subDistrict , 29 -> mainLineType
     */
    @Override
    public void requestFirstSpinnersData() {
        //  enabled 22,   type 24,    assetStatus 3,
        model.getGeoMasters(view.getAppContext(), this, new int[]{22, 24, 3}, 1);
    }

    @Override
    public void requestSecondSpinnersData() {
        // districtName 25, subDistrict 28,
        model.getGeoMasters(view.getAppContext(), this, new int[]{25, 28}, 2);
    }

    @Override
    public void requestPassFirstStepDataToActivity(PumpObject pump) {
        if (!pump.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        pump.setLatitude(pump.getLatitude().split(",")[0]);
        pump.setLongitude(pump.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof AddPumpActivity) {
            ((AddPumpActivity) view.getAppContext()).passFirstObj(pump);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(PumpObject pump) {
        if (view.getAppContext() instanceof AddPumpActivity) {
            ((AddPumpActivity) view.getAppContext()).passSecondObj(pump);
        }
    }

    @Override
    public void requestAddPump(PumpObject pump) {
        pump.setCreatedAt(returnValidNumbers(mainView.getCurrentDate()));
        model.insertPump(view.getAppContext(), this, pump);
    }

    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }


    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQrCode(((AddPumpActivity) view.getAppContext()));
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        firstView.initializeScanner(integrator);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents());
        }
    }

    @Override
    public void handleQRScannerResult(String contents) {
        firstView.setStationCode(contents.replaceAll("[^0-9]", ""));
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getPumpsLocation(view.getAppContext(), this, prevLatLng);
    }

    @Override
    public void requestDatePickerDialog(String dateString) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        view.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        secondView.setLastUpdated(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();

    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index) {
        if (index == 1) handleFirstView(masters);
        else if (index == 2) handleSecondView(masters);
    }

    private void handleFirstView(ArrayList<GeoMasterObj> masters) {
        ArrayList<String>
                enabled = new ArrayList<>(),
                type = new ArrayList<>(),
                assetStatus = new ArrayList<>();

        ArrayList<Integer>
                enabledIds = new ArrayList<>(),
                typeIds = new ArrayList<>(),
                assetStatusIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 22) {
                enabled.add(masters.get(i).getName());
                enabledIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 24) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 3) {
                assetStatus.add(masters.get(i).getName());
                assetStatusIds.add(masters.get(i).getGeoMasterId());
            }
        }

        firstView.loadSpinnersData(enabled, enabledIds,
                type, typeIds,
                assetStatus, assetStatusIds);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> masters) {
        ArrayList<String>
                districtName = new ArrayList<>(),
                subDistrict = new ArrayList<>();

        ArrayList<Integer>
                districtNameIds = new ArrayList<>(),
                subDistrictIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 25) {
                districtName.add(masters.get(i).getName());
                districtNameIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 28) {
                subDistrict.add(masters.get(i).getName());
                subDistrictIds.add(masters.get(i).getGeoMasterId());
            }
        }

        secondView.loadSpinnersData(districtName, districtNameIds,
                subDistrict, subDistrictIds);
    }

    @Override
    public void onPumpInsertionCalled(long flag) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.add_pump_done_successfully));
            mainView.backToParent();
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_save_pump));
        }
    }

    @Override
    public void onGetPumpsLocationCalled(ArrayList<HashMap<String, String>> pumpsLocation, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], pumpsLocation, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", pumpsLocation, PICK_LOCATION_REQUEST);
        }
    }
}
