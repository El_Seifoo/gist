package com.seifoo.neomit.gisgathering.home.breaks.add;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.HashMap;

public class FirstFragment extends Fragment implements Step, AddBreakMVP.View, AddBreakMVP.FirstView {
    private EditText latLngEditText, breakNumberEditText, buildingNumberEditText, pipeNumberEditText, streetNameEditText, breakDateEditText;
    private Spinner pipeTypeSpinner, districtNameSpinner, subZoneSpinner, diameterSpinner;

    private AddBreakMVP.Presenter presenter;

    public static FirstFragment newInstance() {
        return new FirstFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_break_first, container, false);

        presenter = new AddBreakPresenter(this, this, new AddBreakModel());


        pipeTypeSpinner = (Spinner) view.findViewById(R.id.pipe_type_spinner);
        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);
        subZoneSpinner = (Spinner) view.findViewById(R.id.sub_district_spinner);
        diameterSpinner = (Spinner) view.findViewById(R.id.diameter_spinner);


        latLngEditText = (EditText) view.findViewById(R.id.location);
        breakNumberEditText = (EditText) view.findViewById(R.id.break_number);
        buildingNumberEditText = (EditText) view.findViewById(R.id.building_num);
        pipeNumberEditText = (EditText) view.findViewById(R.id.pipe_number);
        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        breakDateEditText = (EditText) view.findViewById(R.id.break_date);

        breakDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(breakDateEditText.getText().toString().trim());

            }
        });

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(latLngEditText.getText().toString().trim());
            }
        });


        presenter.requestFirstSpinnersData();

        return view;
    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFirstStepDataToActivity(new BreakObject(
                latLngEditText.getText().toString().trim(), latLngEditText.getText().toString().trim(),
                breakNumberEditText.getText().toString().trim(),
                pipeTypeIds.get(pipeTypeSpinner.getSelectedItemPosition()), districtNameIds.get(districtNameSpinner.getSelectedItemPosition()),
                subZoneIds.get(subZoneSpinner.getSelectedItemPosition()), buildingNumberEditText.getText().toString().trim(),
                pipeNumberEditText.getText().toString().trim(), streetNameEditText.getText().toString().trim(),
                breakDateEditText.getText().toString().trim(), diameterIds.get(diameterSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    ArrayList<Integer> pipeTypeIds, districtNameIds, subZoneIds, diameterIds;

    @Override
    public void loadSpinnersData(ArrayList<String> pipeType, ArrayList<Integer> pipeTypeIds, ArrayList<String> districtName, ArrayList<Integer> districtNameIds, ArrayList<String> subZone, ArrayList<Integer> subZoneIds, ArrayList<String> diameter, ArrayList<Integer> diameterIds) {
        this.pipeTypeIds = pipeTypeIds;
        this.districtNameIds = districtNameIds;
        this.subZoneIds = subZoneIds;
        this.diameterIds = diameterIds;


        pipeTypeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, pipeType));
        districtNameSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, districtName));
        subZoneSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, subZone));
        diameterSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameter));
    }

    @Override
    public void showLocationError(String errorMessage) {
        latLngEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((AddBreakActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("latLng", latLngs);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void setLocation(String location) {
        latLngEditText.setText(location);
    }

    @Override
    public void setBreakDate(String breakDate) {
        breakDateEditText.setText(breakDate);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }
}
