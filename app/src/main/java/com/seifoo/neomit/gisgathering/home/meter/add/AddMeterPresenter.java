package com.seifoo.neomit.gisgathering.home.meter.add;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;

import androidx.core.app.ActivityCompat;

import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class AddMeterPresenter implements AddMeterMVP.Presenter, AddMeterModel.DBCallback {
    private static final int PICK_LOCATION_REQUEST = 5050;
    private AddMeterMVP.MainView mainView;
    private AddMeterMVP.FirstView firstView;
    private AddMeterMVP.SecondView secondView;
    private AddMeterMVP.ThirdView thirdView;
    private AddMeterMVP.FourthView fourthView;
    private AddMeterMVP.FifthView fifthView;
    private AddMeterModel model;

    /*
        0 -> meter diameter , 1 -> meter brand , 2 - > meter type , 3 -> meter status , 4 -> meter material , 5 -> meter remarks
        6 -> read type , 7 -> pipe after meter , 8 -> pipe size , 9 -> maintenance area , 10 -> meter box Position , 11 -> meter box type
        12 -> cover status , 13 -> location , 14 -> building usage , 15 -> sub-building type , 16 -> water schedule , 17 -> water connection type
        18 -> pipe material , 19 -> new meter brand , 20 ->valve type  , 21 -> valve status , 22 -> yes_no , 23 -> reducer diameter
        24 -> FireType , 25 -> districts , 26 -> houseConnectionType , 27 -> valveJob
     */
    @Override
    public void requestFirstSpinnersData() {
        //meterDiameterSpinner 0, meterBrandSpinner 1, meterTypeSpinner 2, meterStatusSpinner 3, meterMaterialSpinner 4, meterRemarksSpinner 5, readTypeSpinner 6
        model.getGeoMasters(mainView.getAppContext(), this, new int[]{0, 1, 2, 3, 4, 5, 6}, 1);
    }

    @Override
    public void requestSecondSpinnersData() {
        //pipeAfterMeterSpinner 7, pipeSizeSpinner 8, meterMaintenanceAreaSpinner 9, meterBoxPositionSpinner 10, meterBoxTypeSpinner 11,
        //            coverStatusSpinner 12, locationSpinner 13, buildingUsageSpinner 14, waterConnectionTypeSpinner 17, pipeMaterialSpinner 18
        model.getGeoMasters(mainView.getAppContext(), this, new int[]{7, 8, 9, 10, 11, 12, 13, 14, 17, 18}, 2);
    }

    @Override
    public void requestThirdSpinnersData() {
        //valveTypeSpinner 20, valveStatusSpinner 21, enabledSpinner 22, reducerDiameterSpinner 23
        model.getGeoMasters(mainView.getAppContext(), this, new int[]{20, 21, 22, 23}, 3);
    }

    @Override
    public void requestFourthSpinnersData() {
        //subBuildingTypeSpinner 15, waterScheduleSpinner 16, newMeterBrandSpinner 19, districtNameSpinner 25
        model.getGeoMasters(mainView.getAppContext(), this, new int[]{15, 16, 19, 25}, 4);
    }

    @Override
    public void requestPassFirstStepDataToActivity(MeterObject meterObject) {
        if (meterObject.getMeterLatitude() == null) {
            mainView.showToastMessage(mainView.getAppContext().getString(R.string.location_is_required));
            firstView.showLocationError(mainView.getAppContext().getString(R.string.location_is_required));
            return;
        }
        if (mainView.getAppContext() instanceof AddMeterActivity) {
            ((AddMeterActivity) mainView.getAppContext()).passFirstStepData(meterObject);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(MeterObject meterObject) {
        if (mainView.getAppContext() instanceof AddMeterActivity) {
            ((AddMeterActivity) mainView.getAppContext()).passSecondStepData(meterObject);
        }
    }

    @Override
    public void requestPassThirdStepDataToActivity(MeterObject meterObject) {
        if (mainView.getAppContext() instanceof AddMeterActivity) {
            ((AddMeterActivity) mainView.getAppContext()).passThirdStepData(meterObject);
        }
    }

    @Override
    public void requestPassFourthStepDataToActivity(MeterObject meterObject) {
        if (mainView.getAppContext() instanceof AddMeterActivity) {
            ((AddMeterActivity) mainView.getAppContext()).passFourthStepData(meterObject);
        }
    }

    @Override
    public void requestPassFifthStepDataToActivity(MeterObject meterObject) {
        if (mainView.getAppContext() instanceof AddMeterActivity) {
            ((AddMeterActivity) mainView.getAppContext()).passFifthStepData(meterObject);
        }
    }

    @Override
    public void requestAddMeterObject(MeterObject meterObject) {
        meterObject.setCreatedAt(returnValidNumbers(mainView.getCurrentDate()));
        model.insertMeterObj(mainView.getAppContext(), this, meterObject);
    }

    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.serial_num_qr:
                        requestQrCode(((AddMeterActivity) mainView.getAppContext()), mainView.getAppContext().getString(R.string.serial_number));
                        break;
                    case R.id.hcn_qr:
                        requestQrCode(((AddMeterActivity) mainView.getAppContext()), mainView.getAppContext().getString(R.string.hcn));
                        break;
                    case R.id.meter_address_qr:
                        requestQrCode(((AddMeterActivity) mainView.getAppContext()), mainView.getAppContext().getString(R.string.meter_address));
                        break;
                    case R.id.plate_num_qr:
                        requestQrCode(((AddMeterActivity) mainView.getAppContext()), mainView.getAppContext().getString(R.string.plate_number));
                        break;
                }
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext, String flagString) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (flagString.equals(mainView.getAppContext().getString(R.string.hcn))) {
            flag = 1;
        } else if (flagString.equals(mainView.getAppContext().getString(R.string.meter_address))) {
            flag = 2;
        } else if (flagString.equals(mainView.getAppContext().getString(R.string.plate_number))) {
            flag = 3;
        }

        if (flag == 0 || flag == 3) {
            firstView.initializeScanner(integrator, flag);
        } else {
            fourthView.initializeScanner(integrator, flag);
        }
    }

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void onFourthViewActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void handleQRScannerResult(String contents, int flag) {
        contents = contents.replaceAll("[^0-9]", "");
        if (flag == 0)
            firstView.setSerialNumber(contents);
        else if (flag == 1)
            fourthView.setHCN(contents);
        else if (flag == 2)
            fourthView.setMeterAddress(contents);
        else
            firstView.setPlateNumber(contents);

    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getMetersLocation(mainView.getAppContext(), this, prevLatLng);

    }


    @Override
    public void requestDatePickerDialog(String dateString) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        mainView.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        thirdView.setDate(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();

    }


    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> geoMastersList, int index) {
        if (index == 1) handleFirstView(geoMastersList);
        else if (index == 2) handleSecondView(geoMastersList);
        else if (index == 3) handleThirdView(geoMastersList);
        else if (index == 4) handleFourthView(geoMastersList);
    }

    @Override
    public void onMeterInsertionCalled(long flag) {
        if (flag > 0) {
            mainView.showToastMessage(mainView.getAppContext().getString(R.string.done_successfully));
            mainView.backToParent();
        } else {
            mainView.showToastMessage(mainView.getAppContext().getString(R.string.failed_to_save));
        }
    }

    @Override
    public void onGetMetersLocationCalled(ArrayList<HashMap<String, String>> metersLocation, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], metersLocation, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", metersLocation, PICK_LOCATION_REQUEST);
        }
    }

    private void handleFirstView(ArrayList<GeoMasterObj> geoMastersList) {
        ArrayList<String>
                meterDiameter = new ArrayList<>(),
                meterBrand = new ArrayList<>(),
                meterType = new ArrayList<>(),
                meterStatus = new ArrayList<>(),
                meterMaterial = new ArrayList<>(),
                meterRemarks = new ArrayList<>(),
                readType = new ArrayList<>();

        ArrayList<Integer>
                meterDiameterIds = new ArrayList<>(),
                meterBrandIds = new ArrayList<>(),
                meterTypeIds = new ArrayList<>(),
                meterStatusIds = new ArrayList<>(),
                meterMaterialIds = new ArrayList<>(),
                meterRemarksIds = new ArrayList<>(),
                readTypeIds = new ArrayList<>();

        //meterDiameterSpinner 0, meterBrandSpinner 1, meterTypeSpinner 2, meterStatusSpinner 3,
        // meterMaterialSpinner 4, meterRemarksSpinner 5, readTypeSpinner 6
        for (int i = 0; i < geoMastersList.size(); i++) {
            if (geoMastersList.get(i).getType() == 0) {
                meterDiameter.add(geoMastersList.get(i).getName());
                meterDiameterIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 1) {
                meterBrand.add(geoMastersList.get(i).getName());
                meterBrandIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 2) {
                meterType.add(geoMastersList.get(i).getName());
                meterTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 3) {
                meterStatus.add(geoMastersList.get(i).getName());
                meterStatusIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 4) {
                meterMaterial.add(geoMastersList.get(i).getName());
                meterMaterialIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 5) {
                meterRemarks.add(geoMastersList.get(i).getName());
                meterRemarksIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 6) {
                readType.add(geoMastersList.get(i).getName());
                readTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            }
        }

        firstView.loadSpinnersData(meterDiameter, meterDiameterIds, meterBrand, meterBrandIds, meterType, meterTypeIds,
                meterStatus, meterStatusIds, meterMaterial, meterMaterialIds, meterRemarks, meterRemarksIds, readType, readTypeIds);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> geoMastersList) {

        ArrayList<String>
                pipeAfterMeter = new ArrayList<>(),
                pipeSize = new ArrayList<>(),
                meterMaintenanceArea = new ArrayList<>(),
                meterBoxPosition = new ArrayList<>(),
                meterBoxType = new ArrayList<>(),
                coverStatus = new ArrayList<>(),
                location = new ArrayList<>(),
                buildingUsage = new ArrayList<>(),
                waterConnectionType = new ArrayList<>(),
                pipeMaterial = new ArrayList<>();
        ArrayList<Integer>
                pipeAfterMeterIds = new ArrayList<>(),
                pipeSizeIds = new ArrayList<>(),
                meterMaintenanceAreaIds = new ArrayList<>(),
                meterBoxPositionIds = new ArrayList<>(),
                meterBoxTypeIds = new ArrayList<>(),
                coverStatusIds = new ArrayList<>(),
                locationIds = new ArrayList<>(),
                buildingUsageIds = new ArrayList<>(),
                waterConnectionTypeIds = new ArrayList<>(),
                pipeMaterialIds = new ArrayList<>();

        //pipeAfterMeterSpinner 7, pipeSizeSpinner 8, meterMaintenanceAreaSpinner 9, meterBoxPositionSpinner 10, meterBoxTypeSpinner 11,
        //            coverStatusSpinner 12, locationSpinner 13, buildingUsageSpinner 14, waterConnectionTypeSpinner 17, pipeMaterialSpinner 18
        for (int i = 0; i < geoMastersList.size(); i++) {
            if (geoMastersList.get(i).getType() == 7) {
                pipeAfterMeter.add(geoMastersList.get(i).getName());
                pipeAfterMeterIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 8) {
                pipeSize.add(geoMastersList.get(i).getName());
                pipeSizeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 9) {
                meterMaintenanceArea.add(geoMastersList.get(i).getName());
                meterMaintenanceAreaIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 10) {
                meterBoxPosition.add(geoMastersList.get(i).getName());
                meterBoxPositionIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 11) {
                meterBoxType.add(geoMastersList.get(i).getName());
                meterBoxTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 12) {
                coverStatus.add(geoMastersList.get(i).getName());
                coverStatusIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 13) {
                location.add(geoMastersList.get(i).getName());
                locationIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 14) {
                buildingUsage.add(geoMastersList.get(i).getName());
                buildingUsageIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 17) {
                waterConnectionType.add(geoMastersList.get(i).getName());
                waterConnectionTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 18) {
                pipeMaterial.add(geoMastersList.get(i).getName());
                pipeMaterialIds.add(geoMastersList.get(i).getGeoMasterId());
            }

        }

        secondView.loadSpinnersData(
                pipeAfterMeter, pipeAfterMeterIds,
                pipeSize, pipeSizeIds,
                meterMaintenanceArea, meterMaintenanceAreaIds,
                meterBoxPosition, meterBoxPositionIds,
                meterBoxType, meterBoxTypeIds,
                coverStatus, coverStatusIds,
                location, locationIds,
                buildingUsage, buildingUsageIds,
                waterConnectionType, waterConnectionTypeIds,
                pipeMaterial, pipeMaterialIds);
    }

    private void handleThirdView(ArrayList<GeoMasterObj> geoMastersList) {
        ArrayList<String>
                valveType = new ArrayList<>(),
                valveStatus = new ArrayList<>(),
                enabled = new ArrayList<>(),
                reducerDiameter = new ArrayList<>();
        ArrayList<Integer>
                valveTypeIds = new ArrayList<>(),
                valveStatusIds = new ArrayList<>(),
                enabledIds = new ArrayList<>(),
                reducerDiameterIds = new ArrayList<>();


        //valveTypeSpinner 20, valveStatusSpinner 21, enabledSpinner 22, reducerDiameterSpinner 23
        for (int i = 0; i < geoMastersList.size(); i++) {
            if (geoMastersList.get(i).getType() == 20) {
                valveType.add(geoMastersList.get(i).getName());
                valveTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 21) {
                valveStatus.add(geoMastersList.get(i).getName());
                valveStatusIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 22) {
                enabled.add(geoMastersList.get(i).getName());
                enabledIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 23) {
                reducerDiameter.add(geoMastersList.get(i).getName());
                reducerDiameterIds.add(geoMastersList.get(i).getGeoMasterId());
            }
        }

        thirdView.loadSpinnersData(valveType, valveTypeIds,
                valveStatus, valveStatusIds,
                enabled, enabledIds,
                reducerDiameter, reducerDiameterIds);
    }


    private void handleFourthView(ArrayList<GeoMasterObj> geoMastersList) {
        ArrayList<String>
                subBuildingType = new ArrayList<>(),
                waterSchedule = new ArrayList<>(),
                newMeterBrand = new ArrayList<>(),
                districtName = new ArrayList<>();
        ArrayList<Integer>
                subBuildingTypeIds = new ArrayList<>(),
                waterScheduleIds = new ArrayList<>(),
                newMeterBrandIds = new ArrayList<>(),
                districtNameIds = new ArrayList<>();

        //subBuildingTypeSpinner 15, waterScheduleSpinner 16, newMeterBrandSpinner 19, districtNameSpinner 25
        for (int i = 0; i < geoMastersList.size(); i++) {
            if (geoMastersList.get(i).getType() == 15) {
                subBuildingType.add(geoMastersList.get(i).getName());
                subBuildingTypeIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 16) {
                waterSchedule.add(geoMastersList.get(i).getName());
                waterScheduleIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 19) {
                newMeterBrand.add(geoMastersList.get(i).getName());
                newMeterBrandIds.add(geoMastersList.get(i).getGeoMasterId());
            } else if (geoMastersList.get(i).getType() == 25) {
                districtName.add(geoMastersList.get(i).getName());
                districtNameIds.add(geoMastersList.get(i).getGeoMasterId());
            }
        }

        fourthView.loadSpinnersData(subBuildingType, subBuildingTypeIds,
                waterSchedule, waterScheduleIds,
                newMeterBrand, newMeterBrandIds,
                districtName, districtNameIds);
    }

    protected AddMeterPresenter(AddMeterMVP.MainView mainView, AddMeterModel model) {
        this.mainView = mainView;
        this.model = model;
    }

    protected AddMeterPresenter(AddMeterMVP.MainView mainView, AddMeterMVP.FirstView firstView, AddMeterModel model) {
        this.mainView = mainView;
        this.firstView = firstView;
        this.model = model;
    }

    protected AddMeterPresenter(AddMeterMVP.MainView mainView, AddMeterMVP.SecondView secondView, AddMeterModel model) {
        this.mainView = mainView;
        this.secondView = secondView;
        this.model = model;
    }

    protected AddMeterPresenter(AddMeterMVP.MainView mainView, AddMeterMVP.ThirdView thirdView, AddMeterModel model) {

        this.mainView = mainView;
        this.thirdView = thirdView;
        this.model = model;
    }

    protected AddMeterPresenter(AddMeterMVP.MainView mainView, AddMeterMVP.FourthView fourthView, AddMeterModel model) {

        this.mainView = mainView;
        this.fourthView = fourthView;
        this.model = model;
    }

    protected AddMeterPresenter(AddMeterMVP.MainView mainView, AddMeterMVP.FifthView fifthView, AddMeterModel model) {
        this.mainView = mainView;
        this.fifthView = fifthView;
        this.model = model;
    }


}
