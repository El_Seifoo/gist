package com.seifoo.neomit.gisgathering.home.valve.add;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, AddValveMVP.View, AddValveMVP.SecondView {
    private Spinner coverStatusSpinner, enabledSpinner, districtsSpinner, subDistrictsSpinner, existInFieldSpinner, existInMapSpinner, waterScheduleSpinner, remarksSpinner;
    private EditText dmaZoneEditText, streetNameEditText, sectorNameEditText;

    private AddValveMVP.Presenter presenter;

    public static SecondFragment newInstance() {
        return new SecondFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_valve_second, container, false);

        presenter = new AddValvePresenter(this, this, new AddValveModel());

        coverStatusSpinner = (Spinner) view.findViewById(R.id.cover_status_spinner);
        enabledSpinner = (Spinner) view.findViewById(R.id.enabled_spinner);
        districtsSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);
        subDistrictsSpinner = (Spinner) view.findViewById(R.id.sub_district_spinner);
        existInFieldSpinner = (Spinner) view.findViewById(R.id.exist_in_field_spinner);
        existInMapSpinner = (Spinner) view.findViewById(R.id.exist_in_map_spinner);
        waterScheduleSpinner = (Spinner) view.findViewById(R.id.water_schedule_spinner);
        remarksSpinner = (Spinner) view.findViewById(R.id.remarks_spinner);

        dmaZoneEditText = (EditText) view.findViewById(R.id.dma_zone);
        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);

        presenter.requestSecondSpinnersData();

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new ValveObject(coverStatusIds.get(coverStatusSpinner.getSelectedItemPosition()),
                enabledIds.get(enabledSpinner.getSelectedItemPosition()), dmaZoneEditText.getText().toString().trim(),
                districtsIds.get(districtsSpinner.getSelectedItemPosition()), subDistrictsIds.get(subDistrictsSpinner.getSelectedItemPosition()),
                streetNameEditText.getText().toString().trim(), sectorNameEditText.getText().toString().trim(),
                existInFieldIds.get(existInFieldSpinner.getSelectedItemPosition()), existInMapIds.get(existInMapSpinner.getSelectedItemPosition()),
                waterScheduleIds.get(waterScheduleSpinner.getSelectedItemPosition()), remarksIds.get(remarksSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> coverStatusIds, enabledIds, districtsIds, subDistrictsIds, existInFieldIds, existInMapIds, waterScheduleIds, remarksIds;

    @Override
    public void loadSpinnersData(ArrayList<String> coverStatus, ArrayList<Integer> coverStatusIds,
                                 ArrayList<String> enabled, ArrayList<Integer> enabledIds,
                                 ArrayList<String> districts, ArrayList<Integer> districtsIds,
                                 ArrayList<String> subDistricts, ArrayList<Integer> subDistrictsIds,
                                 ArrayList<String> existInField, ArrayList<Integer> existInFieldIds,
                                 ArrayList<String> existInMap, ArrayList<Integer> existInMapIds,
                                 ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,
                                 ArrayList<String> remarks, ArrayList<Integer> remarksIds) {
        this.coverStatusIds = coverStatusIds;
        this.enabledIds = enabledIds;
        this.districtsIds = districtsIds;
        this.subDistrictsIds = subDistrictsIds;
        this.existInFieldIds = existInFieldIds;
        this.existInMapIds = existInMapIds;
        this.waterScheduleIds = waterScheduleIds;
        this.remarksIds = remarksIds;


        coverStatusSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, coverStatus));
        enabledSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, enabled));
        districtsSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, districts));
        subDistrictsSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, subDistricts));
        existInFieldSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, existInField));
        existInMapSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, existInMap));
        waterScheduleSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, waterSchedule));
        remarksSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, remarks));
    }
}
