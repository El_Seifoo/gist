package com.seifoo.neomit.gisgathering.home.pump.rejected.edit;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class EditRejectedPumpsModel {
    public void getGeoMasters(Context context, DBCallback callback, int[] types, int[] selectedIds, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))), selectedIds, index);
    }

    public void insertPumpObj(Context context, DBCallback callback, PumpObject pump) {
        callback.onPumpInsertionCalled(DataBaseHelper.getmInstance(context).insertPump(pump));
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index);

        void onPumpInsertionCalled(long flag);

    }
}
