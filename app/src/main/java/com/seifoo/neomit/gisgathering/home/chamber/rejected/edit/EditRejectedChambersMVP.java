package com.seifoo.neomit.gisgathering.home.chamber.rejected.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;

import java.util.ArrayList;

public interface EditRejectedChambersMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        void showToastMessage(String message);

        void backToParent();

        Context getActContext();
    }

    interface FirstView {
         void loadSpinnersData(ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds, int pipeMaterialIndex,
                              ArrayList<String> diameter, ArrayList<Integer> diameterIds, int diameterIndex,
                              ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator);

        void setCHN(String data);

        void setData(String length, String width, String depth, String depthUnderPipe, String depthAbovePipe, String dmaZone);

        void navigateToTheMap(String latitude, String longitude, int requestCode);

        void setLocation(String location);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> subDistrictName, ArrayList<Integer> subDistrictNameIds, int subDistrictNameIndex,
                              ArrayList<String> diameterAirValve, ArrayList<Integer> diameterAirValveIds, int diameterAirValveIndex,
                              ArrayList<String> diameterWAValve, ArrayList<Integer> diameterWAValveIds, int diameterWAValveIndex,
                              ArrayList<String> diameterISOValve, ArrayList<Integer> diameterISOValveIds, int diameterISOValveIndex,
                              ArrayList<String> diameterFlowMeter, ArrayList<Integer> diameterFlowMeterIds, int diameterFlowMeterIndex,
                              ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex,
                              ArrayList<String> remarks, ArrayList<Integer> remarksIds, int remarksIndex);

        void setLastUpdated(String date);

        void setData(String streetName, String streetNumber, String subName,String sectorName ,String imageNumber);
    }

    interface Presenter {
        void requestFirstSpinnersData(ChamberObject chamber);

        void requestSecondSpinnersData(ChamberObject chamber);

        void requestPassFirstStepDataToActivity(ChamberObject chamber);

        void requestPassSecondStepDataToActivity(ChamberObject chamber);

        void requestEditRejectedChamber(ChamberObject chamber, String createdAt, long id);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString);
    }
}
