package com.seifoo.neomit.gisgathering.home;

import android.content.Context;

import java.util.ArrayList;

public interface HomeMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void loadListData(ArrayList<Integer> imgs, String[] titles);

        void navigateToDestination(int position);

        void navigateLogin();
    }

    interface Presenter {

        void requestListData();

        void onItemClicked(int position);
    }
}
