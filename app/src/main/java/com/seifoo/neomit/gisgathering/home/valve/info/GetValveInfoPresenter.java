package com.seifoo.neomit.gisgathering.home.valve.info;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class GetValveInfoPresenter implements GetValveInfoMVP.Presenter, GetValveInfoModel.VolleyCallback {
    private GetValveInfoMVP.View view;
    private GetValveInfoModel model;

    public GetValveInfoPresenter(GetValveInfoMVP.View view, GetValveInfoModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestQrCode(Activity activity) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        view.initializeScanner(integrator);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                view.setSerialNumber(result.getContents().replaceAll("[^0-9]", ""));
        }
    }

    @Override
    public void requestGetInfo(String serialNumber) {
        if (serialNumber.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.serial_number_is_required));
            return;
        }
        view.showProgress();
        model.getInfo(view.getActContext(), this, serialNumber);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ValveObject valve = new ValveObject(
                        returnValidString(data.getJSONObject(0).getString("Y_MAP")),
                        returnValidString(data.getJSONObject(0).getString("X_MAP")),
                        returnValidInt(data.getJSONObject(0).getString("VALVE_JOB")),
                        returnValidString(data.getJSONObject(0).getString("SERIAL_No")),
                        returnValidInt(data.getJSONObject(0).getString("MATERIAL_ID")),
                        returnValidInt(data.getJSONObject(0).getString("DIAMETER_ID")),
                        returnValidInt(data.getJSONObject(0).getString("VALVE_TYPE")),
                        returnValidString(data.getJSONObject(0).getString("NO_OF_TURNS_FULL_OPEN_CLOSE")),
                        returnValidString(data.getJSONObject(0).getString("NO_OF_TURNS_OPEN")),
                        returnValidInt(data.getJSONObject(0).getString("LOCK")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("LOCK")),
                        returnValidInt(data.getJSONObject(0).getString("VALVE_STATUS_ID")),
                        returnValidString(data.getJSONObject(0).getString("Elevation")),
                        returnValidString(data.getJSONObject(0).getString("ground_Elevation")),
                        returnValidInt(data.getJSONObject(0).getString("COVER_STATUS_ID")),
                        returnValidInt(data.getJSONObject(0).getString("ENABLED_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("ENABLED_ID")),
                        returnValidString(data.getJSONObject(0).getString("DMA_ZONE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("DISTRICT_NAME_ID")),
                        returnValidInt(data.getJSONObject(0).getString("SUP_DISTRICT_ID")),
                        returnValidString(data.getJSONObject(0).getString("STREET_NAME")),
                        returnValidString(data.getJSONObject(0).getString("SECTOR_NAME")),
                        returnValidInt(data.getJSONObject(0).getString("EXIST_IN_FIELD_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("EXIST_IN_FIELD_ID")),
                        returnValidInt(data.getJSONObject(0).getString("EXIST_IN_MAP_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("EXIST_IN_MAP_ID")),
                        returnValidInt(data.getJSONObject(0).getString("WATER_SCHEDULE")),
                        returnValidInt(data.getJSONObject(0).getString("REMARKS")),
                        returnValidDate1(data.getJSONObject(0).getString("CreatedDate")));
                view.loadValveInfo(valve);
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_valve_not_exist));
        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_valve_not_exist));
    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
