package com.seifoo.neomit.gisgathering.home.meter.rejected;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.home.meter.getMap.GetMeterMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.meter.offline.OfflineMetersAdapter;
import com.seifoo.neomit.gisgathering.home.meter.rejected.edit.EditRejectedMeterActivity;
import com.seifoo.neomit.gisgathering.home.meter.rejected.rejectedMap.RejectedMapActivity;

import java.util.ArrayList;

public class RejectedMetersActivity extends AppCompatActivity implements RejectedMetersMVP.View, OfflineMetersAdapter.MeterObjectListItemListener {
    private static final int EDIT_REJECTED_METER_REQUEST = 1;
    private static final int SHOW_ALL_IN_MAP_REQUEST = 2;
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private OfflineMetersAdapter adapter;
    private ProgressBar progressBar;
    private Button showLocations;

    private RejectedMetersMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_meters);

        getSupportActionBar().setTitle(getString(R.string.rejected_meters));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new RejectedMetersPresenter(this, new RejectedMetersModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);


        showLocations = (Button) findViewById(R.id.show_locations);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.rejected_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineMetersAdapter(this, false);

        presenter.requestRejected();
    }


    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyListText() {
        showLocations.setVisibility(View.GONE);
        emptyListTextView.setText(getString(R.string.no_meters_available));
        adapter.clear();
    }

    @Override
    public void loadRejectedMeters(ArrayList<MeterObject> rejectedMeters) {
        emptyListTextView.setText("");
        adapter.setMetersList(rejectedMeters);
        recyclerView.setAdapter(adapter);
        handleMapButton(rejectedMeters);
    }

    private void handleMapButton(final ArrayList<MeterObject> meterObjects) {

        showLocations.setVisibility(View.VISIBLE);
        showLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RejectedMetersActivity.this, RejectedMapActivity.class);
                intent.putExtra("Meters", meterObjects);
                startActivityForResult(intent, SHOW_ALL_IN_MAP_REQUEST);
            }
        });
    }

    @Override
    public void onListItemClickListener(int viewId, int position, MeterObject meterObject) {
        switch (viewId) {
            case R.id.more_details:
                Intent intent = new Intent(this, EditRejectedMeterActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("meterObj", meterObject);
                startActivityForResult(intent, EDIT_REJECTED_METER_REQUEST);
                break;
            case R.id.map:
                Intent intent1 = new Intent(this, GetMeterMapDetailsActivity.class);
                intent1.putExtra("Meter", meterObject);
                startActivityForResult(intent1, SHOW_ALL_IN_MAP_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_REJECTED_METER_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.removeItem(this, data.getExtras().getInt("position"));
            return;
        }
        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            presenter.requestRejected();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
