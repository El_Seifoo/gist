package com.seifoo.neomit.gisgathering.home.mainLine.offline;

import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;

import java.util.ArrayList;

public interface OfflineMainLineMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void showToastMessage(String message);

        void loadMainLinesData(ArrayList<MainLineObject> mainLines, boolean isVisible);

        void removeListItem(int position);

        void navigateDestination(String key, MainLineObject mainLine, Class destination);

        void navigateToEditMainLine(Intent intent, int requestCode);

        void updateListItem(int position, MainLineObject mainLine);

        void removeListItem(long id);

        void navigateToMap(int RequestCode, MainLineObject mainLine, int position);

        int getSpinnerSelectedPosition();

        void navigateToMap2(int requestCode, ArrayList<MainLineObject> mainLines);
    }

    interface Presenter {
        void requestMainLinesData(int type);

        void requestRemoveMainLineObjById(long id, int position);

        void OnListItemClickListener(int viewId, int position, MainLineObject mainLine);

        void requestEditMainLineData(int position, MainLineObject mainLine);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestUploadData(int type);

        void showLocations(ArrayList<MainLineObject> mainLines);
    }
}
