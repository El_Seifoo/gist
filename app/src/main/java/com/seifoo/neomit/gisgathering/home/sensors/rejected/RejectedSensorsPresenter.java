package com.seifoo.neomit.gisgathering.home.sensors.rejected;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RejectedSensorsPresenter implements RejectedSensorsMVP.Presenter, RejectedSensorsModel.VolleyCallback, RejectedSensorsModel.DBCallback {
    private RejectedSensorsMVP.View view;
    private RejectedSensorsModel model;

    public RejectedSensorsPresenter(RejectedSensorsMVP.View view, RejectedSensorsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestSensors() {
        view.showProgress();
        model.getRejected(view.getAppContext(), this);
    }


    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ArrayList<SensorObject> rejectedSensors = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {
                    rejectedSensors.add(
                            new SensorObject(
                                    data.getJSONObject(i).getLong("Corrected_Id"),
                                    returnValidString(data.getJSONObject(0).getString("Y_MAP")),
                                    returnValidString(data.getJSONObject(0).getString("X_MAP")),
                                    returnValidString(data.getJSONObject(0).getString("SERIAL_NO")),
                                    returnValidString(data.getJSONObject(0).getString("ASSET_NO")),
                                    returnValidInt(data.getJSONObject(0).getString("DEVICE_TYPE_ID")),
                                    returnValidInt(data.getJSONObject(0).getString("ASSET_STATUS_ID")),
                                    returnValidString(data.getJSONObject(0).getString("ELEVATION")),
                                    returnValidString(data.getJSONObject(0).getString("CHAMBER_STATUS")),
                                    returnValidInt(data.getJSONObject(0).getString("F_TYPE_ID")),
                                    returnValidString(data.getJSONObject(0).getString("LINE_NO")),
                                    returnValidInt(data.getJSONObject(0).getString("MAINTENANCE_AREA_ID")),
                                    returnValidDate(data.getJSONObject(0).getString("COMMISSION_DATE")),
                                    returnValidInt(data.getJSONObject(0).getString("ENABLED_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("ENABLED_ID")),
                                    returnValidInt(data.getJSONObject(0).getString("DISTRICTS_NAME")),
                                    returnValidString(data.getJSONObject(0).getString("STREET_NAME")),
                                    returnValidString(data.getJSONObject(0).getString("DMA_ZONE_ID")),
                                    returnValidString(data.getJSONObject(0).getString("SECTOR_NAME")),
                                    returnValidInt(data.getJSONObject(0).getString("DIAMATER_ID")),
                                    returnValidInt(data.getJSONObject(0).getString("PIPE_MATERIAL_ID")),
                                    returnValidDate(data.getJSONObject(0).getString("LAST_UPDATED")),
                                    returnValidString(data.getJSONObject(i).getString("REASON")),
                                    returnValidDate1(data.getJSONObject(0).getString("CreatedDate"))));
//x -> longitude , y -> latitude
                }
                model.checkDatabaseSensors(view.getAppContext(), this, rejectedSensors);
            } else {
                view.hideProgress();
                view.showEmptyListText();
            }
        } else {
            view.hideProgress();
            view.showEmptyListText();
        }
    }

    private String returnValidString(String data) {
        return data.isEmpty() || data.toLowerCase().equals("null") ? "" : data;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onCheckingSensorsCalled(ArrayList<SensorObject> sensors) {
        view.hideProgress();
        if (sensors.isEmpty())
            view.showEmptyListText();
        else
            view.loadRejectedSensors(sensors);
    }
}
