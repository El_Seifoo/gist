package com.seifoo.neomit.gisgathering.home.sensors.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface EditSensorMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent(SensorObject sensor);
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> deviceType, ArrayList<Integer> deviceTypeIds, int deviceTypeIndex,
                              ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds, int assetStatusIndex,
                              ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex,
                              ArrayList<String> maintenanceArea, ArrayList<Integer> maintenanceAreaIds, int maintenanceAreaIndex);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator, int flag);

        void setSerialNumber(String serialNumber);

        void setAssetNumber(String assetNumber);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);

        void setData(String elevation, String chamberStatus, String lineNumber);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex,
                              ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex,
                              ArrayList<String> diameter, ArrayList<Integer> diameterIds, int diameterIndex,
                              ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds, int pipeMaterialIndex);

        void setCommissionDate(String commissionDate);

        void setLastUpdate(String lastUpdate);

        void setData(String streetName, String dmaZone, String sectorName);
    }

    interface Presenter {
        void requestFirstSpinnersData(SensorObject sensor);

        void requestSecondSpinnersData(SensorObject sensor);

        void requestPassFirstStepDataToActivity(SensorObject sensor);

        void requestPassSecondStepDataToActivity(SensorObject sensor);

        void requestEditSensor(SensorObject sensor, String createdAt, long id);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext, String flagString);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents, int flag);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString, int index);

    }
}
