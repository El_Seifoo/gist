package com.seifoo.neomit.gisgathering.login;

import android.content.Context;
import android.content.Intent;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {
    private BottomSheetBehavior bottomSheetBehavior;
    private LinearLayout bottomSheetContainer;
    private EditText username, password, ip;
    private ProgressBar progressBar;

    private LoginMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(this, new LoginModel());
        presenter.isUserLoggedIn();

    }

    public void onClick(View view) {
        presenter.onViewClicked(view);
        switch (view.getId()) {
            case R.id.bottom_sheet_title:
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                else if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
            case R.id.login_btn:

        }
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateHome(Class destination) {
        Intent homeIntent = new Intent(this, destination);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);
    }

    @Override
    public BottomSheetBehavior getBottomSheetBehavior() {
        return bottomSheetBehavior;
    }

    @Override
    public void setBottomSheetState(int state) {
        bottomSheetBehavior.setState(state);
    }

    @Override
    public String getUsername() {
        return username.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return password.getText().toString().trim();
    }

    @Override
    public String getIP() {
        return ip.getText().toString().trim();
    }

    @Override
    public void validateUserName(String message) {
        username.setError(message);
    }

    @Override
    public void validatePassword(String message) {
        password.setError(message);
    }

    @Override
    public void validateIp(String message) {
        ip.setError(message);
    }

    @Override
    public void initializeLoginViews() {
        bottomSheetContainer = (LinearLayout) findViewById(R.id.bottom_sheet_container);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetContainer);

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        username = (EditText) findViewById(R.id.username);
        username.setText(MySingleton.getmInstance(getActContext()).getStringSharedPref(Constants.USER_NAME, ""));
        password = (EditText) findViewById(R.id.password);
        ip = (EditText) findViewById(R.id.ip);
        ip.setText(MySingleton.getmInstance(getActContext()).getStringSharedPref(Constants.USER_IP, ""));
    }
}

