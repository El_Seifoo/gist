package com.seifoo.neomit.gisgathering.subHome;

import android.content.Context;
import android.view.MenuItem;

import com.seifoo.neomit.gisgathering.home.meter.add.AddMeterActivity;

import java.util.ArrayList;

public interface SubHomeMVP {
    interface View {

        Context getBContext();

        void loadListData(ArrayList<Integer> imgs, String[] titles);

        boolean backHome();

        void navigateToDestination(Class destination);


    }

    interface Presenter {

        void requestListData();

        void onItemClicked(int position, int whichFeature);

        boolean onOptionsItemSelected(MenuItem item);
    }
}
