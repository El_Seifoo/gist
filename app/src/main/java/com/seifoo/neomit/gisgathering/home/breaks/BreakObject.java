package com.seifoo.neomit.gisgathering.home.breaks;

import java.io.Serializable;
import java.util.Map;

public class BreakObject implements Serializable {
    private long id, breakId;
    private String latitude, longitude, breakNumber;
    private int pipeType, districtName, subZone;
    private String buildingNumber, pipeNumber, streetName, breakDate;
    private int diameter, pipeMaterial;
    private String breakDetails, usedEquipment;
    private int maintenanceCompany, maintenanceType, soilType, asphalt;
    private String dimensionDrilling;
    private int maintenanceDeviceType, procedure;
    private Map<String, String> photos;
    private String reason, createdAt;


    public BreakObject(String latitude, String longitude, String breakNumber, int pipeType, int districtName,
                       int subZone, String buildingNumber, String pipeNumber, String streetName, String breakDate, int diameter) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.breakNumber = breakNumber;
        this.pipeType = pipeType;
        this.districtName = districtName;
        this.subZone = subZone;
        this.buildingNumber = buildingNumber;
        this.pipeNumber = pipeNumber;
        this.streetName = streetName;
        this.breakDate = breakDate;
        this.diameter = diameter;
    }

    public BreakObject getFirstObject() {
        return new BreakObject(latitude, longitude, breakNumber, pipeType, districtName,
                subZone, buildingNumber, pipeNumber, streetName, breakDate, diameter);
    }

    public BreakObject(int pipeMaterial, String breakDetails, String usedEquipment, int maintenanceCompany, int maintenanceType,
                       int soilType, int asphalt, String dimensionDrilling, int maintenanceDeviceType, int procedure) {
        this.pipeMaterial = pipeMaterial;
        this.breakDetails = breakDetails;
        this.usedEquipment = usedEquipment;
        this.maintenanceCompany = maintenanceCompany;
        this.maintenanceType = maintenanceType;
        this.soilType = soilType;
        this.asphalt = asphalt;
        this.dimensionDrilling = dimensionDrilling;
        this.maintenanceDeviceType = maintenanceDeviceType;
        this.procedure = procedure;
    }

    public BreakObject getSecondObject() {
        return new BreakObject(pipeMaterial, breakDetails, usedEquipment, maintenanceCompany, maintenanceType,
                soilType, asphalt, dimensionDrilling, maintenanceDeviceType, procedure);
    }

    public BreakObject(Map<String, String> photos) {
        this.photos = photos;
    }

    public BreakObject getThirdObject() {
        return new BreakObject(photos);
    }


    public BreakObject(BreakObject firstObj, BreakObject secondOb, BreakObject thirdObj) {

        this.latitude = firstObj.getLatitude();
        this.longitude = firstObj.getLongitude();
        this.breakNumber = firstObj.getBreakNumber();
        this.pipeType = firstObj.getPipeType();
        this.districtName = firstObj.getDistrictName();
        this.subZone = firstObj.getSubZone();
        this.buildingNumber = firstObj.getBuildingNumber();
        this.pipeNumber = firstObj.getPipeNumber();
        this.streetName = firstObj.getStreetName();
        this.breakDate = firstObj.getBreakDate();
        this.diameter = firstObj.getDiameter();

        this.pipeMaterial = secondOb.getPipeMaterial();
        this.breakDetails = secondOb.getBreakDetails();
        this.usedEquipment = secondOb.getUsedEquipment();
        this.maintenanceCompany = secondOb.getMaintenanceCompany();
        this.maintenanceType = secondOb.getMaintenanceType();
        this.soilType = secondOb.getSoilType();
        this.asphalt = secondOb.getAsphalt();
        this.dimensionDrilling = secondOb.getDimensionDrilling();
        this.maintenanceDeviceType = secondOb.getMaintenanceDeviceType();
        this.procedure = secondOb.getProcedure();

        this.photos = thirdObj.getPhotos();
    }

    public BreakObject(long id, long breakId, String latitude, String longitude, String breakNumber, int pipeType,
                       int districtName, int subZone, String buildingNumber, String pipeNumber, String streetName,
                       String breakDate, int diameter, int pipeMaterial, String breakDetails, String usedEquipment,
                       int maintenanceCompany, int maintenanceType, int soilType, int asphalt, String dimensionDrilling,
                       int maintenanceDeviceType, int procedure, Map<String, String> photos, String createdAt) {
        this.id = id;
        this.breakId = breakId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.breakNumber = breakNumber;
        this.pipeType = pipeType;
        this.districtName = districtName;
        this.subZone = subZone;
        this.buildingNumber = buildingNumber;
        this.pipeNumber = pipeNumber;
        this.streetName = streetName;
        this.breakDate = breakDate;
        this.diameter = diameter;
        this.pipeMaterial = pipeMaterial;
        this.breakDetails = breakDetails;
        this.usedEquipment = usedEquipment;
        this.maintenanceCompany = maintenanceCompany;
        this.maintenanceType = maintenanceType;
        this.soilType = soilType;
        this.asphalt = asphalt;
        this.dimensionDrilling = dimensionDrilling;
        this.maintenanceDeviceType = maintenanceDeviceType;
        this.procedure = procedure;
        this.photos = photos;
        this.createdAt = createdAt;
    }

    public BreakObject(String latitude, String longitude, String breakNumber, int pipeType, int districtName, int subZone,
                       String buildingNumber, String pipeNumber, String streetName, String breakDate, int diameter, int pipeMaterial,
                       String breakDetails, String usedEquipment, int maintenanceCompany, int maintenanceType, int soilType, int asphalt,
                       String dimensionDrilling, int maintenanceDeviceType, int procedure, Map<String, String> photos, String createdAt) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.breakNumber = breakNumber;
        this.pipeType = pipeType;
        this.districtName = districtName;
        this.subZone = subZone;
        this.buildingNumber = buildingNumber;
        this.pipeNumber = pipeNumber;
        this.streetName = streetName;
        this.breakDate = breakDate;
        this.diameter = diameter;
        this.pipeMaterial = pipeMaterial;
        this.breakDetails = breakDetails;
        this.usedEquipment = usedEquipment;
        this.maintenanceCompany = maintenanceCompany;
        this.maintenanceType = maintenanceType;
        this.soilType = soilType;
        this.asphalt = asphalt;
        this.dimensionDrilling = dimensionDrilling;
        this.maintenanceDeviceType = maintenanceDeviceType;
        this.procedure = procedure;
        this.photos = photos;
        this.createdAt = createdAt;
    }

    public BreakObject(long breakId, String latitude, String longitude, String breakNumber, int pipeType, int districtName,
                       int subZone, String buildingNumber, String pipeNumber, String streetName, String breakDate, int diameter,
                       int pipeMaterial, String breakDetails, String usedEquipment, int maintenanceCompany, int maintenanceType,
                       int soilType, int asphalt, String dimensionDrilling, int maintenanceDeviceType, int procedure, Map<String, String> photos,
                       String reason, String createdAt) {
        this.breakId = breakId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.breakNumber = breakNumber;
        this.pipeType = pipeType;
        this.districtName = districtName;
        this.subZone = subZone;
        this.buildingNumber = buildingNumber;
        this.pipeNumber = pipeNumber;
        this.streetName = streetName;
        this.breakDate = breakDate;
        this.diameter = diameter;
        this.pipeMaterial = pipeMaterial;
        this.breakDetails = breakDetails;
        this.usedEquipment = usedEquipment;
        this.maintenanceCompany = maintenanceCompany;
        this.maintenanceType = maintenanceType;
        this.soilType = soilType;
        this.asphalt = asphalt;
        this.dimensionDrilling = dimensionDrilling;
        this.maintenanceDeviceType = maintenanceDeviceType;
        this.procedure = procedure;
        this.photos = photos;
        this.reason = reason;
        this.createdAt = createdAt;
    }


    public long getId() {
        return id;
    }

    public long getBreakId() {
        return breakId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getBreakNumber() {
        return breakNumber;
    }

    public int getPipeType() {
        return pipeType;
    }

    public int getDistrictName() {
        return districtName;
    }

    public int getSubZone() {
        return subZone;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public String getPipeNumber() {
        return pipeNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getBreakDate() {
        return breakDate;
    }

    public int getDiameter() {
        return diameter;
    }

    public int getPipeMaterial() {
        return pipeMaterial;
    }

    public String getBreakDetails() {
        return breakDetails;
    }

    public String getUsedEquipment() {
        return usedEquipment;
    }

    public int getMaintenanceCompany() {
        return maintenanceCompany;
    }

    public int getMaintenanceType() {
        return maintenanceType;
    }

    public int getSoilType() {
        return soilType;
    }

    public int getAsphalt() {
        return asphalt;
    }

    public String getDimensionDrilling() {
        return dimensionDrilling;
    }

    public int getMaintenanceDeviceType() {
        return maintenanceDeviceType;
    }

    public int getProcedure() {
        return procedure;
    }

    public Map<String, String> getPhotos() {
        return photos;
    }
    public String getReason() {
        return reason;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setBreakId(long breakId) {
        this.breakId = breakId;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
