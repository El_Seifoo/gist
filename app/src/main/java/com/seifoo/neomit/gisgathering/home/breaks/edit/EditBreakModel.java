package com.seifoo.neomit.gisgathering.home.breaks.edit;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class EditBreakModel {

    public void getGeoMasters(Context context, DBCallback callback, int[] types, int[] selectedIds, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))), selectedIds, index);
    }

    public void updateBreakObj(Context context, DBCallback callback, BreakObject breakObj) {
        callback.onBreakUpdatingCalled(DataBaseHelper.getmInstance(context).updateBreak(breakObj), breakObj);
    }

    protected void getBreaksLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetBreaksLocationCalled(DataBaseHelper.getmInstance(context).getBreaksLocation(), prevLatLng);
    }
    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index);

        void onBreakUpdatingCalled(int flag, BreakObject breakObj);

        void onGetBreaksLocationCalled(ArrayList<HashMap<String,String>> valvesLocation, String prevLatLng);
    }

}
