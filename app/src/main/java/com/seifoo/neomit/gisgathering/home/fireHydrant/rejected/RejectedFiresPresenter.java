package com.seifoo.neomit.gisgathering.home.fireHydrant.rejected;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RejectedFiresPresenter implements RejectedFiresMVP.Presenter, RejectedFiresModel.VolleyCallback, RejectedFiresModel.DBCallback {
    private RejectedFiresMVP.View view;
    private RejectedFiresModel model;

    public RejectedFiresPresenter(RejectedFiresMVP.View view, RejectedFiresModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestFires() {
        view.showProgress();
        model.getRejected(view.getAppContext(), this);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ArrayList<FireHydrantObject> rejectedFires = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {
                    /*
                    long fireHydrantId, String fireSerialNumber, String fireNumber, String fireHeight, String fireCommissionDate,
                             String fireDistrictName, String fireStreetName, String fireSectorName, int fireRemarks, String fireWithWater, int fireValveType,
                             int fireWaterSchedule, String fireLatitude, String fireLongitude, String reason, int fireBarrelDiameter, int fireMaterialId,
                             int fireTypeId, int fireStatus, int fireBrand, int fireMaintenanceArea
                     */
                    rejectedFires.add(new FireHydrantObject(data.getJSONObject(i).getLong("Corrected_Id"),
                            returnValidString(data.getJSONObject(i).getString("SERIAL_NO")),
                            returnValidString(data.getJSONObject(i).getString("FH_NUMBER")),
                            returnValidString(data.getJSONObject(i).getString("HEIGHT")),
                            returnValidDate(data.getJSONObject(i).getString("COMMISSION_DATE")),
                            returnValidInt(data.getJSONObject(i).getString("DISTRICT_NAME_ID")),
                            returnValidString(data.getJSONObject(i).getString("STREET_NAME")),
                            returnValidString(data.getJSONObject(i).getString("SECTOR_NAME")),
                            returnValidInt(data.getJSONObject(i).getString("REMARKS")),
                            returnValidString(data.getJSONObject(i).getString("WITH_WATER")),
                            returnValidInt(data.getJSONObject(i).getString("FEATURE_TYPE_VALVE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("WATER_SCHEDULE_ID")),
                            returnValidString(data.getJSONObject(i).getString("Y_MAP")),
                            returnValidString(data.getJSONObject(i).getString("X_MAP")),
                            returnValidString(data.getJSONObject(i).getString("Reason")),
                            returnValidInt(data.getJSONObject(i).getString("BARREL_DIAMETER_ID")),
                            returnValidInt(data.getJSONObject(i).getString("MATERIAL_ID")),
                            data.getJSONObject(i).getString("F_TYPE_ID").isEmpty() || data.getJSONObject(i).getString("F_TYPE_ID").toLowerCase().equals("null") ? 1 : data.getJSONObject(i).getInt("F_TYPE_ID"),
                            returnValidInt(data.getJSONObject(i).getString("FIREHYDRANT_STATUS_ID")),
                            returnValidInt(data.getJSONObject(i).getString("FIREHYDRANT_BRAND")),
                            returnValidInt(data.getJSONObject(i).getString("MAINTENANCE_AREA_ID")),
                            returnValidDate1(data.getJSONObject(i).getString("CreatedDate"))));
//x -> longitude , y -> latitude
                }
                model.checkDatabaseFires(view.getAppContext(), this, rejectedFires);
            } else {
                view.hideProgress();
                view.showEmptyListText();
            }
        } else {
            view.hideProgress();
            view.showEmptyListText();
        }
    }

    private String returnValidString(String data) {
        return data.isEmpty() || data.toLowerCase().equals("null") ? "" : data;
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }
    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onCheckingFiresCalled(ArrayList<FireHydrantObject> meterObjects) {
        view.hideProgress();
        if (meterObjects.isEmpty())
            view.showEmptyListText();
        else
            view.loadRejectedFires(meterObjects);
    }

}
