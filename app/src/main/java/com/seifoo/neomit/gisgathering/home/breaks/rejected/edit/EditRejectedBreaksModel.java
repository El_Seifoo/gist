package com.seifoo.neomit.gisgathering.home.breaks.rejected.edit;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class EditRejectedBreaksModel {
    public void getGeoMasters(Context context, DBCallback callback, int[] types, int[] selectedIds, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types,
                MySingleton.getmInstance(context).getStringSharedPref(Constants.APP_LANGUAGE, context.getString(R.string.default_language_value))), selectedIds, index);
    }

    public void insertValveObj(Context context, DBCallback callback, BreakObject breakObk) {
        callback.onBreakInsertionCalled(DataBaseHelper.getmInstance(context).insertBreak(breakObk));
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> geoMastersList, int[] selectedIds, int index);

        void onBreakInsertionCalled(long flag);
    }
}
