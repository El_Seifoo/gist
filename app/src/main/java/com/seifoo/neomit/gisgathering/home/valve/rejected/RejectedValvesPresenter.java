package com.seifoo.neomit.gisgathering.home.valve.rejected;


import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RejectedValvesPresenter implements RejectedValvesMVP.Presenter, RejectedValvesModel.VolleyCallback, RejectedValvesModel.DBCallback {
    private RejectedValvesMVP.View view;
    private RejectedValvesModel model;

    public RejectedValvesPresenter(RejectedValvesMVP.View view, RejectedValvesModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestValves() {
        view.showProgress();
        model.getRejected(view.getAppContext(), this);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ArrayList<ValveObject> rejectedValves = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {
                    rejectedValves.add(new ValveObject(
                            data.getJSONObject(i).getLong("Corrected_Id"),
                            returnValidString(data.getJSONObject(i).getString("Y_MAP")),
                            returnValidString(data.getJSONObject(i).getString("X_MAP")),
                            returnValidInt(data.getJSONObject(i).getString("VALVE_JOB")),
                            returnValidString(data.getJSONObject(i).getString("SERIAL_NO")),
                            returnValidInt(data.getJSONObject(i).getString("MATERIAL_ID")),
                            returnValidInt(data.getJSONObject(i).getString("DIAMETER_ID")),
                            returnValidInt(data.getJSONObject(i).getString("VALVE_TYPE")),
                            returnValidString(data.getJSONObject(i).getString("NO_OF_TURNS_FULL_OPEN_CLOSE")),
                            returnValidString(data.getJSONObject(i).getString("NO_OF_TURNS_OPEN")),
                            returnValidInt(data.getJSONObject(i).getString("LOCK")) == -1 ? 1 : returnValidInt(data.getJSONObject(i).getString("LOCK")),
                            returnValidInt(data.getJSONObject(i).getString("VALVE_STATUS_ID")),
                            returnValidString(data.getJSONObject(i).getString("Elevation")),
                            returnValidString(data.getJSONObject(i).getString("ground_Elevation")),
                            returnValidInt(data.getJSONObject(i).getString("COVER_STATUS_ID")),
                            returnValidInt(data.getJSONObject(i).getString("ENABLED_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(i).getString("ENABLED_ID")),
                            returnValidString(data.getJSONObject(i).getString("DMA_ZONE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("DISTRICT_NO_ID")),
                            returnValidInt(data.getJSONObject(i).getString("SUP_DISTRICT_ID")),
                            returnValidString(data.getJSONObject(i).getString("STREET_NAME")),
                            returnValidString(data.getJSONObject(i).getString("SECTOR_NAME")),
                            returnValidInt(data.getJSONObject(i).getString("EXIST_IN_FIELD_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(i).getString("EXIST_IN_FIELD_ID")),
                            returnValidInt(data.getJSONObject(i).getString("EXIST_IN_MAP_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(i).getString("EXIST_IN_MAP_ID")),
                            returnValidInt(data.getJSONObject(i).getString("WATER_SCHEDULE")),
                            returnValidInt(data.getJSONObject(i).getString("REMARKS")),
                            returnValidString(data.getJSONObject(i).getString("Reason")),
                            returnValidDate1(data.getJSONObject(i).getString("CreatedDate"))));
//x -> longitude , y -> latitude
                }
                model.checkDatabaseValves(view.getAppContext(), this, rejectedValves);
            } else {
                view.hideProgress();
                view.showEmptyListText();
            }
        } else {
            view.hideProgress();
            view.showEmptyListText();
        }
    }

    private String returnValidString(String data) {
        return data.isEmpty() || data.toLowerCase().equals("null") ? "" : data;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onCheckingValvesCalled(ArrayList<ValveObject> valves) {
        view.hideProgress();
        if (valves.isEmpty())
            view.showEmptyListText();
        else
            view.loadRejectedValves(valves);
    }
}
