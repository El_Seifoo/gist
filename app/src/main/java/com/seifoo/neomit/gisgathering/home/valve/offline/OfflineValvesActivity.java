package com.seifoo.neomit.gisgathering.home.valve.offline;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.home.valve.map.GetValveMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.valve.rejected.map.RejectedValvesMapActivity;

import java.util.ArrayList;

public class OfflineValvesActivity extends AppCompatActivity implements OfflineValvesMVP.View, OfflineValvesAdapter.ValveObjectListItemListener {
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private OfflineValvesAdapter adapter;
    private Button uploadButton;
    private ProgressBar progressBar;
    private Spinner spinner;
    private Button showLocations;

    private OfflineValvesMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_valves);

        getSupportActionBar().setTitle(getString(R.string.upload));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new OfflineValvesPresenter(this, new OfflineValvesModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        uploadButton = (Button) findViewById(R.id.upload_button);
        showLocations = (Button) findViewById(R.id.show_locations);
        showLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.showLocations(valves);
            }
        });

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.offline_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineValvesAdapter(this, true);

        spinner = (Spinner) findViewById(R.id.type_spinner);
        spinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.offline_types)));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                presenter.requestValvesData(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestUploadData(spinner.getSelectedItemPosition());
            }
        });

    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        uploadButton.setClickable(false);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        uploadButton.setClickable(true);
    }

    @Override
    public void showEmptyListText() {
        emptyListTextView.setText(getString(R.string.no_valves_available));
        adapter.clear();
        showLocations.setVisibility(View.GONE);
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private ArrayList<ValveObject> valves;
    @Override
    public void loadValvesData(ArrayList<ValveObject> valve, boolean isVisible) {
        this.valves = valve;
        emptyListTextView.setText("");
        adapter.setList(valve);
        recyclerView.setAdapter(adapter);
        showLocations.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void removeListItem(int position) {
        adapter.removeItem(this, position);
    }

    @Override
    public void navigateDestination(String key, ValveObject valve, Class destination) {
        Intent intent = new Intent(this, destination);
        intent.putExtra(key, valve);
        startActivity(intent);
    }

    @Override
    public void navigateToEditValve(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void updateListItem(int position, ValveObject valve) {
        adapter.updateItem(position, valve);
    }

    @Override
    public void navigateToMap(int requestCode, ValveObject valve, int position) {
        Intent intent1 = new Intent(this, GetValveMapDetailsActivity.class);
        intent1.putExtra("Valve", valve);
        intent1.putExtra("position", position);
        intent1.putExtra("OfflineEdit", "");
        startActivityForResult(intent1, requestCode);
    }

    @Override
    public int getSpinnerSelectedPosition() {
        return spinner.getSelectedItemPosition();
    }

    @Override
    public void navigateToMap2(int requestCode, ArrayList<ValveObject> valves) {
        Intent intent = new Intent(OfflineValvesActivity.this, RejectedValvesMapActivity.class);
        intent.putExtra("Valves", valves);
        intent.putExtra("OfflineEdit", "");
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void removeListItem(long id) {
        adapter.updateItem(this, id);
    }

    @Override
    public void onListItemClickListener(int viewId, int position, ValveObject valve) {
        presenter.OnListItemClickListener(viewId, position, valve);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
