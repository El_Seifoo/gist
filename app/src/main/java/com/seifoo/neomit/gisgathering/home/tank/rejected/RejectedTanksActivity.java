package com.seifoo.neomit.gisgathering.home.tank.rejected;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.home.tank.map.GetTankMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.tank.offline.OfflineTanksAdapter;
import com.seifoo.neomit.gisgathering.home.tank.rejected.edit.EditRejectedTanksActivity;
import com.seifoo.neomit.gisgathering.home.tank.rejected.map.RejectedTanksMapActivity;

import java.util.ArrayList;

public class RejectedTanksActivity extends AppCompatActivity implements RejectedTanksMVP.View, OfflineTanksAdapter.TankObjectListItemListener {
    private static final int EDIT_REJECTED_TANK_REQUEST = 1;
    private static final int SHOW_ALL_IN_MAP_REQUEST = 2;
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private OfflineTanksAdapter adapter;
    private ProgressBar progressBar;
    private Button showLocations;

    private RejectedTanksMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_tanks);

        getSupportActionBar().setTitle(getString(R.string.rejected_tanks));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new RejectedTanksPresenter(this, new RejectedTanksModel());
        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);


        showLocations = (Button) findViewById(R.id.show_locations);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.rejected_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineTanksAdapter(this, false);

        presenter.requestTanks();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyListText() {
        showLocations.setVisibility(View.GONE);
        emptyListTextView.setText(getString(R.string.no_valves_available));
        adapter.clear();
    }

    @Override
    public void loadRejectedTanks(ArrayList<TankObject> tanks) {
        emptyListTextView.setText("");
        adapter.setList(tanks);
        recyclerView.setAdapter(adapter);
        handleMapButton(tanks);
    }

    private void handleMapButton(final ArrayList<TankObject> tanks) {
        for (int i = 0; i < tanks.size(); i++) {
            if (tanks.get(i).getLatitude() != null && tanks.get(i).getLongitude() != null) {
                if (!tanks.get(i).getLatitude().isEmpty() && !tanks.get(i).getLongitude().isEmpty()) {
                    showLocations.setVisibility(View.VISIBLE);
                    showLocations.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(RejectedTanksActivity.this, RejectedTanksMapActivity.class);
                            intent.putExtra("Tanks", tanks);
                            startActivityForResult(intent, SHOW_ALL_IN_MAP_REQUEST);
                        }
                    });
                    break;
                }
            }
        }
    }

    @Override
    public void onListItemClickListener(int viewId, int position, TankObject tank) {
        switch (viewId) {
            case R.id.more_details:
                Intent intent = new Intent(this, EditRejectedTanksActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("TankObj", tank);
                startActivityForResult(intent, EDIT_REJECTED_TANK_REQUEST);
                break;
            case R.id.map:
                Intent intent1 = new Intent(this, GetTankMapDetailsActivity.class);
                intent1.putExtra("Tank", tank);
                startActivityForResult(intent1, SHOW_ALL_IN_MAP_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_REJECTED_TANK_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.removeItem(this, data.getExtras().getInt("position"));
            return;
        }
        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            presenter.requestTanks();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
