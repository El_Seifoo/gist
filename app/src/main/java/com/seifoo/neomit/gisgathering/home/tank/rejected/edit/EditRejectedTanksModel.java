package com.seifoo.neomit.gisgathering.home.tank.rejected.edit;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class EditRejectedTanksModel {
    public void getGeoMasters(Context context, DBCallback callback, int[] types, int[] selectedIds, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types,
                MySingleton.getmInstance(context).getStringSharedPref(Constants.APP_LANGUAGE, context.getString(R.string.default_language_value))), selectedIds, index);
    }

    public void insertTankObj(Context context, DBCallback callback, TankObject tank) {
        callback.onTankInsertionCalled(DataBaseHelper.getmInstance(context).insertTank(tank));
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> geoMastersList, int[] selectedIds, int index);

        void onTankInsertionCalled(long flag);
    }
}
