package com.seifoo.neomit.gisgathering.home.tank.info.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class GetTankInfoDetailsModel {
    public void returnValidGeoMaster(Context context, DBCallback callback, TankObject tank, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds(ids, types, lang), tank);
    }

    protected interface DBCallback {
        void onConvertingIdsCalled(ArrayList<String> strings, TankObject tank);
    }
}
