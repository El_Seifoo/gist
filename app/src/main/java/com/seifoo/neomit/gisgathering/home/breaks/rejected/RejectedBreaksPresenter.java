package com.seifoo.neomit.gisgathering.home.breaks.rejected;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class RejectedBreaksPresenter implements RejectedBreaksMVP.Presenter, RejectedBreaksModel.VolleyCallback, RejectedBreaksModel.DBCallback {
    private RejectedBreaksMVP.View view;
    private RejectedBreaksModel model;

    public RejectedBreaksPresenter(RejectedBreaksMVP.View view, RejectedBreaksModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestBreaks() {
        view.showProgress();
        model.getRejected(view.getAppContext(), this);
    }

    /*
    "Corrected_Id": 2,
            "BREAK_NO": "2",
            "PIPE_TYPE": "1",
            "PIPE_NUMBER": "1",
            "DISTRIC_NAME": "1",
            "SUB_ZONE": "1",
            "BUILDING_NO": "1",
            "STREET_NAME": "qqq",
            "BREAK_DATE": "2019-03-31T22:20:00",
            "DIAMETER": "1",
            "PIPE_MATERIAL": "1",
            "Break_Details": "1",
            "EQUIPMENT_USED": "1",
            "MAINTENANCE_COMPANY": "1",
            "MAINTENANCE_TYPE": "1",
            "SOIL_TYPE": "1",
            "ASPHALT": "1",
            "DIMANTION_DRILLING": "1",
            "TYPE_DEVICE_MAINTANCE": "1",
            "PROCEDURE": "1",
            "X_Map": 42.125165,
            "Y_Map": 24.1654,
            "Reason": "invalid Diameter",
            "CreatedDate": "2019-04-17T12:58:00"
     */
    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ArrayList<BreakObject> rejectedBreaks = new ArrayList<>();
                Map<String, String> photos;
                String before = "", after = "";
                for (int i = 0; i < data.length(); i++) {
                    photos = new LinkedHashMap<>();
                    if (data.getJSONObject(i).has("beforeImage"))
                        before = data.getJSONObject(i).getString("beforeImage");
                    if (data.getJSONObject(i).has("afterImage"))
                        after = data.getJSONObject(i).getString("afterImage");

                    photos.put("beforeImage", before);
                    photos.put("afterImage", after);

                    rejectedBreaks.add(new BreakObject(
                            data.getJSONObject(i).getLong("Corrected_Id"),
                            returnValidString(data.getJSONObject(i).getString("Y_Map")),
                            returnValidString(data.getJSONObject(i).getString("X_Map")),
                            returnValidString(data.getJSONObject(i).getString("BREAK_NO")),
                            returnValidInt(data.getJSONObject(i).getString("PIPE_TYPE")),
                            returnValidInt(data.getJSONObject(i).getString("DISTRIC_NAME")),
                            returnValidInt(data.getJSONObject(i).getString("SUB_ZONE")),
                            returnValidString(data.getJSONObject(i).getString("BUILDING_NO")),
                            returnValidString(data.getJSONObject(i).getString("PIPE_NUMBER")),
                            returnValidString(data.getJSONObject(i).getString("STREET_NAME")),
                            returnValidDate(data.getJSONObject(i).getString("BREAK_DATE")),
                            returnValidInt(data.getJSONObject(i).getString("DIAMETER")),
                            returnValidInt(data.getJSONObject(i).getString("PIPE_MATERIAL")),
                            returnValidString(data.getJSONObject(i).getString("Break_Details")),
                            returnValidString(data.getJSONObject(i).getString("EQUIPMENT_USED")),
                            returnValidInt(data.getJSONObject(i).getString("MAINTENANCE_COMPANY")),
                            returnValidInt(data.getJSONObject(i).getString("MAINTENANCE_TYPE")),
                            returnValidInt(data.getJSONObject(i).getString("SOIL_TYPE")),
                            returnValidInt(data.getJSONObject(i).getString("ASPHALT")) == -1 ? 1 : returnValidInt(data.getJSONObject(i).getString("ASPHALT")),
                            returnValidString(data.getJSONObject(i).getString("DIMANTION_DRILLING")),
                            returnValidInt(data.getJSONObject(i).getString("TYPE_DEVICE_MAINTANCE")),
                            returnValidInt(data.getJSONObject(i).getString("PROCEDURE")),
                            photos,
                            returnValidString(data.getJSONObject(i).getString("Reason")),
                            returnValidDate1(data.getJSONObject(i).getString("CreatedDate"))));
//x -> longitude , y -> latitude
                }
                model.checkDatabaseBreaks(view.getAppContext(), this, rejectedBreaks);
            } else {
                view.hideProgress();
                view.showEmptyListText();
            }
        } else {
            view.hideProgress();
            view.showEmptyListText();
        }
    }

    private String returnValidString(String data) {
        return data.isEmpty() || data.toLowerCase().equals("null") ? "" : data;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onCheckingBreaksCalled(ArrayList<BreakObject> breaks) {
        view.hideProgress();
        if (breaks.isEmpty())
            view.showEmptyListText();
        else
            view.loadRejectedBreaks(breaks);
    }
}
