package com.seifoo.neomit.gisgathering.home.chamber.info.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface GetChamberInfoDetailsMVP {
    interface View {
        Context getAppContext();

        Context getActContext();

        void loadChamberMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {
        void requestChamberData(ChamberObject chamber);
    }
}
