package com.seifoo.neomit.gisgathering.home.mainLine.rejected;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;

import java.util.ArrayList;

public interface RejectedMainLinesMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showEmptyListText();

        void loadRejectedMainLines(ArrayList<MainLineObject> mainLines);
    }

    interface Presenter {
        void requestMainLines();
    }
}
