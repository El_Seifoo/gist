package com.seifoo.neomit.gisgathering.home.chamber.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public interface AddChamberMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();

        String getCurrentDate();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds,
                              ArrayList<String> diameter, ArrayList<Integer> diameterIds,
                              ArrayList<String> districtName, ArrayList<Integer> districtNameIds);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator);

        void setCHN(String data);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> subDistrictName, ArrayList<Integer> subDistrictNameIds,
                              ArrayList<String> diameterAirValve, ArrayList<Integer> diameterAirValveIds,
                              ArrayList<String> diameterWAValve, ArrayList<Integer> diameterWAValveIds,
                              ArrayList<String> diameterISOValve, ArrayList<Integer> diameterISOValveIds,
                              ArrayList<String> diameterFlowMeter, ArrayList<Integer> diameterFlowMeterIds,
                              ArrayList<String> type, ArrayList<Integer> typeIds,
                              ArrayList<String> remarks, ArrayList<Integer> remarksIds);

        void setLastUpdated(String date);
    }

    interface Presenter {
        void requestFirstSpinnersData();

        void requestSecondSpinnersData();

        void requestPassFirstStepDataToActivity(ChamberObject chamber);

        void requestPassSecondStepDataToActivity(ChamberObject chamber);

        void requestAddChamber(ChamberObject chamber);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString);
    }
}
