package com.seifoo.neomit.gisgathering.home.mainLine.info.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class GetMainLineInfoDetailsModel {
    public void returnValidGeoMaster(Context context, DBCallback callback, MainLineObject mainLine, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds(ids, types, lang), mainLine);
    }

    protected interface DBCallback {
        void onConvertingIdsCalled(ArrayList<String> strings, MainLineObject mainLine);
    }
}
