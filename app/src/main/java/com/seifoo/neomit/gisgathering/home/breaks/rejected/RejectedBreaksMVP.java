package com.seifoo.neomit.gisgathering.home.breaks.rejected;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;

import java.util.ArrayList;

public interface RejectedBreaksMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showEmptyListText();

        void loadRejectedBreaks(ArrayList<BreakObject> breaks);
    }

    interface Presenter {
        void requestBreaks();
    }
}
