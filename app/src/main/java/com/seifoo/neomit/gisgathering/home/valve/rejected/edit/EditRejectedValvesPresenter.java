package com.seifoo.neomit.gisgathering.home.valve.rejected.edit;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.util.ArrayList;
import java.util.Collection;

import static android.app.Activity.RESULT_OK;

public class EditRejectedValvesPresenter implements EditRejectedValvesMVP.Presenter, EditRejectedValvesModel.DBCallback {
    private EditRejectedValvesMVP.View view;
    private EditRejectedValvesMVP.MainView mainView;
    private EditRejectedValvesMVP.FirstView firstView;
    private EditRejectedValvesMVP.SecondView secondView;
    private EditRejectedValvesModel model;

    public EditRejectedValvesPresenter(EditRejectedValvesMVP.View view, EditRejectedValvesMVP.MainView mainView, EditRejectedValvesModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public EditRejectedValvesPresenter(EditRejectedValvesMVP.View view, EditRejectedValvesMVP.FirstView firstView, EditRejectedValvesModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public EditRejectedValvesPresenter(EditRejectedValvesMVP.View view, EditRejectedValvesMVP.SecondView secondView, EditRejectedValvesModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    @Override
    public void requestFirstSpinnersData(ValveObject valve) {
        // valveJob,  material, diameter,  type, lock,   status,
        model.getGeoMasters(view.getAppContext(), this, new int[]{27, 4, 0, 20, 22, 21},
                new int[]{valve.getValveJob(), valve.getMaterial(), valve.getDiameter(), valve.getType(), valve.getLock(), valve.getStatus()},
                1);

        firstView.setLocation(valve.getLatitude() + "," + valve.getLongitude());
        firstView.setSerialNumber(valve.getSerialNumber());
        firstView.setData(valve.getFullNumberOfTurns(), valve.getNumberOfTurns(), valve.getElevation(), valve.getGroundElevation());

    }

    @Override
    public void requestSecondSpinnersData(ValveObject valve) {
        //  coverStatus,  enabled, districts,  ((subDistricts)),   existInField,  existInMap,   waterSchedule,  remarks
        model.getGeoMasters(view.getAppContext(), this, new int[]{12, 22, 25, 28, 22, 22, 16, 5},
                new int[]{valve.getCoverStatus(), valve.getEnabled(), valve.getDistrictName(), valve.getSubDistrict(),
                        valve.getExistInField(), valve.getExistInMap(), valve.getWaterSchedule(), valve.getRemarks()},
                2);

        secondView.setData(valve.getDmaZone(), valve.getStreetName(), valve.getSectorName());
    }

    @Override
    public void requestPassFirstStepDataToActivity(ValveObject valve) {
        if (!valve.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        valve.setLatitude(valve.getLatitude().split(",")[0]);
        valve.setLongitude(valve.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof EditRejectedValvesActivity) {
            ((EditRejectedValvesActivity) view.getAppContext()).passFirstObj(valve);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(ValveObject valve) {
        if (view.getAppContext() instanceof EditRejectedValvesActivity) {
            ((EditRejectedValvesActivity) view.getAppContext()).passSecondObj(valve);
        }
    }

    @Override
    public void requestEditRejectedValve(ValveObject valve, String createdAt, long id) {
        valve.setValveId(id);
        valve.setCreatedAt(returnValidNumbers(createdAt));
        model.insertValveObj(view.getAppContext(), this, valve);
    }

    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQrCode(((EditRejectedValvesActivity) view.getAppContext()));
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        firstView.initializeScanner(integrator);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents());
        }
    }

    @Override
    public void handleQRScannerResult(String contents) {
        firstView.setSerialNumber(contents.replaceAll("[^0-9]", ""));
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", PICK_LOCATION_REQUEST);
        }
    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index) {
        if (index == 1) handleFirstView(masters, selectedIds);
        else if (index == 2) handleSecondView(masters, selectedIds);
    }

    private void handleFirstView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                valveJob = new ArrayList<>(),
                material = new ArrayList<>(),
                diameter = new ArrayList<>(),
                type = new ArrayList<>(),
                lock = new ArrayList<>(),
                status = new ArrayList<>();

        ArrayList<Integer>
                valveJobIds = new ArrayList<>(),
                materialIds = new ArrayList<>(),
                diameterIds = new ArrayList<>(),
                typeIds = new ArrayList<>(),
                lockIds = new ArrayList<>(),
                statusIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 27) {
                valveJob.add(masters.get(i).getName());
                valveJobIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 4) {
                material.add(masters.get(i).getName());
                materialIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                diameter.add(masters.get(i).getName());
                diameterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 20) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 22) {
                lock.add(masters.get(i).getName());
                lockIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 21) {
                status.add(masters.get(i).getName());
                statusIds.add(masters.get(i).getGeoMasterId());
            }
        }


        int valveJobIndex = returnIndex(valveJobIds, selectedIds[0]);
        int materialIndex = returnIndex(materialIds, selectedIds[1]);
        int diameterIndex = returnIndex(diameterIds, selectedIds[2]);
        int typeIndex = returnIndex(typeIds, selectedIds[3]);
        int lockIndex = returnIndex(lockIds, selectedIds[4]);
        int statusIndex = returnIndex(statusIds, selectedIds[5]);

        firstView.loadSpinnersData(valveJob, valveJobIds, valveJobIndex,
                material, materialIds, materialIndex,
                diameter, diameterIds, diameterIndex,
                type, typeIds, typeIndex,
                lock, lockIds, lockIndex,
                status, statusIds, statusIndex);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                coverStatus = new ArrayList<>(),
                enabled = new ArrayList<>(),
                districts = new ArrayList<>(),
                subDistricts = new ArrayList<>(),
                existInField = new ArrayList<>(),
                existInMap = new ArrayList<>(),
                waterSchedule = new ArrayList<>(),
                remarks = new ArrayList<>();

        ArrayList<Integer>
                coverStatusIds = new ArrayList<>(),
                enabledIds = new ArrayList<>(),
                districtsIds = new ArrayList<>(),
                subDistrictsIds = new ArrayList<>(),
                existInFieldIds = new ArrayList<>(),
                existInMapIds = new ArrayList<>(),
                waterScheduleIds = new ArrayList<>(),
                remarksIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 12) {
                coverStatus.add(masters.get(i).getName());
                coverStatusIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 22) {
                enabled.add(masters.get(i).getName());
                enabledIds.add(masters.get(i).getGeoMasterId());

                existInField.add(masters.get(i).getName());
                existInFieldIds.add(masters.get(i).getGeoMasterId());

                existInMap.add(masters.get(i).getName());
                existInMapIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 25) {
                districts.add(masters.get(i).getName());
                districtsIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 28) {
                subDistricts.add(masters.get(i).getName());
                subDistrictsIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 16) {
                waterSchedule.add(masters.get(i).getName());
                waterScheduleIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 5) {
                remarks.add(masters.get(i).getName());
                remarksIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int coverStatusIndex = returnIndex(coverStatusIds, selectedIds[0]);
        int enabledIndex = returnIndex(enabledIds, selectedIds[1]);
        int districtsIndex = returnIndex(districtsIds, selectedIds[2]);
        int subDistrictsIndex = returnIndex(subDistrictsIds, selectedIds[3]);
        int existInFieldIndex = returnIndex(existInFieldIds, selectedIds[4]);
        int existInMapIndex = returnIndex(existInMapIds, selectedIds[5]);
        int waterScheduleIndex = returnIndex(waterScheduleIds, selectedIds[6]);
        int remarksIndex = returnIndex(remarksIds, selectedIds[7]);

        secondView.loadSpinnersData(coverStatus, coverStatusIds, coverStatusIndex,
                enabled, enabledIds, enabledIndex,
                districts, districtsIds, districtsIndex,
                subDistricts, subDistrictsIds, subDistrictsIndex,
                existInField, existInFieldIds, existInFieldIndex,
                existInMap, existInMapIds, existInMapIndex,
                waterSchedule, waterScheduleIds, waterScheduleIndex,
                remarks, remarksIds, remarksIndex);
    }

    private int returnIndex(ArrayList<Integer> ids, int selectedId) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == selectedId)
                return i;
        }
        return 0;
    }


    @Override
    public void onValveInsertionCalled(long flag) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.edit_valve_done_successfully));
            mainView.backToParent();
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_save_valve));
        }
    }
}
