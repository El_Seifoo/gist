package com.seifoo.neomit.gisgathering.home.pump.rejected.map;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.home.pump.edit.EditPumpActivity;
import com.seifoo.neomit.gisgathering.home.pump.rejected.edit.EditRejectedPumpsActivity;

import java.util.ArrayList;

public class RejectedPumpsMapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final int EDIT_OFFLINE_PUMPS_REQUEST = 4;
    private ArrayList<PumpObject> pumps;
    private ArrayList<Marker> markers;
    private final static int EDIT_REJECTED_PUMPS_REQUEST = 1;
    private static final int LOCATION_REQUEST_CODE = 2;
    private static final int REQUEST_CHECK_SETTINGS = 3;
    private Button mapTypeButton;
    private View mapView;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_pumps_map);

        getSupportActionBar().setTitle(getString(R.string.map));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayList<PumpObject> valveObjects = (ArrayList<PumpObject>) getIntent().getSerializableExtra("Pumps");

        pumps = new ArrayList<>();
        for (int i = 0; i < valveObjects.size(); i++) {
            if (valveObjects.get(i).getLatitude() != null && valveObjects.get(i).getLongitude() != null) {
                if (!valveObjects.get(i).getLatitude().isEmpty() && !valveObjects.get(i).getLongitude().isEmpty()) {
                    pumps.add(valveObjects.get(i));
                }
            }
        }

        mapTypeButton = (Button) findViewById(R.id.map_type_button);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        mapTypeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (googleMap.getMapType() == GoogleMap.MAP_TYPE_NORMAL) {
                    googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    mapTypeButton.setText(getString(R.string.normal));
                } else if (googleMap.getMapType() == GoogleMap.MAP_TYPE_SATELLITE) {
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    mapTypeButton.setText(getString(R.string.satellite));
                }
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(30, 0, 30, 30);
            layoutParams.setMargins(30, 0, 30, 30);
        }

        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                flag = false;
                checkUserLocationPermissions();
                return true;
            }
        });

        if (!pumps.isEmpty()) {
            markers = new ArrayList<>();
            for (int i = 0; i < pumps.size(); i++) {
                markers.add(googleMap.addMarker(
                        new MarkerOptions().position(new LatLng(
                                Double.parseDouble(pumps.get(i).getLatitude()),
                                Double.parseDouble(pumps.get(i).getLongitude())))));

                if (i == 0)
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                            Double.parseDouble(pumps.get(i).getLatitude()),
                            Double.parseDouble(pumps.get(i).getLongitude())), 12));
            }

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    for (int i = 0; i < markers.size(); i++) {
                        if (markers.get(i).equals(marker)) {
                            if (getIntent().hasExtra("OfflineEdit")) {
                                Intent intent = new Intent(RejectedPumpsMapActivity.this, EditPumpActivity.class);
                                intent.putExtra("PumpObj", pumps.get(i));
                                intent.putExtra("position", i);
                                startActivityForResult(intent, EDIT_OFFLINE_PUMPS_REQUEST);
                            } else {
                                Intent intent = new Intent(RejectedPumpsMapActivity.this, EditRejectedPumpsActivity.class);
                                intent.putExtra("position", i);
                                intent.putExtra("PumpObj", pumps.get(i));
                                startActivityForResult(intent, EDIT_REJECTED_PUMPS_REQUEST);
                            }

                            return true;
                        }
                    }
                    return false;
                }
            });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_REJECTED_PUMPS_REQUEST && resultCode == RESULT_OK && data != null) {
            Marker marker = markers.get(data.getExtras().getInt("position"));
            marker.remove();
            pumps.remove(data.getExtras().getInt("position"));
            markers.remove(data.getExtras().getInt("position"));
            return;
        }

        if (requestCode == EDIT_OFFLINE_PUMPS_REQUEST && resultCode == RESULT_OK && data != null) {
            pumps.set(data.getExtras().getInt("position"), (PumpObject) data.getSerializableExtra("editedPumpObj"));
            return;
        }
        // check user decision about turning location on or not
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case RESULT_OK: {

                    setInitialLocation();
//                        mRequestingLocationUpdates = true;
                    break;
                }
                case Activity.RESULT_CANCELED: {
                    // The user was asked to change settings, but chose not to
//                        mRequestingLocationUpdates = false;
                    break;
                }
                default: {
                    break;
                }
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void checkUserLocationPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST_CODE);
            } else {
                setupLocationManager();
            }
        } else {
            setupLocationManager();
        }
    }

    private GoogleApiClient googleApiClient;

    private void setupLocationManager() {
        // buildGoogleApiClient();
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
        }
        googleApiClient.connect();
        createLocationRequest();
    }

    private LocationRequest request;


    protected void createLocationRequest() {
        request = new LocationRequest();
        request.setSmallestDisplacement(10);
        request.setFastestInterval(1000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(request);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient,
                        builder.build());


        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {

                    case LocationSettingsStatusCodes.SUCCESS:
                        setInitialLocation();
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    RejectedPumpsMapActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.

                        break;

                }


            }
        });
    }

    private boolean flag;

    // getting user current location
    private void setInitialLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, request, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (!flag) {
                    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15);
                    mMap.animateCamera(update);
                    flag = true;
                }

            }
        });

    }

    // check if user grants permissions or not
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        setupLocationManager();
                    }
                }
            }
            break;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
