package com.seifoo.neomit.gisgathering.home.sensors.offline;

import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;
import com.seifoo.neomit.gisgathering.home.sensors.edit.EditSensorActivity;
import com.seifoo.neomit.gisgathering.home.sensors.offline.details.OfflineSensorsDetailsActivity;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class OfflineSensorsPresenter implements OfflineSensorsMVP.Presenter, OfflineSensorsModel.VolleyCallback, OfflineSensorsModel.DBCallback {
    private OfflineSensorsMVP.View view;
    private OfflineSensorsModel model;

    public OfflineSensorsPresenter(OfflineSensorsMVP.View view, OfflineSensorsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestSensorsData(int type) {
        model.getSensorsList(view.getAppContext(), this, type, 0);
    }

    @Override
    public void requestRemoveSensorObjById(long id, int position) {
        model.removeSensorObject(view.getActContext(), this, id, position);
    }

    private static final int SHOW_ALL_IN_MAP_REQUEST = 10;

    @Override
    public void OnListItemClickListener(int viewId, int position, SensorObject valve) {
        switch (viewId) {
            case R.id.more_details:
                view.navigateDestination("OfflineSensorObj", valve, OfflineSensorsDetailsActivity.class);
                break;
            case R.id.remove_item:
                this.requestRemoveSensorObjById(valve.getId(), position);
                break;
            case R.id.edit_item:
                this.requestEditSensorData(position, valve);
                break;
            case R.id.map:
                view.navigateToMap(SHOW_ALL_IN_MAP_REQUEST, valve, position);
        }
    }

    private static final int EDIT_SENSOR_OBJECT_REQUEST = 1;

    @Override
    public void requestEditSensorData(int position, SensorObject valve) {
        Intent intent = new Intent(view.getAppContext(), EditSensorActivity.class);
        intent.putExtra("SensorObj", valve);
        intent.putExtra("position", position);
        view.navigateToEditSensor(intent, EDIT_SENSOR_OBJECT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_SENSOR_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            view.updateListItem(data.getExtras().getInt("position"), (SensorObject) data.getSerializableExtra("editedSensorObj"));
            return;
        }

        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            this.requestSensorsData(view.getSpinnerSelectedPosition());
        }
    }

    @Override
    public void requestUploadData(int type) {
        model.getSensorsList(view.getAppContext(), this, type, 1);
    }

    @Override
    public void showLocations(ArrayList<SensorObject> sensors) {
        view.navigateToMap2(SHOW_ALL_IN_MAP_REQUEST,sensors);
    }

    @Override
    public void onSyncingResponse(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        ArrayList<Long> accepted = new ArrayList<>(), rejected = new ArrayList<>();
        for (int i = 0; i < responseArray.length(); i++) {
            if (responseArray.getJSONObject(i).getString("status").equals("success"))
                accepted.add(responseArray.getJSONObject(i).getLong("id"));
            else rejected.add(responseArray.getJSONObject(i).getLong("id"));
        }

        if (!accepted.isEmpty()) {
            for (int i = 0; i < accepted.size(); i++) {
                model.removeSensorObject(view.getActContext(), this, accepted.get(i), -1);
            }
        }
    }

    @Override
    public void onSyncingError(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onGetSensorsListCalled(ArrayList<SensorObject> sensors, int index) {
        if (index == 0) {
            if (sensors.isEmpty())
                view.showEmptyListText();
            else {
                boolean isVisible = false;
                for (int i = 0; i < sensors.size(); i++) {
                    if (sensors.get(i).getLatitude() != null && sensors.get(i).getLongitude() != null) {
                        if (!sensors.get(i).getLatitude().isEmpty() && !sensors.get(i).getLongitude().isEmpty()) {
                            isVisible = true;
                            break;
                        }
                    }
                }
                view.loadSensorsData(sensors,isVisible);
            }
        } else {
            if (sensors.isEmpty())
                view.showToastMessage(view.getActContext().getString(R.string.no_sensors_to_synchronize));
            else {
                view.showProgress();
                model.uploadSensorsData(view.getActContext(), this, sensors);
            }
        }
    }

    @Override
    public void onRemoveSensorObjectCalled(int flag, int position, long id) {
        if (flag > 0) {
            if (position == -1) {
                view.removeListItem(id);
            } else
                view.removeListItem(position);
        } else view.showToastMessage(view.getActContext().getString(R.string.failed_to_delete));
    }
}
