package com.seifoo.neomit.gisgathering.home.pump.offline;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;
import com.seifoo.neomit.gisgathering.utils.Urls;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class OfflinePumpsModel {

    protected void uploadPumpsData(final Context context, final VolleyCallback callback, final ArrayList<PumpObject> pumps) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSyncingResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("parsing error ", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onSyncingError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""));
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            /*

                Json[data][0][CODE]:1
                Json[data][0][STATION_NAME]:1
                Json[data][0][ENABLED_ID]:1
                Json[data][0][F_TYPE]:1
                Json[data][0][ASSET_STATUS_ID]:1
                Json[data][0][STREET_NAME]:a
                Json[data][0][DISTRICT_NAME_ID]:1
                Json[data][0][SUP_DISTRICT_ID]:1
                Json[data][0][DMA_ZONE_ID]:1
                Json[data][0][SUB_NAME]:aa
                Json[data][0][SECTOR_NAME]:az
                Json[data][0][LAST_UPDATED]:31-03-2019 22:20
                Json[data][0][CreatedDate]:31-03-2019 22:20
                Json[data][0][REMARKS]:xyz
                Json[data][0][RECORD_ID]:10
                Json[tableName]:pump
                Json[databaseName]:re_al_qwayah

             */
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                for (int i = 0; i < pumps.size(); i++) {
                    if (pumps.get(i).getPumpId() != 0.0d)
                        params.put("Json[data][" + i + "][Corrected_Id]", pumps.get(i).getPumpId() + "");
                    params.put("Json[data][" + i + "][RECORD_ID]", pumps.get(i).getId() + "");
                    params.put("Json[data][" + i + "][CODE]", pumps.get(i).getCode() + "");
                    params.put("Json[data][" + i + "][STATION_NAME]", pumps.get(i).getStationName());
                    params.put("Json[data][" + i + "][ENABLED_ID]", pumps.get(i).getEnabled() + "");
                    params.put("Json[data][" + i + "][F_TYPE]", pumps.get(i).getType() + "");
                    params.put("Json[data][" + i + "][ASSET_STATUS_ID]", pumps.get(i).getAssetStatus() + "");
                    params.put("Json[data][" + i + "][STREET_NAME]", pumps.get(i).getStreetName());
                    params.put("Json[data][" + i + "][DISTRICT_NAME_ID]", pumps.get(i).getDistrictName() + "");
                    params.put("Json[data][" + i + "][SUP_DISTRICT_ID]", pumps.get(i).getSubDistrict() + "");
                    params.put("Json[data][" + i + "][DMA_ZONE_ID]", pumps.get(i).getDmaZone());
                    params.put("Json[data][" + i + "][SECTOR_NAME]", pumps.get(i).getSectorName());
                    params.put("Json[data][" + i + "][LAST_UPDATED]", pumps.get(i).getLastUpdated());
                    params.put("Json[data][" + i + "][REMARKS]", pumps.get(i).getRemarks());
                    params.put("Json[data][" + i + "][CreatedDate]", pumps.get(i).getCreatedAt());// created date ....
                    params.put("Json[data][" + i + "][X_MAP]", !pumps.get(i).getLongitude().isEmpty() ? pumps.get(i).getLongitude() : "360");
                    params.put("Json[data][" + i + "][Y_MAP]", !pumps.get(i).getLatitude().isEmpty() ? pumps.get(i).getLatitude() : "360");
                }
                params.put("Json[tableName]", context.getString(R.string.api_pump_table_name));
                params.put("Json[databaseName]", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""));
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected void getPumpsList(Context context, DBCallback callback, int type, int index) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetPumpsListCalled(DataBaseHelper.getmInstance(context).getAllPumps(type), index);
    }

    protected void removePumpObject(Context context, DBCallback callback, long id, int position) {
        callback.onRemoveValveObjectCalled(DataBaseHelper.getmInstance(context).deletePump(id), position, id);
    }

    protected interface VolleyCallback {
        void onSyncingResponse(String response) throws JSONException;

        void onSyncingError(VolleyError error);
    }

    protected interface DBCallback {

        void onGetPumpsListCalled(ArrayList<PumpObject> pumps, int index);

        void onRemoveValveObjectCalled(int flag, int position, long id);
    }
}
