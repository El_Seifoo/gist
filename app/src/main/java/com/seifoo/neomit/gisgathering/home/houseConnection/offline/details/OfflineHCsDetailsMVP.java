package com.seifoo.neomit.gisgathering.home.houseConnection.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface OfflineHCsDetailsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();


        void loadHcMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestHcDetails(HouseConnectionObject houseConnectionObject);
    }
}
