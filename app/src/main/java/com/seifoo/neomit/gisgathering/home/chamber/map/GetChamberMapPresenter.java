package com.seifoo.neomit.gisgathering.home.chamber.map;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class GetChamberMapPresenter implements GetChamberMapMVP.Presenter, GetChamberMapModel.VolleyCallback {
    private GetChamberMapMVP.View view;
    private GetChamberMapModel model;

    public GetChamberMapPresenter(GetChamberMapMVP.View view, GetChamberMapModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestQrCode(Activity activity) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        view.initializeScanner(integrator);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                view.setCHN(result.getContents().replaceAll("[^0-9]", ""));
        }
    }

    @Override
    public void requestGetMap(String chNumber) {
        if (chNumber.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.ch_number_is_required));
            return;
        }
        view.showProgress();
        model.getMap(view.getActContext(), this, chNumber);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                if (returnValidString(data.getJSONObject(0).getString("X_Map")).isEmpty() || returnValidString(data.getJSONObject(0).getString("Y_Map")).isEmpty())
                    view.showToastMessage(view.getActContext().getString(R.string.this_chamber_has_no_location));
                else
                    view.loadChamberMap(returnValidString(data.getJSONObject(0).getString("Y_Map")), returnValidString(data.getJSONObject(0).getString("X_Map")));
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_chamber_has_no_location));

        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_chamber_has_no_location));
    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
