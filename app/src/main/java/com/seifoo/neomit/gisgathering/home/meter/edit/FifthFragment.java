package com.seifoo.neomit.gisgathering.home.meter.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

public class FifthFragment extends Fragment implements Step, EditMeterMVP.MainView, EditMeterMVP.FifthView {
    private EditText dmaZone, locationNumberEditText, buildingNumberEditText, buildingDuplicationEditText,
            buildingNumberMEditText, buildingDescriptionEditText, streetNumberEditText, streetNumberMEditText, subNameEditText, arabicNameEditText;
    private RadioGroup customerActivated;

    public static FifthFragment newInstance(MeterObject meterObj) {
        FifthFragment fragment = new FifthFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("meterObj", meterObj);
        fragment.setArguments(args);
        return fragment;
    }

    private EditMeterMVP.Presenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fifth_edit, container, false);

        presenter = new EditMeterPresenter(this, this, new EditMeterModel());

        dmaZone = (EditText) view.findViewById(R.id.dma_zone);
        locationNumberEditText = (EditText) view.findViewById(R.id.location_num);
        buildingNumberEditText = (EditText) view.findViewById(R.id.building_num);
        buildingDuplicationEditText = (EditText) view.findViewById(R.id.building_duplication);
        buildingNumberMEditText = (EditText) view.findViewById(R.id.building_num_m);
        buildingDescriptionEditText = (EditText) view.findViewById(R.id.building_description);
        streetNumberEditText = (EditText) view.findViewById(R.id.street_num);
        streetNumberMEditText = (EditText) view.findViewById(R.id.street_num_m);
        subNameEditText = (EditText) view.findViewById(R.id.sub_name);
        arabicNameEditText = (EditText) view.findViewById(R.id.arabic_name);

        customerActivated = (RadioGroup) view.findViewById(R.id.customer_activated_radio_group);
        ((RadioButton) customerActivated.getChildAt(0)).setChecked(true);


        presenter.requestFifthStepData((MeterObject) getArguments().getSerializable("meterObj"));
        return view;
    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFifthStepDataToActivity(new MeterObject(
                dmaZone.getText().toString().trim(),
                locationNumberEditText.getText().toString().trim(),
                buildingNumberEditText.getText().toString().trim(),
                buildingDuplicationEditText.getText().toString().trim(),
                buildingNumberMEditText.getText().toString().trim(),
                buildingDescriptionEditText.getText().toString().trim(),
                streetNumberEditText.getText().toString().trim(),
                streetNumberMEditText.getText().toString().trim(),
                subNameEditText.getText().toString().trim(),
                arabicNameEditText.getText().toString().trim(),
                getCheckedButton(customerActivated).equals(getContext().getString(R.string.yes)) ? "1" : "0"));

        return null;
    }

    private String getCheckedButton(RadioGroup radioGroup) {
        int checkedButtonId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) getView().findViewById(checkedButtonId);
        return radioButton.getText().toString().trim();
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void backToParent(MeterObject meterObject) {

    }


    @Override
    public void setMeterData(String dmaZoneString, String locationNumber, String buildingNumber, String buildingDuplication, String buildingNumberM, String buildingDescription,
                             String streetNumber, String streetNumberM, String subName, String arabicName, int customerActiveIndex) {

        dmaZone.setText(dmaZoneString);
        locationNumberEditText.setText(locationNumber);
        buildingNumberEditText.setText(buildingNumber);
        buildingDuplicationEditText.setText(buildingDuplication);
        buildingNumberMEditText.setText(buildingNumberM);
        buildingDescriptionEditText.setText(buildingDescription);
        streetNumberEditText.setText(streetNumber);
        streetNumberMEditText.setText(streetNumberM);
        subNameEditText.setText(subName);
        arabicNameEditText.setText(arabicName);
        ((RadioButton) customerActivated.getChildAt(customerActiveIndex)).setChecked(true);
    }



}
