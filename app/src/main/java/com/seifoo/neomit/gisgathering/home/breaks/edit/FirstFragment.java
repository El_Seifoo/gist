package com.seifoo.neomit.gisgathering.home.breaks.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.HashMap;

public class FirstFragment extends Fragment implements Step, EditBreakMVP.View, EditBreakMVP.FirstView {
    private EditText latLngEditText, breakNumberEditText, buildingNumberEditText, pipeNumberEditText, streetNameEditText, breakDateEditText;
    private Spinner pipeTypeSpinner, districtNameSpinner, subZoneSpinner, diameterSpinner;

    private EditBreakMVP.Presenter presenter;

    public static FirstFragment newInstance(BreakObject breakObject) {
        FirstFragment fragment = new FirstFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("BreakObj", breakObject);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_break_first_edit, container, false);

        presenter = new EditBreakPresenter(this, this, new EditBreakModel());


        pipeTypeSpinner = (Spinner) view.findViewById(R.id.pipe_type_spinner);
        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);
        subZoneSpinner = (Spinner) view.findViewById(R.id.sub_district_spinner);
        diameterSpinner = (Spinner) view.findViewById(R.id.diameter_spinner);


        latLngEditText = (EditText) view.findViewById(R.id.location);
        breakNumberEditText = (EditText) view.findViewById(R.id.break_number);
        buildingNumberEditText = (EditText) view.findViewById(R.id.building_num);
        pipeNumberEditText = (EditText) view.findViewById(R.id.pipe_number);
        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        breakDateEditText = (EditText) view.findViewById(R.id.break_date);

        breakDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(breakDateEditText.getText().toString().trim());

            }
        });

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(latLngEditText.getText().toString().trim());
            }
        });


        presenter.requestFirstSpinnersData((BreakObject) getArguments().getSerializable("BreakObj"));

        return view;
    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFirstStepDataToActivity(new BreakObject(
                latLngEditText.getText().toString().trim(), latLngEditText.getText().toString().trim(),
                breakNumberEditText.getText().toString().trim(),
                pipeTypeIds.get(pipeTypeSpinner.getSelectedItemPosition()), districtNameIds.get(districtNameSpinner.getSelectedItemPosition()),
                subZoneIds.get(subZoneSpinner.getSelectedItemPosition()), buildingNumberEditText.getText().toString().trim(),
                pipeNumberEditText.getText().toString().trim(), streetNameEditText.getText().toString().trim(),
                breakDateEditText.getText().toString().trim(), diameterIds.get(diameterSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    ArrayList<Integer> pipeTypeIds, districtNameIds, subZoneIds, diameterIds;

    @Override
    public void loadSpinnersData(ArrayList<String> pipeType, ArrayList<Integer> pipeTypeIds, int pipeTypeIndex, ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex, ArrayList<String> subZone, ArrayList<Integer> subZoneIds, int subZoneIndex, ArrayList<String> diameter, ArrayList<Integer> diameterIds, int diameterIndex) {
        this.pipeTypeIds = pipeTypeIds;
        this.districtNameIds = districtNameIds;
        this.subZoneIds = subZoneIds;
        this.diameterIds = diameterIds;


        pipeTypeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, pipeType));
        pipeTypeSpinner.setSelection(pipeTypeIndex);
        districtNameSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, districtName));
        districtNameSpinner.setSelection(districtNameIndex);
        subZoneSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, subZone));
        subZoneSpinner.setSelection(subZoneIndex);
        diameterSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameter));
        diameterSpinner.setSelection(diameterIndex);
    }

    @Override
    public void showLocationError(String errorMessage) {
        latLngEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((EditBreakActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("latLng", latLngs);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void setLocation(String location) {
        latLngEditText.setText(location);
    }

    @Override
    public void setBreakDate(String breakDate) {
        breakDateEditText.setText(breakDate);
    }

    @Override
    public void setData(String breakNumber, String buildingNumber, String pipeNumber, String streetName) {
        breakNumberEditText.setText(breakNumber);
        buildingNumberEditText.setText(buildingNumber);
        pipeNumberEditText.setText(pipeNumber);
        streetNameEditText.setText(streetName);
    }
}
