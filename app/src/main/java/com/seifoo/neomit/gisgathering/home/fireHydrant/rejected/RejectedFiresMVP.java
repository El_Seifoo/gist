package com.seifoo.neomit.gisgathering.home.fireHydrant.rejected;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;

import java.util.ArrayList;

public interface RejectedFiresMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showEmptyListText();

        void loadRejectedFires(ArrayList<FireHydrantObject> fires);
    }

    interface Presenter {
        void requestFires();
    }
}
