package com.seifoo.neomit.gisgathering.home.meter.getInfo.details;

import android.util.Log;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class DetailsPresenter implements DetailsMVP.Presenter, DetailsModel.DBCallback {
    private DetailsMVP.View view;
    private DetailsModel model;

    public DetailsPresenter(DetailsMVP.View view, DetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestMeterData(MeterObject meterObject) {

      /*
        0 -> meter diameter , 1 -> meter brand , 2 - > meter type , 3 -> meter status , 4 -> meter material , 5 -> meter remarks
        6 -> read type , 7 -> pipe after meter , 8 -> pipe size , 9 -> maintenance area , 10 -> meter box Position , 11 -> meter box type
        12 -> cover status , 13 -> location , 14 -> building usage , 15 -> sub-building type , 16 -> water schedule , 17 -> water connection type
        18 -> pipe material , 19 -> new meter brand , 20 ->valve type  , 21 -> valve status , 22 -> yes_no , 23 -> reducer diameter
        24 -> FireType , 25 -> districts , 26 -> houseConnectionType , 27 -> valveJob
     */
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();
        ids.add(meterObject.getMeterMeterDiameterId());
        types.add(0);
        ids.add(meterObject.getMeterMeterBrandId());
        types.add(1);
        ids.add(meterObject.getMeterMeterTypeId());
        types.add(2);
        ids.add(meterObject.getMeterMeterStatusId());
        types.add(3);
        ids.add(meterObject.getMeterMeterMaterialId());
        types.add(4);
        ids.add(meterObject.getMeterMeterRemarksId());
        types.add(5);
        ids.add(meterObject.getMeterReadTypeId());
        types.add(6);
        ids.add(meterObject.getMeterPipeAfterMeterId());
        types.add(7);
        ids.add(meterObject.getMeterPipeSizeId());
        types.add(8);
        ids.add(meterObject.getMeterMaintenanceAreaId());
        types.add(9);
        ids.add(meterObject.getMeterMeterBoxPositionId());
        types.add(10);
        ids.add(meterObject.getMeterMeterBoxTypeId());
        types.add(11);
        ids.add(meterObject.getMeterCoverStatusId());
        types.add(12);
        ids.add(meterObject.getMeterLocationId());
        types.add(13);
        ids.add(meterObject.getMeterBuildingUsageId());
        types.add(14);
        ids.add(meterObject.getMeterSubBuildingTypeId());
        types.add(15);
        ids.add(meterObject.getMeterWaterScheduleId());
        types.add(16);
        ids.add(meterObject.getMeterWaterConnectionTypeId());
        types.add(17);
        ids.add(meterObject.getMeterPipeMaterialId());
        types.add(18);
        ids.add(meterObject.getMeterBrandNewMeterId());
        types.add(19);
        ids.add(meterObject.getMeterValveTypeId());
        types.add(20);
        ids.add(meterObject.getMeterValveStatusId());
        types.add(21);
        ids.add(meterObject.getMeterEnabledId());
        types.add(22);
        ids.add(meterObject.getMeterReducerDiameterId());
        types.add(23);
        ids.add(meterObject.getMeterDistrictName());
        types.add(25);

        Log.e("districtName: ", meterObject.getMeterDistrictName() + "");
        model.returnValidGeoMaster(view.getAppContext(), this, meterObject, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));
    }

    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, MeterObject meterObject) {
        for (int i = 0; i < strings.size(); i++) {
            Log.e("ssss", strings.get(i));
        }
        ArrayList<MoreDetails> list = new ArrayList<>();
        if (meterObject.getMeterLatitude() != null && meterObject.getMeterLongitude() != null)
            list.add(new MoreDetails(view.getActContext().getString(R.string.location_1), meterObject.getMeterLatitude() + "," + meterObject.getMeterLongitude()));
        else list.add(new MoreDetails(view.getActContext().getString(R.string.location_1), ""));
        list.add(new MoreDetails(view.getActContext().getString(R.string.serial_number), meterObject.getMeterSerialNumber()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.plate_number), meterObject.getMeterPlateNumber()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.meter_diameter), strings.get(0)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.meter_brand), strings.get(1)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.meter_type), strings.get(2)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.meter_status), strings.get(3)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.meter_material), strings.get(4)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.meter_remarks), strings.get(5)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.read_type), strings.get(6)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.pipe_after_meter), strings.get(7)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.pipe_size), strings.get(8)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.meter_maintenance_area), strings.get(9)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.meter_box_position), strings.get(10)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.meter_box_type), strings.get(11)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.cover_status), strings.get(12)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.location), strings.get(13)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.water_connection_type), strings.get(17)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.building_usage), strings.get(14)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.pipe_material), strings.get(18)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.valve_type), strings.get(20)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.valve_status), strings.get(21)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.enabled), strings.get(22)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.reducer_diameter), strings.get(23)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.last_updated), meterObject.getMeterLastUpdated()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.post_code), meterObject.getMeterPostCode()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.sceco_num), meterObject.getMeterScecoNumber()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.number_of_electric_meters), meterObject.getMeterNumberOfElectricMeters()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.last_reading), meterObject.getMeterLastReading()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.number_of_floors), meterObject.getMeterNumberOfFloors()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.street_name), meterObject.getMeterStreetName()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.sector_name), meterObject.getMeterSectorName()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.is_sewer_connection_exist),
                meterObject.getMeterIsSewerConnectionExist().equals("1") ? view.getActContext().getString(R.string.exist) : view.getActContext().getString(R.string.not_exist)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.ground_elevation), meterObject.getGroundElevation()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.elevation), meterObject.getElevation()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.hcn), meterObject.getMeterHcn()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.meter_address), meterObject.getMeterMeterAddress()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.sub_building_type), strings.get(15)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.water_schedule), strings.get(16)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.new_meter_brand), strings.get(19)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(24)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.dma_zone), meterObject.getMeterDmaZone()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.location_number), meterObject.getMeterLocationNumber()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.building_num), meterObject.getMeterBuildingNumber()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.building_duplication), meterObject.getMeterBuildingDuplication()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.building_num_m), meterObject.getMeterBuildingNumberM()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.building_description), meterObject.getMeterBuildingDescription()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.street_num), meterObject.getMeterStreetNumber()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.street_num_m), meterObject.getMeterStreetNumberM()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.sub_name), meterObject.getMeterSubName()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.arabic_name), meterObject.getMeterArabicName()));
        list.add(new MoreDetails(view.getActContext().getString(R.string.customer_activated),
                meterObject.getMeterCustomerActive().equals("1") ? view.getActContext().getString(R.string.yes) : view.getActContext().getString(R.string.no)));
        list.add(new MoreDetails(view.getActContext().getString(R.string.created_at), meterObject.getCreatedAt()));


        view.loadMeterMoreDetails(list);
    }
}
