package com.seifoo.neomit.gisgathering.home.breaks.map;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetBreakMapPresenter implements GetBreakMapMVP.Presenter, GetBreakMapModel.VolleyCallback {
    private GetBreakMapMVP.View view;
    private GetBreakMapModel model;

    public GetBreakMapPresenter(GetBreakMapMVP.View view, GetBreakMapModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestGetMap(String breakNumber) {
        if (breakNumber.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.break_number_is_required));
            return;
        }
        view.showProgress();
        model.getMap(view.getActContext(), this, breakNumber);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                if (returnValidString(data.getJSONObject(0).getString("X_Map")).isEmpty() || returnValidString(data.getJSONObject(0).getString("Y_Map")).isEmpty())
                    view.showToastMessage(view.getActContext().getString(R.string.this_break_has_no_location));
                else
                    view.loadBreakMap(returnValidString(data.getJSONObject(0).getString("Y_Map")), returnValidString(data.getJSONObject(0).getString("X_Map")));
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_break_has_no_location));

        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_break_has_no_location));
    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
