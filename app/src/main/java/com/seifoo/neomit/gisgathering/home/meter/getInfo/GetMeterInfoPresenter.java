package com.seifoo.neomit.gisgathering.home.meter.getInfo;


import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class GetMeterInfoPresenter implements GetMeterInfoMVP.Presenter, GetMeterInfoModel.VolleyCallback {
    private GetMeterInfoMVP.View view;
    private GetMeterInfoModel model;

    public GetMeterInfoPresenter(GetMeterInfoMVP.View view, GetMeterInfoModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void whichQRClicked(int id) {
        switch (id) {
            case R.id.serial_num_qr:
                requestQrCode(((GetMeterInfoActivity) view.getAppContext()), view.getAppContext().getString(R.string.serial_number));
                break;
            case R.id.hcn_qr:
                requestQrCode(((GetMeterInfoActivity) view.getAppContext()), view.getAppContext().getString(R.string.hcn));
                break;
            case R.id.meter_address_qr:
                requestQrCode(((GetMeterInfoActivity) view.getAppContext()), view.getAppContext().getString(R.string.meter_address));
                break;
            case R.id.plate_num_qr:
                requestQrCode(((GetMeterInfoActivity) view.getAppContext()), view.getAppContext().getString(R.string.plate_number));
                break;
        }
    }

    @Override
    public void requestQrCode(Activity activity, String flagString) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (flagString.equals(view.getAppContext().getString(R.string.serial_number))) {
            flag = 0;
        } else if (flagString.equals(view.getAppContext().getString(R.string.hcn))) {
            flag = 1;
        } else if (flagString.equals(view.getAppContext().getString(R.string.meter_address))) {
            flag = 2;
        } else if (flagString.equals(view.getAppContext().getString(R.string.plate_number))) {
            flag = 3;
        }
        view.initializeScanner(integrator, flag);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void handleQRScannerResult(String contents, int flag) {
        contents = contents.replaceAll("[^0-9]", "");
        if (flag == 0) {
            view.setSerialNumber(contents);
        } else if (flag == 1) {
            view.setHCN(contents);
        } else if (flag == 2) {
            view.setMeterAddress(contents);
        } else {
            view.setPlateNumber(contents);
        }
    }

    @Override
    public void requestGetInfo(String serialNumber, String hcn, String meterAddress, String plateNumber) {
        if (serialNumber.isEmpty() && hcn.isEmpty() && meterAddress.isEmpty() && plateNumber.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.fill_at_least_one_field_to_continue));
            return;
        }
        view.showProgress();
        model.getInfo(view.getActContext(), this, serialNumber, hcn, meterAddress, plateNumber);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                MeterObject meterObject = new MeterObject(returnValidString(data.getJSONObject(0).getString("SERIAL_NO")),
                        returnValidString(data.getJSONObject(0).getString("HCN")),
                        returnValidString(data.getJSONObject(0).getString("METER_ADDRESS")),
                        returnValidString(data.getJSONObject(0).getString("PLATE_NO")),
                        returnValidInt(data.getJSONObject(0).getString("METER_DIAMETER_ID")),
                        returnValidInt(data.getJSONObject(0).getString("METER_BRAND_ID")),
                        returnValidInt(data.getJSONObject(0).getString("METER_TYPE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("METER_STATUS_ID")),
                        returnValidInt(data.getJSONObject(0).getString("METER_MATERIAL_ID")),
                        returnValidInt(data.getJSONObject(0).getString("METER_REMARKS_ID")),
                        returnValidInt(data.getJSONObject(0).getString("READ_TYPE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("PIPE_AFTER_METER_ID")),
                        returnValidInt(data.getJSONObject(0).getString("PIPE_SIZE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("MAINTENANCE_AREA_ID")),
                        returnValidInt(data.getJSONObject(0).getString("METER_BOX_POSITION_ID")),
                        returnValidInt(data.getJSONObject(0).getString("METER_BOX_TYPES_ID")),
                        returnValidInt(data.getJSONObject(0).getString("COVER_STATUS")),
                        returnValidInt(data.getJSONObject(0).getString("LOCATION_IN_BUILDING_ID")),
                        returnValidInt(data.getJSONObject(0).getString("BUILDING_USAGE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("SUB_BUILDING_TYPE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("WATER_SCHEDULE")),
                        returnValidInt(data.getJSONObject(0).getString("WATER_CONNECTION_TYPE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("PIPE_MATERIAL_ID")),
                        returnValidInt(data.getJSONObject(0).getString("BRAND_NEW_METER")),
                        returnValidInt(data.getJSONObject(0).getString("VALVE_TYPE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("VALVE_STATUS_ID")),
                        returnValidString(data.getJSONObject(0).getString("ENABLED_ID")).equals("1") ||
                                returnValidString(data.getJSONObject(0).getString("ENABLED_ID")).toLowerCase().equals("yes") ? 1 : 0,
                        returnValidInt(data.getJSONObject(0).getString("REDUCER_DIAMETER_ID")),
                        returnValidInt(data.getJSONObject(0).getString("DISTRICT_NAME_ID")),
                        returnValidString(data.getJSONObject(0).getString("DMA_ZONE_ID")),
                        returnValidString(data.getJSONObject(0).getString("LAST_UPDATED")),
                        returnValidString(data.getJSONObject(0).getString("LOCATION_NO")),
                        returnValidString(data.getJSONObject(0).getString("POST_CODE")),
                        returnValidString(data.getJSONObject(0).getString("SCECO_NO")),
                        returnValidString(data.getJSONObject(0).getString("NumberOfElectricMetres")),
                        returnValidString(data.getJSONObject(0).getString("LAST_READING")),
                        returnValidString(data.getJSONObject(0).getString("BUILDING_NO")),
                        returnValidString(data.getJSONObject(0).getString("NO_OF_FLOORS")),
                        returnValidString(data.getJSONObject(0).getString("BUILDING_DUP")),
                        returnValidString(data.getJSONObject(0).getString("BUILDING_NO_M")),
                        returnValidString(data.getJSONObject(0).getString("BUILD_DESC")),
                        returnValidString(data.getJSONObject(0).getString("STREET_SER_NU")),
                        returnValidString(data.getJSONObject(0).getString("STREET_NAME")),
                        returnValidString(data.getJSONObject(0).getString("STREET_NO_M")),
                        returnValidString(data.getJSONObject(0).getString("SECTOR_NAME")),
                        returnValidString(data.getJSONObject(0).getString("SUB_NAME")),
                        returnValidString(data.getJSONObject(0).getString("IsSewerConnectionExist")),
                        returnValidString(data.getJSONObject(0).getString("ARABIC_NAME")),
                        returnValidString(data.getJSONObject(0).getString("CUSTOMER_ACTIVE_ID")),
                        returnValidString(data.getJSONObject(0).getString("Y_Map")),
                        returnValidString(data.getJSONObject(0).getString("X_Map")),
                        returnValidString(data.getJSONObject(0).getString("GROUND_ELEVATION")),
                        returnValidString(data.getJSONObject(0).getString("ELEVATION")),
                        returnValidDate1(data.getJSONObject(0).getString("CreatedDate")));
                view.loadMeterInfo(meterObject);
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_meter_not_exist));
        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_meter_not_exist));

    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}


/*
{
    "status": "Success",
    "mesg": "OK",
    "data": [
        {
            "SERIAL_NO": "",
            "HCN": "79071107562000",
            "METER_ADDRESS": null,
            "PLATE_NO": "7911075620",
            "METER_DIAMETER_ID": "25 MM",
            "METER_BRAND_ID": "ITRON",
            "METER_TYPE_ID": "Mechanical",
            "METER_STATUS_ID": "WORKING",
            "METER_MATERIAL_ID": "COPPER",
            "METER_REMARKS_ID": null,
            "READ_TYPE_ID": null,
            "PIPE_AFTER_METER_ID": "Connected",
            "PIPE_SIZE_ID": "25 MM",
            "METER_BOX_POSITION_ID": "Wall",
            "MAINTENANCE_AREA_ID": null,
            "METER_BOX_TYPES_ID": null,
            "COVER_STATUS": "غطاء جيد",
            "LOCATION_IN_BUILDING_ID": "OutSide",
            "BUILDING_USAGE_ID": "Commercial",
            "SUB_BUILDING_TYPE_ID": "Hotel",
            "WATER_SCHEDULE": null,
            "WATER_CONNECTION_TYPE_ID": "HC",
            "PIPE_MATERIAL_ID": null,
            "BRAND_NEW_METER": null,
            "VALVE_TYPE_ID": null,
            "VALVE_STATUS_ID": "Open",
            "ENABLED_ID": 1,
            "REDUCER_DIAMETER_ID": null,
            "DISTRICT_NAME_ID": "حى 889",
            "DMA_ZONE_ID": 2,
            "LAST_UPDATED": null,
            "LOCATION_NO": 127,
            "POST_CODE": "4382-78",
            "SCECO_NO": "8267145",
            "NumberOfElectricMetres": null,
            "LAST_READING": 687,
            "BUILDING_NO": "063",
            "NO_OF_FLOORS": 3,
            "BUILDING_DUP": "00",
            "BUILDING_NO_M": "620",
            "BUILD_DESC": "فندق الرمانسية",
            "STREET_SER_NU": "027",
            "STREET_NAME": "جابر بن حيان",
            "STREET_NO_M": "075",
            "SECTOR_NAME": null,
            "SUB_NAME": null,
            "IsSewerConnectionExist": null,
            "ARABIC_NAME": "خالد ناصر المطوع",
            "CUSTOMER_ACTIVE_ID": "YES",
            "X_Map": 45.29217001,
            "Y_Map": 24.06923757
        }
    ]
}
 */
