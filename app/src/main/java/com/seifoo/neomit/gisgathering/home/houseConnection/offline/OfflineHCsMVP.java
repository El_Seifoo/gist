package com.seifoo.neomit.gisgathering.home.houseConnection.offline;

import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;

import java.util.ArrayList;

public interface OfflineHCsMVP {

    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void showToastMessage(String message);

        void loadHCsData(ArrayList<HouseConnectionObject> Hcs, boolean isVisible);

        void removeListItem(int position);

        void navigateDestination(String key, HouseConnectionObject HC, Class destination);

        void navigateToEditFire(Intent intent, int requestCode);

        void updateListItem(int position, HouseConnectionObject HC);

        void removeListItem(long id);

        void navigateToMap(int requestCode, HouseConnectionObject object, int position);

        int getSpinnerSelectedPosition();

        void navigateToMap2(int requestCode, ArrayList<HouseConnectionObject> hcs);
    }

    interface Presenter {
        void requestHCsData(int type);

        void requestRemoveHcObjById(long id, int position);

        void OnListItemClickListener(int viewId, int position, HouseConnectionObject object);

        void requestEditHcData(int position, HouseConnectionObject object);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestUploadData(int type);

        void showLocations(ArrayList<HouseConnectionObject> hcs);
    }
}
