package com.seifoo.neomit.gisgathering.home.pump.offline;

import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.home.pump.edit.EditPumpActivity;
import com.seifoo.neomit.gisgathering.home.pump.offline.details.OfflinePumpsDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class OfflinePumpsPresenter implements OfflinePumpsMVP.Presenter, OfflinePumpsModel.VolleyCallback, OfflinePumpsModel.DBCallback {
    private OfflinePumpsMVP.View view;
    private OfflinePumpsModel model;

    public OfflinePumpsPresenter(OfflinePumpsMVP.View view, OfflinePumpsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestPumpsData(int type) {
        model.getPumpsList(view.getAppContext(), this, type, 0);
    }

    @Override
    public void requestRemovePumpObjById(long id, int position) {
        model.removePumpObject(view.getActContext(), this, id, position);
    }

    private static final int SHOW_ALL_IN_MAP_REQUEST = 10;

    @Override
    public void OnListItemClickListener(int viewId, int position, PumpObject pump) {
        switch (viewId) {
            case R.id.more_details:
                view.navigateDestination("OfflinePumpObj", pump, OfflinePumpsDetailsActivity.class);
                break;
            case R.id.remove_item:
                this.requestRemovePumpObjById(pump.getId(), position);
                break;
            case R.id.edit_item:
                this.requestEditPumpData(position, pump);
                break;
            case R.id.map:
                view.navigateToMap(SHOW_ALL_IN_MAP_REQUEST, pump, position);
        }
    }

    private static final int EDIT_PUMP_OBJECT_REQUEST = 1;

    @Override
    public void requestEditPumpData(int position, PumpObject pump) {
        Intent intent = new Intent(view.getAppContext(), EditPumpActivity.class);
        intent.putExtra("PumpObj", pump);
        intent.putExtra("position", position);
        view.navigateToEditPump(intent, EDIT_PUMP_OBJECT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_PUMP_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            view.updateListItem(data.getExtras().getInt("position"), (PumpObject) data.getSerializableExtra("editedPumpObj"));
            return;
        }

        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            this.requestPumpsData(view.getSpinnerSelectedPosition());
        }
    }

    @Override
    public void requestUploadData(int type) {
        model.getPumpsList(view.getAppContext(), this, type, 1);
    }

    @Override
    public void showLocations(ArrayList<PumpObject> pumps) {
        view.navigateToMap2(SHOW_ALL_IN_MAP_REQUEST, pumps);
    }

    @Override
    public void onSyncingResponse(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        ArrayList<Long> accepted = new ArrayList<>(), rejected = new ArrayList<>();
        for (int i = 0; i < responseArray.length(); i++) {
            if (responseArray.getJSONObject(i).getString("status").equals("success"))
                accepted.add(responseArray.getJSONObject(i).getLong("id"));
            else rejected.add(responseArray.getJSONObject(i).getLong("id"));
        }

        if (!accepted.isEmpty()) {
            for (int i = 0; i < accepted.size(); i++) {
                model.removePumpObject(view.getActContext(), this, accepted.get(i), -1);
            }
        }
    }

    @Override
    public void onSyncingError(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onGetPumpsListCalled(ArrayList<PumpObject> pumps, int index) {
        if (index == 0) {
            if (pumps.isEmpty())
                view.showEmptyListText();
            else {
                boolean isVisible = false;
                for (int i = 0; i < pumps.size(); i++) {
                    if (pumps.get(i).getLatitude() != null && pumps.get(i).getLongitude() != null) {
                        if (!pumps.get(i).getLatitude().isEmpty() && !pumps.get(i).getLongitude().isEmpty()) {
                            isVisible = true;
                            break;
                        }
                    }
                }
                view.loadPumpsData(pumps, isVisible);
            }
        } else {
            if (pumps.isEmpty())
                view.showToastMessage(view.getActContext().getString(R.string.no_pumps_to_synchronize));
            else {
                view.showProgress();
                model.uploadPumpsData(view.getActContext(), this, pumps);
            }
        }
    }

    @Override
    public void onRemoveValveObjectCalled(int flag, int position, long id) {
        if (flag > 0) {
            if (position == -1) {
                view.removeListItem(id);
            } else
                view.removeListItem(position);
        } else view.showToastMessage(view.getActContext().getString(R.string.failed_to_delete));
    }
}
