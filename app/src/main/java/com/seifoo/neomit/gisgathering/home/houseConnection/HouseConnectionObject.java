package com.seifoo.neomit.gisgathering.home.houseConnection;

import java.io.Serializable;

public class HouseConnectionObject implements Serializable {
    private long id, houseConnectionId;
    private String serialNumber, HCN;
    private int type, diameter, material, assetStatus, districtName;
    private String subDistrict, streetName, sectorName;
    private int waterSchedule, remarks;
    private String latitude, longitude;
    private String reason, createdAt;


    // first step const.
    public HouseConnectionObject(String latitude, String longitude, String serialNumber,
                                 String HCN, int type, int diameter, int material, int assetStatus) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.HCN = HCN;
        this.type = type;
        this.diameter = diameter;
        this.material = material;
        this.assetStatus = assetStatus;
    }

    public HouseConnectionObject getFirstObject() {
        return new HouseConnectionObject(latitude, longitude, serialNumber, HCN, type, diameter, material, assetStatus);
    }

    // second step const.
    public HouseConnectionObject(int districtName, String subDistrict, String streetName,
                                 String sectorName, int waterSchedule, int remarks) {
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.streetName = streetName;
        this.sectorName = sectorName;
        this.waterSchedule = waterSchedule;
        this.remarks = remarks;
    }

    public HouseConnectionObject getSecondObject() {
        return new HouseConnectionObject(districtName, subDistrict, streetName, sectorName, waterSchedule, remarks);
    }

    public HouseConnectionObject(HouseConnectionObject firstObj, HouseConnectionObject secondObj) {
        this.latitude = firstObj.getLatitude();
        this.longitude = firstObj.getLongitude();
        this.serialNumber = firstObj.getSerialNumber();
        this.HCN = firstObj.getHCN();
        this.type = firstObj.getType();
        this.diameter = firstObj.getDiameter();
        this.material = firstObj.getMaterial();
        this.assetStatus = firstObj.getAssetStatus();

        this.districtName = secondObj.getDistrictName();
        this.subDistrict = secondObj.getSubDistrict();
        this.streetName = secondObj.getStreetName();
        this.sectorName = secondObj.getSectorName();
        this.waterSchedule = secondObj.getWaterSchedule();
        this.remarks = secondObj.getRemarks();
    }

    public HouseConnectionObject(long id, long houseConnectionId, String serialNumber, String HCN, int type, int diameter,
                                 int material, int assetStatus, int districtName, String subDistrict, String streetName, String sectorName,
                                 int waterSchedule, int remarks, String latitude, String longitude, String createdAt) {
        this.id = id;
        this.houseConnectionId = houseConnectionId;
        this.serialNumber = serialNumber;
        this.HCN = HCN;
        this.type = type;
        this.diameter = diameter;
        this.material = material;
        this.assetStatus = assetStatus;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.streetName = streetName;
        this.sectorName = sectorName;
        this.waterSchedule = waterSchedule;
        this.remarks = remarks;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdAt = createdAt;
    }

    public HouseConnectionObject(String serialNumber, String HCN, int type, int diameter, int material,
                                 int assetStatus, int districtName, String subDistrict, String streetName,
                                 String sectorName, int waterSchedule, int remarks, String latitude, String longitude, String createdAt) {
        this.serialNumber = serialNumber;
        this.HCN = HCN;
        this.type = type;
        this.diameter = diameter;
        this.material = material;
        this.assetStatus = assetStatus;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.streetName = streetName;
        this.sectorName = sectorName;
        this.waterSchedule = waterSchedule;
        this.remarks = remarks;
        this.latitude = latitude;
        this.longitude = longitude;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public long getHouseConnectionId() {
        return houseConnectionId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getHCN() {
        return HCN;
    }

    public int getType() {
        return type;
    }

    public int getDiameter() {
        return diameter;
    }

    public int getMaterial() {
        return material;
    }

    public int getAssetStatus() {
        return assetStatus;
    }

    public int getDistrictName() {
        return districtName;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getSectorName() {
        return sectorName;
    }

    public int getWaterSchedule() {
        return waterSchedule;
    }

    public int getRemarks() {
        return remarks;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setHouseConnectionId(long houseConnectionId) {
        this.houseConnectionId = houseConnectionId;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    //rejected const.
    public HouseConnectionObject(long houseConnectionId, String serialNumber, String HCN, int type,
                                 int diameter, int material, int assetStatus, int districtName, String subDistrict,
                                 String streetName, String sectorName, int waterSchedule, int remarks, String latitude,
                                 String longitude, String reason, String createdAt) {
        this.houseConnectionId = houseConnectionId;
        this.serialNumber = serialNumber;
        this.HCN = HCN;
        this.type = type;
        this.diameter = diameter;
        this.material = material;
        this.assetStatus = assetStatus;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.streetName = streetName;
        this.sectorName = sectorName;
        this.waterSchedule = waterSchedule;
        this.remarks = remarks;
        this.latitude = latitude;
        this.longitude = longitude;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
