package com.seifoo.neomit.gisgathering.home.chamber.rejected;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RejectedChambersPresenter implements RejectedChambersMVP.Presenter, RejectedChambersModel.VolleyCallback, RejectedChambersModel.DBCallback {
    private RejectedChambersMVP.View view;
    private RejectedChambersModel model;

    public RejectedChambersPresenter(RejectedChambersMVP.View view, RejectedChambersModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestChambers() {
        view.showProgress();
        model.getRejected(view.getAppContext(), this);
    }

    /*
    "Corrected_Id": 3,
            "X_Map": "45.22523",
            "Y_Map": "24.255",
            "MATERIAL_PIPE": "1",
            "CH_NUMBER": "1",
            "LENGTH": 1,
            "WIDTH": 1,
            "DEPTH": 1,
            "DIAMETER": "1",
            "DEPTH_UNDER_PIPE": 1,
            "DEPTH_ABOVE_PIPE": 1,
            "DISTRICT_NAME_ID": "1",
            "DMA_ZONE_ID": 1,
            "SUB_DISTRICT_NO": "1",
            "STREET_NAME": null,
            "STREET_SER_NU": "1",
            "SUB_NAME": "1",
            "SECTOR_NAME": "1",
            "DIAMETER_AIR_VALVE": 1,
            "DIAMETER_WA_VALVE": 1,
            "DIAMETER_ISO_VALVE": 1,
            "DIAMETER_FLOWMETER": 1,
            "IMAGE_NO": "1",
            "F_TYPE_ID": "1",
            "UPDATE_FIELD": "1",
            "REMARKS": null,
            "Reason": "Invalid "
     */
    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ArrayList<ChamberObject> rejectedChambers = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {
                    rejectedChambers.add(new ChamberObject(data.getJSONObject(i).getLong("Corrected_Id"),
                            returnValidString(data.getJSONObject(i).getString("Y_Map")),
                            returnValidString(data.getJSONObject(i).getString("X_Map")),
                            returnValidInt(data.getJSONObject(i).getString("MATERIAL_PIPE")),
                            returnValidString(data.getJSONObject(i).getString("CH_NUMBER")),
                            returnValidString(data.getJSONObject(i).getString("LENGTH")),
                            returnValidString(data.getJSONObject(i).getString("WIDTH")),
                            returnValidString(data.getJSONObject(i).getString("DEPTH")),
                            returnValidInt(data.getJSONObject(i).getString("DIAMETER")),
                            returnValidString(data.getJSONObject(i).getString("DEPTH_UNDER_PIPE")),
                            returnValidString(data.getJSONObject(i).getString("DEPTH_ABOVE_PIPE")),
                            returnValidInt(data.getJSONObject(i).getString("DISTRICT_NAME_ID")),
                            returnValidString(data.getJSONObject(i).getString("DMA_ZONE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("SUB_DISTRICT_NO")),
                            returnValidString(data.getJSONObject(i).getString("STREET_NAME")),
                            returnValidString(data.getJSONObject(i).getString("STREET_SER_NU")),
                            returnValidString(data.getJSONObject(i).getString("SUB_NAME")),
                            returnValidString(data.getJSONObject(i).getString("SECTOR_NAME")),
                            returnValidInt(data.getJSONObject(i).getString("DIAMETER_AIR_VALVE")),
                            returnValidInt(data.getJSONObject(i).getString("DIAMETER_WA_VALVE")),
                            returnValidInt(data.getJSONObject(i).getString("DIAMETER_ISO_VALVE")),
                            returnValidInt(data.getJSONObject(i).getString("DIAMETER_FLOWMETER")),
                            returnValidString(data.getJSONObject(i).getString("IMAGE_NO")),
                            returnValidInt(data.getJSONObject(i).getString("F_TYPE_ID")),
                            returnValidDate(data.getJSONObject(i).getString("UPDATE_FIELD")),
                            returnValidInt(data.getJSONObject(i).getString("REMARKS")),
                            returnValidString(data.getJSONObject(i).getString("Reason")),
                            returnValidDate1(data.getJSONObject(i).getString("CreatedDate"))));
//x -> longitude , y -> latitude
                }
                model.checkDatabaseChambers(view.getAppContext(), this, rejectedChambers);
            } else {
                view.hideProgress();
                view.showEmptyListText();
            }
        } else {
            view.hideProgress();
            view.showEmptyListText();
        }
    }

    private String returnValidString(String data) {
        return data.isEmpty() || data.toLowerCase().equals("null") ? "" : data;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onCheckingChambersCalled(ArrayList<ChamberObject> chambers) {
        view.hideProgress();
        if (chambers.isEmpty())
            view.showEmptyListText();
        else
            view.loadRejectedChambers(chambers);
    }
}
