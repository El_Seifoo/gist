package com.seifoo.neomit.gisgathering.home.pump.info.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class GetPumpInfoDetailsModel {

    public void returnValidGeoMaster(Context context, DBCallback callback, PumpObject pump, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds1(ids, types, lang), pump);
    }

    protected interface DBCallback {
        void onConvertingIdsCalled(ArrayList<String> strings, PumpObject pump);
    }
}
