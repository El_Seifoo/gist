package com.seifoo.neomit.gisgathering.home.valve.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

import java.util.ArrayList;

public interface OfflineValvesDetailsMVP {

    interface View {
        Context getActContext();

        Context getAppContext();


        void loadValveMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestValveDetails(ValveObject valve);
    }
}
