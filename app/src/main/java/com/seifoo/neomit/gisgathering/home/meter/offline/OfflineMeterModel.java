package com.seifoo.neomit.gisgathering.home.meter.offline;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;
import com.seifoo.neomit.gisgathering.utils.Urls;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class OfflineMeterModel {

    protected void uploadMeterData(final Context context, final VolleyCallback callback, final ArrayList<MeterObject> meters) {

//        Log.e("metersLocation",meters.get(0).getMeterLatitude());
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSyncingResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("parsing error ", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onSyncingError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""));
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                for (int i = 0; i < meters.size(); i++) {
                    if (meters.get(i).getMeterId() != 0.0d)
                        params.put("Json[data][" + i + "][Corrected_Id]", meters.get(i).getMeterId() + "");

                    params.put("Json[data][" + i + "][RECORD_ID]", meters.get(i).getId() + "");
                    params.put("Json[data][" + i + "][SERIAL_NO]", meters.get(i).getMeterSerialNumber());
                    params.put("Json[data][" + i + "][HCN]", meters.get(i).getMeterHcn());
                    params.put("Json[data][" + i + "][METER_ADDRESS]", meters.get(i).getMeterMeterAddress());
                    params.put("Json[data][" + i + "][PLATE_NO]", meters.get(i).getMeterPlateNumber());
                    params.put("Json[data][" + i + "][METER_DIAMETER_ID]", meters.get(i).getMeterMeterDiameterId() + "");
                    params.put("Json[data][" + i + "][METER_BRAND_ID]", meters.get(i).getMeterMeterBrandId() + "");
                    params.put("Json[data][" + i + "][METER_TYPE_ID]", meters.get(i).getMeterMeterTypeId() + "");
                    params.put("Json[data][" + i + "][METER_STATUS_ID]", meters.get(i).getMeterMeterStatusId() + "");
                    params.put("Json[data][" + i + "][METER_MATERIAL_ID]", meters.get(i).getMeterMeterMaterialId() + "");
                    params.put("Json[data][" + i + "][METER_REMARKS_ID]", meters.get(i).getMeterMeterRemarksId() + "");
                    params.put("Json[data][" + i + "][READ_TYPE_ID]", meters.get(i).getMeterReadTypeId() + "");
                    params.put("Json[data][" + i + "][PIPE_AFTER_METER_ID]", meters.get(i).getMeterPipeAfterMeterId() + "");
                    params.put("Json[data][" + i + "][PIPE_SIZE_ID]", meters.get(i).getMeterPipeSizeId() + "");
                    params.put("Json[data][" + i + "][MAINTENANCE_AREA_ID]", meters.get(i).getMeterMaintenanceAreaId() + "");
                    params.put("Json[data][" + i + "][METER_BOX_POSITION_ID]", meters.get(i).getMeterMeterBoxPositionId() + "");
                    params.put("Json[data][" + i + "][METER_BOX_TYPES_ID]", meters.get(i).getMeterMeterBoxTypeId() + "");
                    params.put("Json[data][" + i + "][COVER_STATUS]", meters.get(i).getMeterCoverStatusId() + "");
                    params.put("Json[data][" + i + "][LOCATION_IN_BUILDING_ID]", meters.get(i).getMeterLocationId() + "");
                    params.put("Json[data][" + i + "][BUILDING_USAGE_ID]", meters.get(i).getMeterBuildingUsageId() + "");
                    params.put("Json[data][" + i + "][SUB_BUILDING_TYPE_ID]", meters.get(i).getMeterSubBuildingTypeId() + "");
                    params.put("Json[data][" + i + "][WATER_SCHEDULE]", meters.get(i).getMeterWaterScheduleId() + "");
                    params.put("Json[data][" + i + "][WATER_CONNECTION_TYPE_ID]", meters.get(i).getMeterWaterConnectionTypeId() + "");
                    params.put("Json[data][" + i + "][PIPE_MATERIAL_ID]", meters.get(i).getMeterPipeMaterialId() + "");
                    params.put("Json[data][" + i + "][BRAND_NEW_METER]", meters.get(i).getMeterBrandNewMeterId() + "");
                    params.put("Json[data][" + i + "][VALVE_TYPE_ID]", meters.get(i).getMeterValveTypeId() + "");
                    params.put("Json[data][" + i + "][VALVE_STATUS_ID]", meters.get(i).getMeterValveStatusId() + "");
                    params.put("Json[data][" + i + "][ENABLED_ID]", meters.get(i).getMeterEnabledId() + "");
                    params.put("Json[data][" + i + "][REDUCER_DIAMETER_ID]", meters.get(i).getMeterReducerDiameterId() + "");
                    params.put("Json[data][" + i + "][DISTRICT_NAME_ID]", meters.get(i).getMeterDistrictName()+"");
                    params.put("Json[data][" + i + "][DMA_ZONE_ID]", meters.get(i).getMeterDmaZone());
                    params.put("Json[data][" + i + "][LAST_UPDATED]", meters.get(i).getMeterLastUpdated());
                    params.put("Json[data][" + i + "][LOCATION_NO]", meters.get(i).getMeterLocationNumber());
                    params.put("Json[data][" + i + "][POST_CODE]", meters.get(i).getMeterPostCode());
                    params.put("Json[data][" + i + "][SCECO_NO]", meters.get(i).getMeterScecoNumber());
                    params.put("Json[data][" + i + "][NumberOfElectricMetres]", meters.get(i).getMeterNumberOfElectricMeters());
                    params.put("Json[data][" + i + "][LAST_READING]", meters.get(i).getMeterLastReading().isEmpty() ? "-1" : meters.get(i).getMeterLastReading());
                    params.put("Json[data][" + i + "][BUILDING_NO]", meters.get(i).getMeterBuildingNumber());
                    params.put("Json[data][" + i + "][NO_OF_FLOORS]", meters.get(i).getMeterNumberOfFloors());
                    params.put("Json[data][" + i + "][BUILDING_DUP]", meters.get(i).getMeterBuildingDuplication());
                    params.put("Json[data][" + i + "][BUILDING_NO_M]", meters.get(i).getMeterBuildingNumberM());
                    params.put("Json[data][" + i + "][BUILD_DESC]", meters.get(i).getMeterBuildingDescription());
                    params.put("Json[data][" + i + "][STREET_SER_NU]", meters.get(i).getMeterStreetNumber());
                    params.put("Json[data][" + i + "][STREET_NAME]", meters.get(i).getMeterStreetName());
                    params.put("Json[data][" + i + "][STREET_NO_M]", meters.get(i).getMeterStreetNumberM());
                    params.put("Json[data][" + i + "][SECTOR_NAME]", meters.get(i).getMeterSectorName());
                    params.put("Json[data][" + i + "][SUB_NAME]", meters.get(i).getMeterSubName());
                    params.put("Json[data][" + i + "][IsSewerConnectionExist]", meters.get(i).getMeterIsSewerConnectionExist().equals("1") ? context.getString(R.string.exist) : context.getString(R.string.not_exist));
                    params.put("Json[data][" + i + "][ARABIC_NAME]", meters.get(i).getMeterArabicName());
                    params.put("Json[data][" + i + "][CUSTOMER_ACTIVE_ID]", meters.get(i).getMeterCustomerActive().equals("1") ? context.getString(R.string.yes) : context.getString(R.string.no));
                    params.put("Json[data][" + i + "][X_Map]", meters.get(i).getMeterLongitude() != null ? meters.get(i).getMeterLongitude() : "360");
                    params.put("Json[data][" + i + "][Y_Map]", meters.get(i).getMeterLatitude() != null ? meters.get(i).getMeterLatitude() : "360");
                    params.put("Json[data][" + i + "][GROUND_ELEVATION]", meters.get(i).getGroundElevation());
                    params.put("Json[data][" + i + "][ELEVATION]", meters.get(i).getElevation());
                    params.put("Json[data][" + i + "][CreatedDate]", meters.get(i).getCreatedAt());
                }
                params.put("Json[tableName]", context.getString(R.string.api_meter_table_name));
                params.put("Json[databaseName]", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""));
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }
    /*

    x -> longitude , y -> latitude
son[data][0][SERIAL_NO]:80000114
Json[data][0][HCN]:8000002
Json[data][0][METER_ADDRESS]:8000001234
Json[data][0][PLATE_NO]:9000001
Json[data][0][METER_DIAMETER_ID]:1
Json[data][0][METER_BRAND_ID]:2
Json[data][0][METER_TYPE_ID]:1
Json[data][0][METER_STATUS_ID]:1
Json[data][0][METER_MATERIAL_ID]:2
Json[data][0][METER_REMARKS_ID]:
Json[data][0][READ_TYPE_ID]:3
Json[data][0][PIPE_AFTER_METER_ID]:1
Json[data][0][PIPE_SIZE_ID]:1
Json[data][0][MAINTENANCE_AREA_ID]:1
Json[data][0][METER_BOX_POSITION_ID]:1
Json[data][0][METER_BOX_TYPES_ID]:
Json[data][0][COVER_STATUS]:غطاء جيد
Json[data][0][LOCATION_IN_BUILDING_ID]:1
Json[data][0][BUILDING_USAGE_ID]:1
Json[data][0][SUB_BUILDING_TYPE_ID]:1
Json[data][0][WATER_SCHEDULE]:
Json[data][0][WATER_CONNECTION_TYPE_ID]:1
Json[data][0][PIPE_MATERIAL_ID]:
Json[data][0][BRAND_NEW_METER]:3
Json[data][0][VALVE_TYPE_ID]:
Json[data][0][VALVE_STATUS_ID]:1
Json[data][0][ENABLED_ID]:1
Json[data][0][REDUCER_DIAMETER_ID]:
Json[data][0][DISTRICT_NAME_ID]:حى الزهور
Json[data][0][DMA_ZONE_ID]:1
Json[data][0][LAST_UPDATED]:
Json[data][0][LOCATION_NO]:66
Json[data][0][POST_CODE]:3763-39
Json[data][0][SCECO_NO]:8266905
Json[data][0][NumberOfElectricMetres]:
Json[data][0][LAST_READING]:0.00000000
Json[data][0][BUILDING_NO]:117
Json[data][0][NO_OF_FLOORS]:2
Json[data][0][BUILDING_DUP]:00
Json[data][0][BUILDING_NO_M]:560
Json[data][0][BUILD_DESC]:مدرسه اجيال المعرفة الأهلية
Json[data][0][STREET_SER_NU]:030
Json[data][0][STREET_NAME]:شارع زيد بن ثابت
Json[data][0][STREET_NO_M]:045
Json[data][0][SECTOR_NAME]:
Json[data][0][SUB_NAME]:
Json[data][0][IsSewerConnectionExist]:
Json[data][0][ARABIC_NAME]:سعدبد اللهيب
Json[data][0][CUSTOMER_ACTIVE_ID]:YES
Json[data][0][X_Map]:45.28914151
Json[data][0][Y_Map]:24.06415872
Json[tableName]:meteringpoint
Json[databaseName]:re_al_qwayah
     */

    protected void getMetersList(Context context, DBCallback callback,int type, int index) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetMetersListCalled(DataBaseHelper.getmInstance(context).getAllMeters(type), index);
    }

    protected void removeMeterObject(Context context, DBCallback callback, long id, int position) {
        callback.onRemoveMeterObjectCalled(DataBaseHelper.getmInstance(context).deleteMeterById(id), position, id);
    }

    protected interface VolleyCallback {
        void onSyncingResponse(String response) throws JSONException;

        void onSyncingError(VolleyError error);
    }

    protected interface DBCallback {

        void onGetMetersListCalled(ArrayList<MeterObject> meterObjectList, int index);

        void onRemoveMeterObjectCalled(int flag, int position, long id);
    }
}