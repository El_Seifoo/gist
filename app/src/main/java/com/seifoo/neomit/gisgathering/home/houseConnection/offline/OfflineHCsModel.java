package com.seifoo.neomit.gisgathering.home.houseConnection.offline;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;
import com.seifoo.neomit.gisgathering.utils.Urls;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class OfflineHCsModel {

    protected void uploadHCsData(final Context context, final VolleyCallback callback, final ArrayList<HouseConnectionObject> HCs) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSyncingResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("parsing error ", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onSyncingError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""));
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                for (int i = 0; i < HCs.size(); i++) {
                    if (HCs.get(i).getHouseConnectionId() != 0.0d)
                        params.put("Json[data][" + i + "][Corrected_Id]", HCs.get(i).getHouseConnectionId() + "");
                    params.put("Json[data][" + i + "][RECORD_ID]", HCs.get(i).getId() + "");
                    params.put("Json[data][" + i + "][SERIAL_NO]", HCs.get(i).getSerialNumber());
                    params.put("Json[data][" + i + "][HCN]", HCs.get(i).getHCN());
                    params.put("Json[data][" + i + "][F_TYPE_ID]", HCs.get(i).getType() + "");
                    params.put("Json[data][" + i + "][DIAMETER_ID]", HCs.get(i).getDiameter() + "");
                    params.put("Json[data][" + i + "][MATERIAL_ID]", HCs.get(i).getMaterial() + "");
                    params.put("Json[data][" + i + "][ASSET_STATUS_ID]", HCs.get(i).getAssetStatus() + "");
                    params.put("Json[data][" + i + "][DISTRICT_NAME_ID]", HCs.get(i).getDistrictName() + "");
                    params.put("Json[data][" + i + "][SUP_DISTRICT_ID]", HCs.get(i).getSubDistrict());
                    params.put("Json[data][" + i + "][STREET_NAME]", HCs.get(i).getStreetName());
                    params.put("Json[data][" + i + "][SECTOR_NAME]", HCs.get(i).getSectorName());
                    params.put("Json[data][" + i + "][WATER_SCHEDULE_ID]", HCs.get(i).getWaterSchedule() + "");
                    params.put("Json[data][" + i + "][REMARKS]", HCs.get(i).getRemarks() + "");
                    params.put("Json[data][" + i + "][CreatedDate]", HCs.get(i).getCreatedAt());
                    params.put("Json[data][" + i + "][X_Map]", !HCs.get(i).getLongitude().isEmpty() ? HCs.get(i).getLongitude() : "360");
                    params.put("Json[data][" + i + "][Y_Map]", !HCs.get(i).getLatitude().isEmpty() ? HCs.get(i).getLatitude() : "360");
                }
                params.put("Json[tableName]", context.getString(R.string.api_house_connection_table_name));
                params.put("Json[databaseName]", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""));
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected void getHCsList(Context context, DBCallback callback, int type, int index) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetHCsListCalled(DataBaseHelper.getmInstance(context).getAllHCs(type), index);
    }

    protected void removeHcObject(Context context, DBCallback callback, long id, int position) {
        callback.onRemoveHcObjectCalled(DataBaseHelper.getmInstance(context).deleteHC(id), position, id);
    }

    protected interface VolleyCallback {
        void onSyncingResponse(String response) throws JSONException;

        void onSyncingError(VolleyError error);
    }

    protected interface DBCallback {

        void onGetHCsListCalled(ArrayList<HouseConnectionObject> HCs, int index);

        void onRemoveHcObjectCalled(int flag, int position, long id);
    }
}
