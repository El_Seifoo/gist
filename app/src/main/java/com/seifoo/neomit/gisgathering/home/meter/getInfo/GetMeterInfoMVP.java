package com.seifoo.neomit.gisgathering.home.meter.getInfo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;

public interface GetMeterInfoMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void initializeScanner(IntentIntegrator integrator, int flag);

        void setSerialNumber(String data);

        void setHCN(String data);

        void setMeterAddress(String data);

        void setPlateNumber(String data);

        void loadMeterInfo(MeterObject moreDetails);
    }

    interface Presenter {

        void whichQRClicked(int id);

        void requestQrCode(Activity activity, String flagString);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents, int flag);

        void requestGetInfo(String serialNumber, String hcn, String meterAddress, String plateNumber);
    }
}
