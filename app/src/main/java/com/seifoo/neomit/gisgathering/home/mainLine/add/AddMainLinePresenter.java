package com.seifoo.neomit.gisgathering.home.mainLine.add;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class AddMainLinePresenter implements AddMainLineMVP.Presenter, AddMainLineModel.DBCallback {
    private AddMainLineMVP.View view;
    private AddMainLineMVP.MainView mainView;
    private AddMainLineMVP.FirstView firstView;
    private AddMainLineMVP.SecondView secondView;
    private AddMainLineModel model;

    public AddMainLinePresenter(AddMainLineMVP.View view, AddMainLineMVP.SecondView secondView, AddMainLineModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    public AddMainLinePresenter(AddMainLineMVP.View view, AddMainLineMVP.FirstView firstView, AddMainLineModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public AddMainLinePresenter(AddMainLineMVP.View view, AddMainLineMVP.MainView mainView, AddMainLineModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    @Override
    public void requestFirstSpinnersData() {
        //  diameter 0, type 24, enabled 22, material 4
        model.getGeoMasters(view.getAppContext(), this, new int[]{0, 24, 22, 4}, 1);
    }

    @Override
    public void requestSecondSpinnersData() {
        //  districtName 25, subDistrict 28, waterSchedule 16, assetStatus 3 , remarks 5
        model.getGeoMasters(view.getAppContext(), this, new int[]{25, 28, 16, 3, 5}, 2);

    }

    @Override
    public void requestPassFirstStepDataToActivity(MainLineObject mainLine) {
        if (!mainLine.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        mainLine.setLatitude(mainLine.getLatitude().split(",")[0]);
        mainLine.setLongitude(mainLine.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof AddMainLineActivity) {
            ((AddMainLineActivity) view.getAppContext()).passFirstObj(mainLine);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(MainLineObject mainLine) {
        if (view.getAppContext() instanceof AddMainLineActivity) {
            ((AddMainLineActivity) view.getAppContext()).passSecondObj(mainLine);
        }
    }

    @Override
    public void requestAddMainLine(MainLineObject mainLine) {
        mainLine.setCreatedAt(returnValidNumbers(mainView.getCurrentDate()));
        model.insertMainLine(view.getAppContext(), this, mainLine);
    }

    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQrCode(((AddMainLineActivity) view.getAppContext()));
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        firstView.initializeScanner(integrator);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents());
        }
    }

    @Override
    public void handleQRScannerResult(String contents) {
        firstView.setSerialNumber(contents.replaceAll("[^0-9]", ""));
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getMainLinesLocation(view.getAppContext(), this, prevLatLng);
    }

    @Override
    public void requestDatePickerDialog(String dateString, final int index) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        view.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        if (index == 0)
                            secondView.setLastUpdated(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                        else
                            secondView.setAssigned(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();
    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index) {
        if (index == 1) handleFirstView(masters);
        else if (index == 2) handleSecondView(masters);
    }

    private void handleFirstView(ArrayList<GeoMasterObj> masters) {
        ArrayList<String>
                diameter = new ArrayList<>(),
                type = new ArrayList<>(),
                enabled = new ArrayList<>(),
                material = new ArrayList<>();


        ArrayList<Integer>
                diameterIds = new ArrayList<>(),
                typeIds = new ArrayList<>(),
                enabledIds = new ArrayList<>(),
                materialIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 0) {
                diameter.add(masters.get(i).getName());
                diameterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 24) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 22) {
                enabled.add(masters.get(i).getName());
                enabledIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 4) {
                material.add(masters.get(i).getName());
                materialIds.add(masters.get(i).getGeoMasterId());
            }
        }

        firstView.loadSpinnersData(diameter, diameterIds,
                type, typeIds,
                enabled, enabledIds,
                material, materialIds);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> masters) {
        ArrayList<String>
                districtName = new ArrayList<>(),
                subDistrict = new ArrayList<>(),
                waterSchedule = new ArrayList<>(),
                assetStatus = new ArrayList<>(),
                remarks = new ArrayList<>();

        ArrayList<Integer>
                districtNameIds = new ArrayList<>(),
                subDistrictIds = new ArrayList<>(),
                waterScheduleIds = new ArrayList<>(),
                assetStatusIds = new ArrayList<>(),
                remarksIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 25) {
                districtName.add(masters.get(i).getName());
                districtNameIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 28) {
                subDistrict.add(masters.get(i).getName());
                subDistrictIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 16) {
                waterSchedule.add(masters.get(i).getName());
                waterScheduleIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 3) {
                assetStatus.add(masters.get(i).getName());
                assetStatusIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 5) {
                remarks.add(masters.get(i).getName());
                remarksIds.add(masters.get(i).getGeoMasterId());
            }
        }

        secondView.loadSpinnersData(districtName, districtNameIds,
                subDistrict, subDistrictIds,
                waterSchedule, waterScheduleIds,
                assetStatus, assetStatusIds,
                remarks, remarksIds);
    }

    @Override
    public void onMainLineInsertionCalled(long flag) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.add_main_line_done_successfully));
            mainView.backToParent();
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_save_main_line));
        }
    }

    @Override
    public void onGetMainLinesLocationCalled(ArrayList<HashMap<String, String>> mainLinesLocation, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], mainLinesLocation, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", mainLinesLocation, PICK_LOCATION_REQUEST);
        }
    }
}
