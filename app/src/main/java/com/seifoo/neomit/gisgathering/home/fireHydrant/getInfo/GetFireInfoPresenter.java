package com.seifoo.neomit.gisgathering.home.fireHydrant.getInfo;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class GetFireInfoPresenter implements GetFireInfoMVP.Presenter, GetFireInfoModel.VolleyCallback {
    private GetFireInfoMVP.View view;
    private GetFireInfoModel model;

    public GetFireInfoPresenter(GetFireInfoMVP.View view, GetFireInfoModel model) {
        this.view = view;
        this.model = model;
    }


    @Override
    public void whichQRClicked(int id) {
        switch (id) {
            case R.id.serial_num_qr:
                requestQrCode(((GetFireInfoActivity) view.getAppContext()), view.getAppContext().getString(R.string.serial_number));
                break;
            case R.id.fh_num_qr:
                requestQrCode(((GetFireInfoActivity) view.getAppContext()), view.getAppContext().getString(R.string.fh_number));
                break;
        }
    }

    @Override
    public void requestQrCode(Activity activity, String flagString) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (flagString.equals(view.getAppContext().getString(R.string.fh_number))) {
            flag = 1;
        }
        view.initializeScanner(integrator, flag);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void handleQRScannerResult(String contents, int flag) {
        contents = contents.replaceAll("[^0-9]", "");
        if (flag == 0) {
            view.setSerialNumber(contents);
        } else if (flag == 1) {
            view.setFHNumber(contents);
        }
    }

    @Override
    public void requestGetInfo(String serialNumber, String fhNumber) {
        if (serialNumber.isEmpty() && fhNumber.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.fill_at_least_one_field_to_continue));
            return;
        }
        view.showProgress();
        model.getInfo(view.getActContext(), this, serialNumber, fhNumber);
    }


    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                /*
                String fireSerialNumber, String fireNumber, String fireHeight, String fireCommissionDate, String fireDistrictName,
                             String fireStreetName, String fireSectorName, int fireRemarks, String fireWithWater, int fireValveType, int fireWaterSchedule,
                             String fireLatitude, String fireLongitude, int fireBarrelDiameter, int fireMaterialId, int fireTypeId, int fireStatus,
                             int fireBrand, int fireMaintenanceArea
                 */
                FireHydrantObject fire = new FireHydrantObject(returnValidString(data.getJSONObject(0).getString("SERIAL_NO")),
                        returnValidString(data.getJSONObject(0).getString("FH_NUMBER")),
                        returnValidString(data.getJSONObject(0).getString("HEIGHT")),
                        returnValidDate(data.getJSONObject(0).getString("COMMISSION_DATE")),
                        returnValidInt(data.getJSONObject(0).getString("DISTRICT_NAME_ID")),
                        returnValidString(data.getJSONObject(0).getString("STREET_NAME")),
                        returnValidString(data.getJSONObject(0).getString("SECTOR_NAME")),
                        returnValidInt(data.getJSONObject(0).getString("REMARKS")),
                        returnValidString(data.getJSONObject(0).getString("WITH_WATER")),
                        returnValidInt(data.getJSONObject(0).getString("FEATURE_TYPE_VALVE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("WATER_SCHEDULE_ID")),
                        returnValidString(data.getJSONObject(0).getString("Y_MAP")),
                        returnValidString(data.getJSONObject(0).getString("X_MAP")),
                        returnValidInt(data.getJSONObject(0).getString("BARREL_DIAMETER_ID")),
                        returnValidInt(data.getJSONObject(0).getString("MATERIAL_ID")),
                        returnValidInt(data.getJSONObject(0).getString("F_TYPE_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("F_TYPE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("FIREHYDRANT_BRAND")),
                        returnValidInt(data.getJSONObject(0).getString("FIREHYDRANT_STATUS_ID")),
                        returnValidInt(data.getJSONObject(0).getString("MAINTENANCE_AREA_ID")),
                        returnValidDate1(data.getJSONObject(0).getString("CreatedDate")));
                view.loadFireInfo(fire);
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_fire_hydrant_not_exist));
        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_fire_hydrant_not_exist));
    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }
    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
