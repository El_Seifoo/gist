package com.seifoo.neomit.gisgathering.home.breaks.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class OfflineBreakDetailsModel {
    public void returnValidGeoMaster(Context context, DBCallback callback, BreakObject breakObj, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds1(ids, types, lang), breakObj);
    }

    protected interface DBCallback {
        void onConvertingIdsCalled(ArrayList<String> strings, BreakObject breakObj);
    }
}
