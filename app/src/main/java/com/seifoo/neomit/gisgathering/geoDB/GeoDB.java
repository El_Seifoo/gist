package com.seifoo.neomit.gisgathering.geoDB;

public class GeoDB {
    private long id;
    private String name ;

    public GeoDB(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
