package com.seifoo.neomit.gisgathering.home.tank.offline;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;
import com.seifoo.neomit.gisgathering.utils.Urls;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class OfflineTanksModel {

    protected void uploadTanksData(final Context context, final VolleyCallback callback, final ArrayList<TankObject> tanks) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSyncingResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("parsing error ", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onSyncingError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""));
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            /*
Json[data][0][Corrected_Id]:// optional key - must for edit only
Json[data][0][X_Map]:45.250200
Json[data][0][Y_Map]:24.2525
Json[data][0][SERIAL_NO]:1500
Json[data][0][TANK_NAME]:tanks 1
Json[data][0][FEATURE_TYPE_ID]:1
Json[data][0][ENABLED_ID]:1
Json[data][0][COMMISSION_DATE]:31-03-2019 22:20
Json[data][0][TANK_DIAMETER]:1
Json[data][0][ACTIVE_VOLUME]:1
Json[data][0][INACTIVE_VOLUME]:1
Json[data][0][LOW_LEVEL]:1
Json[data][0][HIGH_LEVEL]:1
Json[data][0][ASSET_STATUS_ID]:1
Json[data][0][STREET_NAME]:xyz
Json[data][0][DISTRICT_NAME_ID]:1
Json[data][0][SECTOR_NAME]:xyz
Json[data][0][REGION_ID]:1
Json[data][0][REMARKS]:asdf
Json[data][0][LAST_UPDATED]:31-03-2019 22:20
Json[data][0][CreatedDate]:31-03-2019 22:20
Json[data][0][RECORD_ID]:20
Json[tableName]:tank
Json[databaseName]:RE_Al_Qwayah
             */
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                for (int i = 0; i < tanks.size(); i++) {
                    if (tanks.get(i).getTankId() != 0.0d)
                        params.put("Json[data][" + i + "][Corrected_Id]", tanks.get(i).getTankId() + "");
                    params.put("Json[data][" + i + "][RECORD_ID]", tanks.get(i).getId() + "");
                    params.put("Json[data][" + i + "][SERIAL_NO]", tanks.get(i).getSerialNumber());
                    params.put("Json[data][" + i + "][TANK_NAME]", tanks.get(i).getTankName());
                    params.put("Json[data][" + i + "][FEATURE_TYPE_ID]", tanks.get(i).getType()+"");
                    params.put("Json[data][" + i + "][ENABLED_ID]", tanks.get(i).getEnabled() + "");
                    params.put("Json[data][" + i + "][COMMISSION_DATE]", tanks.get(i).getCommissionDate());
                    params.put("Json[data][" + i + "][TANK_DIAMETER]", tanks.get(i).getTankDiameter() + "");
                    params.put("Json[data][" + i + "][ACTIVE_VOLUME]", tanks.get(i).getActiveVolume());
                    params.put("Json[data][" + i + "][INACTIVE_VOLUME]", tanks.get(i).getInActiveVolume());
                    params.put("Json[data][" + i + "][LOW_LEVEL]", tanks.get(i).getLowLevel());
                    params.put("Json[data][" + i + "][HIGH_LEVEL]", tanks.get(i).getHighLevel());
                    params.put("Json[data][" + i + "][ASSET_STATUS_ID]", tanks.get(i).getAssetStatus() + "");
                    params.put("Json[data][" + i + "][STREET_NAME]", tanks.get(i).getStreetName());
                    params.put("Json[data][" + i + "][DISTRICT_NAME_ID]", tanks.get(i).getDistrictName() + "");
                    params.put("Json[data][" + i + "][SECTOR_NAME]", tanks.get(i).getSectorName());
                    params.put("Json[data][" + i + "][REGION_ID]", tanks.get(i).getRegion());
                    params.put("Json[data][" + i + "][REMARKS]", tanks.get(i).getRemarks() + "");
                    params.put("Json[data][" + i + "][LAST_UPDATED]", tanks.get(i).getLastUpdated());
                    params.put("Json[data][" + i + "][CreatedDate]", tanks.get(i).getCreatedAt());// created date ....
                    params.put("Json[data][" + i + "][X_Map]", !tanks.get(i).getLongitude().isEmpty() ? tanks.get(i).getLongitude() : "360");
                    params.put("Json[data][" + i + "][Y_Map]", !tanks.get(i).getLatitude().isEmpty() ? tanks.get(i).getLatitude() : "360");
                }
                params.put("Json[tableName]", context.getString(R.string.api_tank_table_name));
                params.put("Json[databaseName]", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""));
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected void getTanksList(Context context, DBCallback callback, int type, int index) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetTanksListCalled(DataBaseHelper.getmInstance(context).getAllTanks(type), index);
    }

    protected void removeTankObject(Context context, DBCallback callback, long id, int position) {
        callback.onRemoveTankObjectCalled(DataBaseHelper.getmInstance(context).deleteTank(id), position, id);
    }

    protected interface VolleyCallback {
        void onSyncingResponse(String response) throws JSONException;

        void onSyncingError(VolleyError error);
    }

    protected interface DBCallback {

        void onGetTanksListCalled(ArrayList<TankObject> tanks, int index);

        void onRemoveTankObjectCalled(int flag, int position, long id);
    }
}
