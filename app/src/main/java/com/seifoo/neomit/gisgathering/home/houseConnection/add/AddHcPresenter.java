package com.seifoo.neomit.gisgathering.home.houseConnection.add;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

public class AddHcPresenter implements AddHcMVP.Presenter, AddHcModel.DBCallback {
    private AddHcMVP.View view;
    private AddHcMVP.MainView mainView;
    private AddHcMVP.FirstView firstView;
    private AddHcMVP.SecondView secondView;
    private AddHcModel model;


    public AddHcPresenter(AddHcMVP.View view, AddHcMVP.MainView mainView, AddHcModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public AddHcPresenter(AddHcMVP.View view, AddHcMVP.FirstView firstView, AddHcModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public AddHcPresenter(AddHcMVP.View view, AddHcMVP.SecondView secondView, AddHcModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }


    @Override
    public void requestFirstSpinnersData() {
        // 26,0,4,3 houseConnectionType,meter diameter,meter material,meter status
        model.getGeoMasters(view.getAppContext(), this, new int[]{26, 0, 4, 3}, 1);
    }

    @Override
    public void requestSecondSpinnersData() {
        // 25,16,5 districts,water schedule,meter remarks
        model.getGeoMasters(view.getAppContext(), this, new int[]{25, 16, 5}, 2);
    }

    @Override
    public void requestPassFirstStepDataToActivity(HouseConnectionObject houseConnection) {
        if (!houseConnection.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        houseConnection.setLatitude(houseConnection.getLatitude().split(",")[0]);
        houseConnection.setLongitude(houseConnection.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof AddHCActivity) {
            ((AddHCActivity) view.getAppContext()).passFirstObj(houseConnection);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(HouseConnectionObject houseConnection) {
        if (view.getAppContext() instanceof AddHCActivity) {
            ((AddHCActivity) view.getAppContext()).passSecondObj(houseConnection);
        }
    }

    @Override
    public void requestAddHC(HouseConnectionObject houseConnection) {
        houseConnection.setCreatedAt(returnValidNumbers(mainView.getCurrentDate()));
        model.insertHC(view.getAppContext(), this, houseConnection);
    }

    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View clickedView) {
                switch (clickedView.getId()) {
                    case R.id.serial_num_qr:
                        requestQrCode(((AddHCActivity) view.getAppContext()), view.getAppContext().getString(R.string.serial_number));
                        break;
                    case R.id.hcn_qr:
                        requestQrCode(((AddHCActivity) view.getAppContext()), view.getAppContext().getString(R.string.hcn));
                        break;
                }
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext, String flagString) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (flagString.equals(view.getAppContext().getString(R.string.hcn))) {
            flag = 1;
        }

        firstView.initializeScanner(integrator, flag);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void handleQRScannerResult(String contents, int flag) {
        contents = contents.replaceAll("[^0-9]", "");
        if (flag == 0) {
            firstView.setSerialNumber(contents);
        } else if (flag == 1) {
            firstView.setHCN(contents);
        }
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getHCsLocation(view.getAppContext(), this, prevLatLng);
    }


    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index) {
        if (index == 1) handleFirstView(masters);
        else if (index == 2) handleSecondView(masters);
    }

    // 26,0,4,3 houseConnectionType,meter diameter,meter material,meter status
    private void handleFirstView(ArrayList<GeoMasterObj> masters) {
        ArrayList<String> type = new ArrayList<>(), diameter = new ArrayList<>(), material = new ArrayList<>(), status = new ArrayList<>();
        ArrayList<Integer> typeIds = new ArrayList<>(), diameterIds = new ArrayList<>(), materialIds = new ArrayList<>(), statusIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 26) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                diameter.add(masters.get(i).getName());
                diameterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 4) {
                material.add(masters.get(i).getName());
                materialIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 3) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            }
        }

        firstView.loadSpinnersData(type, typeIds,
                diameter, diameterIds,
                material, materialIds,
                type, typeIds);
    }

    // 25,16,5 districts,water schedule,meter remarks
    private void handleSecondView(ArrayList<GeoMasterObj> masters) {
        ArrayList<String> districts = new ArrayList<>(), waterSchedule = new ArrayList<>(), remarks = new ArrayList<>();
        ArrayList<Integer> districtsIds = new ArrayList<>(), waterScheduleIds = new ArrayList<>(), remarksIds = new ArrayList<>();
        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 25) {
                districts.add(masters.get(i).getName());
                districtsIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 16) {
                waterSchedule.add(masters.get(i).getName());
                waterScheduleIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 5) {
                remarks.add(masters.get(i).getName());
                remarksIds.add(masters.get(i).getGeoMasterId());
            }
        }

        secondView.loadSpinnersData(districts, districtsIds,
                waterSchedule, waterScheduleIds,
                remarks, remarksIds);
    }

    @Override
    public void onHcInsertionCalled(long flag) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.add_house_connection_done_successfully));
            mainView.backToParent();
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_save_fire));
        }
    }

    @Override
    public void onGetHCsLocationCalled(ArrayList<HashMap<String, String>> chambersLocation, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], chambersLocation, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", chambersLocation, PICK_LOCATION_REQUEST);
        }
    }
}
