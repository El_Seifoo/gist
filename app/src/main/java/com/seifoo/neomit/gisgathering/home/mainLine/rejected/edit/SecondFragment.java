package com.seifoo.neomit.gisgathering.home.mainLine.rejected.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, EditRejectedMainLinesMVP.View, EditRejectedMainLinesMVP.SecondView {
    private EditText lastUpdatedEditText, assignedEditText, dmaZoneEditText, subNameEditText, sectorNameEditText;
    private Spinner districtNameSpinner, subDistrictSpinner, waterScheduleSpinner, assetStatusSpinner, remarksSpinner;

    private EditRejectedMainLinesMVP.Presenter presenter;

    public static SecondFragment newInstance(MainLineObject mainLine) {
        SecondFragment fragment = new SecondFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("MainLineObj", mainLine);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_line_second, container, false);

        presenter = new EditRejectedMainLinesPresenter(this, this, new EditRejectedMainLinesModel());

        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);
        subDistrictSpinner = (Spinner) view.findViewById(R.id.sub_district_spinner);
        waterScheduleSpinner = (Spinner) view.findViewById(R.id.water_schedule_spinner);
        assetStatusSpinner = (Spinner) view.findViewById(R.id.asset_status_spinner);
        remarksSpinner = (Spinner) view.findViewById(R.id.remarks_spinner);

        lastUpdatedEditText = (EditText) view.findViewById(R.id.last_updated);
        assignedEditText = (EditText) view.findViewById(R.id.assigned);
        dmaZoneEditText = (EditText) view.findViewById(R.id.dma_zone);
        subNameEditText = (EditText) view.findViewById(R.id.sub_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);

        lastUpdatedEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(lastUpdatedEditText.getText().toString().toString(),0);
            }
        });
        assignedEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestDatePickerDialog(assignedEditText.getText().toString().toString(), 1);
            }
        });

        presenter.requestSecondSpinnersData((MainLineObject) getArguments().getSerializable("MainLineObj"));

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new MainLineObject(lastUpdatedEditText.getText().toString().trim(),
                districtNameIds.get(districtNameSpinner.getSelectedItemPosition()), assignedEditText.getText().toString().trim(),
                subDistrictIds.get(subDistrictSpinner.getSelectedItemPosition()), dmaZoneEditText.getText().toString().trim(),
                subNameEditText.getText().toString().trim(), sectorNameEditText.getText().toString().trim(),
                waterScheduleIds.get(waterScheduleSpinner.getSelectedItemPosition()), assetStatusIds.get(assetStatusSpinner.getSelectedItemPosition()),
                remarksIds.get(remarksSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> districtNameIds, subDistrictIds, waterScheduleIds, assetStatusIds, remarksIds;

    @Override
    public void loadSpinnersData(ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex, ArrayList<String> subDistrict, ArrayList<Integer> subDistrictIds, int subDistrictIndex, ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds, int waterScheduleIndex, ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds, int assetStatusIndex, ArrayList<String> remarks, ArrayList<Integer> remarksIds, int remarksIndex) {
        this.districtNameIds = districtNameIds;
        this.subDistrictIds = subDistrictIds;
        this.waterScheduleIds = waterScheduleIds;
        this.assetStatusIds = assetStatusIds;
        this.remarksIds = remarksIds;

        districtNameSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, districtName));
        districtNameSpinner.setSelection(districtNameIndex);
        subDistrictSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, subDistrict));
        subDistrictSpinner.setSelection(subDistrictIndex);
        waterScheduleSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, waterSchedule));
        waterScheduleSpinner.setSelection(waterScheduleIndex);
        assetStatusSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, assetStatus));
        assetStatusSpinner.setSelection(assetStatusIndex);
        remarksSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, remarks));
        remarksSpinner.setSelection(remarksIndex);
    }

    @Override
    public void setLastUpdated(String date) {
        lastUpdatedEditText.setText(date);
    }

    @Override
    public void setAssigned(String date) {
        assignedEditText.setText(date);
    }

    @Override
    public void setData(String dmaZone, String subName, String sectorName) {
        dmaZoneEditText.setText(dmaZone);
        subNameEditText.setText(subName);
        sectorNameEditText.setText(sectorName);
    }
}
