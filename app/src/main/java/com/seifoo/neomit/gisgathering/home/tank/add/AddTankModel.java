package com.seifoo.neomit.gisgathering.home.tank.add;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class AddTankModel {
    public void getGeoMasters(Context context, DBCallback callback, int[] types, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))), index);
    }

    public void insertTank(Context context, DBCallback callback, TankObject tank) {
        callback.onTankInsertionCalled(DataBaseHelper.getmInstance(context).insertTank(tank));
    }

    protected void getTanksLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetTanksLocationCalled(DataBaseHelper.getmInstance(context).getTanksLocation(), prevLatLng);
    }
    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index);

        void onTankInsertionCalled(long flag);

        void onGetTanksLocationCalled(ArrayList<HashMap<String,String>> tanksLocation, String prevLatLng);
    }

}
