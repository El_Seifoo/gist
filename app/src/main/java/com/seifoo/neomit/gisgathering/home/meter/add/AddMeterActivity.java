package com.seifoo.neomit.gisgathering.home.meter.add;

import android.content.Context;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddMeterActivity extends AppCompatActivity implements StepperLayout.StepperListener, AddMeterMVP.MainView {

    private StepperLayout mStepperLayout;
    private AddMeterMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_meter);

        getSupportActionBar().setTitle(getString(R.string.add_meter));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new AddMeterPresenter(this, new AddMeterModel());
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new MyStepperAdapter(getSupportFragmentManager(), this));
        mStepperLayout.setListener(this);
    }

    // seial 123 , plate 456, Mdiameter 50mm , brand MSD , Mtype Others , MStatus NWorking , MMaterial N.A , remarks broken , ReadType MobApp
    // pipAMeter Connected , PipSize 32mm , MArea MA3 , MBoxP Inside chamber, MBoxT Doris , CovStat Good cover , location inside, BuilUsage Agriculture
    // wConnType HC DC , PipMaterial Bronce , ValveT ball , ValveStat Close , enabled no , reducerDiam 40x50mm, date 24/3/2019 , postal 987
    // sceco 654 , noem 321 , lastRead 741 , nof 852 , streetName asd , secName zxc, sConnection notExist , groundEle 1010 , Ele 2020
    // HCN 0909 , address 0808,subBuildType palace , sche Tue , Nbrand BAYLAN,districtName dist1
    // DMA qwe,locationNum 5454, builNum 111,buildD 222 ,buildNM 333 , buDesc 444 , streetNum 555 ,stNM 666 , subname aaa, ar bbb , cusA yes


    @Override
    public void onCompleted(View completeButton) {
        presenter.requestAddMeterObject(new MeterObject(firstStepMeterObj, secondStepMeterObj, thirdStepMeterObj, fourthStepMeterObj, fifthStepMeterObj));
    }

    @Override
    public void onError(VerificationError verificationError) {
//        Toast.makeText(this, "onError! -> " + verificationError.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStepSelected(int newStepPosition) {
//        Toast.makeText(this, "onStepSelected! -> " + newStepPosition, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onReturn() {
//        finish();
    }

    private MeterObject firstStepMeterObj, secondStepMeterObj, thirdStepMeterObj, fourthStepMeterObj, fifthStepMeterObj;

    protected void passFirstStepData(MeterObject meterObject) {
        firstStepMeterObj = meterObject;
    }

    protected void passSecondStepData(MeterObject meterObject) {
        secondStepMeterObj = meterObject;
    }

    protected void passThirdStepData(MeterObject meterObject) {
        thirdStepMeterObj = meterObject;
    }

    protected void passFourthStepData(MeterObject meterObject) {
        fourthStepMeterObj = meterObject;
    }

    protected void passFifthStepData(MeterObject meterObject) {
        fifthStepMeterObj = meterObject;
    }


    protected void changeStepperPosition(int position) {
        mStepperLayout.setCurrentStepPosition(position);
    }


    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void backToParent() {
        finish();
    }

    @Override
    public String getCurrentDate() {
        Calendar today = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return sdf.format(today.getTime());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class MyStepperAdapter extends AbstractFragmentStepAdapter {


        public MyStepperAdapter(FragmentManager fm, Context context) {
            super(fm, context);
        }

        @Override
        public Step createStep(int position) {
            switch (position) {
                case 0:
                    return FirstFragment.newInstance(position);
                case 1:
                    return SecondFragment.newInstance(position);
                case 2:
                    return ThirdFragment.newInstance(position);
                case 3:
                    return FourthFragment.newInstance(position);
                case 4:
                default:
                    return FifthFragment.newInstance(position);
            }
        }

        @Override
        public int getCount() {
            return 5;
        }


        @NonNull
        @Override
        public StepViewModel getViewModel(@IntRange(from = 0) int position) {
            //Override this method to set Step title for the Tabs, not necessary for other stepper types
            return new StepViewModel.Builder(context)
                    .setTitle("") //can be a CharSequence instead
                    .create();
        }
    }
}
