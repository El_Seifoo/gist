package com.seifoo.neomit.gisgathering.home.houseConnection.rejected.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;

import java.util.ArrayList;

public interface EditRejectedHCsMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        void showToastMessage(String message);

        void backToParent();

        Context getActContext();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex,
                              ArrayList<String> diameter, ArrayList<Integer> diameterIds, int diameterIndex,
                              ArrayList<String> material, ArrayList<Integer> materialIds, int materialIndex,
                              ArrayList<String> status, ArrayList<Integer> statusIds, int statusIndex);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator, int flag);

        void setSerialNumber(String data);

        void setHCN(String data);

        void navigateToTheMap(String latitude, String longitude, int requestCode);

        void setLocation(String location);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> districts, ArrayList<Integer> districtsIds, int districtsIndex,
                              ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds, int waterScheduleIndex,
                              ArrayList<String> remarks, ArrayList<Integer> remarksIds, int remarksIndex);

        void setData(String subDistrict, String streetName, String sectorName);
    }

    interface Presenter {
        void requestFirstSpinnersData(HouseConnectionObject houseConnection);

        void requestSecondSpinnersData(HouseConnectionObject houseConnection);

        void requestPassFirstStepDataToActivity(HouseConnectionObject houseConnection);

        void requestPassSecondStepDataToActivity(HouseConnectionObject houseConnection);

        void requestEditRejectedHC(HouseConnectionObject houseConnection, String createdAt, long id);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext, String flagString);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents, int flag);

        void requestPickLocation(String prevLatLng);
    }
}
