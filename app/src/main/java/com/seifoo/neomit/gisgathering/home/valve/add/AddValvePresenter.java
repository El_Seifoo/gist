package com.seifoo.neomit.gisgathering.home.valve.add;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

public class AddValvePresenter implements AddValveMVP.Presenter, AddValveModel.DBCallback {
    private AddValveMVP.View view;
    private AddValveMVP.MainView mainView;
    private AddValveMVP.FirstView firstView;
    private AddValveMVP.SecondView secondView;
    private AddValveModel model;

    public AddValvePresenter(AddValveMVP.View view, AddValveMVP.MainView mainView, AddValveModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public AddValvePresenter(AddValveMVP.View view, AddValveMVP.FirstView firstView, AddValveModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public AddValvePresenter(AddValveMVP.View view, AddValveMVP.SecondView secondView, AddValveModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    /*
        0 -> meter diameter , 1 -> meter brand , 2 - > meter type , 3 -> meter status , 4 -> meter material , 5 -> meter remarks
        6 -> read type , 7 -> pipe after meter , 8 -> pipe size , 9 -> maintenance area , 10 -> meter box Position , 11 -> meter box type
        12 -> cover status , 13 -> location , 14 -> building usage , 15 -> sub-building type , 16 -> water schedule , 17 -> water connection type
        18 -> pipe material , 19 -> new meter brand , 20 ->valve type  , 21 -> valve status , 22 -> yes_no , 23 -> reducer diameter
        24 -> FireType , 25 -> districts , 26 -> houseConnectionType , 27 -> valveJob , 28 -> subDistrict , 29 -> mainLineType
     */
    @Override
    public void requestFirstSpinnersData() {
        // valveJob,  material, diameter,  type, lock,   status,{27, 4, 0, 20, 22, 21}
        model.getGeoMasters(view.getAppContext(), this, new int[]{27, 4, 0, 20, 22, 21}, 1);
    }

    @Override
    public void requestSecondSpinnersData() {
        //  coverStatus,  enabled, districts,  ((subDistricts)),   existInField,  existInMap,   waterSchedule,  remarks{12, 22, 25, 28, 22, 22, 16, 5}
        model.getGeoMasters(view.getAppContext(), this, new int[]{12, 22, 25, 28, 22, 22, 16, 5}, 2);
    }

    @Override
    public void requestPassFirstStepDataToActivity(ValveObject valve) {
        if (!valve.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        valve.setLatitude(valve.getLatitude().split(",")[0]);
        valve.setLongitude(valve.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof AddValveActivity) {
            ((AddValveActivity) view.getAppContext()).passFirstObj(valve);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(ValveObject valve) {
        if (view.getAppContext() instanceof AddValveActivity) {
            ((AddValveActivity) view.getAppContext()).passSecondObj(valve);
        }
    }

    @Override
    public void requestAddValve(ValveObject valve) {
        valve.setCreatedAt(returnValidNumbers(mainView.getCurrentDate()));
        model.insertValve(view.getAppContext(), this, valve);
    }

    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQrCode(((AddValveActivity) view.getAppContext()));
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        firstView.initializeScanner(integrator);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents());
        }
    }

    @Override
    public void handleQRScannerResult(String contents) {
        firstView.setSerialNumber(contents.replaceAll("[^0-9]", ""));
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getValvesLocation(view.getAppContext(), this, prevLatLng);
    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index) {
        if (index == 1) handleFirstView(masters);
        else if (index == 2) handleSecondView(masters);
    }

    private void handleFirstView(ArrayList<GeoMasterObj> masters) {
        ArrayList<String>
                valveJob = new ArrayList<>(),
                material = new ArrayList<>(),
                diameter = new ArrayList<>(),
                type = new ArrayList<>(),
                lock = new ArrayList<>(),
                status = new ArrayList<>();

        ArrayList<Integer>
                valveJobIds = new ArrayList<>(),
                materialIds = new ArrayList<>(),
                diameterIds = new ArrayList<>(),
                typeIds = new ArrayList<>(),
                lockIds = new ArrayList<>(),
                statusIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 27) {
                valveJob.add(masters.get(i).getName());
                valveJobIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 4) {
                material.add(masters.get(i).getName());
                materialIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                diameter.add(masters.get(i).getName());
                diameterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 20) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 22) {
                lock.add(masters.get(i).getName());
                lockIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 21) {
                status.add(masters.get(i).getName());
                statusIds.add(masters.get(i).getGeoMasterId());
            }
        }

        firstView.loadSpinnersData(valveJob, valveJobIds,
                material, materialIds,
                diameter, diameterIds,
                type, typeIds,
                lock, lockIds,
                status, statusIds);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> masters) {
        ArrayList<String>
                coverStatus = new ArrayList<>(),
                enabled = new ArrayList<>(),
                districts = new ArrayList<>(),
                subDistricts = new ArrayList<>(),
                existInField = new ArrayList<>(),
                existInMap = new ArrayList<>(),
                waterSchedule = new ArrayList<>(),
                remarks = new ArrayList<>();

        ArrayList<Integer>
                coverStatusIds = new ArrayList<>(),
                enabledIds = new ArrayList<>(),
                districtsIds = new ArrayList<>(),
                subDistrictsIds = new ArrayList<>(),
                existInFieldIds = new ArrayList<>(),
                existInMapIds = new ArrayList<>(),
                waterScheduleIds = new ArrayList<>(),
                remarksIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 12) {
                coverStatus.add(masters.get(i).getName());
                coverStatusIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 22) {
                enabled.add(masters.get(i).getName());
                enabledIds.add(masters.get(i).getGeoMasterId());

                existInField.add(masters.get(i).getName());
                existInFieldIds.add(masters.get(i).getGeoMasterId());

                existInMap.add(masters.get(i).getName());
                existInMapIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 25) {
                districts.add(masters.get(i).getName());
                districtsIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 28) {
                subDistricts.add(masters.get(i).getName());
                subDistrictsIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 16) {
                waterSchedule.add(masters.get(i).getName());
                waterScheduleIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 5) {
                remarks.add(masters.get(i).getName());
                remarksIds.add(masters.get(i).getGeoMasterId());
            }
        }

        secondView.loadSpinnersData(coverStatus, coverStatusIds,
                enabled, enabledIds,
                districts, districtsIds,
                subDistricts, subDistrictsIds,
                existInField, existInFieldIds,
                existInMap, existInMapIds,
                waterSchedule, waterScheduleIds,
                remarks, remarksIds);
    }

    @Override
    public void onValveInsertionCalled(long flag) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.add_valve_done_successfully));
            mainView.backToParent();
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_save_valve));
        }
    }

    @Override
    public void onGetValvesLocationCalled(ArrayList<HashMap<String, String>> valvesLocation, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], valvesLocation, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", valvesLocation, PICK_LOCATION_REQUEST);
        }
    }
}
