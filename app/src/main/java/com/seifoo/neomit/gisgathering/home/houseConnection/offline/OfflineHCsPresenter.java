package com.seifoo.neomit.gisgathering.home.houseConnection.offline;

import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.home.houseConnection.edit.EditHCActivity;
import com.seifoo.neomit.gisgathering.home.houseConnection.offline.details.OfflineHCsDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class OfflineHCsPresenter implements OfflineHCsMVP.Presenter, OfflineHCsModel.VolleyCallback, OfflineHCsModel.DBCallback {
    private OfflineHCsMVP.View view;
    private OfflineHCsModel model;

    public OfflineHCsPresenter(OfflineHCsMVP.View view, OfflineHCsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestHCsData(int type) {
        model.getHCsList(view.getAppContext(), this, type, 0);
    }

    @Override
    public void requestRemoveHcObjById(long id, int position) {
        model.removeHcObject(view.getActContext(), this, id, position);
    }

    private static final int SHOW_ALL_IN_MAP_REQUEST = 10;

    @Override
    public void OnListItemClickListener(int viewId, int position, HouseConnectionObject object) {
        switch (viewId) {
            case R.id.more_details:
                view.navigateDestination("OfflineHcObj", object, OfflineHCsDetailsActivity.class);
                break;
            case R.id.remove_item:
                this.requestRemoveHcObjById(object.getId(), position);
                break;
            case R.id.edit_item:
                this.requestEditHcData(position, object);
                break;
            case R.id.map:
                view.navigateToMap(SHOW_ALL_IN_MAP_REQUEST, object, position);
        }
    }

    private static final int EDIT_HC_OBJECT_REQUEST = 1;

    @Override
    public void requestEditHcData(int position, HouseConnectionObject object) {
        Intent intent = new Intent(view.getAppContext(), EditHCActivity.class);
        intent.putExtra("HcObj", object);
        intent.putExtra("position", position);
        view.navigateToEditFire(intent, EDIT_HC_OBJECT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_HC_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            view.updateListItem(data.getExtras().getInt("position"), (HouseConnectionObject) data.getSerializableExtra("editedHcObj"));
            return;
        }
        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            this.requestHCsData(view.getSpinnerSelectedPosition());
        }
    }

    @Override
    public void requestUploadData(int type) {
        model.getHCsList(view.getAppContext(), this, type, 1);
    }

    @Override
    public void showLocations(ArrayList<HouseConnectionObject> hcs) {
        view.navigateToMap2(SHOW_ALL_IN_MAP_REQUEST, hcs);
    }

    @Override
    public void onSyncingResponse(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        ArrayList<Long> accepted = new ArrayList<>(), rejected = new ArrayList<>();
        for (int i = 0; i < responseArray.length(); i++) {
            if (responseArray.getJSONObject(i).getString("status").equals("success"))
                accepted.add(responseArray.getJSONObject(i).getLong("id"));
            else rejected.add(responseArray.getJSONObject(i).getLong("id"));
        }

        if (!accepted.isEmpty()) {
            for (int i = 0; i < accepted.size(); i++) {
                model.removeHcObject(view.getActContext(), this, accepted.get(i), -1);
            }
        }
    }

    @Override
    public void onSyncingError(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onGetHCsListCalled(ArrayList<HouseConnectionObject> HCs, int index) {
        if (index == 0) {
            if (HCs.isEmpty())
                view.showEmptyListText();
            else {
                boolean isVisible = false;
                for (int i = 0; i < HCs.size(); i++) {
                    if (HCs.get(i).getLatitude() != null && HCs.get(i).getLongitude() != null) {
                        if (!HCs.get(i).getLatitude().isEmpty() && !HCs.get(i).getLongitude().isEmpty()) {
                            isVisible = true;
                            break;
                        }
                    }
                }
                view.loadHCsData(HCs, isVisible);
            }
        } else {
            if (HCs.isEmpty())
                view.showToastMessage(view.getActContext().getString(R.string.no_fires_to_synchronize));
            else {
                view.showProgress();
                model.uploadHCsData(view.getActContext(), this, HCs);
            }
        }
    }

    @Override
    public void onRemoveHcObjectCalled(int flag, int position, long id) {
        if (flag > 0) {
            if (position == -1) {
                view.removeListItem(id);
            } else
                view.removeListItem(position);
        } else view.showToastMessage(view.getActContext().getString(R.string.failed_to_delete));
    }
}
