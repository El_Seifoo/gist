package com.seifoo.neomit.gisgathering.home.valve.offline;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;
import com.seifoo.neomit.gisgathering.utils.Urls;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class OfflineValvesModel {

    protected void uploadValvesData(final Context context, final VolleyCallback callback, final ArrayList<ValveObject> valves) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSyncingResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("parsing error ", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onSyncingError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""));
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            /*
            Json[data][0][Corrected_Id]: //optional
Json[data][0][VALVE_JOB]:1
Json[data][0][SERIAL_NO]:1
Json[data][0][MATERIAL_ID]:1
Json[data][0][DIAMETER_ID]:1
Json[data][0][VALVE_TYPE]:1
Json[data][0][NO_OF_TURNS_FULL_OPEN_CLOSE]:1
Json[data][0][NO_OF_TURNS_OPEN]:1
Json[data][0][LOCK]:1
Json[data][0][VALVE_STATUS_ID]:1
Json[data][0][Elevation]:1
Json[data][0][ground_Elevation]:1
Json[data][0][COVER_STATUS_ID]:1
Json[data][0][ENABLED_ID]:1
Json[data][0][DMA_ZONE_ID]:1
Json[data][0][DISTRICT_NAME_ID]:1
Json[data][0][SUP_DISTRICT_ID]:1
Json[data][0][STREET_NAME]:1
Json[data][0][SECTOR_NAME]:1
Json[data][0][EXIST_IN_FIELD_ID]:1
Json[data][0][EXIST_IN_MAP_ID]:1
Json[data][0][WATER_SCHEDULE]:1
Json[data][0][X_MAP]:1
Json[data][0][Y_MAP]:1
Json[data][0][REMARKS]:1
Json[data][0][RECORD_ID]:1
Json[data][0][CreatedDate]:12-01-2018
Json[tableName]:valve
Json[databaseName]:r_AL_Mjmaa
             */
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                for (int i = 0; i < valves.size(); i++) {
                    if (valves.get(i).getValveId() != 0.0d)
                        params.put("Json[data][" + i + "][Corrected_Id]", valves.get(i).getValveId() + "");
                    params.put("Json[data][" + i + "][RECORD_ID]", valves.get(i).getId() + "");
                    params.put("Json[data][" + i + "][VALVE_JOB]", valves.get(i).getValveJob()+"");
                    params.put("Json[data][" + i + "][SERIAL_NO]", valves.get(i).getSerialNumber());
                    params.put("Json[data][" + i + "][MATERIAL_ID]", valves.get(i).getMaterial()+"");
                    params.put("Json[data][" + i + "][DIAMETER_ID]", valves.get(i).getDiameter() + "");
                    params.put("Json[data][" + i + "][VALVE_TYPE]", valves.get(i).getType() + "");
                    params.put("Json[data][" + i + "][NO_OF_TURNS_FULL_OPEN_CLOSE]", valves.get(i).getFullNumberOfTurns());
                    params.put("Json[data][" + i + "][NO_OF_TURNS_OPEN]", valves.get(i).getNumberOfTurns());
                    params.put("Json[data][" + i + "][LOCK]", valves.get(i).getLock() + "");
                    params.put("Json[data][" + i + "][VALVE_STATUS_ID]", valves.get(i).getStatus()+"");
                    params.put("Json[data][" + i + "][Elevation]", valves.get(i).getElevation());
                    params.put("Json[data][" + i + "][ground_Elevation]", valves.get(i).getGroundElevation());
                    params.put("Json[data][" + i + "][COVER_STATUS_ID]", valves.get(i).getCoverStatus() + "");
                    params.put("Json[data][" + i + "][ENABLED_ID]", valves.get(i).getEnabled() + "");
                    params.put("Json[data][" + i + "][DMA_ZONE_ID]", valves.get(i).getDmaZone());
                    params.put("Json[data][" + i + "][DISTRICT_NAME_ID]", valves.get(i).getDistrictName() + "");
                    params.put("Json[data][" + i + "][SUP_DISTRICT_ID]", valves.get(i).getSubDistrict() + "");
                    params.put("Json[data][" + i + "][STREET_NAME]", valves.get(i).getStreetName());
                    params.put("Json[data][" + i + "][SECTOR_NAME]", valves.get(i).getSectorName());
                    params.put("Json[data][" + i + "][EXIST_IN_FIELD_ID]", valves.get(i).getExistInField()+"");
                    params.put("Json[data][" + i + "][EXIST_IN_MAP_ID]", valves.get(i).getExistInMap() + "");
                    params.put("Json[data][" + i + "][WATER_SCHEDULE]", valves.get(i).getWaterSchedule() + "");
                    params.put("Json[data][" + i + "][REMARKS]", valves.get(i).getRemarks() + "");
                    params.put("Json[data][" + i + "][CreatedDate]", valves.get(i).getCreatedAt());// created date ....
                    params.put("Json[data][" + i + "][X_MAP]", !valves.get(i).getLongitude().isEmpty() ? valves.get(i).getLongitude() : "360");
                    params.put("Json[data][" + i + "][Y_MAP]", !valves.get(i).getLatitude().isEmpty() ? valves.get(i).getLatitude() : "360");
                }
                params.put("Json[tableName]", context.getString(R.string.api_valve_table_name));
                params.put("Json[databaseName]", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""));
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected void getValvesList(Context context, DBCallback callback, int type, int index) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetValvesListCalled(DataBaseHelper.getmInstance(context).getAllValves(type), index);
    }

    protected void removeValveObject(Context context, DBCallback callback, long id, int position) {
        callback.onRemoveValveObjectCalled(DataBaseHelper.getmInstance(context).deleteValve(id), position, id);
    }

    protected interface VolleyCallback {
        void onSyncingResponse(String response) throws JSONException;

        void onSyncingError(VolleyError error);
    }

    protected interface DBCallback {

        void onGetValvesListCalled(ArrayList<ValveObject> valves, int index);

        void onRemoveValveObjectCalled(int flag, int position, long id);
    }
}
