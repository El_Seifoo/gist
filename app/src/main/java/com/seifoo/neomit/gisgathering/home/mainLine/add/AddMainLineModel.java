package com.seifoo.neomit.gisgathering.home.mainLine.add;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class AddMainLineModel {
    public void getGeoMasters(Context context, DBCallback callback, int[] types, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))), index);
    }

    public void insertMainLine(Context context, DBCallback callback, MainLineObject mainLine) {
        callback.onMainLineInsertionCalled(DataBaseHelper.getmInstance(context).insertMainLine(mainLine));
    }

    protected void getMainLinesLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetMainLinesLocationCalled(DataBaseHelper.getmInstance(context).getMainLinesLocation(), prevLatLng);
    }
    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index);

        void onMainLineInsertionCalled(long flag);

        void onGetMainLinesLocationCalled(ArrayList<HashMap<String,String>> mainLinesLocation, String prevLatLng);
    }
}
