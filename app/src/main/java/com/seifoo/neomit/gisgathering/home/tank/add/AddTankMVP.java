package com.seifoo.neomit.gisgathering.home.tank.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface AddTankMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();

        String getCurrentDate();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> type, ArrayList<Integer> typeIds,
                              ArrayList<String> enabled, ArrayList<Integer> enabledIds,
                              ArrayList<String> tankDiameter, ArrayList<Integer> tankDiameterIds);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator);

        void setSerialNumber(String data);

        void setCommissionDate(String commissionDate);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);

    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds,
                              ArrayList<String> districtName, ArrayList<Integer> districtNameIds);

        void setLastUpdated(String lastUpdated);
    }

    interface Presenter {
        void requestFirstSpinnersData();

        void requestSecondSpinnersData();

        void requestPassFirstStepDataToActivity(TankObject tank);

        void requestPassSecondStepDataToActivity(TankObject tank);

        void requestAddTank(TankObject tank);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString, int index);
    }
}
