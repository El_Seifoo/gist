package com.seifoo.neomit.gisgathering.home.fireHydrant.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface AddFiresMVP {
    // common view ...
    interface View {
        Context getAppContext();
    }

    // activity ...
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();

        String getCurrentDate();

    }


    interface FirstView {

        void loadSpinnersData(ArrayList<String> barrelDiameter, ArrayList<Integer> barrelDiameterIds,
                              ArrayList<String> material, ArrayList<Integer> materialIds,
                              ArrayList<String> type, ArrayList<Integer> typeIds,
                              ArrayList<String> districtsNames, ArrayList<Integer> districtsNamesIds);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator, int flag);

        void setSerialNumber(String data);

        void setFHNumber(String data);

        void setCommissionDate(String date);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> fireHydrateStatus, ArrayList<Integer> fireHydrateStatusIds,
                              ArrayList<String> remarks, ArrayList<Integer> remarksIds,
                              ArrayList<String> fireValveType, ArrayList<Integer> fireValveTypeIds,
                              ArrayList<String> fireHydrateBrand, ArrayList<Integer> fireHydrateBrandIds,
                              ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,
                              ArrayList<String> maintenanceArea, ArrayList<Integer> maintenanceAreaIds);
    }


    interface Presenter {
        void requestFirstSpinnersData();

        void requestSecondSpinnersData();

        void requestPassFirstStepDataToActivity(FireHydrantObject fireHydrantObject);

        void requestPassSecondStepDataToActivity(FireHydrantObject fireHydrantObject);

        void requestAddFireObject(FireHydrantObject object);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext, String flagString);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents, int flag);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String date);
    }
}
