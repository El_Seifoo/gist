package com.seifoo.neomit.gisgathering.home.chamber.rejected;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;

import java.util.ArrayList;

public interface RejectedChambersMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showEmptyListText();

        void loadRejectedChambers(ArrayList<ChamberObject> chambers);
    }

    interface Presenter {
        void requestChambers();
    }
}
