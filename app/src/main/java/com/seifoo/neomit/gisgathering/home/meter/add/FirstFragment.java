package com.seifoo.neomit.gisgathering.home.meter.add;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.HashMap;

public class FirstFragment extends Fragment implements Step, AddMeterMVP.MainView, AddMeterMVP.FirstView {
    private EditText latitudeLongitudeEditText, serialNumberEditText, plateNumberEditText;
    private Spinner meterDiameterSpinner, meterBrandSpinner, meterTypeSpinner, meterStatusSpinner, meterMaterialSpinner, meterRemarksSpinner, readTypeSpinner;

    private AddMeterMVP.Presenter presenter;

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void backToParent() {

    }

    @Override
    public String getCurrentDate() {
        return null;
    }

    public static FirstFragment newInstance(int position) {
        FirstFragment fragment = new FirstFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        presenter = new AddMeterPresenter(this, this, new AddMeterModel());

        latitudeLongitudeEditText = (EditText) view.findViewById(R.id.location);
        serialNumberEditText = (EditText) view.findViewById(R.id.serial_num);
        plateNumberEditText = (EditText) view.findViewById(R.id.plate_num);

        meterDiameterSpinner = (Spinner) view.findViewById(R.id.meter_diameter_spinner);
        meterBrandSpinner = (Spinner) view.findViewById(R.id.meter_brand_spinner);
        meterTypeSpinner = (Spinner) view.findViewById(R.id.meter_type_spinner);
        meterStatusSpinner = (Spinner) view.findViewById(R.id.meter_status_spinner);
        meterMaterialSpinner = (Spinner) view.findViewById(R.id.meter_material_spinner);
        meterRemarksSpinner = (Spinner) view.findViewById(R.id.meter_remarks_spinner);
        readTypeSpinner = (Spinner) view.findViewById(R.id.read_type_spinner);

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(latitudeLongitudeEditText.getText().toString().trim());
            }
        });

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.serial_num_qr)));
        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.plate_num_qr)));

        presenter.requestFirstSpinnersData();

        return view;
    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
//        Log.e("verfStep", "step 1");
        presenter.requestPassFirstStepDataToActivity(new MeterObject(
                latitudeLongitudeEditText.getText().toString().trim(),
                serialNumberEditText.getText().toString().trim(),
                plateNumberEditText.getText().toString().trim(),
                meterDiameterIds.get(meterDiameterSpinner.getSelectedItemPosition()),
                meterBrandIds.get(meterBrandSpinner.getSelectedItemPosition()),
                meterTypeIds.get(meterTypeSpinner.getSelectedItemPosition()),
                meterStatusIds.get(meterStatusSpinner.getSelectedItemPosition()),
                meterMaterialIds.get(meterMaterialSpinner.getSelectedItemPosition()),
                meterRemarksIds.get(meterRemarksSpinner.getSelectedItemPosition()),
                readTypeIds.get(readTypeSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {
//        Log.e("selected", "selected");
    }

    @Override
    public void onError(@NonNull VerificationError error) {
//        Log.e("error", error.toString());
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }


    ArrayList<Integer> meterDiameterIds, meterBrandIds, meterTypeIds, meterStatusIds, meterMaterialIds, meterRemarksIds, readTypeIds;

    @Override
    public void loadSpinnersData(ArrayList<String> meterDiameter, ArrayList<Integer> meterDiameterIds,
                                 ArrayList<String> meterBrand, ArrayList<Integer> meterBrandIds,
                                 ArrayList<String> meterType, ArrayList<Integer> meterTypeIds,
                                 ArrayList<String> meterStatus, ArrayList<Integer> meterStatusIds,
                                 ArrayList<String> meterMaterial, ArrayList<Integer> meterMaterialIds,
                                 ArrayList<String> meterRemarks, ArrayList<Integer> meterRemarksIds,
                                 ArrayList<String> readType, ArrayList<Integer> readTypeIds) {


        this.meterDiameterIds = meterDiameterIds;
        this.meterBrandIds = meterBrandIds;
        this.meterTypeIds = meterTypeIds;
        this.meterStatusIds = meterStatusIds;
        this.meterMaterialIds = meterMaterialIds;
        this.meterRemarksIds = meterRemarksIds;
        this.readTypeIds = readTypeIds;

        meterDiameterSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterDiameter));
        meterBrandSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterBrand));
        meterTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterType));
        meterStatusSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterStatus));
        meterMaterialSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterMaterial));
        meterRemarksSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterRemarks));
        readTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, readType));
    }

    @Override
    public void showLocationError(String errorMessage) {
        latitudeLongitudeEditText.setError(errorMessage);
        ((AddMeterActivity) getContext()).changeStepperPosition(-1);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator, int flag) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }

    @Override
    public void setPlateNumber(String data) {
        plateNumberEditText.setText(data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFirstViewActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setLocation(String location) {
        latitudeLongitudeEditText.setText(location);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("latLng", latLngs);
        startActivityForResult(intent, requestCode);
    }
}
