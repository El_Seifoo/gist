package com.seifoo.neomit.gisgathering.login;


import com.google.android.material.bottomsheet.BottomSheetBehavior;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.HomeActivity;
import com.seifoo.neomit.gisgathering.geoDB.GeoDBActivity;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginPresenter implements LoginMVP.Presenter, LoginModel.VolleyCallback, LoginModel.DBCallback {
    private LoginMVP.View view;
    private LoginModel model;

    public LoginPresenter(LoginMVP.View view, LoginModel model) {
        this.view = view;
        this.model = model;
    }


    @Override
    public void isUserLoggedIn() {
        if (MySingleton.getmInstance(view.getAppContext()).isLoggedIn()) {
            if (MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.API_DB_NAME, "").isEmpty())
                view.navigateHome(GeoDBActivity.class);
            else view.navigateHome(HomeActivity.class);
        } else
            view.initializeLoginViews();

    }

    @Override
    public void requestLogin(String username, String password, String ip) {
        if (username.isEmpty()) {
            view.validateUserName(view.getAppContext().getString(R.string.username_field_is_required));
            return;
        }
        if (password.isEmpty()) {
            view.validatePassword(view.getAppContext().getString(R.string.password_field_is_required));
            return;
        }
        if (ip.isEmpty()) {
            view.validateIp(view.getAppContext().getString(R.string.ip_field_is_required));
            return;
        }
        view.showProgress();
        model.login(view.getAppContext(), this, username, password, ip);

    }

    @Override
    public void onViewClicked(View clickedView) {
        switch (clickedView.getId()) {
            case R.id.bottom_sheet_title:
                if (view.getBottomSheetBehavior().getState() == BottomSheetBehavior.STATE_COLLAPSED)
                    view.setBottomSheetState(BottomSheetBehavior.STATE_EXPANDED);
                else if (view.getBottomSheetBehavior().getState() == BottomSheetBehavior.STATE_EXPANDED)
                    view.setBottomSheetState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
            case R.id.login_btn:
                this.requestLogin(view.getUsername(), view.getPassword(), view.getIP());
        }
    }

    @Override
    public void onLoginSucceeded(String response, String userId, String ip) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        if (jsonObject.has("access_token")) {
            MySingleton.getmInstance(view.getAppContext()).saveStringSharedPref(Constants.USER_TOKEN, jsonObject.getString("token_type").concat(" ").concat(jsonObject.getString("access_token")));
            MySingleton.getmInstance(view.getAppContext()).saveStringSharedPref(Constants.USER_NAME, userId);
            MySingleton.getmInstance(view.getAppContext()).saveStringSharedPref(Constants.USER_IP, ip);
            MySingleton.getmInstance(view.getAppContext()).loginUser();
            model.getGeoMasters(view.getAppContext(), this);

        } else {
            view.hideProgress();
            view.showToastMessage(view.getAppContext().getString(R.string.invalid_username_or_password));
        }
    }

    @Override
    public void onGeoMastersSucceeded(String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        String[] keys = view.getAppContext().getResources().getStringArray(R.array.geo_masters_keys);
        ArrayList<GeoMasterObj> geoMastersList = new ArrayList<>();
        JSONArray geoMaster = null;
        for (int i = 0; i < keys.length; i++) {
            if (jsonObject.get(keys[i]) instanceof JSONArray) {
                geoMaster = jsonObject.getJSONArray(keys[i]);
                for (int j = 0; j < geoMaster.length(); j++) {
                    geoMastersList.add(new GeoMasterObj(i == 28 || i == 29 ? geoMaster.getJSONObject(j).getInt("Id") : geoMaster.getJSONObject(j).getInt("id"),
                            geoMaster.getJSONObject(j).getString("Ar"),
                            geoMaster.getJSONObject(j).getString("En"), i));
                }
            }
        }

        model.insertGeoMasters(view.getAppContext(), this, geoMastersList);
    }

    @Override
    public void onRequestFailed(VolleyError error, int step) {
        view.hideProgress();

//        String body;
//        try {
//            //get status code here
//            String statusCode = String.valueOf(error.networkResponse.statusCode);
//            //get response body and parse with appropriate encoding
//            if (error.networkResponse.data != null) {
//                try {
//                    body = new String(error.networkResponse.data, "UTF-8");
//                    view.showToastMessage(body);
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                    view.showToastMessage(e.toString());
//                }
//            }
//        } catch (Exception e) {
//            view.showToastMessage("SS: " + e.toString());
//        }
        //do stuff with the body...


        view.showToastMessage(error instanceof AuthFailureError ? view.getActContext().getString(R.string.invalid_username_or_password) :
                error instanceof NoConnectionError ? (step == 0 ? view.getActContext().getString(R.string.no_internet_connection_or_your_host_address_is_wrong) : view.getActContext().getString(R.string.no_internet_connection)) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
        if (step == 1) {
            String username = MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.USER_NAME, "");
            String ip = MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.USER_IP, "");
            DataBaseHelper.getmInstance(view.getActContext()).deleteAllGeoMasters();
            MySingleton.getmInstance(view.getActContext()).logout();
            MySingleton.getmInstance(view.getActContext()).saveStringSharedPref(Constants.USER_IP, ip);
            MySingleton.getmInstance(view.getActContext()).saveStringSharedPref(Constants.USER_NAME, username);
        }
    }


    @Override
    public void onInsertionCalled(boolean flag) {
        view.hideProgress();
        if (flag) {
            if (MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.API_DB_NAME, "").isEmpty())
                view.navigateHome(GeoDBActivity.class);
            else view.navigateHome(HomeActivity.class);
        } else {
            view.showToastMessage(view.getAppContext().getString(R.string.wrong_message_1));
            String username = MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.USER_NAME, "");
            String ip = MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.USER_IP, "");
            DataBaseHelper.getmInstance(view.getActContext()).deleteAllGeoMasters();
            MySingleton.getmInstance(view.getActContext()).logout();
            MySingleton.getmInstance(view.getActContext()).saveStringSharedPref(Constants.USER_IP, ip);
            MySingleton.getmInstance(view.getActContext()).saveStringSharedPref(Constants.USER_NAME, username);
        }
    }
}
