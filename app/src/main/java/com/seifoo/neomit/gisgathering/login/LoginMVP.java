package com.seifoo.neomit.gisgathering.login;

import android.content.Context;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

public interface LoginMVP {
    interface View {

        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void navigateHome(Class destination);

        BottomSheetBehavior getBottomSheetBehavior();

        void setBottomSheetState(int state);

        String getUsername();

        String getPassword();

        String getIP();

        void validateUserName(String message);

        void validatePassword(String message);

        void validateIp(String message);

        void initializeLoginViews();
    }

    interface Presenter {
        void isUserLoggedIn();

        void requestLogin(String username, String password, String ip);

        void onViewClicked(android.view.View view);
    }
}
