package com.seifoo.neomit.gisgathering.home.fireHydrant.offline.offlineFiresDetails;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.meter.offline.offlineMeterDetails.OfflineDetailsAdapter;

import java.util.ArrayList;

public class OfflineFiresDetailsActivity extends AppCompatActivity implements OfflineFiresDetailsMVP.View {
    private RecyclerView recyclerView;
    private OfflineDetailsAdapter adapter;

    private OfflineFiresDetailsMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_fires_details);

        getSupportActionBar().setTitle(getString(R.string.details));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new OfflineFiresDetailsPresenter(this, new OfflineFiresDetailsModel());

        recyclerView = (RecyclerView) findViewById(R.id.offline_details_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineDetailsAdapter(true);

        presenter.requestFireDetails((FireHydrantObject) getIntent().getExtras().getSerializable("OfflineFireObj"));

        //Diameter 75MM // material PVC // type COLL // status NOT // remarks UNDERGROUND // valveType Normal // brand MSD // schedule Mon // area MA9
        //Diameter 32MM // material N.A // type COLL // status NOT // remarks UNDERGROUND // valveType Ball // brand MSD // schedule Mon // area MA22
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void loadFireMoreDetails(ArrayList<MoreDetails> list) {
        adapter.setDetailsList(list);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
