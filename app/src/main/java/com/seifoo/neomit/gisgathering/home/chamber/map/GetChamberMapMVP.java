package com.seifoo.neomit.gisgathering.home.chamber.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;

public interface GetChamberMapMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void initializeScanner(IntentIntegrator integrator);

        void setCHN(String data);

        void loadChamberMap(String latitude, String longitude);
    }

    interface Presenter {
        void requestQrCode(Activity activity);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestGetMap(String chNumber);
    }
}
