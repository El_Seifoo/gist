package com.seifoo.neomit.gisgathering.home.pump.offline;

import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.pump.PumpObject;

import java.util.ArrayList;

public interface OfflinePumpsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void showToastMessage(String message);

        void loadPumpsData(ArrayList<PumpObject> pump, boolean isVisible);

        void removeListItem(int position);

        void navigateDestination(String key, PumpObject pump, Class destination);

        void navigateToEditPump(Intent intent, int requestCode);

        void updateListItem(int position, PumpObject pump);

        void removeListItem(long id);

        void navigateToMap(int RequestCode, PumpObject pump, int position);

        int getSpinnerSelectedPosition();

        void navigateToMap2(int requestCode, ArrayList<PumpObject> pumps);
    }

    interface Presenter {
        void requestPumpsData(int type);

        void requestRemovePumpObjById(long id, int position);

        void OnListItemClickListener(int viewId, int position, PumpObject pump);

        void requestEditPumpData(int position, PumpObject pump);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestUploadData(int type);

        void showLocations(ArrayList<PumpObject> pumps);
    }
}
