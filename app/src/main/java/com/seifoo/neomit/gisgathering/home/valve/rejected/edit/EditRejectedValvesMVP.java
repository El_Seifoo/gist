package com.seifoo.neomit.gisgathering.home.valve.rejected.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

import java.util.ArrayList;

public interface EditRejectedValvesMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        void showToastMessage(String message);

        void backToParent();

        Context getActContext();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> valveJob, ArrayList<Integer> valveJobIds, int valveJobIndex,
                              ArrayList<String> material, ArrayList<Integer> materialIds, int materialIndex,
                              ArrayList<String> diameter, ArrayList<Integer> diameterIds, int diameterIndex,
                              ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex,
                              ArrayList<String> lock, ArrayList<Integer> lockIds, int lockIndex,
                              ArrayList<String> status, ArrayList<Integer> statusIds, int statusIndex);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator);

        void setSerialNumber(String data);

        void navigateToTheMap(String latitude, String longitude, int requestCode);

        void setLocation(String location);

        void setData(String fullNumberOfTurns, String numberOfTurns, String elevation, String groundElevation);

    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> coverStatus, ArrayList<Integer> coverStatusIds,int coverStatusIndex,
                              ArrayList<String> enabled, ArrayList<Integer> enabledIds,int enabledIndex,
                              ArrayList<String> districts, ArrayList<Integer> districtsIds,int districtsIndex,
                              ArrayList<String> subDistricts, ArrayList<Integer> subDistrictsIds,int subDistrictsIndex,
                              ArrayList<String> existInField, ArrayList<Integer> existInFieldIds,int existInFieldIndex,
                              ArrayList<String> existInMap, ArrayList<Integer> existInMapIds,int existInMapIndex,
                              ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,int waterScheduleIndex,
                              ArrayList<String> remarks, ArrayList<Integer> remarksIds,int remarksIndex);

        void setData(String dmaZone , String streetName , String sectorName);
    }

    interface Presenter {
        void requestFirstSpinnersData(ValveObject valve);

        void requestSecondSpinnersData(ValveObject valve);

        void requestPassFirstStepDataToActivity(ValveObject valve);

        void requestPassSecondStepDataToActivity(ValveObject valve);

        void requestEditRejectedValve(ValveObject valve, String createdAt, long id);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents);

        void requestPickLocation(String prevLatLng);
    }
}
