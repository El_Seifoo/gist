package com.seifoo.neomit.gisgathering.home.breaks.info.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface GetBreakInfoDetailsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void loadBreakMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestBreakDetails(BreakObject breakObj);
    }
}
