package com.seifoo.neomit.gisgathering.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class MySingleton {
    private static MySingleton mInstance;
    private RequestQueue requestQueue;
    private static Context context;
    private SharedPreferences sharedPreferences;

    private MySingleton(Context context) {
        this.context = context;
        requestQueue = getRequestQueue();
        sharedPreferences = getSharedPreferences();
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    public static synchronized MySingleton getmInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MySingleton(context);
        }

        return mInstance;
    }


    public void addToRQ(Request request) {
        requestQueue.add(request);
    }


    public SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    // login ...> user token , username , ip
    // save app language
    public void saveStringSharedPref(String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getStringSharedPref(String key, String defaultValue) {
        return getSharedPreferences().getString(key, defaultValue);
    }


    /*
     *  saving that user logged in
     */
    public void loginUser() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(Constants.IS_LOGGED_IN, true);
        editor.apply();
    }

    /*
     *  Check if user logged in or not
     */
    public boolean isLoggedIn() {
        return getSharedPreferences().getBoolean(Constants.IS_LOGGED_IN, false);
    }

    /*
     *  Logging the user out by deleting all
     *  user data from sharedPref
     */
    public void logout() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.clear();
        editor.apply();
    }
}
