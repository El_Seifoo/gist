package com.seifoo.neomit.gisgathering.home.tank.rejected.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;

import java.util.ArrayList;

public interface EditRejectedTanksMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();

    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex,
                              ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex,
                              ArrayList<String> tankDiameter, ArrayList<Integer> tankDiameterIds, int tankDiameterIndex);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator);

        void setSerialNumber(String data);

        void setCommissionDate(String commissionDate);

        void navigateToTheMap(String latitude, String longitude, int requestCode);

        void setLocation(String location);

        void setData(String tankName, String activeVolume);

    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds, int assetStatusIndex,
                              ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex);

        void setLastUpdated(String lastUpdated);

        void setData(String inActiveVolume, String lowLevel, String highLevel, String streetName, String region,String sectorName,String remarks);
    }

    interface Presenter {
        void requestFirstSpinnersData(TankObject tank);

        void requestSecondSpinnersData(TankObject tank);

        void requestPassFirstStepDataToActivity(TankObject tank);

        void requestPassSecondStepDataToActivity(TankObject tank);

        void requestEditRejectedTank(TankObject tank, String createdAt, long id);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString, int index);
    }

}
