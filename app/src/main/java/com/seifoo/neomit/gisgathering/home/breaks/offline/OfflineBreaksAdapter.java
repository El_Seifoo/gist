package com.seifoo.neomit.gisgathering.home.breaks.offline;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.breaks.rejected.RejectedBreaksActivity;

import java.util.ArrayList;

public class OfflineBreaksAdapter extends RecyclerView.Adapter<OfflineBreaksAdapter.Holder> {
    private ArrayList<BreakObject> list;
    private final BreakObjectListItemListener listItemListener;
    private boolean isOffline;

    public OfflineBreaksAdapter(BreakObjectListItemListener listItemListener, boolean isOffline) {
        this.listItemListener = listItemListener;
        this.isOffline = isOffline;
    }

    public void setList(ArrayList<BreakObject> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public interface BreakObjectListItemListener {
        void onListItemClickListener(int viewId, int position, BreakObject breakObj);
    }

    public void removeItem(Context context, int position) {
        list.remove(position);
        if (list.isEmpty()) {
            if (context instanceof OfflineBreaksActivity)
                ((OfflineBreaksActivity) context).showEmptyListText();
            else if (context instanceof RejectedBreaksActivity)
                ((RejectedBreaksActivity) context).showEmptyListText();
        }
        notifyDataSetChanged();
    }

    public void updateItem(int position, BreakObject editedObj) {
        list.set(position, editedObj);
        notifyDataSetChanged();
    }

    public void updateItem(Context context, long id) {
        for (int i = 0; i < list.size(); i++) {
            if (id == list.get(i).getId()) {
                list.remove(i);
                if (list.isEmpty()) {
                    if (context instanceof OfflineBreaksActivity)
                        ((OfflineBreaksActivity) context).showEmptyListText();
                    else if (context instanceof RejectedBreaksActivity)
                        ((RejectedBreaksActivity) context).showEmptyListText();
                }
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void clear() {
        if (list != null) {
            list.clear();
            notifyDataSetChanged();
        }
    }


    @NonNull
    @Override
    public OfflineBreaksAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offline_break_list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OfflineBreaksAdapter.Holder holder, int position) {
        if (isOffline) {
            holder.removeItem.setVisibility(View.VISIBLE);
            holder.editItem.setVisibility(View.VISIBLE);
        } else {
            if (list.get(position).getReason() != null) {
                if (!list.get(position).getReason().isEmpty()) {
                    holder.reason.setVisibility(View.VISIBLE);
                    holder.reason.setText(
                            Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.reason) + ": </b></font>" + list.get(position).getReason()),
                            TextView.BufferType.SPANNABLE);
                } else holder.reason.setVisibility(View.GONE);
            } else holder.reason.setVisibility(View.GONE);


        }


        if (list.get(position).getLatitude() != null && list.get(position).getLongitude() != null) {
            if (!list.get(position).getLatitude().isEmpty() && !list.get(position).getLongitude().isEmpty()) {
                holder.map.setVisibility(View.VISIBLE);
            } else holder.map.setVisibility(View.GONE);
        } else holder.map.setVisibility(View.GONE);

        if (list.get(position).getBreakNumber() != null) {
            if (!list.get(position).getBreakNumber().isEmpty()) {
                holder.breakNumber.setVisibility(View.VISIBLE);
                holder.breakNumber.setText(
                        Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.break_number) + ": </b></font>" + list.get(position).getBreakNumber()),
                        TextView.BufferType.SPANNABLE);

            } else holder.breakNumber.setVisibility(View.GONE);
        } else holder.breakNumber.setVisibility(View.GONE);


        if (!list.get(position).getCreatedAt().isEmpty()) {
            holder.createdAt.setVisibility(View.VISIBLE);
            holder.createdAt.setText(
                    Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.created_at) + ": </b></font>" + list.get(position).getCreatedAt()),
                    TextView.BufferType.SPANNABLE);
        } else holder.createdAt.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView breakNumber, reason, moreDetails, map, createdAt;
        ImageView removeItem, editItem;

        public Holder(@NonNull View itemView) {
            super(itemView);

            breakNumber = (TextView) itemView.findViewById(R.id.break_number);
            moreDetails = (TextView) itemView.findViewById(R.id.more_details);
            createdAt = (TextView) itemView.findViewById(R.id.created_at);
            map = (TextView) itemView.findViewById(R.id.map);

            map.setOnClickListener(this);
            moreDetails.setOnClickListener(this);
            if (isOffline) {
                removeItem = (ImageView) itemView.findViewById(R.id.remove_item);
                editItem = (ImageView) itemView.findViewById(R.id.edit_item);
                removeItem.setOnClickListener(this);
                editItem.setOnClickListener(this);
            } else {
                reason = (TextView) itemView.findViewById(R.id.reason);
            }
        }
        @Override
        public void onClick(View v) {
            listItemListener.onListItemClickListener(v.getId(), getAdapterPosition(), list.get(getAdapterPosition()));
        }
    }
}
