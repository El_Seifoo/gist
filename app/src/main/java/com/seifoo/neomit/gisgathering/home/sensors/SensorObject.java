package com.seifoo.neomit.gisgathering.home.sensors;

import java.io.Serializable;

public class SensorObject implements Serializable {
    private long id, sensorId;
    private String latitude, longitude, serialNumber, assetNumber;
    private int deviceType, assetStatus;
    private String elevation, chamberStatus;
    private int type;
    private String lineNumber;
    private int maintenanceArea;
    private String commissionDate;
    private int enabled, districtName;
    private String streetName, dmaZone, sectorName;
    private int diameter, pipeMaterial;
    private String lastUpdate, reason, createdAt;


    public SensorObject(String latitude, String longitude, String serialNumber, String assetNumber, int deviceType,
                        int assetStatus, String elevation, String chamberStatus, int type, String lineNumber, int maintenanceArea) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.assetNumber = assetNumber;
        this.deviceType = deviceType;
        this.assetStatus = assetStatus;
        this.elevation = elevation;
        this.chamberStatus = chamberStatus;
        this.type = type;
        this.lineNumber = lineNumber;
        this.maintenanceArea = maintenanceArea;
    }

    public SensorObject getFirstObj() {
        return new SensorObject(latitude, longitude, serialNumber, assetNumber, deviceType,
                assetStatus, elevation, chamberStatus, type, lineNumber, maintenanceArea);
    }

    public SensorObject(String commissionDate, int enabled, int districtName, String streetName,
                        String dmaZone, String sectorName, int diameter, int pipeMaterial, String lastUpdate) {
        this.commissionDate = commissionDate;
        this.enabled = enabled;
        this.districtName = districtName;
        this.streetName = streetName;
        this.dmaZone = dmaZone;
        this.sectorName = sectorName;
        this.diameter = diameter;
        this.pipeMaterial = pipeMaterial;
        this.lastUpdate = lastUpdate;
    }

    public SensorObject getSecondObj() {
        return new SensorObject(commissionDate, enabled, districtName, streetName,
                dmaZone, sectorName, diameter, pipeMaterial, lastUpdate);
    }

    public SensorObject(SensorObject firstObj, SensorObject secondObj) {
        this.latitude = firstObj.getLatitude();
        this.longitude = firstObj.getLongitude();
        this.serialNumber = firstObj.getSerialNumber();
        this.assetNumber = firstObj.getAssetNumber();
        this.deviceType = firstObj.getDeviceType();
        this.assetStatus = firstObj.getAssetStatus();
        this.elevation = firstObj.getElevation();
        this.chamberStatus = firstObj.getChamberStatus();
        this.type = firstObj.getType();
        this.lineNumber = firstObj.getLineNumber();
        this.maintenanceArea = firstObj.getMaintenanceArea();

        this.commissionDate = secondObj.getCommissionDate();
        this.enabled = secondObj.getEnabled();
        this.districtName = secondObj.getDistrictName();
        this.streetName = secondObj.getStreetName();
        this.dmaZone = secondObj.getDmaZone();
        this.sectorName = secondObj.getSectorName();
        this.diameter = secondObj.getDiameter();
        this.pipeMaterial = secondObj.getPipeMaterial();
        this.lastUpdate = secondObj.getLastUpdate();
    }

    public SensorObject(long id, long sensorId, String latitude, String longitude, String serialNumber,
                        String assetNumber, int deviceType, int assetStatus, String elevation, String chamberStatus,
                        int type, String lineNumber, int maintenanceArea, String commissionDate, int enabled, int districtName,
                        String streetName, String dmaZone, String sectorName, int diameter, int pipeMaterial, String lastUpdate, String createdAt) {
        this.id = id;
        this.sensorId = sensorId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.assetNumber = assetNumber;
        this.deviceType = deviceType;
        this.assetStatus = assetStatus;
        this.elevation = elevation;
        this.chamberStatus = chamberStatus;
        this.type = type;
        this.lineNumber = lineNumber;
        this.maintenanceArea = maintenanceArea;
        this.commissionDate = commissionDate;
        this.enabled = enabled;
        this.districtName = districtName;
        this.streetName = streetName;
        this.dmaZone = dmaZone;
        this.sectorName = sectorName;
        this.diameter = diameter;
        this.pipeMaterial = pipeMaterial;
        this.lastUpdate = lastUpdate;
        this.createdAt = createdAt;
    }

    public SensorObject(String latitude, String longitude, String serialNumber, String assetNumber,
                        int deviceType, int assetStatus, String elevation, String chamberStatus, int type,
                        String lineNumber, int maintenanceArea, String commissionDate, int enabled, int districtName,
                        String streetName, String dmaZone, String sectorName, int diameter, int pipeMaterial, String lastUpdate, String createdAt) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.assetNumber = assetNumber;
        this.deviceType = deviceType;
        this.assetStatus = assetStatus;
        this.elevation = elevation;
        this.chamberStatus = chamberStatus;
        this.type = type;
        this.lineNumber = lineNumber;
        this.maintenanceArea = maintenanceArea;
        this.commissionDate = commissionDate;
        this.enabled = enabled;
        this.districtName = districtName;
        this.streetName = streetName;
        this.dmaZone = dmaZone;
        this.sectorName = sectorName;
        this.diameter = diameter;
        this.pipeMaterial = pipeMaterial;
        this.lastUpdate = lastUpdate;
        this.createdAt = createdAt;
    }

    public SensorObject(long sensorId, String latitude, String longitude, String serialNumber, String assetNumber,
                        int deviceType, int assetStatus, String elevation, String chamberStatus, int type, String lineNumber,
                        int maintenanceArea, String commissionDate, int enabled, int districtName, String streetName, String dmaZone,
                        String sectorName, int diameter, int pipeMaterial, String lastUpdate, String reason, String createdAt) {
        this.sensorId = sensorId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.assetNumber = assetNumber;
        this.deviceType = deviceType;
        this.assetStatus = assetStatus;
        this.elevation = elevation;
        this.chamberStatus = chamberStatus;
        this.type = type;
        this.lineNumber = lineNumber;
        this.maintenanceArea = maintenanceArea;
        this.commissionDate = commissionDate;
        this.enabled = enabled;
        this.districtName = districtName;
        this.streetName = streetName;
        this.dmaZone = dmaZone;
        this.sectorName = sectorName;
        this.diameter = diameter;
        this.pipeMaterial = pipeMaterial;
        this.lastUpdate = lastUpdate;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public long getSensorId() {
        return sensorId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getAssetNumber() {
        return assetNumber;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public int getAssetStatus() {
        return assetStatus;
    }

    public String getElevation() {
        return elevation;
    }

    public String getChamberStatus() {
        return chamberStatus;
    }

    public int getType() {
        return type;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public int getMaintenanceArea() {
        return maintenanceArea;
    }

    public String getCommissionDate() {
        return commissionDate;
    }

    public int getEnabled() {
        return enabled;
    }

    public int getDistrictName() {
        return districtName;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getDmaZone() {
        return dmaZone;
    }

    public String getSectorName() {
        return sectorName;
    }

    public int getDiameter() {
        return diameter;
    }

    public int getPipeMaterial() {
        return pipeMaterial;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public String getReason() {
        return reason;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSensorId(long sensorId) {
        this.sensorId = sensorId;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
