package com.seifoo.neomit.gisgathering.home.pump.offline.details;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class OfflinePumpsDetailsPresenter implements OfflinePumpsDetailsMVP.Presenter, OfflinePumpsDetailsModel.DBCallback {
    private OfflinePumpsDetailsMVP.View view;
    private OfflinePumpsDetailsModel model;

    public OfflinePumpsDetailsPresenter(OfflinePumpsDetailsMVP.View view, OfflinePumpsDetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestPumpDetails(PumpObject pump) {
        //  enabled 22,   type 24,    assetStatus 3,
        // districtName 25, subDistrict 28,
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();
        ids.add(pump.getEnabled());
        types.add(22);
        ids.add(pump.getType());
        types.add(24);
        ids.add(pump.getAssetStatus());
        types.add(3);
        ids.add(pump.getDistrictName());
        types.add(25);
        ids.add(pump.getSubDistrict());
        types.add(28);

        model.returnValidGeoMaster(view.getAppContext(), this, pump, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));

    }

    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, PumpObject pump) {
        ArrayList<MoreDetails> moreDetails = new ArrayList<>();

        if (!pump.getLatitude().isEmpty() && !pump.getLongitude().isEmpty())
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), pump.getLatitude() + "," + pump.getLongitude()));
        else
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), ""));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.station_code), pump.getCode()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.station_name), pump.getStationName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.enabled), strings.get(0)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.type), strings.get(1)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.asset_status), strings.get(2)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_name), pump.getStreetName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(3)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sub_district), strings.get(4)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.dma_zone), pump.getDmaZone()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sector_name), pump.getSectorName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.last_updated), pump.getLastUpdated()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.remarks), pump.getRemarks()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.created_at), pump.getCreatedAt()));

        view.loadPumpMoreDetails(moreDetails);
    }
}
