package com.seifoo.neomit.gisgathering.home.meter.add;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, AddMeterMVP.MainView, AddMeterMVP.SecondView {
    private Spinner pipeAfterMeterSpinner, pipeSizeSpinner, meterMaintenanceAreaSpinner, meterBoxPositionSpinner, meterBoxTypeSpinner,
            coverStatusSpinner, locationSpinner, buildingUsageSpinner, waterConnectionTypeSpinner, pipeMaterialSpinner;

    @Override
    public void showToastMessage(String message) {

    }

    @Override
    public void backToParent() {

    }

    @Override
    public String getCurrentDate() {
        return null;
    }

    private AddMeterMVP.Presenter presenter;

    public static SecondFragment newInstance(int position) {
        SecondFragment fragment = new SecondFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        presenter = new AddMeterPresenter(this, this, new AddMeterModel());

        pipeAfterMeterSpinner = (Spinner) view.findViewById(R.id.pipe_after_meter_spinner);
        pipeSizeSpinner = (Spinner) view.findViewById(R.id.pipe_size_spinner);
        meterMaintenanceAreaSpinner = (Spinner) view.findViewById(R.id.meter_maintenance_area_spinner);
        meterBoxPositionSpinner = (Spinner) view.findViewById(R.id.meter_box_position_spinner);
        meterBoxTypeSpinner = (Spinner) view.findViewById(R.id.meter_box_type_spinner);
        coverStatusSpinner = (Spinner) view.findViewById(R.id.cover_status_spinner);
        locationSpinner = (Spinner) view.findViewById(R.id.location_spinner);
        buildingUsageSpinner = (Spinner) view.findViewById(R.id.building_usage_spinner);

        waterConnectionTypeSpinner = (Spinner) view.findViewById(R.id.water_connection_type_spinner);
        pipeMaterialSpinner = (Spinner) view.findViewById(R.id.pipe_material_spinner);

        presenter.requestSecondSpinnersData();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
//        Log.e("verfStep", "step");
        presenter.requestPassSecondStepDataToActivity(new MeterObject(pipeAfterMeterIds.get(pipeAfterMeterSpinner.getSelectedItemPosition()), pipeSizeIds.get(pipeSizeSpinner.getSelectedItemPosition()), meterMaintenanceAreaIds.get(meterMaintenanceAreaSpinner.getSelectedItemPosition()), meterBoxPositionIds.get(meterBoxPositionSpinner.getSelectedItemPosition()), meterBoxTypeIds.get(meterBoxTypeSpinner.getSelectedItemPosition()),
                coverStatusIds.get(coverStatusSpinner.getSelectedItemPosition()), locationIds.get(locationSpinner.getSelectedItemPosition()), buildingUsageIds.get(buildingUsageSpinner.getSelectedItemPosition()), waterConnectionTypeIds.get(waterConnectionTypeSpinner.getSelectedItemPosition()),
                pipeMaterialIds.get(pipeMaterialSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {
//        Log.e("selected", "selected");
    }

    @Override
    public void onError(@NonNull VerificationError error) {
//        Log.e("error", error.toString());
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> pipeAfterMeterIds, pipeSizeIds, meterMaintenanceAreaIds, meterBoxPositionIds,
            meterBoxTypeIds, coverStatusIds, locationIds, buildingUsageIds, waterConnectionTypeIds, pipeMaterialIds;

    @Override
    public void loadSpinnersData(ArrayList<String> pipeAfterMeter, ArrayList<Integer> pipeAfterMeterIds,
                                 ArrayList<String> pipeSize, ArrayList<Integer> pipeSizeIds,
                                 ArrayList<String> meterMaintenanceArea, ArrayList<Integer> meterMaintenanceAreaIds,
                                 ArrayList<String> meterBoxPosition, ArrayList<Integer> meterBoxPositionIds,
                                 ArrayList<String> meterBoxType, ArrayList<Integer> meterBoxTypeIds,
                                 ArrayList<String> coverStatus, ArrayList<Integer> coverStatusIds,
                                 ArrayList<String> location, ArrayList<Integer> locationIds,
                                 ArrayList<String> buildingUsage, ArrayList<Integer> buildingUsageIds,
                                 ArrayList<String> waterConnectionType, ArrayList<Integer> waterConnectionTypeIds,
                                 ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds) {

        this.pipeAfterMeterIds = pipeAfterMeterIds;
        this.pipeSizeIds = pipeSizeIds;
        this.meterMaintenanceAreaIds = meterMaintenanceAreaIds;
        this.meterBoxPositionIds = meterBoxPositionIds;
        this.meterBoxTypeIds = meterBoxTypeIds;
        this.coverStatusIds = coverStatusIds;
        this.locationIds = locationIds;
        this.buildingUsageIds = buildingUsageIds;
        this.waterConnectionTypeIds = waterConnectionTypeIds;
        this.pipeMaterialIds = pipeMaterialIds;

        pipeAfterMeterSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, pipeAfterMeter));
        pipeSizeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, pipeSize));
        meterMaintenanceAreaSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterMaintenanceArea));
        meterBoxPositionSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterBoxPosition));
        meterBoxTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, meterBoxType));
        coverStatusSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, coverStatus));
        locationSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, location));
        buildingUsageSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, buildingUsage));
        waterConnectionTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, waterConnectionType));
        pipeMaterialSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, pipeMaterial));
    }
}
