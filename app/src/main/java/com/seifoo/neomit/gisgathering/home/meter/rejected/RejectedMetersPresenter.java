package com.seifoo.neomit.gisgathering.home.meter.rejected;


import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RejectedMetersPresenter implements RejectedMetersMVP.Presenter, RejectedMetersModel.VolleyCallback, RejectedMetersModel.DBCallback {
    private RejectedMetersMVP.View view;
    private RejectedMetersModel model;

    public RejectedMetersPresenter(RejectedMetersMVP.View view, RejectedMetersModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestRejected() {
        view.showProgress();
        model.getRejected(view.getAppContext(), this);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ArrayList<MeterObject> rejectedMeters = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {
                    rejectedMeters.add(new MeterObject(data.getJSONObject(i).getLong("Corrected_Id"), returnValidString(data.getJSONObject(i).getString("SERIAL_NO")),
                            returnValidString(data.getJSONObject(i).getString("HCN")),
                            returnValidString(data.getJSONObject(i).getString("METER_ADDRESS")),
                            returnValidString(data.getJSONObject(i).getString("PLATE_NO")),
                            returnValidInt(data.getJSONObject(i).getString("METER_DIAMETER_ID")),
                            returnValidInt(data.getJSONObject(i).getString("METER_BRAND_ID")),
                            returnValidInt(data.getJSONObject(i).getString("METER_TYPE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("METER_STATUS_ID")),
                            returnValidInt(data.getJSONObject(i).getString("METER_MATERIAL_ID")),
                            returnValidInt(data.getJSONObject(i).getString("METER_REMARKS_ID")),
                            returnValidInt(data.getJSONObject(i).getString("READ_TYPE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("PIPE_AFTER_METER_ID")),
                            returnValidInt(data.getJSONObject(i).getString("PIPE_SIZE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("MAINTENANCE_AREA_ID")),
                            returnValidInt(data.getJSONObject(i).getString("METER_BOX_POSITION_ID")),
                            returnValidInt(data.getJSONObject(i).getString("METER_BOX_TYPES_ID")),
                            returnValidInt(data.getJSONObject(i).getString("COVER_STATUS")),
                            returnValidInt(data.getJSONObject(i).getString("LOCATION_IN_BUILDING_ID")),
                            returnValidInt(data.getJSONObject(i).getString("BUILDING_USAGE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("SUB_BUILDING_TYPE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("WATER_SCHEDULE")),
                            returnValidInt(data.getJSONObject(i).getString("WATER_CONNECTION_TYPE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("PIPE_MATERIAL_ID")),
                            returnValidInt(data.getJSONObject(i).getString("BRAND_NEW_METER")),
                            returnValidInt(data.getJSONObject(i).getString("VALVE_TYPE_ID")),
                            returnValidInt(data.getJSONObject(i).getString("VALVE_STATUS_ID")),
                            data.getJSONObject(i).getString("ENABLED_ID").isEmpty() || data.getJSONObject(i).getString("ENABLED_ID").toLowerCase().equals("null") ? 1 : data.getJSONObject(i).getInt("ENABLED_ID"),
                            returnValidInt(data.getJSONObject(i).getString("REDUCER_DIAMETER_ID")),
                            returnValidInt(data.getJSONObject(i).getString("DISTRICT_NAME_ID")),
                            returnValidString(data.getJSONObject(i).getString("DMA_ZONE_ID")),
                            returnValidDate(data.getJSONObject(i).getString("LAST_UPDATED")),
                            returnValidString(data.getJSONObject(i).getString("LOCATION_NO")),
                            returnValidString(data.getJSONObject(i).getString("POST_CODE")),
                            returnValidString(data.getJSONObject(i).getString("SCECO_NO")),
                            returnValidString(data.getJSONObject(i).getString("NumberOfElectricMetres")),
                            returnValidString(data.getJSONObject(i).getString("LAST_READING")),
                            returnValidString(data.getJSONObject(i).getString("BUILDING_NO")),
                            returnValidString(data.getJSONObject(i).getString("NO_OF_FLOORS")),
                            returnValidString(data.getJSONObject(i).getString("BUILDING_DUP")),
                            returnValidString(data.getJSONObject(i).getString("BUILDING_NO_M")),
                            returnValidString(data.getJSONObject(i).getString("BUILD_DESC")),
                            returnValidString(data.getJSONObject(i).getString("STREET_SER_NU")),
                            returnValidString(data.getJSONObject(i).getString("STREET_NAME")),
                            returnValidString(data.getJSONObject(i).getString("STREET_NO_M")),
                            returnValidString(data.getJSONObject(i).getString("SECTOR_NAME")),
                            returnValidString(data.getJSONObject(i).getString("SUB_NAME")),
                            returnValidString(data.getJSONObject(i).getString("IsSewerConnectionExist")),
                            returnValidString(data.getJSONObject(i).getString("ARABIC_NAME")),
                            returnValidString(data.getJSONObject(i).getString("CUSTOMER_ACTIVE_ID")),
                            returnValidString(data.getJSONObject(i).getString("Y_Map")),
                            returnValidString(data.getJSONObject(i).getString("X_Map")),
                            returnValidString(data.getJSONObject(i).getString("Reason")),
                            returnValidString(data.getJSONObject(i).getString("GROUND_ELEVATION")),
                            returnValidString(data.getJSONObject(i).getString("ELEVATION")),
                            returnValidDate1(data.getJSONObject(i).getString("CreatedDate"))));
//x -> longitude , y -> latitude
                }
                model.checkDatabaseMeters(view.getAppContext(), this, rejectedMeters);
            } else {
                view.hideProgress();
                view.showEmptyListText();
            }
        } else {
            view.hideProgress();
            view.showEmptyListText();
        }
    }

    private String returnValidString(String data) {
        return data.isEmpty() || data.toLowerCase().equals("null") ? "" : data;
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }
    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onCheckingMetersCalled(ArrayList<MeterObject> meterObjects) {
        //8000069,0110458722
        view.hideProgress();
        if (meterObjects.isEmpty())
            view.showEmptyListText();
        else
            view.loadRejectedMeters(meterObjects);
    }
}


/*
{
    "status": "Success",
    "mesg": "OK",
    "data": [
        {
            "SERIAL_NO": "800009012",
            "HCN": "8000002",
            "METER_ADDRESS": "8000001234",
            "PLATE_NO": "9000001",
            "METER_DIAMETER_ID": "1",
            "METER_BRAND_ID": "2",
            "METER_TYPE_ID": "1",
            "METER_STATUS_ID": "1",
            "METER_MATERIAL_ID": "2",
            "METER_REMARKS_ID": "",
            "Reason": "Wrong Hcn",
            "READ_TYPE_ID": "3",
            "PIPE_AFTER_METER_ID": "1",
            "PIPE_SIZE_ID": "1",
            "METER_BOX_POSITION_ID": "1",
            "MAINTENANCE_AREA_ID": "1",
            "METER_BOX_TYPES_ID": "",
            "COVER_STATUS": "2",
            "LOCATION_IN_BUILDING_ID": "1",
            "BUILDING_USAGE_ID": "1",
            "SUB_BUILDING_TYPE_ID": "1",
            "WATER_SCHEDULE": "",
            "WATER_CONNECTION_TYPE_ID": "1",
            "PIPE_MATERIAL_ID": "",
            "BRAND_NEW_METER": "3",
            "VALVE_TYPE_ID": "",
            "VALVE_STATUS_ID": "1",
            "ENABLED_ID": "1",
            "REDUCER_DIAMETER_ID": "",
            "DISTRICT_NAME_ID": "حى الزهور",
            "DMA_ZONE_ID": "1",
            "LAST_UPDATED": "1900-01-01T00:00:00",
            "LOCATION_NO": 66,
            "POST_CODE": "3763-39",
            "SCECO_NO": "8266905",
            "NumberOfElectricMetres": "",
            "LAST_READING": 0,
            "BUILDING_NO": "117",
            "NO_OF_FLOORS": "2",
            "BUILDING_DUP": "00",
            "BUILDING_NO_M": "560",
            "BUILD_DESC": "مدرسه اجيال المعرفة الأهلية",
            "STREET_SER_NU": "030",
            "STREET_NAME": "شارع زيد بن ثابت",
            "STREET_NO_M": "045",
            "SECTOR_NAME": "",
            "SUB_NAME": "",
            "IsSewerConnectionExist": "",
            "ARABIC_NAME": "سعدبد اللهيب",
            "CUSTOMER_ACTIVE_ID": "YES",
            "X_Map": 45.28914151,
            "Y_Map": 24.06415872
        }
    ]
}
 */
