package com.seifoo.neomit.gisgathering.home.chamber.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface OfflineChambersDetailsMVP {

    interface View {
        Context getActContext();

        Context getAppContext();


        void loadChamberMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestChamberDetails(ChamberObject chamber);
    }
}
