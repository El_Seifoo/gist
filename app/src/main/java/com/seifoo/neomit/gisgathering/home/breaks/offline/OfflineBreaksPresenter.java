package com.seifoo.neomit.gisgathering.home.breaks.offline;

import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.breaks.edit.EditBreakActivity;
import com.seifoo.neomit.gisgathering.home.breaks.offline.details.OfflineBreakDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class OfflineBreaksPresenter implements OfflineBreaksMVP.Presenter, OfflineBreaksModel.VolleyCallback, OfflineBreaksModel.UploadingPhotoCallback, OfflineBreaksModel.DBCallback {
    private OfflineBreaksMVP.View view;
    private OfflineBreaksModel model;

    public OfflineBreaksPresenter(OfflineBreaksMVP.View view, OfflineBreaksModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestBreaksData(int type) {
        model.getBreaksList(view.getAppContext(), this, type, 0);
    }

    private static final int SHOW_ALL_IN_MAP_REQUEST = 10;

    @Override
    public void requestRemoveBreakObjById(long id, int position) {
        model.removeBreakObject(view.getActContext(), this, id, position);
    }

    @Override
    public void OnListItemClickListener(int viewId, int position, BreakObject breakObj) {
        switch (viewId) {
            case R.id.more_details:
                view.navigateDestination("OfflineBreakObj", breakObj, OfflineBreakDetailsActivity.class);
                break;
            case R.id.remove_item:
                this.requestRemoveBreakObjById(breakObj.getId(), position);
                break;
            case R.id.edit_item:
                this.requestEditBreakData(position, breakObj);
                break;
            case R.id.map:
                view.navigateToMap(SHOW_ALL_IN_MAP_REQUEST, breakObj, position);
        }
    }

    private static final int EDIT_BREAK_OBJECT_REQUEST = 1;

    @Override
    public void requestEditBreakData(int position, BreakObject breakObj) {
        Intent intent = new Intent(view.getAppContext(), EditBreakActivity.class);
        intent.putExtra("BreakObj", breakObj);
        intent.putExtra("position", position);
        view.navigateToEditBreak(intent, EDIT_BREAK_OBJECT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_BREAK_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            view.updateListItem(data.getExtras().getInt("position"), (BreakObject) data.getSerializableExtra("editedBreakObj"));
            return;
        }

        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            this.requestBreaksData(view.getSpinnerSelectedPosition());
        }
    }

    @Override
    public void requestUploadData(int type) {
        model.getBreaksList(view.getAppContext(), this, type, 1);
    }

    @Override
    public void showLocations(ArrayList<BreakObject> breaks) {
        view.navigateToMap2(SHOW_ALL_IN_MAP_REQUEST, breaks);
    }

    @Override
    public void onSyncingResponse(String responseString, ArrayList<BreakObject> breaks) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        long acceptedId;

        if (responseArray.getJSONObject(0).getString("status").equals("success")) {
            acceptedId = responseArray.getJSONObject(0).getLong("id");
            if (breaks.get(0).getPhotos() != null) {
                if ((breaks.get(0).getPhotos().containsKey("afterImage") && breaks.get(0).getPhotos().get("afterImage") != null ||
                        (breaks.get(0).getPhotos().containsKey("beforeImage") && breaks.get(0).getPhotos().get("beforeImage") != null))) {
                    model.syncPics(view.getActContext(), this, acceptedId, breaks);
                }else {
                    breaks.remove(0);
                    model.removeBreakObject(view.getActContext(), this, acceptedId, -1);
                    if (!breaks.isEmpty())
                        model.uploadBreaksData(view.getActContext(), this, breaks);
                    else view.hideProgress();
                }
            } else {

                breaks.remove(0);
                model.removeBreakObject(view.getActContext(), this, acceptedId, -1);
                if (!breaks.isEmpty())
                    model.uploadBreaksData(view.getActContext(), this, breaks);
                else view.hideProgress();
            }

        } else {
            breaks.remove(0);
            if (!breaks.isEmpty())
                model.uploadBreaksData(view.getActContext(), this, breaks);
            else view.hideProgress();
        }


    }

    @Override
    public void onSyncingError(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    /*
    [
    {
        "status": "success",
        "key": "1_after",
        "imageUrl": "http://212.118.113.148:98/images/RE_AKharj/1_1-Gist/after.jpg",
        "message": "File Uploaded"
    }
    ]
      -------------------------------------------
    [
    {
        "status": "success",
        "key": "1_before",
        "imageUrl": "http://212.118.113.148:98/images/RE_AKharj/1_1-Gist/before.png",
        "message": "File Uploaded"
    }
    ]
      -------------------------------------------
    [
    {
        "status": "success",
        "key": "1_after",
        "imageUrl": "http://212.118.113.148:98/images/RE_AKharj/1_1-Gist/after.jpg",
        "message": "File Uploaded"
    },
    {
        "status": "success",
        "key": "1_before",
        "imageUrl": "http://212.118.113.148:98/images/RE_AKharj/1_1-Gist/before.png",
        "message": "File Uploaded"
    }
    ]
     */
    @Override
    public void onUploadSuccess(Context context, JSONArray response, long acceptedId, ArrayList<BreakObject> breaks, ArrayList<String> paths) throws JSONException {
        for (int i = 0; i < response.length(); i++) {
            if (!response.getJSONObject(i).getString("status").equals("success")) {
                view.showToastMessage(context.getString(R.string.failed_to_sync_this_record));
                breaks.remove(0);
                if (!breaks.isEmpty())
                    model.uploadBreaksData(view.getActContext(), this, breaks);
                else view.hideProgress();
                return;
            }
        }
        for (int i = 0; i < paths.size(); i++) {
            deleteImage(context, paths.get(i));
        }
        breaks.remove(0);
        model.removeBreakObject(view.getActContext(), this, acceptedId, -1);
        if (!breaks.isEmpty())
            model.uploadBreaksData(view.getActContext(), this, breaks);
        else view.hideProgress();
    }

    public void deleteImage(Context context, String path) {
        Log.e("path", path);
        File fdelete = new File(path);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                Log.e("-->", "file Deleted 1:" + path);
                callBroadCast(context);
            } else {
                Log.e("-->", "file not Deleted 1:" + path);
            }
        }
    }

    public void callBroadCast(Context context) {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(context, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            view.sendBroadCast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    @Override
    public void onUploadFailed(String message) {
        view.hideProgress();
        view.showToastMessage(message);
//        breaks.remove(0);
//        if (!breaks.isEmpty())
//            model.uploadBreaksData(view.getActContext(), this, breaks);
//        else view.hideProgress();
    }

    @Override
    public void onGetBreaksListCalled(ArrayList<BreakObject> breaks, int index) {
        if (index == 0) {
            if (breaks.isEmpty())
                view.showEmptyListText();
            else {
                boolean isVisible = false;
                for (int i = 0; i < breaks.size(); i++) {
                    if (breaks.get(i).getLatitude() != null && breaks.get(i).getLongitude() != null) {
                        if (!breaks.get(i).getLatitude().isEmpty() && !breaks.get(i).getLongitude().isEmpty()) {
                            isVisible = true;
                            break;
                        }
                    }
                }
                view.loadBreaksData(breaks, isVisible);
            }
        } else {
            if (breaks.isEmpty())
                view.showToastMessage(view.getActContext().getString(R.string.no_breaks_to_synchronize));
            else {
                view.showProgress();
                model.uploadBreaksData(view.getActContext(), this, breaks);
            }
        }
    }

    @Override
    public void onRemoveBreakObjectCalled(int flag, int position, long id) {
        if (flag > 0) {
            if (position == -1) {
                view.removeListItem(id);
            } else
                view.removeListItem(position);
        } else view.showToastMessage(view.getActContext().getString(R.string.failed_to_delete));
    }


}
