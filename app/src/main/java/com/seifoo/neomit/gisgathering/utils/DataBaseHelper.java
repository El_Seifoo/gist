package com.seifoo.neomit.gisgathering.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "gis_gathering_2";
    private static final int DATABASE_VERSION = 61;

    private static DataBaseHelper mInstance;

    private static final String SQL_CREATE_GEO_MASTERS_TABLE = "CREATE TABLE " + Constants.GEO_MASTERS_TABLE_NAME + " ( " +
            Constants.GEO_MASTERS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.GEO_MASTERS_ID_1 + " INTEGER ," +
            Constants.GEO_MASTERS_AR_NAME + " TEXT ," +
            Constants.GEO_MASTERS_EN_NAME + " TEXT ," +
            Constants.GEO_MASTERS_TYPE + " INTEGER " +
            " );";

    private static final String SQL_CREATE_METER_TABLE = "CREATE TABLE " + Constants.METER_TABLE_NAME + " ( " +
            Constants.METER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.METER_ID_1 + " TEXT, " +
            Constants.METER_SERIAL_NUMBER + " TEXT, " +
            Constants.METER_HCN + " TEXT, " +
            Constants.METER_METER_ADDRESS + " TEXT, " +
            Constants.METER_PLATE_NUMBER + " TEXT, " +
            Constants.METER_METER_DIAMETER + " INTEGER, " +
            Constants.METER_METER_BRAND + " INTEGER, " +
            Constants.METER_METER_TYPE + " INTEGER, " +
            Constants.METER_METER_STATUS + " INTEGER, " +
            Constants.METER_METER_MATERIAL + " INTEGER, " +
            Constants.METER_METER_REMARKS + " INTEGER, " +
            Constants.METER_READ_TYPE + " INTEGER," +
            Constants.METER_PIPE_AFTER_METER + " INTEGER," +
            Constants.METER_PIPE_SIZE + " INTEGER," +
            Constants.METER_MAINTENANCE_AREA + " INTEGER," +
            Constants.METER_METER_BOX_POSITION + " INTEGER," +
            Constants.METER_METER_BOX_TYPE + " INTEGER," +
            Constants.METER_COVER_STATUS + " INTEGER," +
            Constants.METER_LOCATION + " INTEGER," +
            Constants.METER_BUILDING_USAGE + " INTEGER," +
            Constants.METER_SUB_BUILDING_TYPE + " INTEGER," +
            Constants.METER_WATER_SCHEDULE + " INTEGER," +
            Constants.METER_WATER_CONNECTION_TYPE + " INTEGER," +
            Constants.METER_PIPE_MATERIAL + " INTEGER," +
            Constants.METER_BRAND_NEW_METER + " INTEGER," +
            Constants.METER_VALVE_TYPE + " INTEGER," +
            Constants.METER_VALVE_STATUS + " INTEGER," +
            Constants.METER_ENABLED + " INTEGER," +
            Constants.METER_REDUCER_DIAMETER + " INTEGER," +
            Constants.METER_DISTRICT_NAME + " INTEGER," +
            Constants.METER_DMA_ZONE + " TEXT," +
            Constants.METER_LAST_UPDATED + " TEXT," +
            Constants.METER_LOCATION_NUMBER + " TEXT," +
            Constants.METER_POST_CODE + " TEXT," +
            Constants.METER_SCECO_NUMBER + " TEXT," +
            Constants.METER_NUMBER_OF_ELECTRIC_METERS + " TEXT," +
            Constants.METER_LAST_READING + " TEXT," +
            Constants.METER_BUILDING_NUMBER + " TEXT," +
            Constants.METER_NUMBER_OF_FLOORS + " TEXT," +
            Constants.METER_BUILDING_DUPLICATION + " TEXT," +
            Constants.METER_BUILDING_NUMBER_M + " TEXT," +
            Constants.METER_BUILDING_DESCRIPTION + " TEXT," +
            Constants.METER_STREET_NUMBER + " TEXT," +
            Constants.METER_STREET_NAME + " TEXT," +
            Constants.METER_STREET_NUMBER_M + " TEXT," +
            Constants.METER_SECTOR_NAME + " TEXT," +
            Constants.METER_SUB_NAME + " TEXT," +
            Constants.METER_IS_SEWER_CONNECTION_EXIST + " TEXT," +
            Constants.METER_ARABIC_NAME + " TEXT," +
            Constants.METER_CUSTOMER_ACTIVE + " TEXT," +
            Constants.METER_LATITUDE + " TEXT," +
            Constants.METER_LONGITUDE + " TEXT," +
            Constants.METER_GROUND_ELEVATION + " TEXT," +
            Constants.METER_ELEVATION + " TEXT," +
            Constants.METER_CREATED_AT + " TEXT" +
            " );";

    private static final String SQL_CREATE_FIRE_HYDRANT_TABLE = "CREATE TABLE " + Constants.FIRE_HYDRATE_TABLE_NAME + " ( " +
            Constants.FIRE_HYDRATE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.FIRE_HYDRATE_ID_1 + " TEXT, " +
            Constants.FIRE_HYDRATE_SERIAL_NUMBER + " TEXT, " +
            Constants.FIRE_HYDRATE_NUMBER + " TEXT, " +
            Constants.FIRE_HYDRATE_HEIGHT + " TEXT, " +
            Constants.FIRE_HYDRATE_BARREL_DIAMETER + " INTEGER, " +
            Constants.FIRE_HYDRATE_MATERIAL_ID + " INTEGER, " +
            Constants.FIRE_HYDRATE_TYPE_ID + " INTEGER, " +
            Constants.FIRE_HYDRATE_COMMISSION_DATE + " TEXT, " +
            Constants.FIRE_HYDRATE_DISTRICT_NAME + " INTEGER, " +
            Constants.FIRE_HYDRATE_STREET_NAME + " TEXT, " +
            Constants.FIRE_HYDRATE_SECTOR_NAME + " TEXT," +
            Constants.FIRE_HYDRATE_STATUS + " INTEGER," +
            Constants.FIRE_HYDRATE_REMARKS + " INTEGER," +
            Constants.FIRE_HYDRATE_WITH_WATER + " TEXT," +
            Constants.FIRE_HYDRATE_VALVE_TYPE + " INTEGER," +
            Constants.FIRE_HYDRATE_BRAND + " INTEGER," +
            Constants.FIRE_HYDRATE_WATER_SCHEDULE + " INTEGER," +
            Constants.FIRE_HYDRATE_MAINTENANCE_AREA + " INTEGER," +
            Constants.FIRE_HYDRATE_LATITUDE + " TEXT," +
            Constants.FIRE_HYDRATE_LONGITUDE + " TEXT," +
            Constants.FIRE_HYDRATE_CREATED_AT + " TEXT " +
            ");";

    private static final String SQL_CREATE_HOUSE_CONNECTION_TABLE = "CREATE TABLE " + Constants.HOUSE_CONNECTION_TABLE_NAME + " ( " +
            Constants.HOUSE_CONNECTION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.HOUSE_CONNECTION_ID_1 + " TEXT, " +
            Constants.HOUSE_CONNECTION_SERIAL_NUMBER + " TEXT, " +
            Constants.HOUSE_CONNECTION_NUMBER + " TEXT, " +
            Constants.HOUSE_CONNECTION_TYPE + " INTEGER, " +
            Constants.HOUSE_CONNECTION_DIAMETER + " INTEGER, " +
            Constants.HOUSE_CONNECTION_MATERIAL + " INTEGER, " +
            Constants.HOUSE_CONNECTION_ASSET_STATUS + " INTEGER, " +
            Constants.HOUSE_CONNECTION_DISTRICT_NAME + " INTEGER, " +
            Constants.HOUSE_CONNECTION_SUB_DISTRICT + " TEXT, " +
            Constants.HOUSE_CONNECTION_STREET_NAME + " TEXT," +
            Constants.HOUSE_CONNECTION_SECTOR_NAME + " TEXT," +
            Constants.HOUSE_CONNECTION_WATER_SCHEDULE + " INTEGER," +
            Constants.HOUSE_CONNECTION_REMARKS + " INTEGER," +
            Constants.HOUSE_CONNECTION_LATITUDE + " TEXT," +
            Constants.HOUSE_CONNECTION_LONGITUDE + " TEXT," +
            Constants.HOUSE_CONNECTION_CREATED_AT + " TEXT " +
            ");";

    private static final String SQL_CREATE_VALVE_TABLE = "CREATE TABLE " + Constants.VALVE_TABLE_NAME + " ( " +
            Constants.VALVE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.VALVE_ID_1 + " TEXT, " +
            Constants.VALVE_JOB + " INTEGER, " +
            Constants.VALVE_SERIAL_NUMBER + " TEXT, " +
            Constants.VALVE_MATERIAL + " INTEGER, " +
            Constants.VALVE_DIAMETER + " INTEGER, " +
            Constants.VALVE_TYPE + " INTEGER, " +
            Constants.VALVE_FULL_NUM_OF_TURNS + " TEXT, " +
            Constants.VALVE_NUM_OF_TURNS + " TEXT, " +
            Constants.VALVE_LOCK + " INTEGER, " +
            Constants.VALVE_STATUS + " INTEGER, " +
            Constants.VALVE_ELEVATION + " TEXT, " +
            Constants.VALVE_GROUND_ELEVATION + " TEXT, " +
            Constants.VALVE_COVER_STATUS + " INTEGER, " +
            Constants.VALVE_ENABLED + " INTEGER," +
            Constants.VALVE_DMA_ZONE + " TEXT," +
            Constants.VALVE_DISTRICT_NAME + " INTEGER," +
            Constants.VALVE_SUB_DISTRICT + " INTEGER," +
            Constants.VALVE_STREET_NAME + " TEXT," +
            Constants.VALVE_SECTOR_NAME + " TEXT," +
            Constants.VALVE_EXIST_IN_FIELD + " INTEGER," +
            Constants.VALVE_EXIST_IN_MAP + " INTEGER," +
            Constants.VALVE_WATER_SCHEDULE + " INTEGER," +
            Constants.VALVE_REMARKS + " INTEGER," +
            Constants.VALVE_LATITUDE + " TEXT, " +
            Constants.VALVE_LONGITUDE + " TEXT," +
            Constants.VALVE_CREATED_AT + " TEXT " +
            ");";

    private static final String SQL_CREATE_MAIN_LINE_TABLE = "CREATE TABLE " + Constants.MAIN_LINE_TABLE_NAME + " ( " +
            Constants.MAIN_LINE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.MAIN_LINE_ID_1 + " TEXT, " +
            Constants.MAIN_LINE_SERIAL_NUMBER + " TEXT, " +
            Constants.MAIN_LINE_DIAMETER + " INTEGER, " +
            Constants.MAIN_LINE_TYPE + " INTEGER, " +
            Constants.MAIN_LINE_ENABLED + " INTEGER, " +
            Constants.MAIN_LINE_MATERIAL + " INTEGER, " +
            Constants.MAIN_LINE_STREET_NAME + " TEXT, " +
            Constants.MAIN_LINE_STREET_NUMBER + " TEXT, " +
            Constants.MAIN_LINE_LAST_UPDATED + " TEXT, " +
            Constants.MAIN_LINE_DISTRICT_NAME + " INTEGER, " +
            Constants.MAIN_LINE_ASSIGNED + " TEXT, " +
            Constants.MAIN_LINE_SUB_DISTRICT + " INTEGER, " +
            Constants.MAIN_LINE_DMA_ZONE + " TEXT, " +
            Constants.MAIN_LINE_SUB_NAME + " TEXT," +
            Constants.MAIN_LINE_SECTOR_NAME + " TEXT," +
            Constants.MAIN_LINE_WATER_SCHEDULE + " INTEGER," +
            Constants.MAIN_LINE_ASSET_STATUS + " INTEGER," +
            Constants.MAIN_LINE_REMARKS + " INTEGER, " +
            Constants.MAIN_LINE_LATITUDE + " TEXT," +
            Constants.MAIN_LINE_LONGITUDE + " TEXT, " +
            Constants.MAIN_LINE_CREATED_AT + " TEXT " +
            ");";

    private static final String SQL_CREATE_CHAMBER_TABLE = "CREATE TABLE " + Constants.CHAMBER_TABLE_NAME + " ( " +
            Constants.CHAMBER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.CHAMBER_ID_1 + " TEXT, " +
            Constants.CHAMBER_LATITUDE + " TEXT, " +
            Constants.CHAMBER_LONGITUDE + " TEXT, " +
            Constants.CHAMBER_PIPE_MATERIAL + " INTEGER, " +
            Constants.CHAMBER_CH_NUMBER + " INTEGER, " +
            Constants.CHAMBER_LENGTH + " TEXT, " +
            Constants.CHAMBER_WIDTH + " TEXT, " +
            Constants.CHAMBER_DEPTH + " TEXT, " +
            Constants.CHAMBER_DIAMETER + " INTEGER, " +
            Constants.CHAMBER_DEPTH_UNDER_PIPE + " TEXT, " +
            Constants.CHAMBER_DEPTH_ABOVE_PIPE + " TEXT, " +
            Constants.CHAMBER_DISTRICT_NAME + " INTEGER, " +
            Constants.CHAMBER_DMA_ZONE + " TEXT, " +
            Constants.CHAMBER_SUB_DISTRICT + " INTEGER," +
            Constants.CHAMBER_STREET_NAME + " TEXT," +
            Constants.CHAMBER_STREET_NUMBER + " TEXT," +
            Constants.CHAMBER_SUB_NAME + " TEXT," +
            Constants.CHAMBER_SECTOR_NAME + " TEXT," +
            Constants.CHAMBER_DIAMETER_AIR_VALVE + " INTEGER," +
            Constants.CHAMBER_DIAMETER_WA_VALVE + " INTEGER," +
            Constants.CHAMBER_DIAMETER_ISO_VALVE + " INTEGER," +
            Constants.CHAMBER_DIAMETER_FLOW_METER + " INTEGER," +
            Constants.CHAMBER_IMAGE_NUMBER + " TEXT," +
            Constants.CHAMBER_TYPE + " INTEGER," +
            Constants.CHAMBER_UPDATED_DATE + " TEXT," +
            Constants.CHAMBER_REMARKS + " INTEGER," +
            Constants.CHAMBER_CREATED_AT + " TEXT " +
            ");";

    private static final String SQL_CREATE_TANK_TABLE = "CREATE TABLE " + Constants.TANK_TABLE_NAME + " ( " +
            Constants.TANK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.TANK_ID_1 + " TEXT, " +
            Constants.TANK_LATITUDE + " TEXT, " +
            Constants.TANK_LONGITUDE + " TEXT, " +
            Constants.TANK_SERIAL_NUMBER + " TEXT, " +
            Constants.TANK_NAME + " TEXT, " +
            Constants.TANK_TYPE + " INTEGER, " +
            Constants.TANK_ENABLED + " INTEGER, " +
            Constants.TANK_COMMISSION_DATE + " TEXT, " +
            Constants.TANK_DIAMETER + " INTEGER, " +
            Constants.TANK_ACTIVE_VOLUME + " TEXT, " +
            Constants.TANK_IN_ACTIVE_VOLUME + " TEXT, " +
            Constants.TANK_LOW_LEVEL + " TEXT, " +
            Constants.TANK_HIGH_LEVEL + " TEXT, " +
            Constants.TANK_ASSET_STATUS + " INTEGER," +
            Constants.TANK_STREET_NAME + " TEXT," +
            Constants.TANK_DISTRICT_NAME + " INTEGER," +
            Constants.TANK_SECTOR_NAME + " TEXT," +
            Constants.TANK_REGION + " TEXT," +
            Constants.TANK_REMARKS + " TEXT," +
            Constants.TANK_LAST_UPDATED + " TEXT," +
            Constants.TANK_CREATED_AT + " TEXT " +
            ");";


    private static final String SQL_CREATE_PUMP_TABLE = "CREATE TABLE " + Constants.PUMP_TABLE_NAME + " ( " +
            Constants.PUMP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.PUMP_ID_1 + " TEXT, " +
            Constants.PUMP_LATITUDE + " TEXT, " +
            Constants.PUMP_LONGITUDE + " TEXT, " +
            Constants.PUMP_CODE + " TEXT, " +
            Constants.PUMP_STATION_NAME + " TEXT, " +
            Constants.PUMP_ENABLED + " INTEGER, " +
            Constants.PUMP_TYPE + " INTEGER, " +
            Constants.PUMP_ASSET_STATUS + " INTEGER, " +
            Constants.PUMP_STREET_NAME + " TEXT, " +
            Constants.PUMP_DISTRICT_NAME + " INTEGER, " +
            Constants.PUMP_SUB_DISTRICT + " INTEGER, " +
            Constants.PUMP_DMA_ZONE + " TEXT, " +
            Constants.PUMP_SECTOR_NAME + " TEXT," +
            Constants.PUMP_LAST_UPDATED + " TEXT," +
            Constants.PUMP_REMARKS + " TEXT," +
            Constants.PUMP_CREATED_AT + " TEXT " +
            ");";

    private static final String SQL_CREATE_BREAK_TABLE = "CREATE TABLE " + Constants.BREAK_TABLE_NAME + " ( " +
            Constants.BREAK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.BREAK_ID_1 + " TEXT, " +
            Constants.BREAK_LATITUDE + " TEXT, " +
            Constants.BREAK_LONGITUDE + " TEXT, " +
            Constants.BREAK_BREAK_NUMBER + " TEXT, " +
            Constants.BREAK_PIPE_TYPE + " INTEGER, " +
            Constants.BREAK_DISTRICT_NAME + " INTEGER, " +
            Constants.BREAK_SUB_ZONE + " INTEGER, " +
            Constants.BREAK_BUILDING_NUMBER + " TEXT, " +
            Constants.BREAK_PIPE_NUMBER + " TEXT, " +
            Constants.BREAK_STREET_NAME + " TEXT, " +
            Constants.BREAK_BREAK_DATE + " TEXT, " +
            Constants.BREAK_DIAMETER + " INTEGER, " +
            Constants.BREAK_PIPE_MATERIAL + " INTEGER," +
            Constants.BREAK_BREAK_DETAILS + " TEXT," +
            Constants.BREAK_USED_EQUIPMENT + " TEXT," +
            Constants.BREAK_MAINTENANCE_COMPANY + " INTEGER," +
            Constants.BREAK_MAINTENANCE_TYPE + " INTEGER," +
            Constants.BREAK_SOIL_TYPE + " INTEGER," +
            Constants.BREAK_ASPHALT + " INTEGER," +
            Constants.BREAK_DIMENSION_DRILLING + " TEXT," +
            Constants.BREAK_MAINTENANCE_DEVICE_TYPE + " INTEGER," +
            Constants.BREAK_PROCEDURE + " INTEGER," +
            Constants.BREAK_IMAGE_BEFORE + " TEXT," +
            Constants.BREAK_IMAGE_AFTER + " TEXT," +
            Constants.BREAK_CREATED_AT + " TEXT " +
            ");";

    private static final String SQL_CREATE_SENSOR_POINT_TABLE = "CREATE TABLE " + Constants.SENSOR_POINT_TABLE_NAME + " ( " +
            Constants.SENSOR_POINT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Constants.SENSOR_POINT_ID_1 + " TEXT, " +
            Constants.SENSOR_POINT_LATITUDE + " TEXT, " +
            Constants.SENSOR_POINT_LONGITUDE + " TEXT, " +
            Constants.SENSOR_POINT_SERIAL_NUMBER + " TEXT, " +
            Constants.SENSOR_POINT_ASSET_NUMBER + " TEXT, " +
            Constants.SENSOR_POINT_DEVICE_TYPE + " INTEGER, " +
            Constants.SENSOR_POINT_ASSET_STATUS + " INTEGER, " +
            Constants.SENSOR_POINT_ELEVATION + " TEXT, " +
            Constants.SENSOR_POINT_CHAMBER_STATUS + " TEXT, " +
            Constants.SENSOR_POINT_TYPE + " INTEGER, " +
            Constants.SENSOR_POINT_LINE_NUMBER + " TEXT, " +
            Constants.SENSOR_POINT_MAINTENANCE_AREA + " INTEGER, " +
            Constants.SENSOR_POINT_COMMISSION_DATE + " TEXT," +
            Constants.SENSOR_POINT_ENABLED + " INTEGER," +
            Constants.SENSOR_POINT_DISTRICT_NAME + " INTEGER," +
            Constants.SENSOR_POINT_STREET_NAME + " TEXT," +
            Constants.SENSOR_POINT_DMA_ZONE + " TEXT," +
            Constants.SENSOR_POINT_SECTOR_NAME + " TEXT," +
            Constants.SENSOR_POINT_DIAMETER + " INTEGER," +
            Constants.SENSOR_POINT_PIPE_MATERIAL + " INTEGER," +
            Constants.SENSOR_POINT_LAST_UPDATE + " TEXT," +
            Constants.SENSOR_POINT_CREATED_AT + " TEXT " +
            ");";

    private DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DataBaseHelper getmInstance(Context context) {
        if (mInstance == null)
            mInstance = new DataBaseHelper(context);
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_METER_TABLE);
        db.execSQL(SQL_CREATE_FIRE_HYDRANT_TABLE);
        db.execSQL(SQL_CREATE_HOUSE_CONNECTION_TABLE);
        db.execSQL(SQL_CREATE_VALVE_TABLE);
        db.execSQL(SQL_CREATE_MAIN_LINE_TABLE);
        db.execSQL(SQL_CREATE_CHAMBER_TABLE);
        db.execSQL(SQL_CREATE_TANK_TABLE);
        db.execSQL(SQL_CREATE_PUMP_TABLE);
        db.execSQL(SQL_CREATE_BREAK_TABLE);
        db.execSQL(SQL_CREATE_SENSOR_POINT_TABLE);
        db.execSQL(SQL_CREATE_GEO_MASTERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(dropTable(Constants.METER_TABLE_NAME));
        db.execSQL(dropTable(Constants.FIRE_HYDRATE_TABLE_NAME));
        db.execSQL(dropTable(Constants.HOUSE_CONNECTION_TABLE_NAME));
        db.execSQL(dropTable(Constants.VALVE_TABLE_NAME));
        db.execSQL(dropTable(Constants.MAIN_LINE_TABLE_NAME));
        db.execSQL(dropTable(Constants.CHAMBER_TABLE_NAME));
        db.execSQL(dropTable(Constants.TANK_TABLE_NAME));
        db.execSQL(dropTable(Constants.PUMP_TABLE_NAME));
        db.execSQL(dropTable(Constants.BREAK_TABLE_NAME));
        db.execSQL(dropTable(Constants.SENSOR_POINT_TABLE_NAME));
        db.execSQL(dropTable(Constants.GEO_MASTERS_TABLE_NAME));
        onCreate(db);
    }

    private String dropTable(String tableName) {
        return "DROP TABLE IF EXISTS ".concat(tableName);
    }

    /*



            Meter




     */
    public long insertMeterObject(MeterObject meter) {
        if (meter == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (meter.getMeterId() != 0.0d)
            values.put(Constants.METER_ID_1, meter.getMeterId());
        values.put(Constants.METER_SERIAL_NUMBER, meter.getMeterSerialNumber());
        values.put(Constants.METER_HCN, meter.getMeterHcn());
        values.put(Constants.METER_METER_ADDRESS, meter.getMeterMeterAddress());
        values.put(Constants.METER_PLATE_NUMBER, meter.getMeterPlateNumber());
        values.put(Constants.METER_METER_DIAMETER, meter.getMeterMeterDiameterId());
        values.put(Constants.METER_METER_BRAND, meter.getMeterMeterBrandId());
        values.put(Constants.METER_METER_TYPE, meter.getMeterMeterTypeId());
        values.put(Constants.METER_METER_STATUS, meter.getMeterMeterStatusId());
        values.put(Constants.METER_METER_MATERIAL, meter.getMeterMeterMaterialId());
        values.put(Constants.METER_METER_REMARKS, meter.getMeterMeterRemarksId());
        values.put(Constants.METER_READ_TYPE, meter.getMeterReadTypeId());
        values.put(Constants.METER_PIPE_AFTER_METER, meter.getMeterPipeAfterMeterId());
        values.put(Constants.METER_PIPE_SIZE, meter.getMeterPipeSizeId());
        values.put(Constants.METER_MAINTENANCE_AREA, meter.getMeterMaintenanceAreaId());
        values.put(Constants.METER_METER_BOX_POSITION, meter.getMeterMeterBoxPositionId());
        values.put(Constants.METER_METER_BOX_TYPE, meter.getMeterMeterBoxTypeId());
        values.put(Constants.METER_COVER_STATUS, meter.getMeterCoverStatusId());
        values.put(Constants.METER_LOCATION, meter.getMeterLocationId());
        values.put(Constants.METER_BUILDING_USAGE, meter.getMeterBuildingUsageId());
        values.put(Constants.METER_SUB_BUILDING_TYPE, meter.getMeterSubBuildingTypeId());
        values.put(Constants.METER_WATER_SCHEDULE, meter.getMeterWaterScheduleId());
        values.put(Constants.METER_WATER_CONNECTION_TYPE, meter.getMeterWaterConnectionTypeId());
        values.put(Constants.METER_PIPE_MATERIAL, meter.getMeterPipeMaterialId());
        values.put(Constants.METER_BRAND_NEW_METER, meter.getMeterBrandNewMeterId());
        values.put(Constants.METER_VALVE_TYPE, meter.getMeterValveTypeId());
        values.put(Constants.METER_VALVE_STATUS, meter.getMeterValveStatusId());
        values.put(Constants.METER_ENABLED, meter.getMeterEnabledId());
        values.put(Constants.METER_REDUCER_DIAMETER, meter.getMeterReducerDiameterId());
        values.put(Constants.METER_DISTRICT_NAME, meter.getMeterDistrictName());
        values.put(Constants.METER_DMA_ZONE, meter.getMeterDmaZone());
        values.put(Constants.METER_LAST_UPDATED, meter.getMeterLastUpdated());
        values.put(Constants.METER_LOCATION_NUMBER, meter.getMeterLocationNumber());
        values.put(Constants.METER_POST_CODE, meter.getMeterPostCode());
        values.put(Constants.METER_SCECO_NUMBER, meter.getMeterScecoNumber());
        values.put(Constants.METER_NUMBER_OF_ELECTRIC_METERS, meter.getMeterNumberOfElectricMeters());
        values.put(Constants.METER_LAST_READING, meter.getMeterLastReading());
        values.put(Constants.METER_BUILDING_NUMBER, meter.getMeterBuildingNumber());
        values.put(Constants.METER_NUMBER_OF_FLOORS, meter.getMeterNumberOfFloors());
        values.put(Constants.METER_BUILDING_DUPLICATION, meter.getMeterBuildingDuplication());
        values.put(Constants.METER_BUILDING_NUMBER_M, meter.getMeterBuildingNumberM());
        values.put(Constants.METER_BUILDING_DESCRIPTION, meter.getMeterBuildingDescription());
        values.put(Constants.METER_STREET_NUMBER, meter.getMeterStreetNumber());
        values.put(Constants.METER_STREET_NAME, meter.getMeterStreetName());
        values.put(Constants.METER_STREET_NUMBER_M, meter.getMeterStreetNumberM());
        values.put(Constants.METER_SECTOR_NAME, meter.getMeterSectorName());
        values.put(Constants.METER_SUB_NAME, meter.getMeterSubName());
        values.put(Constants.METER_IS_SEWER_CONNECTION_EXIST, meter.getMeterIsSewerConnectionExist());
        values.put(Constants.METER_ARABIC_NAME, meter.getMeterArabicName());
        values.put(Constants.METER_CUSTOMER_ACTIVE, meter.getMeterCustomerActive());
        values.put(Constants.METER_LATITUDE, meter.getMeterLatitude());
        values.put(Constants.METER_LONGITUDE, meter.getMeterLongitude());
        values.put(Constants.METER_GROUND_ELEVATION, meter.getGroundElevation());
        values.put(Constants.METER_ELEVATION, meter.getElevation());
        values.put(Constants.METER_CREATED_AT, meter.getCreatedAt());

        long flag = db.insert(Constants.METER_TABLE_NAME, Constants.METER_ID_1, values);

        db.close();

        return flag;
    }

    public int updateMeterObject(MeterObject meter) {
        if (meter == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.METER_SERIAL_NUMBER, meter.getMeterSerialNumber());
        values.put(Constants.METER_HCN, meter.getMeterHcn());
        values.put(Constants.METER_METER_ADDRESS, meter.getMeterMeterAddress());
        values.put(Constants.METER_PLATE_NUMBER, meter.getMeterPlateNumber());
        values.put(Constants.METER_METER_DIAMETER, meter.getMeterMeterDiameterId());
        values.put(Constants.METER_METER_BRAND, meter.getMeterMeterBrandId());
        values.put(Constants.METER_METER_TYPE, meter.getMeterMeterTypeId());
        values.put(Constants.METER_METER_STATUS, meter.getMeterMeterStatusId());
        values.put(Constants.METER_METER_MATERIAL, meter.getMeterMeterMaterialId());
        values.put(Constants.METER_METER_REMARKS, meter.getMeterMeterRemarksId());
        values.put(Constants.METER_READ_TYPE, meter.getMeterReadTypeId());
        values.put(Constants.METER_PIPE_AFTER_METER, meter.getMeterPipeAfterMeterId());
        values.put(Constants.METER_PIPE_SIZE, meter.getMeterPipeSizeId());
        values.put(Constants.METER_MAINTENANCE_AREA, meter.getMeterMaintenanceAreaId());
        values.put(Constants.METER_METER_BOX_POSITION, meter.getMeterMeterBoxPositionId());
        values.put(Constants.METER_METER_BOX_TYPE, meter.getMeterMeterBoxTypeId());
        values.put(Constants.METER_COVER_STATUS, meter.getMeterCoverStatusId());
        values.put(Constants.METER_LOCATION, meter.getMeterLocationId());
        values.put(Constants.METER_BUILDING_USAGE, meter.getMeterBuildingUsageId());
        values.put(Constants.METER_SUB_BUILDING_TYPE, meter.getMeterSubBuildingTypeId());
        values.put(Constants.METER_WATER_SCHEDULE, meter.getMeterWaterScheduleId());
        values.put(Constants.METER_WATER_CONNECTION_TYPE, meter.getMeterWaterConnectionTypeId());
        values.put(Constants.METER_PIPE_MATERIAL, meter.getMeterPipeMaterialId());
        values.put(Constants.METER_BRAND_NEW_METER, meter.getMeterBrandNewMeterId());
        values.put(Constants.METER_VALVE_TYPE, meter.getMeterValveTypeId());
        values.put(Constants.METER_VALVE_STATUS, meter.getMeterValveStatusId());
        values.put(Constants.METER_ENABLED, meter.getMeterEnabledId());
        values.put(Constants.METER_REDUCER_DIAMETER, meter.getMeterReducerDiameterId());
        values.put(Constants.METER_DISTRICT_NAME, meter.getMeterDistrictName());
        values.put(Constants.METER_DMA_ZONE, meter.getMeterDmaZone());
        values.put(Constants.METER_LAST_UPDATED, meter.getMeterLastUpdated());
        values.put(Constants.METER_LOCATION_NUMBER, meter.getMeterLocationNumber());
        values.put(Constants.METER_POST_CODE, meter.getMeterPostCode());
        values.put(Constants.METER_SCECO_NUMBER, meter.getMeterScecoNumber());
        values.put(Constants.METER_NUMBER_OF_ELECTRIC_METERS, meter.getMeterNumberOfElectricMeters());
        values.put(Constants.METER_LAST_READING, meter.getMeterLastReading());
        values.put(Constants.METER_BUILDING_NUMBER, meter.getMeterBuildingNumber());
        values.put(Constants.METER_NUMBER_OF_FLOORS, meter.getMeterNumberOfFloors());
        values.put(Constants.METER_BUILDING_DUPLICATION, meter.getMeterBuildingDuplication());
        values.put(Constants.METER_BUILDING_NUMBER_M, meter.getMeterBuildingNumberM());
        values.put(Constants.METER_BUILDING_DESCRIPTION, meter.getMeterBuildingDescription());
        values.put(Constants.METER_STREET_NUMBER, meter.getMeterStreetNumber());
        values.put(Constants.METER_STREET_NAME, meter.getMeterStreetName());
        values.put(Constants.METER_STREET_NUMBER_M, meter.getMeterStreetNumberM());
        values.put(Constants.METER_SECTOR_NAME, meter.getMeterSectorName());
        values.put(Constants.METER_SUB_NAME, meter.getMeterSubName());
        values.put(Constants.METER_IS_SEWER_CONNECTION_EXIST, meter.getMeterIsSewerConnectionExist());
        values.put(Constants.METER_ARABIC_NAME, meter.getMeterArabicName());
        values.put(Constants.METER_CUSTOMER_ACTIVE, meter.getMeterCustomerActive());
        values.put(Constants.METER_LATITUDE, meter.getMeterLatitude());
        values.put(Constants.METER_LONGITUDE, meter.getMeterLongitude());
        values.put(Constants.METER_GROUND_ELEVATION, meter.getGroundElevation());
        values.put(Constants.METER_ELEVATION, meter.getElevation());

        int flag = db.update(Constants.METER_TABLE_NAME, values, Constants.METER_ID + " = ?",
                new String[]{String.valueOf(meter.getId())});

        db.close();

        return flag;
    }

    public int deleteMeterById(long id) {
        if (id < 1) return 0;
        SQLiteDatabase db = this.getWritableDatabase();
        int flag = db.delete(Constants.METER_TABLE_NAME, Constants.METER_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();

        return flag;
    }

    public ArrayList<MeterObject> getAllMeters(int index) {
        ArrayList<MeterObject> metersList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = index == 0 ? "" :
                index == 1 ? " WHERE " + Constants.METER_ID_1 + " IS NULL " :
                        " WHERE " + Constants.METER_ID_1 + " IS NOT NULL ";

        String selectQuery = "SELECT * FROM " + Constants.METER_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.METER_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {

                metersList.add(new MeterObject(cursor.getLong(cursor.getColumnIndex(Constants.METER_ID)),
                        cursor.getLong(cursor.getColumnIndex(Constants.METER_ID_1)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_SERIAL_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_HCN)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_METER_ADDRESS)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_PLATE_NUMBER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_METER_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_METER_BRAND)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_METER_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_METER_STATUS)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_METER_MATERIAL)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_METER_REMARKS)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_READ_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_PIPE_AFTER_METER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_PIPE_SIZE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_MAINTENANCE_AREA)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_METER_BOX_POSITION)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_METER_BOX_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_COVER_STATUS)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_LOCATION)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_BUILDING_USAGE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_SUB_BUILDING_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_WATER_SCHEDULE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_WATER_CONNECTION_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_PIPE_MATERIAL)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_BRAND_NEW_METER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_VALVE_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_VALVE_STATUS)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_ENABLED)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_REDUCER_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.METER_DISTRICT_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_DMA_ZONE)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_LAST_UPDATED)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_LOCATION_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_POST_CODE)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_SCECO_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_NUMBER_OF_ELECTRIC_METERS)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_LAST_READING)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_BUILDING_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_NUMBER_OF_FLOORS)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_BUILDING_DUPLICATION)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_BUILDING_NUMBER_M)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_BUILDING_DESCRIPTION)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_STREET_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_STREET_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_STREET_NUMBER_M)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_SECTOR_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_SUB_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_IS_SEWER_CONNECTION_EXIST)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_ARABIC_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_CUSTOMER_ACTIVE)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_LATITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_LONGITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_GROUND_ELEVATION)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_ELEVATION)),
                        cursor.getString(cursor.getColumnIndex(Constants.METER_CREATED_AT))));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return metersList;
    }

    public ArrayList<HashMap<String, String>> getMetersLocation() {
        ArrayList<HashMap<String, String>> latLngs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = " WHERE " + Constants.METER_ID_1 + " IS NULL ";

        String selectQuery = "SELECT * FROM " + Constants.METER_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.METER_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                latLngs.add(new HashMap<String, String>() {
                    {
                        put("Latitude", cursor.getString(cursor.getColumnIndex(Constants.METER_LATITUDE)));
                        put("Longitude", cursor.getString(cursor.getColumnIndex(Constants.METER_LONGITUDE)));
                    }
                });
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return latLngs;
    }

    public ArrayList<MeterObject> returnNotExistingMeters(ArrayList<MeterObject> meterObjects) {
        SQLiteDatabase db = this.getWritableDatabase();

        int x = 0;
        Cursor cursor;
        while (x < meterObjects.size()) {
            cursor = db.rawQuery("SELECT * FROM " + Constants.METER_TABLE_NAME +
                    " WHERE " + Constants.METER_ID_1 + " = " + meterObjects.get(x).getMeterId(), null);
            if (cursor.getCount() > 0)
                meterObjects.remove(x);
            else x++;
            cursor.close();
        }
        db.close();

        return meterObjects;
    }

    /*



            FireHydrant




     */
    public long insertFireHydrant(FireHydrantObject fireHydrantObject) {
        if (fireHydrantObject == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (fireHydrantObject.getFireHydrantId() != 0.0d)
            values.put(Constants.FIRE_HYDRATE_ID_1, fireHydrantObject.getFireHydrantId());
        values.put(Constants.FIRE_HYDRATE_LATITUDE, fireHydrantObject.getFireLatitude());
        values.put(Constants.FIRE_HYDRATE_LONGITUDE, fireHydrantObject.getFireLongitude());
        values.put(Constants.FIRE_HYDRATE_SERIAL_NUMBER, fireHydrantObject.getFireSerialNumber());
        values.put(Constants.FIRE_HYDRATE_NUMBER, fireHydrantObject.getFireNumber());
        values.put(Constants.FIRE_HYDRATE_HEIGHT, fireHydrantObject.getFireHeight());
        values.put(Constants.FIRE_HYDRATE_BARREL_DIAMETER, fireHydrantObject.getFireBarrelDiameter());
        values.put(Constants.FIRE_HYDRATE_MATERIAL_ID, fireHydrantObject.getFireMaterialId());
        values.put(Constants.FIRE_HYDRATE_TYPE_ID, fireHydrantObject.getFireTypeId());
        values.put(Constants.FIRE_HYDRATE_COMMISSION_DATE, fireHydrantObject.getFireCommissionDate());
        values.put(Constants.FIRE_HYDRATE_DISTRICT_NAME, fireHydrantObject.getFireDistrictName());
        values.put(Constants.FIRE_HYDRATE_STREET_NAME, fireHydrantObject.getFireStreetName());
        values.put(Constants.FIRE_HYDRATE_SECTOR_NAME, fireHydrantObject.getFireSectorName());
        values.put(Constants.FIRE_HYDRATE_STATUS, fireHydrantObject.getFireStatus());
        values.put(Constants.FIRE_HYDRATE_REMARKS, fireHydrantObject.getFireRemarks());
        values.put(Constants.FIRE_HYDRATE_WITH_WATER, fireHydrantObject.getFireWithWater());
        values.put(Constants.FIRE_HYDRATE_VALVE_TYPE, fireHydrantObject.getFireValveType());
        values.put(Constants.FIRE_HYDRATE_BRAND, fireHydrantObject.getFireBrand());
        values.put(Constants.FIRE_HYDRATE_WATER_SCHEDULE, fireHydrantObject.getFireWaterSchedule());
        values.put(Constants.FIRE_HYDRATE_MAINTENANCE_AREA, fireHydrantObject.getFireMaintenanceArea());
        values.put(Constants.FIRE_HYDRATE_CREATED_AT, fireHydrantObject.getCreatedAt());

        long flag = db.insert(Constants.FIRE_HYDRATE_TABLE_NAME, Constants.FIRE_HYDRATE_ID_1, values);

        db.close();

        return flag;
    }

    public int updateFireHydrant(FireHydrantObject fireHydrantObject) {
        if (fireHydrantObject == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constants.FIRE_HYDRATE_LATITUDE, fireHydrantObject.getFireLatitude());
        values.put(Constants.FIRE_HYDRATE_LONGITUDE, fireHydrantObject.getFireLongitude());
        values.put(Constants.FIRE_HYDRATE_SERIAL_NUMBER, fireHydrantObject.getFireSerialNumber());
        values.put(Constants.FIRE_HYDRATE_NUMBER, fireHydrantObject.getFireNumber());
        values.put(Constants.FIRE_HYDRATE_HEIGHT, fireHydrantObject.getFireHeight());
        values.put(Constants.FIRE_HYDRATE_BARREL_DIAMETER, fireHydrantObject.getFireBarrelDiameter());
        values.put(Constants.FIRE_HYDRATE_MATERIAL_ID, fireHydrantObject.getFireMaterialId());
        values.put(Constants.FIRE_HYDRATE_TYPE_ID, fireHydrantObject.getFireTypeId());
        values.put(Constants.FIRE_HYDRATE_COMMISSION_DATE, fireHydrantObject.getFireCommissionDate());
        values.put(Constants.FIRE_HYDRATE_DISTRICT_NAME, fireHydrantObject.getFireDistrictName());
        values.put(Constants.FIRE_HYDRATE_STREET_NAME, fireHydrantObject.getFireStreetName());
        values.put(Constants.FIRE_HYDRATE_SECTOR_NAME, fireHydrantObject.getFireSectorName());
        values.put(Constants.FIRE_HYDRATE_STATUS, fireHydrantObject.getFireStatus());
        values.put(Constants.FIRE_HYDRATE_REMARKS, fireHydrantObject.getFireRemarks());
        values.put(Constants.FIRE_HYDRATE_WITH_WATER, fireHydrantObject.getFireWithWater());
        values.put(Constants.FIRE_HYDRATE_VALVE_TYPE, fireHydrantObject.getFireValveType());
        values.put(Constants.FIRE_HYDRATE_BRAND, fireHydrantObject.getFireBrand());
        values.put(Constants.FIRE_HYDRATE_WATER_SCHEDULE, fireHydrantObject.getFireWaterSchedule());
        values.put(Constants.FIRE_HYDRATE_MAINTENANCE_AREA, fireHydrantObject.getFireMaintenanceArea());

        int flag = db.update(Constants.FIRE_HYDRATE_TABLE_NAME, values, Constants.FIRE_HYDRATE_ID + " = ?",
                new String[]{String.valueOf(fireHydrantObject.getId())});

        db.close();

        return flag;
    }

    public int deleteFireHydrateById(long id) {
        if (id < 1) return 0;

        SQLiteDatabase db = this.getWritableDatabase();

        int flag = db.delete(Constants.FIRE_HYDRATE_TABLE_NAME, Constants.FIRE_HYDRATE_ID + " = ?",
                new String[]{String.valueOf(id)});

        db.close();

        return flag;
    }

    public ArrayList<FireHydrantObject> getAllFireHydrants(int type) {
        ArrayList<FireHydrantObject> fireHydrantObjects = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = type == 0 ? "" :
                type == 1 ? " WHERE " + Constants.FIRE_HYDRATE_ID_1 + " IS NULL " :
                        " WHERE " + Constants.FIRE_HYDRATE_ID_1 + " IS NOT NULL ";

        String selectQuery = "SELECT * FROM " + Constants.FIRE_HYDRATE_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.FIRE_HYDRATE_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {

                fireHydrantObjects.add(new FireHydrantObject(cursor.getLong(cursor.getColumnIndex(Constants.FIRE_HYDRATE_ID)),
                        cursor.getLong(cursor.getColumnIndex(Constants.FIRE_HYDRATE_ID_1)),
                        cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_SERIAL_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_HEIGHT)),
                        cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_COMMISSION_DATE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.FIRE_HYDRATE_DISTRICT_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_STREET_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_SECTOR_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.FIRE_HYDRATE_REMARKS)),
                        cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_WITH_WATER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.FIRE_HYDRATE_VALVE_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.FIRE_HYDRATE_WATER_SCHEDULE)),
                        cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_LATITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_LONGITUDE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.FIRE_HYDRATE_BARREL_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.FIRE_HYDRATE_MATERIAL_ID)),
                        cursor.getInt(cursor.getColumnIndex(Constants.FIRE_HYDRATE_TYPE_ID)),
                        cursor.getInt(cursor.getColumnIndex(Constants.FIRE_HYDRATE_STATUS)),
                        cursor.getInt(cursor.getColumnIndex(Constants.FIRE_HYDRATE_BRAND)),
                        cursor.getInt(cursor.getColumnIndex(Constants.FIRE_HYDRATE_MAINTENANCE_AREA)),
                        cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_CREATED_AT))));

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return fireHydrantObjects;
    }

    public ArrayList<HashMap<String, String>> getFireHydrantsLocation() {
        ArrayList<HashMap<String, String>> latLngs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = " WHERE " + Constants.FIRE_HYDRATE_ID_1 + " IS NULL ";

        String selectQuery = "SELECT * FROM " + Constants.FIRE_HYDRATE_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.FIRE_HYDRATE_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {

                latLngs.add(new HashMap<String, String>() {
                    {
                        put("Latitude", cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_LATITUDE)));
                        put("Longitude", cursor.getString(cursor.getColumnIndex(Constants.FIRE_HYDRATE_LONGITUDE)));
                    }
                });

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return latLngs;
    }

    public ArrayList<FireHydrantObject> returnNotExistingFires(ArrayList<FireHydrantObject> fires) {
        SQLiteDatabase db = this.getWritableDatabase();

        int x = 0;
        Cursor cursor;
        while (x < fires.size()) {
            cursor = db.rawQuery("SELECT * FROM " + Constants.FIRE_HYDRATE_TABLE_NAME +
                    " WHERE " + Constants.FIRE_HYDRATE_ID_1 + " = " + fires.get(x).getFireHydrantId(), null);
            if (cursor.getCount() > 0)
                fires.remove(x);
            else x++;
            cursor.close();
        }
        db.close();

        return fires;
    }

    /*



            HouseConnection




     */
    public long insertHC(HouseConnectionObject houseConnection) {
        if (houseConnection == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (houseConnection.getHouseConnectionId() != 0.0d)
            values.put(Constants.HOUSE_CONNECTION_ID_1, houseConnection.getHouseConnectionId());
        values.put(Constants.HOUSE_CONNECTION_SERIAL_NUMBER, houseConnection.getSerialNumber());
        values.put(Constants.HOUSE_CONNECTION_NUMBER, houseConnection.getHCN());
        values.put(Constants.HOUSE_CONNECTION_TYPE, houseConnection.getType());
        values.put(Constants.HOUSE_CONNECTION_DIAMETER, houseConnection.getDiameter());
        values.put(Constants.HOUSE_CONNECTION_MATERIAL, houseConnection.getMaterial());
        values.put(Constants.HOUSE_CONNECTION_ASSET_STATUS, houseConnection.getAssetStatus());
        values.put(Constants.HOUSE_CONNECTION_DISTRICT_NAME, houseConnection.getDistrictName());
        values.put(Constants.HOUSE_CONNECTION_SUB_DISTRICT, houseConnection.getSubDistrict());
        values.put(Constants.HOUSE_CONNECTION_STREET_NAME, houseConnection.getStreetName());
        values.put(Constants.HOUSE_CONNECTION_SECTOR_NAME, houseConnection.getSectorName());
        values.put(Constants.HOUSE_CONNECTION_WATER_SCHEDULE, houseConnection.getWaterSchedule());
        values.put(Constants.HOUSE_CONNECTION_REMARKS, houseConnection.getRemarks());
        values.put(Constants.HOUSE_CONNECTION_LATITUDE, houseConnection.getLatitude());
        values.put(Constants.HOUSE_CONNECTION_LONGITUDE, houseConnection.getLongitude());
        values.put(Constants.HOUSE_CONNECTION_CREATED_AT, houseConnection.getCreatedAt());

        long flag = db.insert(Constants.HOUSE_CONNECTION_TABLE_NAME, Constants.HOUSE_CONNECTION_ID_1, values);

        db.close();

        return flag;
    }

    public int updateHC(HouseConnectionObject houseConnection) {
        if (houseConnection == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Constants.HOUSE_CONNECTION_SERIAL_NUMBER, houseConnection.getSerialNumber());
        values.put(Constants.HOUSE_CONNECTION_NUMBER, houseConnection.getHCN());
        values.put(Constants.HOUSE_CONNECTION_TYPE, houseConnection.getType());
        values.put(Constants.HOUSE_CONNECTION_DIAMETER, houseConnection.getDiameter());
        values.put(Constants.HOUSE_CONNECTION_MATERIAL, houseConnection.getMaterial());
        values.put(Constants.HOUSE_CONNECTION_ASSET_STATUS, houseConnection.getAssetStatus());
        values.put(Constants.HOUSE_CONNECTION_DISTRICT_NAME, houseConnection.getDistrictName());
        values.put(Constants.HOUSE_CONNECTION_SUB_DISTRICT, houseConnection.getSubDistrict());
        values.put(Constants.HOUSE_CONNECTION_STREET_NAME, houseConnection.getStreetName());
        values.put(Constants.HOUSE_CONNECTION_SECTOR_NAME, houseConnection.getSectorName());
        values.put(Constants.HOUSE_CONNECTION_WATER_SCHEDULE, houseConnection.getWaterSchedule());
        values.put(Constants.HOUSE_CONNECTION_REMARKS, houseConnection.getRemarks());
        values.put(Constants.HOUSE_CONNECTION_LATITUDE, houseConnection.getLatitude());
        values.put(Constants.HOUSE_CONNECTION_LONGITUDE, houseConnection.getLongitude());

        int flag = db.update(Constants.HOUSE_CONNECTION_TABLE_NAME, values, Constants.HOUSE_CONNECTION_ID + " = ?",
                new String[]{String.valueOf(houseConnection.getId())});

        db.close();

        return flag;
    }

    public int deleteHC(long id) {
        if (id < 1) return 0;

        SQLiteDatabase db = this.getWritableDatabase();

        int flag = db.delete(Constants.HOUSE_CONNECTION_TABLE_NAME, Constants.HOUSE_CONNECTION_ID + " = ?",
                new String[]{String.valueOf(id)});

        db.close();

        return flag;
    }

    public ArrayList<HouseConnectionObject> getAllHCs(int type) {
        ArrayList<HouseConnectionObject> HCs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = type == 0 ? "" :
                type == 1 ? " WHERE " + Constants.HOUSE_CONNECTION_ID_1 + " IS NULL " :
                        " WHERE " + Constants.HOUSE_CONNECTION_ID_1 + " IS NOT NULL ";

        String selectQuery = "SELECT * FROM " + Constants.HOUSE_CONNECTION_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.HOUSE_CONNECTION_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                HCs.add(new HouseConnectionObject(
                        cursor.getLong(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_ID)),
                        cursor.getLong(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_ID_1)),
                        cursor.getString(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_SERIAL_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_NUMBER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_MATERIAL)),
                        cursor.getInt(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_ASSET_STATUS)),
                        cursor.getInt(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_DISTRICT_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_SUB_DISTRICT)),
                        cursor.getString(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_STREET_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_SECTOR_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_WATER_SCHEDULE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_REMARKS)),
                        cursor.getString(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_LATITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_LONGITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_CREATED_AT))));

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return HCs;
    }

    public ArrayList<HashMap<String, String>> getHCsLocation() {
        ArrayList<HashMap<String, String>> latLngs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = " WHERE " + Constants.HOUSE_CONNECTION_ID_1 + " IS NULL ";

        String selectQuery = "SELECT * FROM " + Constants.HOUSE_CONNECTION_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.HOUSE_CONNECTION_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                latLngs.add(new HashMap<String, String>() {
                    {
                        put("Latitude", cursor.getString(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_LATITUDE)));
                        put("Longitude", cursor.getString(cursor.getColumnIndex(Constants.HOUSE_CONNECTION_LONGITUDE)));
                    }
                });

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return latLngs;
    }

    public ArrayList<HouseConnectionObject> getNotExistingHCs(ArrayList<HouseConnectionObject> HCs) {
        SQLiteDatabase db = this.getWritableDatabase();

        int x = 0;
        Cursor cursor;
        while (x < HCs.size()) {
            cursor = db.rawQuery("SELECT * FROM " + Constants.HOUSE_CONNECTION_TABLE_NAME +
                    " WHERE " + Constants.HOUSE_CONNECTION_ID_1 + " = " + HCs.get(x).getHouseConnectionId(), null);
            if (cursor.getCount() > 0)
                HCs.remove(x);
            else x++;
            cursor.close();
        }
        db.close();

        return HCs;
    }

    /*



            Valve




     */
    public long insertValve(ValveObject valve) {
        if (valve == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (valve.getValveId() != 0.0d)
            values.put(Constants.VALVE_ID_1, valve.getValveId());
        values.put(Constants.VALVE_JOB, valve.getValveJob());
        values.put(Constants.VALVE_SERIAL_NUMBER, valve.getSerialNumber());
        values.put(Constants.VALVE_MATERIAL, valve.getMaterial());
        values.put(Constants.VALVE_DIAMETER, valve.getDiameter());
        values.put(Constants.VALVE_TYPE, valve.getType());
        values.put(Constants.VALVE_FULL_NUM_OF_TURNS, valve.getFullNumberOfTurns());
        values.put(Constants.VALVE_NUM_OF_TURNS, valve.getNumberOfTurns());
        values.put(Constants.VALVE_LOCK, valve.getLock());
        values.put(Constants.VALVE_STATUS, valve.getStatus());
        values.put(Constants.VALVE_ELEVATION, valve.getElevation());
        values.put(Constants.VALVE_GROUND_ELEVATION, valve.getGroundElevation());
        values.put(Constants.VALVE_COVER_STATUS, valve.getCoverStatus());
        values.put(Constants.VALVE_ENABLED, valve.getEnabled());
        values.put(Constants.VALVE_DMA_ZONE, valve.getDmaZone());
        values.put(Constants.VALVE_DISTRICT_NAME, valve.getDistrictName());
        values.put(Constants.VALVE_SUB_DISTRICT, valve.getSubDistrict());
        values.put(Constants.VALVE_STREET_NAME, valve.getStreetName());
        values.put(Constants.VALVE_SECTOR_NAME, valve.getSectorName());
        values.put(Constants.VALVE_EXIST_IN_FIELD, valve.getExistInField());
        values.put(Constants.VALVE_EXIST_IN_MAP, valve.getExistInMap());
        values.put(Constants.VALVE_WATER_SCHEDULE, valve.getWaterSchedule());
        values.put(Constants.VALVE_REMARKS, valve.getRemarks());
        values.put(Constants.VALVE_LATITUDE, valve.getLatitude());
        values.put(Constants.VALVE_LONGITUDE, valve.getLongitude());
        values.put(Constants.VALVE_CREATED_AT, valve.getCreatedAt());

        long flag = db.insert(Constants.VALVE_TABLE_NAME, Constants.VALVE_ID_1, values);

        db.close();

        return flag;
    }

    public int updateValve(ValveObject valve) {
        if (valve == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Constants.VALVE_JOB, valve.getValveJob());
        values.put(Constants.VALVE_SERIAL_NUMBER, valve.getSerialNumber());
        values.put(Constants.VALVE_MATERIAL, valve.getMaterial());
        values.put(Constants.VALVE_DIAMETER, valve.getDiameter());
        values.put(Constants.VALVE_TYPE, valve.getType());
        values.put(Constants.VALVE_FULL_NUM_OF_TURNS, valve.getFullNumberOfTurns());
        values.put(Constants.VALVE_NUM_OF_TURNS, valve.getNumberOfTurns());
        values.put(Constants.VALVE_LOCK, valve.getLock());
        values.put(Constants.VALVE_STATUS, valve.getStatus());
        values.put(Constants.VALVE_ELEVATION, valve.getElevation());
        values.put(Constants.VALVE_GROUND_ELEVATION, valve.getGroundElevation());
        values.put(Constants.VALVE_COVER_STATUS, valve.getCoverStatus());
        values.put(Constants.VALVE_ENABLED, valve.getEnabled());
        values.put(Constants.VALVE_DMA_ZONE, valve.getDmaZone());
        values.put(Constants.VALVE_DISTRICT_NAME, valve.getDistrictName());
        values.put(Constants.VALVE_SUB_DISTRICT, valve.getSubDistrict());
        values.put(Constants.VALVE_STREET_NAME, valve.getStreetName());
        values.put(Constants.VALVE_SECTOR_NAME, valve.getSectorName());
        values.put(Constants.VALVE_EXIST_IN_FIELD, valve.getExistInField());
        values.put(Constants.VALVE_EXIST_IN_MAP, valve.getExistInMap());
        values.put(Constants.VALVE_WATER_SCHEDULE, valve.getWaterSchedule());
        values.put(Constants.VALVE_REMARKS, valve.getRemarks());
        values.put(Constants.VALVE_LATITUDE, valve.getLatitude());

        int flag = db.update(Constants.VALVE_TABLE_NAME, values, Constants.VALVE_ID + " = ?",
                new String[]{String.valueOf(valve.getId())});

        db.close();

        return flag;
    }

    public int deleteValve(long id) {
        if (id < 1) return 0;

        SQLiteDatabase db = this.getWritableDatabase();

        int flag = db.delete(Constants.VALVE_TABLE_NAME, Constants.VALVE_ID + " = ?",
                new String[]{String.valueOf(id)});

        db.close();

        return flag;
    }

    public ArrayList<ValveObject> getAllValves(int type) {
        ArrayList<ValveObject> valves = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = type == 0 ? "" :
                type == 1 ? " WHERE " + Constants.VALVE_ID_1 + " IS NULL " :
                        " WHERE " + Constants.VALVE_ID_1 + " IS NOT NULL ";

        String selectQuery = "SELECT * FROM " + Constants.VALVE_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.VALVE_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                valves.add(new ValveObject(
                        cursor.getLong(cursor.getColumnIndex(Constants.VALVE_ID)),
                        cursor.getLong(cursor.getColumnIndex(Constants.VALVE_ID_1)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_LATITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_LONGITUDE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_JOB)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_SERIAL_NUMBER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_MATERIAL)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_TYPE)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_FULL_NUM_OF_TURNS)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_NUM_OF_TURNS)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_LOCK)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_STATUS)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_ELEVATION)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_GROUND_ELEVATION)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_COVER_STATUS)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_ENABLED)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_DMA_ZONE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_DISTRICT_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_SUB_DISTRICT)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_STREET_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_SECTOR_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_EXIST_IN_FIELD)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_EXIST_IN_MAP)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_WATER_SCHEDULE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.VALVE_REMARKS)),
                        cursor.getString(cursor.getColumnIndex(Constants.VALVE_CREATED_AT))));

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return valves;
    }

    public ArrayList<HashMap<String, String>> getValvesLocation() {
        ArrayList<HashMap<String, String>> latLngs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = " WHERE " + Constants.VALVE_ID_1 + " IS NULL ";

        String selectQuery = "SELECT * FROM " + Constants.VALVE_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.VALVE_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                latLngs.add(new HashMap<String, String>() {
                    {
                        put("Latitude", cursor.getString(cursor.getColumnIndex(Constants.VALVE_LATITUDE)));
                        put("Longitude", cursor.getString(cursor.getColumnIndex(Constants.VALVE_LONGITUDE)));
                    }
                });
            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return latLngs;
    }

    public ArrayList<ValveObject> getNotExistingValves(ArrayList<ValveObject> valves) {
        SQLiteDatabase db = this.getWritableDatabase();

        int x = 0;
        Cursor cursor;
        while (x < valves.size()) {
            cursor = db.rawQuery("SELECT * FROM " + Constants.VALVE_TABLE_NAME +
                    " WHERE " + Constants.VALVE_ID_1 + " = " + valves.get(x).getValveId(), null);
            if (cursor.getCount() > 0)
                valves.remove(x);
            else x++;
            cursor.close();
        }
        db.close();

        return valves;
    }

    /*



                MainLine




    */
    public long insertMainLine(MainLineObject mainLine) {
        if (mainLine == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (mainLine.getMainLineId() != 0.0d)
            values.put(Constants.MAIN_LINE_ID_1, mainLine.getMainLineId());
        values.put(Constants.MAIN_LINE_SERIAL_NUMBER, mainLine.getSerialNumber());
        values.put(Constants.MAIN_LINE_DIAMETER, mainLine.getDiameter());
        values.put(Constants.MAIN_LINE_TYPE, mainLine.getType());
        values.put(Constants.MAIN_LINE_ENABLED, mainLine.getEnabled());
        values.put(Constants.MAIN_LINE_MATERIAL, mainLine.getMaterial());
        values.put(Constants.MAIN_LINE_STREET_NAME, mainLine.getStreetName());
        values.put(Constants.MAIN_LINE_STREET_NUMBER, mainLine.getStreetNumber());
        values.put(Constants.MAIN_LINE_LAST_UPDATED, mainLine.getLastUpdated());
        values.put(Constants.MAIN_LINE_DISTRICT_NAME, mainLine.getDistrictName());
        values.put(Constants.MAIN_LINE_ASSIGNED, mainLine.getAssigned());
        values.put(Constants.MAIN_LINE_SUB_DISTRICT, mainLine.getSubDistrict());
        values.put(Constants.MAIN_LINE_DMA_ZONE, mainLine.getDmaZone());
        values.put(Constants.MAIN_LINE_SUB_NAME, mainLine.getSubName());
        values.put(Constants.MAIN_LINE_SECTOR_NAME, mainLine.getSectorName());
        values.put(Constants.MAIN_LINE_WATER_SCHEDULE, mainLine.getWaterSchedule());
        values.put(Constants.MAIN_LINE_ASSET_STATUS, mainLine.getAssetStatus());
        values.put(Constants.MAIN_LINE_REMARKS, mainLine.getRemarks());
        values.put(Constants.MAIN_LINE_LATITUDE, mainLine.getLatitude());
        values.put(Constants.MAIN_LINE_LONGITUDE, mainLine.getLongitude());
        values.put(Constants.MAIN_LINE_CREATED_AT, mainLine.getCreatedAt());

        long flag = db.insert(Constants.MAIN_LINE_TABLE_NAME, Constants.MAIN_LINE_ID_1, values);

        db.close();

        return flag;
    }

    public int updateMainLine(MainLineObject mainLine) {
        if (mainLine == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Constants.MAIN_LINE_SERIAL_NUMBER, mainLine.getSerialNumber());
        values.put(Constants.MAIN_LINE_DIAMETER, mainLine.getDiameter());
        values.put(Constants.MAIN_LINE_TYPE, mainLine.getType());
        values.put(Constants.MAIN_LINE_ENABLED, mainLine.getEnabled());
        values.put(Constants.MAIN_LINE_MATERIAL, mainLine.getMaterial());
        values.put(Constants.MAIN_LINE_STREET_NAME, mainLine.getStreetName());
        values.put(Constants.MAIN_LINE_STREET_NUMBER, mainLine.getStreetNumber());
        values.put(Constants.MAIN_LINE_LAST_UPDATED, mainLine.getLastUpdated());
        values.put(Constants.MAIN_LINE_DISTRICT_NAME, mainLine.getDistrictName());
        values.put(Constants.MAIN_LINE_ASSIGNED, mainLine.getAssigned());
        values.put(Constants.MAIN_LINE_SUB_DISTRICT, mainLine.getSubDistrict());
        values.put(Constants.MAIN_LINE_DMA_ZONE, mainLine.getDmaZone());
        values.put(Constants.MAIN_LINE_SUB_NAME, mainLine.getSubName());
        values.put(Constants.MAIN_LINE_SECTOR_NAME, mainLine.getSectorName());
        values.put(Constants.MAIN_LINE_WATER_SCHEDULE, mainLine.getWaterSchedule());
        values.put(Constants.MAIN_LINE_ASSET_STATUS, mainLine.getAssetStatus());
        values.put(Constants.MAIN_LINE_REMARKS, mainLine.getRemarks());
        values.put(Constants.MAIN_LINE_LATITUDE, mainLine.getLatitude());
        values.put(Constants.MAIN_LINE_LONGITUDE, mainLine.getLongitude());


        int flag = db.update(Constants.MAIN_LINE_TABLE_NAME, values, Constants.MAIN_LINE_ID + " = ?",
                new String[]{String.valueOf(mainLine.getId())});

        db.close();

        return flag;
    }

    public int deleteMainLine(long id) {
        if (id < 1) return 0;

        SQLiteDatabase db = this.getWritableDatabase();

        int flag = db.delete(Constants.MAIN_LINE_TABLE_NAME, Constants.MAIN_LINE_ID + " = ?",
                new String[]{String.valueOf(id)});

        db.close();

        return flag;
    }

    public ArrayList<MainLineObject> getAllMainLines(int type) {
        ArrayList<MainLineObject> mainLines = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = type == 0 ? "" :
                type == 1 ? " WHERE " + Constants.MAIN_LINE_ID_1 + " IS NULL " :
                        " WHERE " + Constants.MAIN_LINE_ID_1 + " IS NOT NULL ";

        String selectQuery = "SELECT * FROM " + Constants.MAIN_LINE_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.MAIN_LINE_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                mainLines.add(new MainLineObject(
                        cursor.getLong(cursor.getColumnIndex(Constants.MAIN_LINE_ID)),
                        cursor.getLong(cursor.getColumnIndex(Constants.MAIN_LINE_ID_1)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_LATITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_LONGITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_SERIAL_NUMBER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.MAIN_LINE_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.MAIN_LINE_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.MAIN_LINE_ENABLED)),
                        cursor.getInt(cursor.getColumnIndex(Constants.MAIN_LINE_MATERIAL)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_STREET_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_STREET_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_LAST_UPDATED)),
                        cursor.getInt(cursor.getColumnIndex(Constants.MAIN_LINE_DISTRICT_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_ASSIGNED)),
                        cursor.getInt(cursor.getColumnIndex(Constants.MAIN_LINE_SUB_DISTRICT)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_DMA_ZONE)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_SUB_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_SECTOR_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.MAIN_LINE_WATER_SCHEDULE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.MAIN_LINE_ASSET_STATUS)),
                        cursor.getInt(cursor.getColumnIndex(Constants.MAIN_LINE_REMARKS)),
                        cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_CREATED_AT))));

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return mainLines;
    }

    public ArrayList<HashMap<String, String>> getMainLinesLocation() {
        ArrayList<HashMap<String, String>> latLngs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = " WHERE " + Constants.MAIN_LINE_ID_1 + " IS NULL ";

        String selectQuery = "SELECT * FROM " + Constants.MAIN_LINE_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.MAIN_LINE_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                latLngs.add(new HashMap<String, String>() {
                    {
                        put("Latitude", cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_LATITUDE)));
                        put("Longitude", cursor.getString(cursor.getColumnIndex(Constants.MAIN_LINE_LONGITUDE)));
                    }
                });
            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return latLngs;
    }

    public ArrayList<MainLineObject> getNotExistingMainLines(ArrayList<MainLineObject> mainLines) {
        SQLiteDatabase db = this.getWritableDatabase();

        int x = 0;
        Cursor cursor;
        while (x < mainLines.size()) {
            cursor = db.rawQuery("SELECT * FROM " + Constants.MAIN_LINE_TABLE_NAME +
                    " WHERE " + Constants.MAIN_LINE_ID_1 + " = " + mainLines.get(x).getMainLineId(), null);
            if (cursor.getCount() > 0)
                mainLines.remove(x);
            else x++;
            cursor.close();
        }
        db.close();

        return mainLines;
    }

    /*



            Chamber




     */
    public long insertChamber(ChamberObject chamber) {
        if (chamber == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (chamber.getChamberId() != 0.0d)
            values.put(Constants.CHAMBER_ID_1, chamber.getChamberId());
        values.put(Constants.CHAMBER_LATITUDE, chamber.getLatitude());
        values.put(Constants.CHAMBER_LONGITUDE, chamber.getLongitude());
        values.put(Constants.CHAMBER_PIPE_MATERIAL, chamber.getPipeMaterial());
        values.put(Constants.CHAMBER_CH_NUMBER, chamber.getChNumber());
        values.put(Constants.CHAMBER_LENGTH, chamber.getLength());
        values.put(Constants.CHAMBER_WIDTH, chamber.getWidth());
        values.put(Constants.CHAMBER_DEPTH, chamber.getDepth());
        values.put(Constants.CHAMBER_DIAMETER, chamber.getDiameter());
        values.put(Constants.CHAMBER_DEPTH_UNDER_PIPE, chamber.getDepthUnderPipe());
        values.put(Constants.CHAMBER_DEPTH_ABOVE_PIPE, chamber.getDepthAbovePipe());
        values.put(Constants.CHAMBER_DISTRICT_NAME, chamber.getDistrictName());
        values.put(Constants.CHAMBER_DMA_ZONE, chamber.getDmaZone());
        values.put(Constants.CHAMBER_SUB_DISTRICT, chamber.getSubDistrictName());
        values.put(Constants.CHAMBER_STREET_NAME, chamber.getStreetName());
        values.put(Constants.CHAMBER_STREET_NUMBER, chamber.getStreetNumber());
        values.put(Constants.CHAMBER_SUB_NAME, chamber.getSubName());
        values.put(Constants.CHAMBER_SECTOR_NAME, chamber.getSectorName());
        values.put(Constants.CHAMBER_DIAMETER_AIR_VALVE, chamber.getDiameterAirValve());
        values.put(Constants.CHAMBER_DIAMETER_WA_VALVE, chamber.getDiameterWAValve());
        values.put(Constants.CHAMBER_DIAMETER_ISO_VALVE, chamber.getDiameterISOValve());
        values.put(Constants.CHAMBER_DIAMETER_FLOW_METER, chamber.getDiameterFlowMeter());
        values.put(Constants.CHAMBER_IMAGE_NUMBER, chamber.getImageNumber());
        values.put(Constants.CHAMBER_TYPE, chamber.getType());
        values.put(Constants.CHAMBER_UPDATED_DATE, chamber.getUpdatedDate());
        values.put(Constants.CHAMBER_REMARKS, chamber.getRemarks());
        values.put(Constants.CHAMBER_CREATED_AT, chamber.getCreatedAt());


        long flag = db.insert(Constants.CHAMBER_TABLE_NAME, Constants.CHAMBER_ID_1, values);

        db.close();

        return flag;
    }

    public int updateChamber(ChamberObject chamber) {
        if (chamber == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Constants.CHAMBER_LATITUDE, chamber.getLatitude());
        values.put(Constants.CHAMBER_LONGITUDE, chamber.getLongitude());
        values.put(Constants.CHAMBER_PIPE_MATERIAL, chamber.getPipeMaterial());
        values.put(Constants.CHAMBER_CH_NUMBER, chamber.getChNumber());
        values.put(Constants.CHAMBER_LENGTH, chamber.getLength());
        values.put(Constants.CHAMBER_WIDTH, chamber.getWidth());
        values.put(Constants.CHAMBER_DEPTH, chamber.getDepth());
        values.put(Constants.CHAMBER_DIAMETER, chamber.getDiameter());
        values.put(Constants.CHAMBER_DEPTH_UNDER_PIPE, chamber.getDepthUnderPipe());
        values.put(Constants.CHAMBER_DEPTH_ABOVE_PIPE, chamber.getDepthAbovePipe());
        values.put(Constants.CHAMBER_DISTRICT_NAME, chamber.getDistrictName());
        values.put(Constants.CHAMBER_DMA_ZONE, chamber.getDmaZone());
        values.put(Constants.CHAMBER_SUB_DISTRICT, chamber.getSubDistrictName());
        values.put(Constants.CHAMBER_STREET_NAME, chamber.getStreetName());
        values.put(Constants.CHAMBER_STREET_NUMBER, chamber.getStreetNumber());
        values.put(Constants.CHAMBER_SUB_NAME, chamber.getSubName());
        values.put(Constants.CHAMBER_SECTOR_NAME, chamber.getSectorName());
        values.put(Constants.CHAMBER_DIAMETER_AIR_VALVE, chamber.getDiameterAirValve());
        values.put(Constants.CHAMBER_DIAMETER_WA_VALVE, chamber.getDiameterWAValve());
        values.put(Constants.CHAMBER_DIAMETER_ISO_VALVE, chamber.getDiameterISOValve());
        values.put(Constants.CHAMBER_DIAMETER_FLOW_METER, chamber.getDiameterFlowMeter());
        values.put(Constants.CHAMBER_IMAGE_NUMBER, chamber.getImageNumber());
        values.put(Constants.CHAMBER_TYPE, chamber.getType());
        values.put(Constants.CHAMBER_UPDATED_DATE, chamber.getUpdatedDate());
        values.put(Constants.CHAMBER_REMARKS, chamber.getRemarks());
        values.put(Constants.CHAMBER_CREATED_AT, chamber.getCreatedAt());


        int flag = db.update(Constants.CHAMBER_TABLE_NAME, values, Constants.CHAMBER_ID + " = ?",
                new String[]{String.valueOf(chamber.getId())});

        db.close();

        return flag;
    }

    public int deleteChamber(long id) {
        if (id < 1) return 0;

        SQLiteDatabase db = this.getWritableDatabase();

        int flag = db.delete(Constants.CHAMBER_TABLE_NAME, Constants.CHAMBER_ID + " = ?",
                new String[]{String.valueOf(id)});

        db.close();

        return flag;
    }

    public ArrayList<ChamberObject> getAllChambers(int type) {
        ArrayList<ChamberObject> chambers = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = type == 0 ? "" :
                type == 1 ? " WHERE " + Constants.CHAMBER_ID_1 + " IS NULL " :
                        " WHERE " + Constants.CHAMBER_ID_1 + " IS NOT NULL ";

        String selectQuery = "SELECT * FROM " + Constants.CHAMBER_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.CHAMBER_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                chambers.add(new ChamberObject(
                        cursor.getLong(cursor.getColumnIndex(Constants.CHAMBER_ID)),
                        cursor.getLong(cursor.getColumnIndex(Constants.CHAMBER_ID_1)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_LATITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_LONGITUDE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.CHAMBER_PIPE_MATERIAL)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_CH_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_LENGTH)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_WIDTH)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_DEPTH)),
                        cursor.getInt(cursor.getColumnIndex(Constants.CHAMBER_DIAMETER)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_DEPTH_UNDER_PIPE)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_DEPTH_ABOVE_PIPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.CHAMBER_DISTRICT_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_DMA_ZONE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.CHAMBER_SUB_DISTRICT)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_STREET_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_STREET_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_SUB_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_SECTOR_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.CHAMBER_DIAMETER_AIR_VALVE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.CHAMBER_DIAMETER_WA_VALVE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.CHAMBER_DIAMETER_ISO_VALVE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.CHAMBER_DIAMETER_FLOW_METER)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_IMAGE_NUMBER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.CHAMBER_TYPE)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_UPDATED_DATE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.CHAMBER_REMARKS)),
                        cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_CREATED_AT))));

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return chambers;
    }

    public ArrayList<HashMap<String, String>> getChambersLocation() {
        ArrayList<HashMap<String, String>> latLngs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = " WHERE " + Constants.CHAMBER_ID_1 + " IS NULL ";


        String selectQuery = "SELECT * FROM " + Constants.CHAMBER_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.CHAMBER_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                latLngs.add(new HashMap<String, String>() {
                    {
                        put("Latitude", cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_LATITUDE)));
                        put("Longitude", cursor.getString(cursor.getColumnIndex(Constants.CHAMBER_LONGITUDE)));
                    }
                });

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return latLngs;
    }

    public ArrayList<ChamberObject> getNotExistingChambers(ArrayList<ChamberObject> chambers) {
        SQLiteDatabase db = this.getWritableDatabase();

        int x = 0;
        Cursor cursor;
        while (x < chambers.size()) {
            cursor = db.rawQuery("SELECT * FROM " + Constants.CHAMBER_TABLE_NAME +
                    " WHERE " + Constants.CHAMBER_ID_1 + " = " + chambers.get(x).getChamberId(), null);
            if (cursor.getCount() > 0)
                chambers.remove(x);
            else x++;
            cursor.close();
        }
        db.close();

        return chambers;
    }

    /*



            Tank




     */
    public long insertTank(TankObject tank) {
        if (tank == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (tank.getTankId() != 0.0d)
            values.put(Constants.TANK_ID_1, tank.getTankId());
        values.put(Constants.TANK_LATITUDE, tank.getLatitude());
        values.put(Constants.TANK_LONGITUDE, tank.getLongitude());
        values.put(Constants.TANK_SERIAL_NUMBER, tank.getSerialNumber());
        values.put(Constants.TANK_NAME, tank.getTankName());
        values.put(Constants.TANK_TYPE, tank.getType());
        values.put(Constants.TANK_ENABLED, tank.getEnabled());
        values.put(Constants.TANK_COMMISSION_DATE, tank.getCommissionDate());
        values.put(Constants.TANK_DIAMETER, tank.getTankDiameter());
        values.put(Constants.TANK_ACTIVE_VOLUME, tank.getActiveVolume());
        values.put(Constants.TANK_IN_ACTIVE_VOLUME, tank.getInActiveVolume());
        values.put(Constants.TANK_LOW_LEVEL, tank.getLowLevel());
        values.put(Constants.TANK_HIGH_LEVEL, tank.getHighLevel());
        values.put(Constants.TANK_ASSET_STATUS, tank.getAssetStatus());
        values.put(Constants.TANK_STREET_NAME, tank.getStreetName());
        values.put(Constants.TANK_DISTRICT_NAME, tank.getDistrictName());
        values.put(Constants.TANK_SECTOR_NAME, tank.getSectorName());
        values.put(Constants.TANK_REGION, tank.getRegion());
        values.put(Constants.TANK_REMARKS, tank.getRemarks());
        values.put(Constants.TANK_LAST_UPDATED, tank.getLastUpdated());
        values.put(Constants.TANK_CREATED_AT, tank.getCreatedAt());

        long flag = db.insert(Constants.TANK_TABLE_NAME, Constants.TANK_ID_1, values);

        db.close();

        return flag;
    }

    public int updateTank(TankObject tank) {
        if (tank == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Constants.TANK_LATITUDE, tank.getLatitude());
        values.put(Constants.TANK_LONGITUDE, tank.getLongitude());
        values.put(Constants.TANK_SERIAL_NUMBER, tank.getSerialNumber());
        values.put(Constants.TANK_NAME, tank.getTankName());
        values.put(Constants.TANK_TYPE, tank.getType());
        values.put(Constants.TANK_ENABLED, tank.getEnabled());
        values.put(Constants.TANK_COMMISSION_DATE, tank.getCommissionDate());
        values.put(Constants.TANK_DIAMETER, tank.getTankDiameter());
        values.put(Constants.TANK_ACTIVE_VOLUME, tank.getActiveVolume());
        values.put(Constants.TANK_IN_ACTIVE_VOLUME, tank.getInActiveVolume());
        values.put(Constants.TANK_LOW_LEVEL, tank.getLowLevel());
        values.put(Constants.TANK_HIGH_LEVEL, tank.getHighLevel());
        values.put(Constants.TANK_ASSET_STATUS, tank.getAssetStatus());
        values.put(Constants.TANK_STREET_NAME, tank.getStreetName());
        values.put(Constants.TANK_DISTRICT_NAME, tank.getDistrictName());
        values.put(Constants.TANK_SECTOR_NAME, tank.getSectorName());
        values.put(Constants.TANK_REGION, tank.getRegion());
        values.put(Constants.TANK_REMARKS, tank.getRemarks());
        values.put(Constants.TANK_LAST_UPDATED, tank.getLastUpdated());
        values.put(Constants.TANK_CREATED_AT, tank.getCreatedAt());


        int flag = db.update(Constants.TANK_TABLE_NAME, values, Constants.TANK_ID + " = ?",
                new String[]{String.valueOf(tank.getId())});

        db.close();

        return flag;
    }

    public int deleteTank(long id) {
        if (id < 1) return 0;

        SQLiteDatabase db = this.getWritableDatabase();

        int flag = db.delete(Constants.TANK_TABLE_NAME, Constants.TANK_ID + " = ?",
                new String[]{String.valueOf(id)});

        db.close();

        return flag;
    }

    public ArrayList<TankObject> getAllTanks(int type) {
        ArrayList<TankObject> tanks = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = type == 0 ? "" :
                type == 1 ? " WHERE " + Constants.TANK_ID_1 + " IS NULL " :
                        " WHERE " + Constants.TANK_ID_1 + " IS NOT NULL ";

        String selectQuery = "SELECT * FROM " + Constants.TANK_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.TANK_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                tanks.add(new TankObject(
                        cursor.getLong(cursor.getColumnIndex(Constants.TANK_ID)),
                        cursor.getLong(cursor.getColumnIndex(Constants.TANK_ID_1)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_LATITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_LONGITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_SERIAL_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.TANK_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.TANK_ENABLED)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_COMMISSION_DATE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.TANK_DIAMETER)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_ACTIVE_VOLUME)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_IN_ACTIVE_VOLUME)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_LOW_LEVEL)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_HIGH_LEVEL)),
                        cursor.getInt(cursor.getColumnIndex(Constants.TANK_ASSET_STATUS)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_STREET_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.TANK_DISTRICT_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_SECTOR_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_REGION)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_REMARKS)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_LAST_UPDATED)),
                        cursor.getString(cursor.getColumnIndex(Constants.TANK_CREATED_AT))));

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return tanks;
    }

    public ArrayList<HashMap<String, String>> getTanksLocation() {
        ArrayList<HashMap<String, String>> latLngs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = " WHERE " + Constants.TANK_ID_1 + " IS NULL ";

        String selectQuery = "SELECT * FROM " + Constants.TANK_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.TANK_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                latLngs.add(new HashMap<String, String>() {
                    {
                        put("Latitude", cursor.getString(cursor.getColumnIndex(Constants.TANK_LATITUDE)));
                        put("Longitude", cursor.getString(cursor.getColumnIndex(Constants.TANK_LONGITUDE)));
                    }
                });
            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return latLngs;
    }

    public ArrayList<TankObject> getNotExistingTanks(ArrayList<TankObject> tanks) {
        SQLiteDatabase db = this.getWritableDatabase();

        int x = 0;
        Cursor cursor;
        while (x < tanks.size()) {
            cursor = db.rawQuery("SELECT * FROM " + Constants.TANK_TABLE_NAME +
                    " WHERE " + Constants.TANK_ID_1 + " = " + tanks.get(x).getTankId(), null);
            if (cursor.getCount() > 0)
                tanks.remove(x);
            else x++;
            cursor.close();
        }
        db.close();

        return tanks;
    }

    /*



            (Pump)




     */
    public long insertPump(PumpObject pump) {
        if (pump == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (pump.getPumpId() != 0.0d)
            values.put(Constants.PUMP_ID_1, pump.getPumpId());
        values.put(Constants.PUMP_LATITUDE, pump.getLatitude());
        values.put(Constants.PUMP_LONGITUDE, pump.getLongitude());
        values.put(Constants.PUMP_CODE, pump.getCode());
        values.put(Constants.PUMP_STATION_NAME, pump.getStationName());
        values.put(Constants.PUMP_ENABLED, pump.getEnabled());
        values.put(Constants.PUMP_TYPE, pump.getType());
        values.put(Constants.PUMP_ASSET_STATUS, pump.getAssetStatus());
        values.put(Constants.PUMP_STREET_NAME, pump.getStreetName());
        values.put(Constants.PUMP_DISTRICT_NAME, pump.getDistrictName());
        values.put(Constants.PUMP_SUB_DISTRICT, pump.getSubDistrict());
        values.put(Constants.PUMP_DMA_ZONE, pump.getDmaZone());
        values.put(Constants.PUMP_SECTOR_NAME, pump.getSectorName());
        values.put(Constants.PUMP_REMARKS, pump.getRemarks());
        values.put(Constants.PUMP_LAST_UPDATED, pump.getLastUpdated());
        values.put(Constants.PUMP_CREATED_AT, pump.getCreatedAt());

        long flag = db.insert(Constants.PUMP_TABLE_NAME, Constants.PUMP_ID_1, values);

        db.close();

        return flag;
    }

    public int updatePump(PumpObject pump) {
        if (pump == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Constants.PUMP_LATITUDE, pump.getLatitude());
        values.put(Constants.PUMP_LONGITUDE, pump.getLongitude());
        values.put(Constants.PUMP_CODE, pump.getCode());
        values.put(Constants.PUMP_STATION_NAME, pump.getStationName());
        values.put(Constants.PUMP_ENABLED, pump.getEnabled());
        values.put(Constants.PUMP_TYPE, pump.getType());
        values.put(Constants.PUMP_ASSET_STATUS, pump.getAssetStatus());
        values.put(Constants.PUMP_STREET_NAME, pump.getStreetName());
        values.put(Constants.PUMP_DISTRICT_NAME, pump.getDistrictName());
        values.put(Constants.PUMP_SUB_DISTRICT, pump.getSubDistrict());
        values.put(Constants.PUMP_DMA_ZONE, pump.getDmaZone());
        values.put(Constants.PUMP_SECTOR_NAME, pump.getSectorName());
        values.put(Constants.PUMP_REMARKS, pump.getRemarks());
        values.put(Constants.PUMP_LAST_UPDATED, pump.getLastUpdated());
        values.put(Constants.PUMP_CREATED_AT, pump.getCreatedAt());


        int flag = db.update(Constants.PUMP_TABLE_NAME, values, Constants.PUMP_ID + " = ?",
                new String[]{String.valueOf(pump.getId())});

        db.close();

        return flag;
    }

    public int deletePump(long id) {
        if (id < 1) return 0;

        SQLiteDatabase db = this.getWritableDatabase();

        int flag = db.delete(Constants.PUMP_TABLE_NAME, Constants.PUMP_ID + " = ?",
                new String[]{String.valueOf(id)});

        db.close();

        return flag;
    }

    public ArrayList<PumpObject> getAllPumps(int type) {
        ArrayList<PumpObject> pumps = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = type == 0 ? "" :
                type == 1 ? " WHERE " + Constants.PUMP_ID_1 + " IS NULL " :
                        " WHERE " + Constants.PUMP_ID_1 + " IS NOT NULL ";

        String selectQuery = "SELECT * FROM " + Constants.PUMP_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.PUMP_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {

                pumps.add(new PumpObject(
                        cursor.getLong(cursor.getColumnIndex(Constants.PUMP_ID)),
                        cursor.getLong(cursor.getColumnIndex(Constants.PUMP_ID_1)),
                        cursor.getString(cursor.getColumnIndex(Constants.PUMP_LATITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.PUMP_LONGITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.PUMP_CODE)),
                        cursor.getString(cursor.getColumnIndex(Constants.PUMP_STATION_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.PUMP_ENABLED)),
                        cursor.getInt(cursor.getColumnIndex(Constants.PUMP_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.PUMP_ASSET_STATUS)),
                        cursor.getString(cursor.getColumnIndex(Constants.PUMP_STREET_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.PUMP_DISTRICT_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.PUMP_SUB_DISTRICT)),
                        cursor.getString(cursor.getColumnIndex(Constants.PUMP_DMA_ZONE)),
                        cursor.getString(cursor.getColumnIndex(Constants.PUMP_SECTOR_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.PUMP_LAST_UPDATED)),
                        cursor.getString(cursor.getColumnIndex(Constants.PUMP_REMARKS)),
                        cursor.getString(cursor.getColumnIndex(Constants.PUMP_CREATED_AT))));

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return pumps;
    }

    public ArrayList<HashMap<String, String>> getPumpsLocation() {
        ArrayList<HashMap<String, String>> latLngs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = " WHERE " + Constants.PUMP_ID_1 + " IS NULL ";

        String selectQuery = "SELECT * FROM " + Constants.PUMP_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.PUMP_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                latLngs.add(new HashMap<String, String>() {
                    {
                        put("Latitude", cursor.getString(cursor.getColumnIndex(Constants.PUMP_LATITUDE)));
                        put("Longitude", cursor.getString(cursor.getColumnIndex(Constants.PUMP_LONGITUDE)));
                    }
                });
            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return latLngs;
    }

    public ArrayList<PumpObject> getNotExistingPumps(ArrayList<PumpObject> pumps) {
        SQLiteDatabase db = this.getWritableDatabase();

        int x = 0;
        Cursor cursor;
        while (x < pumps.size()) {
            cursor = db.rawQuery("SELECT * FROM " + Constants.PUMP_TABLE_NAME +
                    " WHERE " + Constants.PUMP_ID_1 + " = " + pumps.get(x).getPumpId(), null);
            if (cursor.getCount() > 0)
                pumps.remove(x);
            else x++;
            cursor.close();
        }
        db.close();

        return pumps;
    }

    /*



            Break




     */

    public long insertBreak(BreakObject breakObj) {
        if (breakObj == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (breakObj.getBreakId() != 0.0d)
            values.put(Constants.BREAK_ID_1, breakObj.getBreakId());
        values.put(Constants.BREAK_LATITUDE, breakObj.getLatitude());
        values.put(Constants.BREAK_LONGITUDE, breakObj.getLongitude());
        values.put(Constants.BREAK_BREAK_NUMBER, breakObj.getBreakNumber());
        values.put(Constants.BREAK_PIPE_TYPE, breakObj.getPipeType());
        values.put(Constants.BREAK_DISTRICT_NAME, breakObj.getDistrictName());
        values.put(Constants.BREAK_SUB_ZONE, breakObj.getSubZone());
        values.put(Constants.BREAK_BUILDING_NUMBER, breakObj.getBuildingNumber());
        values.put(Constants.BREAK_PIPE_NUMBER, breakObj.getPipeNumber());
        values.put(Constants.BREAK_STREET_NAME, breakObj.getStreetName());
        values.put(Constants.BREAK_BREAK_DATE, breakObj.getBreakDate());
        values.put(Constants.BREAK_DIAMETER, breakObj.getDiameter());
        values.put(Constants.BREAK_PIPE_MATERIAL, breakObj.getPipeMaterial());
        values.put(Constants.BREAK_BREAK_DETAILS, breakObj.getBreakDetails());
        values.put(Constants.BREAK_USED_EQUIPMENT, breakObj.getUsedEquipment());
        values.put(Constants.BREAK_MAINTENANCE_COMPANY, breakObj.getMaintenanceCompany());
        values.put(Constants.BREAK_MAINTENANCE_TYPE, breakObj.getMaintenanceType());
        values.put(Constants.BREAK_SOIL_TYPE, breakObj.getSoilType());
        values.put(Constants.BREAK_ASPHALT, breakObj.getAsphalt());
        values.put(Constants.BREAK_DIMENSION_DRILLING, breakObj.getDimensionDrilling());
        values.put(Constants.BREAK_MAINTENANCE_DEVICE_TYPE, breakObj.getMaintenanceDeviceType());
        values.put(Constants.BREAK_PROCEDURE, breakObj.getProcedure());
        if (breakObj.getPhotos() != null) {
            if (breakObj.getPhotos().containsKey("beforeImage") && breakObj.getPhotos().get("beforeImage") != null)
                values.put(Constants.BREAK_IMAGE_BEFORE, breakObj.getPhotos().get("beforeImage"));
            if (breakObj.getPhotos().containsKey("afterImage") && breakObj.getPhotos().get("afterImage") != null)
                values.put(Constants.BREAK_IMAGE_AFTER, breakObj.getPhotos().get("afterImage"));
        }
//        values.put(Constants.BREAK_IMAGE_BEFORE, breakObj.getPhotos().get("beforeImage"));
//        values.put(Constants.BREAK_IMAGE_AFTER, breakObj.getPhotos().get("afterImage"));
        values.put(Constants.BREAK_CREATED_AT, breakObj.getCreatedAt());


        long flag = db.insert(Constants.BREAK_TABLE_NAME, Constants.BREAK_ID_1, values);

        db.close();

        return flag;
    }

    public int updateBreak(BreakObject breakObj) {
        if (breakObj == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Constants.BREAK_LATITUDE, breakObj.getLatitude());
        values.put(Constants.BREAK_LONGITUDE, breakObj.getLongitude());
        values.put(Constants.BREAK_BREAK_NUMBER, breakObj.getBreakNumber());
        values.put(Constants.BREAK_PIPE_TYPE, breakObj.getPipeType());
        values.put(Constants.BREAK_DISTRICT_NAME, breakObj.getDistrictName());
        values.put(Constants.BREAK_SUB_ZONE, breakObj.getSubZone());
        values.put(Constants.BREAK_BUILDING_NUMBER, breakObj.getBuildingNumber());
        values.put(Constants.BREAK_PIPE_NUMBER, breakObj.getPipeNumber());
        values.put(Constants.BREAK_STREET_NAME, breakObj.getStreetName());
        values.put(Constants.BREAK_BREAK_DATE, breakObj.getBreakDate());
        values.put(Constants.BREAK_DIAMETER, breakObj.getDiameter());
        values.put(Constants.BREAK_PIPE_MATERIAL, breakObj.getPipeMaterial());
        values.put(Constants.BREAK_BREAK_DETAILS, breakObj.getBreakDetails());
        values.put(Constants.BREAK_USED_EQUIPMENT, breakObj.getUsedEquipment());
        values.put(Constants.BREAK_MAINTENANCE_COMPANY, breakObj.getMaintenanceCompany());
        values.put(Constants.BREAK_MAINTENANCE_TYPE, breakObj.getMaintenanceType());
        values.put(Constants.BREAK_SOIL_TYPE, breakObj.getSoilType());
        values.put(Constants.BREAK_ASPHALT, breakObj.getAsphalt());
        values.put(Constants.BREAK_DIMENSION_DRILLING, breakObj.getDimensionDrilling());
        values.put(Constants.BREAK_MAINTENANCE_DEVICE_TYPE, breakObj.getMaintenanceDeviceType());
        values.put(Constants.BREAK_PROCEDURE, breakObj.getProcedure());
        if (breakObj.getPhotos() != null) {
            if (breakObj.getPhotos().containsKey("beforeImage") && breakObj.getPhotos().get("beforeImage") != null)
                values.put(Constants.BREAK_IMAGE_BEFORE, breakObj.getPhotos().get("beforeImage"));
            if (breakObj.getPhotos().containsKey("afterImage") && breakObj.getPhotos().get("afterImage") != null)
                values.put(Constants.BREAK_IMAGE_AFTER, breakObj.getPhotos().get("afterImage"));
        }
//        values.put(Constants.BREAK_IMAGE_BEFORE, breakObj.getPhotos().get("beforeImage"));
//        values.put(Constants.BREAK_IMAGE_AFTER, breakObj.getPhotos().get("afterImage"));
        values.put(Constants.BREAK_CREATED_AT, breakObj.getCreatedAt());


        int flag = db.update(Constants.BREAK_TABLE_NAME, values, Constants.BREAK_ID + " = ?",
                new String[]{String.valueOf(breakObj.getId())});

        db.close();

        return flag;
    }

    public int deleteBreak(long id) {
        if (id < 1) return 0;

        SQLiteDatabase db = this.getWritableDatabase();

        int flag = db.delete(Constants.BREAK_TABLE_NAME, Constants.BREAK_ID + " = ?",
                new String[]{String.valueOf(id)});

        db.close();

        return flag;
    }

    public ArrayList<BreakObject> getAllBreaks(int type) {
        ArrayList<BreakObject> breaks = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = type == 0 ? "" :
                type == 1 ? " WHERE " + Constants.BREAK_ID_1 + " IS NULL " :
                        " WHERE " + Constants.BREAK_ID_1 + " IS NOT NULL ";

        String selectQuery = "SELECT * FROM " + Constants.BREAK_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.BREAK_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);

        Map<String, String> photos;

        if (cursor.moveToFirst()) {
            do {
                photos = new LinkedHashMap<>();
                photos.put("beforeImage", cursor.getString(cursor.getColumnIndex(Constants.BREAK_IMAGE_BEFORE)));
                photos.put("afterImage", cursor.getString(cursor.getColumnIndex(Constants.BREAK_IMAGE_AFTER)));
                breaks.add(new BreakObject(
                        cursor.getLong(cursor.getColumnIndex(Constants.BREAK_ID)),
                        cursor.getLong(cursor.getColumnIndex(Constants.BREAK_ID_1)),
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_LATITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_LONGITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_BREAK_NUMBER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_PIPE_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_DISTRICT_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_SUB_ZONE)),
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_BUILDING_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_PIPE_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_STREET_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_BREAK_DATE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_PIPE_MATERIAL)),
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_BREAK_DETAILS)),
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_USED_EQUIPMENT)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_MAINTENANCE_COMPANY)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_MAINTENANCE_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_SOIL_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_ASPHALT)),
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_DIMENSION_DRILLING)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_MAINTENANCE_DEVICE_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.BREAK_PROCEDURE)),
                        photos,
                        cursor.getString(cursor.getColumnIndex(Constants.BREAK_CREATED_AT))));

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return breaks;
    }

    public ArrayList<HashMap<String, String>> getBreaksLocation() {
        ArrayList<HashMap<String, String>> latLngs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = " WHERE " + Constants.BREAK_ID_1 + " IS NULL ";

        String selectQuery = "SELECT * FROM " + Constants.BREAK_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.BREAK_ID + " ASC";


        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                latLngs.add(new HashMap<String, String>() {
                    {
                        put("Latitude", cursor.getString(cursor.getColumnIndex(Constants.BREAK_LATITUDE)));
                        put("Longitude", cursor.getString(cursor.getColumnIndex(Constants.BREAK_LONGITUDE)));
                    }
                });
            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return latLngs;
    }

    public ArrayList<BreakObject> getNotExistingBreaks(ArrayList<BreakObject> breaks) {
        SQLiteDatabase db = this.getWritableDatabase();

        int x = 0;
        Cursor cursor;
        while (x < breaks.size()) {
            cursor = db.rawQuery("SELECT * FROM " + Constants.BREAK_TABLE_NAME +
                    " WHERE " + Constants.BREAK_ID_1 + " = " + breaks.get(x).getBreakId(), null);
            if (cursor.getCount() > 0)
                breaks.remove(x);
            else x++;
            cursor.close();
        }
        db.close();

        return breaks;
    }

    /*



            Break




     */

    public long insertSensor(SensorObject sensor) {
        if (sensor == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (sensor.getSensorId() != 0.0d)
            values.put(Constants.SENSOR_POINT_ID_1, sensor.getSensorId());
        values.put(Constants.SENSOR_POINT_LATITUDE, sensor.getLatitude());
        values.put(Constants.SENSOR_POINT_LONGITUDE, sensor.getLongitude());
        values.put(Constants.SENSOR_POINT_SERIAL_NUMBER, sensor.getSerialNumber());
        values.put(Constants.SENSOR_POINT_ASSET_NUMBER, sensor.getAssetNumber());
        values.put(Constants.SENSOR_POINT_DEVICE_TYPE, sensor.getDeviceType());
        values.put(Constants.SENSOR_POINT_ASSET_STATUS, sensor.getAssetStatus());
        values.put(Constants.SENSOR_POINT_ELEVATION, sensor.getElevation());
        values.put(Constants.SENSOR_POINT_CHAMBER_STATUS, sensor.getChamberStatus());
        values.put(Constants.SENSOR_POINT_TYPE, sensor.getType());
        values.put(Constants.SENSOR_POINT_LINE_NUMBER, sensor.getLineNumber());
        values.put(Constants.SENSOR_POINT_MAINTENANCE_AREA, sensor.getMaintenanceArea());
        values.put(Constants.SENSOR_POINT_COMMISSION_DATE, sensor.getCommissionDate());
        values.put(Constants.SENSOR_POINT_ENABLED, sensor.getEnabled());
        values.put(Constants.SENSOR_POINT_DISTRICT_NAME, sensor.getDistrictName());
        values.put(Constants.SENSOR_POINT_STREET_NAME, sensor.getStreetName());
        values.put(Constants.SENSOR_POINT_DMA_ZONE, sensor.getDmaZone());
        values.put(Constants.SENSOR_POINT_SECTOR_NAME, sensor.getSectorName());
        values.put(Constants.SENSOR_POINT_DIAMETER, sensor.getDiameter());
        values.put(Constants.SENSOR_POINT_PIPE_MATERIAL, sensor.getPipeMaterial());
        values.put(Constants.SENSOR_POINT_LAST_UPDATE, sensor.getLastUpdate());
        values.put(Constants.SENSOR_POINT_CREATED_AT, sensor.getCreatedAt());


        long flag = db.insert(Constants.SENSOR_POINT_TABLE_NAME, Constants.SENSOR_POINT_ID_1, values);

        db.close();

        return flag;
    }

    public int updateSensor(SensorObject sensor) {
        if (sensor == null) return -1;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Constants.SENSOR_POINT_LATITUDE, sensor.getLatitude());
        values.put(Constants.SENSOR_POINT_LONGITUDE, sensor.getLongitude());
        values.put(Constants.SENSOR_POINT_SERIAL_NUMBER, sensor.getSerialNumber());
        values.put(Constants.SENSOR_POINT_ASSET_NUMBER, sensor.getAssetNumber());
        values.put(Constants.SENSOR_POINT_DEVICE_TYPE, sensor.getDeviceType());
        values.put(Constants.SENSOR_POINT_ASSET_STATUS, sensor.getAssetStatus());
        values.put(Constants.SENSOR_POINT_ELEVATION, sensor.getElevation());
        values.put(Constants.SENSOR_POINT_CHAMBER_STATUS, sensor.getChamberStatus());
        values.put(Constants.SENSOR_POINT_TYPE, sensor.getType());
        values.put(Constants.SENSOR_POINT_LINE_NUMBER, sensor.getLineNumber());
        values.put(Constants.SENSOR_POINT_MAINTENANCE_AREA, sensor.getMaintenanceArea());
        values.put(Constants.SENSOR_POINT_COMMISSION_DATE, sensor.getCommissionDate());
        values.put(Constants.SENSOR_POINT_ENABLED, sensor.getEnabled());
        values.put(Constants.SENSOR_POINT_DISTRICT_NAME, sensor.getDistrictName());
        values.put(Constants.SENSOR_POINT_STREET_NAME, sensor.getStreetName());
        values.put(Constants.SENSOR_POINT_DMA_ZONE, sensor.getDmaZone());
        values.put(Constants.SENSOR_POINT_SECTOR_NAME, sensor.getSectorName());
        values.put(Constants.SENSOR_POINT_DIAMETER, sensor.getDiameter());
        values.put(Constants.SENSOR_POINT_PIPE_MATERIAL, sensor.getPipeMaterial());
        values.put(Constants.SENSOR_POINT_LAST_UPDATE, sensor.getLastUpdate());
        values.put(Constants.SENSOR_POINT_CREATED_AT, sensor.getCreatedAt());


        int flag = db.update(Constants.SENSOR_POINT_TABLE_NAME, values, Constants.SENSOR_POINT_ID + " = ?",
                new String[]{String.valueOf(sensor.getId())});

        db.close();

        return flag;
    }

    public int deleteSensor(long id) {
        if (id < 1) return 0;

        SQLiteDatabase db = this.getWritableDatabase();

        int flag = db.delete(Constants.SENSOR_POINT_TABLE_NAME, Constants.SENSOR_POINT_ID + " = ?",
                new String[]{String.valueOf(id)});

        db.close();

        return flag;
    }

    public ArrayList<SensorObject> getAllSensors(int type) {
        ArrayList<SensorObject> sensors = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = type == 0 ? "" :
                type == 1 ? " WHERE " + Constants.SENSOR_POINT_ID_1 + " IS NULL " :
                        " WHERE " + Constants.SENSOR_POINT_ID_1 + " IS NOT NULL ";

        String selectQuery = "SELECT * FROM " + Constants.SENSOR_POINT_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.SENSOR_POINT_ID + " ASC";

//        Log.e("select", "select: " + selectQuery);

        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                sensors.add(new SensorObject(
                        cursor.getLong(cursor.getColumnIndex(Constants.SENSOR_POINT_ID)),
                        cursor.getLong(cursor.getColumnIndex(Constants.SENSOR_POINT_ID_1)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_LATITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_LONGITUDE)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_SERIAL_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_ASSET_NUMBER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.SENSOR_POINT_DEVICE_TYPE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.SENSOR_POINT_ASSET_STATUS)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_ELEVATION)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_CHAMBER_STATUS)),
                        cursor.getInt(cursor.getColumnIndex(Constants.SENSOR_POINT_TYPE)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_LINE_NUMBER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.SENSOR_POINT_MAINTENANCE_AREA)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_COMMISSION_DATE)),
                        cursor.getInt(cursor.getColumnIndex(Constants.SENSOR_POINT_ENABLED)),
                        cursor.getInt(cursor.getColumnIndex(Constants.SENSOR_POINT_DISTRICT_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_STREET_NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_DMA_ZONE)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_SECTOR_NAME)),
                        cursor.getInt(cursor.getColumnIndex(Constants.SENSOR_POINT_DIAMETER)),
                        cursor.getInt(cursor.getColumnIndex(Constants.SENSOR_POINT_PIPE_MATERIAL)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_LAST_UPDATE)),
                        cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_CREATED_AT))));

            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return sensors;
    }

    public ArrayList<HashMap<String, String>> getSensorsLocation() {
        ArrayList<HashMap<String, String>> latLngs = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        String whereStatement = " WHERE " + Constants.SENSOR_POINT_ID_1 + " IS NULL ";

        String selectQuery = "SELECT * FROM " + Constants.SENSOR_POINT_TABLE_NAME +
                whereStatement +
                " ORDER BY " +
                Constants.SENSOR_POINT_ID + " ASC";


        final Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                latLngs.add(new HashMap<String, String>() {
                    {
                        put("Latitude", cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_LATITUDE)));
                        put("Longitude", cursor.getString(cursor.getColumnIndex(Constants.SENSOR_POINT_LONGITUDE)));
                    }
                });
            } while (cursor.moveToNext());
        }

        cursor.close();

        db.close();

        return latLngs;
    }

    public ArrayList<SensorObject> getNotExistingSensors(ArrayList<SensorObject> sensors) {
        SQLiteDatabase db = this.getWritableDatabase();

        int x = 0;
        Cursor cursor;
        while (x < sensors.size()) {
            cursor = db.rawQuery("SELECT * FROM " + Constants.SENSOR_POINT_TABLE_NAME +
                    " WHERE " + Constants.SENSOR_POINT_ID_1 + " = " + sensors.get(x).getSensorId(), null);
            if (cursor.getCount() > 0)
                sensors.remove(x);
            else x++;
            cursor.close();
        }
        db.close();

        return sensors;
    }




































    /*














































     */

    public boolean insertGeoMasters(ArrayList<GeoMasterObj> geoMastersList) {
        if (geoMastersList == null) return false;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = null;
        int count = 0;
        long flag;
        for (int i = 0; i < geoMastersList.size(); i++) {
            values = new ContentValues();
            values.put(Constants.GEO_MASTERS_ID_1, geoMastersList.get(i).getGeoMasterId());
            values.put(Constants.GEO_MASTERS_AR_NAME, geoMastersList.get(i).getArName());
            values.put(Constants.GEO_MASTERS_EN_NAME, geoMastersList.get(i).getEnName());
            values.put(Constants.GEO_MASTERS_TYPE, geoMastersList.get(i).getType());
            flag = db.insert(Constants.GEO_MASTERS_TABLE_NAME, null, values);
            if (flag > 0)
                count++;
        }


        db.close();

        return count == geoMastersList.size();
    }

    public ArrayList<GeoMasterObj> getGeoMastersByType(int[] types, String lang) {
        ArrayList<GeoMasterObj> geoMastersList = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        String whereQuery = Constants.GEO_MASTERS_TYPE + " = " + types[0];
        for (int i = 1; i < types.length; i++) {
            whereQuery += " OR " + Constants.GEO_MASTERS_TYPE + " = " + types[i];
        }

        String selectQuery = "SELECT " + (lang.equals("en") ? Constants.GEO_MASTERS_EN_NAME : Constants.GEO_MASTERS_AR_NAME) + " , " +
                Constants.GEO_MASTERS_TYPE + " , " +
                Constants.GEO_MASTERS_ID_1 + " FROM " +
                Constants.GEO_MASTERS_TABLE_NAME + " WHERE " + whereQuery + " ORDER BY " +
                Constants.GEO_MASTERS_ID + " ASC";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                geoMastersList.add(lang.equals("en") ?
                        new GeoMasterObj(cursor.getInt(cursor.getColumnIndex(Constants.GEO_MASTERS_ID_1)), cursor.getString(cursor.getColumnIndex(Constants.GEO_MASTERS_EN_NAME)), cursor.getInt(cursor.getColumnIndex(Constants.GEO_MASTERS_TYPE))) :
                        new GeoMasterObj(cursor.getInt(cursor.getColumnIndex(Constants.GEO_MASTERS_ID_1)), cursor.getString(cursor.getColumnIndex(Constants.GEO_MASTERS_AR_NAME)), cursor.getInt(cursor.getColumnIndex(Constants.GEO_MASTERS_TYPE))));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return geoMastersList;
    }

    public ArrayList<String> returnConvertedGeoMastersIds(ArrayList<Integer> ids, ArrayList<Integer> type, String lang) {
        ArrayList<String> convertedIds = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();

        String whereQuery = "(" + Constants.GEO_MASTERS_ID_1 + " = " + ids.get(0) + " AND " + Constants.GEO_MASTERS_TYPE + " = " + type.get(0) + ")";
        for (int i = 1; i < ids.size(); i++) {
            whereQuery += " OR (" + Constants.GEO_MASTERS_ID_1 + " = " + ids.get(i) + " AND " + Constants.GEO_MASTERS_TYPE + " = " + type.get(i) + ")";
        }

        String selectQuery = "SELECT " + (lang.equals("en") ? Constants.GEO_MASTERS_EN_NAME : Constants.METER_ARABIC_NAME) + " , " +
                Constants.GEO_MASTERS_TYPE + " FROM " +
                Constants.GEO_MASTERS_TABLE_NAME + " WHERE " + whereQuery + " ORDER BY " +
                Constants.GEO_MASTERS_TYPE + " ASC ";


        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                convertedIds.add((lang.equals("en") ? cursor.getString(cursor.getColumnIndex(Constants.GEO_MASTERS_EN_NAME)) :
                        cursor.getString(cursor.getColumnIndex(Constants.GEO_MASTERS_AR_NAME))));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return convertedIds;
    }

    public ArrayList<String> returnConvertedGeoMastersIds1(ArrayList<Integer> ids, ArrayList<Integer> type, String lang) {
        ArrayList<String> convertedIds = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();


        Cursor cursor = null;
        String selectQuery = null;
        for (int i = 0; i < ids.size(); i++) {
            selectQuery = "SELECT " + (lang.equals("en") ? Constants.GEO_MASTERS_EN_NAME : Constants.METER_ARABIC_NAME) + " , " +
                    Constants.GEO_MASTERS_TYPE + " FROM " +
                    Constants.GEO_MASTERS_TABLE_NAME + " WHERE " +
                    "(" + Constants.GEO_MASTERS_ID_1 + " = " + ids.get(i) + " AND " + Constants.GEO_MASTERS_TYPE + " = " + type.get(i) + ")"
                    + " ORDER BY " +
                    Constants.GEO_MASTERS_TYPE + " ASC ";
            cursor = db.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    convertedIds.add((lang.equals("en") ? cursor.getString(cursor.getColumnIndex(Constants.GEO_MASTERS_EN_NAME)) :
                            cursor.getString(cursor.getColumnIndex(Constants.GEO_MASTERS_AR_NAME))));
                } while (cursor.moveToNext());
            } else {
                convertedIds.add("");
            }
            cursor.close();
        }

        db.close();

        return convertedIds;
    }

    public int deleteAllGeoMasters() {
        SQLiteDatabase db = this.getWritableDatabase();
        int flag = db.delete(Constants.GEO_MASTERS_TABLE_NAME, null, null);
        db.close();

        return flag;
    }


}
