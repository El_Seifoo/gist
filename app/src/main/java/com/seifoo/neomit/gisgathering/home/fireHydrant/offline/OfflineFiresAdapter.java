package com.seifoo.neomit.gisgathering.home.fireHydrant.offline;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.fireHydrant.rejected.RejectedFireHydrantActivity;

import java.util.ArrayList;

public class OfflineFiresAdapter extends RecyclerView.Adapter<OfflineFiresAdapter.Holder> {
    private ArrayList<FireHydrantObject> list;
    private final MeterObjectListItemListener listItemListener;
    private boolean isOffline;

    public OfflineFiresAdapter(MeterObjectListItemListener listItemListener, boolean isOffline) {
        this.listItemListener = listItemListener;
        this.isOffline = isOffline;
    }

    public void setList(ArrayList<FireHydrantObject> list) {
        this.list = list;
    }

    public interface MeterObjectListItemListener {
        void onListItemClickListener(int viewId, int position, FireHydrantObject fireHydrantObject);
    }

    public void removeItem(Context context, int position) {
        list.remove(position);
        if (list.isEmpty()) {
            if (context instanceof OfflineFireHydrantActivity)
                ((OfflineFireHydrantActivity) context).showEmptyListText();
            else if (context instanceof RejectedFireHydrantActivity)
                ((RejectedFireHydrantActivity) context).showEmptyListText();
        }
        notifyDataSetChanged();
    }

    public void updateItem(int position, FireHydrantObject editedFireObj) {
        list.set(position, editedFireObj);
        notifyDataSetChanged();
    }

    public void updateItem(Context context, long id) {
        for (int i = 0; i < list.size(); i++) {
            if (id == list.get(i).getId()) {
                list.remove(i);
                if (list.isEmpty()) {
                    if (context instanceof OfflineFireHydrantActivity)
                        ((OfflineFireHydrantActivity) context).showEmptyListText();
                    else if (context instanceof RejectedFireHydrantActivity)
                        ((RejectedFireHydrantActivity) context).showEmptyListText();
                }
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void clear() {
        if (list != null) {
            list.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public OfflineFiresAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offline_fire_list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OfflineFiresAdapter.Holder holder, int position) {
        if (isOffline) {
            holder.removeItem.setVisibility(View.VISIBLE);
            holder.editItem.setVisibility(View.VISIBLE);
        } else {
            if (list.get(position).getReason() != null) {
                if (!list.get(position).getReason().isEmpty()) {
                    holder.reason.setVisibility(View.VISIBLE);
                    holder.reason.setText(
                            Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.reason) + ": </b></font>" + list.get(position).getReason()),
                            TextView.BufferType.SPANNABLE);
                } else holder.reason.setVisibility(View.GONE);
            } else holder.reason.setVisibility(View.GONE);


        }


        if (list.get(position).getFireLatitude() != null && list.get(position).getFireLongitude() != null) {
            if (!list.get(position).getFireLatitude().isEmpty() && !list.get(position).getFireLongitude().isEmpty()) {
                holder.map.setVisibility(View.VISIBLE);
            } else holder.map.setVisibility(View.GONE);
        } else holder.map.setVisibility(View.GONE);

        if (list.get(position).getFireSerialNumber() != null) {
            if (!list.get(position).getFireSerialNumber().isEmpty()) {
                holder.serialNumber.setVisibility(View.VISIBLE);
                holder.serialNumber.setText(
                        Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.serial_number) + ": </b></font>" + list.get(position).getFireSerialNumber()),
                        TextView.BufferType.SPANNABLE);

            } else holder.serialNumber.setVisibility(View.GONE);
        } else holder.serialNumber.setVisibility(View.GONE);


        if (list.get(position).getFireNumber() != null) {
            if (!list.get(position).getFireNumber().isEmpty()) {
                holder.FHNumber.setVisibility(View.VISIBLE);
                holder.FHNumber.setText(
                        Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.fh_number) + ": </b></font>" + list.get(position).getFireNumber()),
                        TextView.BufferType.SPANNABLE);

            } else holder.FHNumber.setVisibility(View.GONE);
        } else holder.FHNumber.setVisibility(View.GONE);

        if (!list.get(position).getCreatedAt().isEmpty()) {
            holder.createdAt.setVisibility(View.VISIBLE);
            holder.createdAt.setText(
                    Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.created_at) + ": </b></font>" + list.get(position).getCreatedAt()),
                    TextView.BufferType.SPANNABLE);

        } else holder.createdAt.setVisibility(View.GONE);


    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView serialNumber, FHNumber, reason, moreDetails, map, createdAt;
        ImageView removeItem, editItem;

        public Holder(@NonNull View itemView) {
            super(itemView);
            serialNumber = (TextView) itemView.findViewById(R.id.serial_number);

            FHNumber = (TextView) itemView.findViewById(R.id.fh_number);

            moreDetails = (TextView) itemView.findViewById(R.id.more_details);
            createdAt = (TextView) itemView.findViewById(R.id.created_at);
            map = (TextView) itemView.findViewById(R.id.map);

            moreDetails.setOnClickListener(this);
            map.setOnClickListener(this);

            if (isOffline) {
                removeItem = (ImageView) itemView.findViewById(R.id.remove_item);
                editItem = (ImageView) itemView.findViewById(R.id.edit_item);
                removeItem.setOnClickListener(this);
                editItem.setOnClickListener(this);
            } else {
                reason = (TextView) itemView.findViewById(R.id.reason);
            }
        }

        @Override
        public void onClick(View view) {
            listItemListener.onListItemClickListener(view.getId(), getAdapterPosition(), list.get(getAdapterPosition()));
        }
    }
}
