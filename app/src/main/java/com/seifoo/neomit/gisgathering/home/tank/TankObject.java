package com.seifoo.neomit.gisgathering.home.tank;

import java.io.Serializable;

public class TankObject implements Serializable {
    private long id, tankId;
    private String latitude, longitude, serialNumber, tankName;
    private int type, enabled;
    private String commissionDate;
    private int tankDiameter;
    private String activeVolume, inActiveVolume, lowLevel, highLevel;
    private int assetStatus;
    private String streetName, region;
    private int districtName;
    private String remarks, sectorName, lastUpdated, reason, createdAt;


    public TankObject(String latitude, String longitude, String serialNumber, String tankName,
                      int type, int enabled, String commissionDate, int tankDiameter, String activeVolume) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.tankName = tankName;
        this.type = type;
        this.enabled = enabled;
        this.commissionDate = commissionDate;
        this.tankDiameter = tankDiameter;
        this.activeVolume = activeVolume;
    }

    public TankObject getFirstObject() {
        return new TankObject(latitude, longitude, serialNumber, tankName,
                type, enabled, commissionDate, tankDiameter, activeVolume);
    }

    public TankObject(String inActiveVolume, String lowLevel, String highLevel, int assetStatus,
                      String streetName, int districtName, String sectorName, String region, String remarks, String lastUpdated) {
        this.inActiveVolume = inActiveVolume;
        this.lowLevel = lowLevel;
        this.highLevel = highLevel;
        this.assetStatus = assetStatus;
        this.streetName = streetName;
        this.districtName = districtName;
        this.sectorName = sectorName;
        this.region = region;
        this.remarks = remarks;
        this.lastUpdated = lastUpdated;
    }

    public TankObject getSecondObject() {
        return new TankObject(inActiveVolume, lowLevel, highLevel, assetStatus,
                streetName, districtName, sectorName, region, remarks, lastUpdated);
    }

    public TankObject(TankObject firstObj, TankObject secondObj) {
        this.latitude = firstObj.getLatitude();
        this.longitude = firstObj.getLongitude();
        this.serialNumber = firstObj.getSerialNumber();
        this.tankName = firstObj.getTankName();
        this.type = firstObj.getType();
        this.enabled = firstObj.getEnabled();
        this.commissionDate = firstObj.getCommissionDate();
        this.tankDiameter = firstObj.getTankDiameter();
        this.activeVolume = firstObj.getActiveVolume();

        this.inActiveVolume = secondObj.getInActiveVolume();
        this.lowLevel = secondObj.getLowLevel();
        this.highLevel = secondObj.getHighLevel();
        this.assetStatus = secondObj.getAssetStatus();
        this.streetName = secondObj.getStreetName();
        this.districtName = secondObj.getDistrictName();
        this.sectorName = secondObj.getSectorName();
        this.region = secondObj.getRegion();
        this.remarks = secondObj.getRemarks();
        this.lastUpdated = secondObj.getLastUpdated();


    }

    public TankObject(long id, long tankId, String latitude, String longitude, String serialNumber,
                      String tankName, int type, int enabled, String commissionDate, int tankDiameter,
                      String activeVolume, String inActiveVolume, String lowLevel, String highLevel,
                      int assetStatus, String streetName, int districtName, String sectorName, String region, String remarks, String lastUpdated, String createdAt) {
        this.id = id;
        this.tankId = tankId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.tankName = tankName;
        this.type = type;
        this.enabled = enabled;
        this.commissionDate = commissionDate;
        this.tankDiameter = tankDiameter;
        this.activeVolume = activeVolume;
        this.inActiveVolume = inActiveVolume;
        this.lowLevel = lowLevel;
        this.highLevel = highLevel;
        this.assetStatus = assetStatus;
        this.streetName = streetName;
        this.districtName = districtName;
        this.sectorName = sectorName;
        this.region = region;
        this.remarks = remarks;
        this.lastUpdated = lastUpdated;
        this.createdAt = createdAt;
    }

    public TankObject(String latitude, String longitude, String serialNumber, String tankName, int type,
                      int enabled, String commissionDate, int tankDiameter, String activeVolume, String inActiveVolume,
                      String lowLevel, String highLevel, int assetStatus, String streetName, int districtName, String sectorName, String region,
                      String remarks, String lastUpdated, String createdAt) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.tankName = tankName;
        this.type = type;
        this.enabled = enabled;
        this.commissionDate = commissionDate;
        this.tankDiameter = tankDiameter;
        this.activeVolume = activeVolume;
        this.inActiveVolume = inActiveVolume;
        this.lowLevel = lowLevel;
        this.highLevel = highLevel;
        this.assetStatus = assetStatus;
        this.streetName = streetName;
        this.districtName = districtName;
        this.sectorName = sectorName;
        this.region = region;
        this.remarks = remarks;
        this.lastUpdated = lastUpdated;
        this.createdAt = createdAt;
    }

    public TankObject(long tankId, String latitude, String longitude, String serialNumber, String tankName, int type,
                      int enabled, String commissionDate, int tankDiameter, String activeVolume, String inActiveVolume,
                      String lowLevel, String highLevel, int assetStatus, String streetName, int districtName, String sectorName, String region,
                      String remarks, String lastUpdated, String reason, String createdAt) {
        this.tankId = tankId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.serialNumber = serialNumber;
        this.tankName = tankName;
        this.type = type;
        this.enabled = enabled;
        this.commissionDate = commissionDate;
        this.tankDiameter = tankDiameter;
        this.activeVolume = activeVolume;
        this.inActiveVolume = inActiveVolume;
        this.lowLevel = lowLevel;
        this.highLevel = highLevel;
        this.assetStatus = assetStatus;
        this.streetName = streetName;
        this.districtName = districtName;
        this.sectorName = sectorName;
        this.region = region;
        this.remarks = remarks;
        this.lastUpdated = lastUpdated;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public long getTankId() {
        return tankId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getTankName() {
        return tankName;
    }

    public int getType() {
        return type;
    }

    public int getEnabled() {
        return enabled;
    }

    public String getCommissionDate() {
        return commissionDate;
    }

    public int getTankDiameter() {
        return tankDiameter;
    }

    public String getActiveVolume() {
        return activeVolume;
    }

    public String getInActiveVolume() {
        return inActiveVolume;
    }

    public String getLowLevel() {
        return lowLevel;
    }

    public String getHighLevel() {
        return highLevel;
    }

    public int getAssetStatus() {
        return assetStatus;
    }

    public String getStreetName() {
        return streetName;
    }

    public int getDistrictName() {
        return districtName;
    }

    public String getSectorName() {
        return sectorName;
    }

    public String getRegion() {
        return region;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getReason() {
        return reason;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTankId(long tankId) {
        this.tankId = tankId;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
