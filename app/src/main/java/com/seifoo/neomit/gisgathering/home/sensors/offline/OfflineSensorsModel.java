package com.seifoo.neomit.gisgathering.home.sensors.offline;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;
import com.seifoo.neomit.gisgathering.utils.Urls;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class OfflineSensorsModel {
    protected void uploadSensorsData(final Context context, final VolleyCallback callback, final ArrayList<SensorObject> sensors) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSyncingResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("parsing error ", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onSyncingError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""));
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            /*
            son[data][0][Corrected_Id]:
Json[data][0][X_MAP]:42.125165
Json[data][0][Y_MAP]:24.1654
Json[data][0][SERIAL_NO]:1
Json[data][0][ASSET_NO]:1
Json[data][0][DEVICE_TYPE_ID]:1
Json[data][0][ASSET_STATUS_ID]:1
Json[data][0][ELEVATION]:1
Json[data][0][CHAMBER_STATUS]:1
Json[data][0][F_TYPE_ID]:1
Json[data][0][LINE_NO]:1
Json[data][0][MAINTENANCE_AREA_ID]:1
Json[data][0][COMMISSION_DATE]:31-03-2019 22:20
Json[data][0][ENABLED_ID]:1
Json[data][0][DISTRICTS_NAME]:abc
Json[data][0][STREET_NAME]:abc
Json[data][0][DMA_ZONE_ID]:1
Json[data][0][SECTOR_NAME]:abc
Json[data][0][DIAMATER_ID]:1
Json[data][0][PIPE_MATERIAL_ID]:1
Json[data][0][LAST_UPDATED]:31-03-2019 22:20
Json[data][0][RECORD_ID]:1
Json[tableName]:sensorpoint
Json[databaseName]:RE_AKharj
             */
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                for (int i = 0; i < sensors.size(); i++) {
                    if (sensors.get(i).getSensorId() != 0.0d)
                        params.put("Json[data][" + i + "][Corrected_Id]", sensors.get(i).getSensorId() + "");
                    params.put("Json[data][" + i + "][RECORD_ID]", sensors.get(i).getId() + "");
                    params.put("Json[data][" + i + "][SERIAL_NO]", sensors.get(i).getSerialNumber());
                    params.put("Json[data][" + i + "][ASSET_NO]", sensors.get(i).getAssetNumber());
                    params.put("Json[data][" + i + "][DEVICE_TYPE_ID]", sensors.get(i).getDeviceType() + "");
                    params.put("Json[data][" + i + "][ASSET_STATUS_ID]", sensors.get(i).getAssetStatus() + "");
                    params.put("Json[data][" + i + "][ELEVATION]", sensors.get(i).getElevation());
                    params.put("Json[data][" + i + "][CHAMBER_STATUS]", sensors.get(i).getChamberStatus());
                    params.put("Json[data][" + i + "][F_TYPE_ID]", sensors.get(i).getType() + "");
                    params.put("Json[data][" + i + "][LINE_NO]", sensors.get(i).getLineNumber());
                    params.put("Json[data][" + i + "][MAINTENANCE_AREA_ID]", sensors.get(i).getMaintenanceArea() + "");
                    params.put("Json[data][" + i + "][COMMISSION_DATE]", sensors.get(i).getCommissionDate());
                    params.put("Json[data][" + i + "][ENABLED_ID]", sensors.get(i).getEnabled() + "");
                    params.put("Json[data][" + i + "][DISTRICTS_NAME]", sensors.get(i).getDistrictName() + "");
                    params.put("Json[data][" + i + "][STREET_NAME]", sensors.get(i).getStreetName());
                    params.put("Json[data][" + i + "][DMA_ZONE_ID]", sensors.get(i).getDmaZone());
                    params.put("Json[data][" + i + "][SECTOR_NAME]", sensors.get(i).getSectorName());
                    params.put("Json[data][" + i + "][DIAMATER_ID]", sensors.get(i).getDiameter() + "");
                    params.put("Json[data][" + i + "][PIPE_MATERIAL_ID]", sensors.get(i).getPipeMaterial() + "");
                    params.put("Json[data][" + i + "][LAST_UPDATED]", sensors.get(i).getLastUpdate());
                    params.put("Json[data][" + i + "][CreatedDate]", sensors.get(i).getCreatedAt());// created date ....
                    params.put("Json[data][" + i + "][X_MAP]", !sensors.get(i).getLongitude().isEmpty() ? sensors.get(i).getLongitude() : "360");
                    params.put("Json[data][" + i + "][Y_MAP]", !sensors.get(i).getLatitude().isEmpty() ? sensors.get(i).getLatitude() : "360");
                }
                params.put("Json[tableName]", context.getString(R.string.api_sensor_table_name));
                params.put("Json[databaseName]", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""));
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected void getSensorsList(Context context, DBCallback callback, int type, int index) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetSensorsListCalled(DataBaseHelper.getmInstance(context).getAllSensors(type), index);
    }

    protected void removeSensorObject(Context context, DBCallback callback, long id, int position) {
        callback.onRemoveSensorObjectCalled(DataBaseHelper.getmInstance(context).deleteSensor(id), position, id);
    }

    protected interface VolleyCallback {
        void onSyncingResponse(String response) throws JSONException;

        void onSyncingError(VolleyError error);
    }

    protected interface DBCallback {

        void onGetSensorsListCalled(ArrayList<SensorObject> sensors, int index);

        void onRemoveSensorObjectCalled(int flag, int position, long id);
    }
}
