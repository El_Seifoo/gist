package com.seifoo.neomit.gisgathering.home.chamber.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class OfflineChambersDetailsModel {
    public void returnValidGeoMaster(Context context, DBCallback callback, ChamberObject chamber, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds1(ids, types, lang), chamber);
    }

    protected interface DBCallback {
        void onConvertingIdsCalled(ArrayList<String> strings, ChamberObject chamber);
    }
}
