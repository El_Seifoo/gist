package com.seifoo.neomit.gisgathering.home.tank.offline;

import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.home.tank.edit.EditTankActivity;
import com.seifoo.neomit.gisgathering.home.tank.offline.details.OfflineTanksDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class OfflineTanksPresenter implements OfflineTanksMVP.Presenter, OfflineTanksModel.VolleyCallback, OfflineTanksModel.DBCallback {
    private OfflineTanksMVP.View view;
    private OfflineTanksModel model;

    public OfflineTanksPresenter(OfflineTanksMVP.View view, OfflineTanksModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestTanksData(int type) {
        model.getTanksList(view.getAppContext(), this, type, 0);
    }

    @Override
    public void requestRemoveTankObjById(long id, int position) {
        model.removeTankObject(view.getActContext(), this, id, position);
    }

    private static final int SHOW_ALL_IN_MAP_REQUEST = 10;

    @Override
    public void OnListItemClickListener(int viewId, int position, TankObject tank) {
        switch (viewId) {
            case R.id.more_details:
                view.navigateDestination("OfflineTankObj", tank, OfflineTanksDetailsActivity.class);
                break;
            case R.id.remove_item:
                this.requestRemoveTankObjById(tank.getId(), position);
                break;
            case R.id.edit_item:
                this.requestEditTankData(position, tank);
                break;
            case R.id.map:
                view.navigateToMap(SHOW_ALL_IN_MAP_REQUEST, tank, position);
        }
    }

    private static final int EDIT_TANK_OBJECT_REQUEST = 1;

    @Override
    public void requestEditTankData(int position, TankObject tank) {
        Intent intent = new Intent(view.getAppContext(), EditTankActivity.class);
        intent.putExtra("TankObj", tank);
        intent.putExtra("position", position);
        view.navigateToEditTank(intent, EDIT_TANK_OBJECT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_TANK_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            view.updateListItem(data.getExtras().getInt("position"), (TankObject) data.getSerializableExtra("editedTankObj"));
            return;
        }

        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            this.requestTanksData(view.getSpinnerSelectedPosition());
        }
    }

    @Override
    public void requestUploadData(int type) {
        model.getTanksList(view.getAppContext(), this, type, 1);
    }

    @Override
    public void showLocations(ArrayList<TankObject> tanks) {
        view.navigateToMap2(SHOW_ALL_IN_MAP_REQUEST, tanks);
    }

    @Override
    public void onSyncingResponse(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        ArrayList<Long> accepted = new ArrayList<>(), rejected = new ArrayList<>();
        for (int i = 0; i < responseArray.length(); i++) {
            if (responseArray.getJSONObject(i).getString("status").equals("success"))
                accepted.add(responseArray.getJSONObject(i).getLong("id"));
            else rejected.add(responseArray.getJSONObject(i).getLong("id"));
        }

        if (!accepted.isEmpty()) {
            for (int i = 0; i < accepted.size(); i++) {
                model.removeTankObject(view.getActContext(), this, accepted.get(i), -1);
            }
        }
    }

    @Override
    public void onSyncingError(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onGetTanksListCalled(ArrayList<TankObject> tanks, int index) {
        if (index == 0) {
            if (tanks.isEmpty())
                view.showEmptyListText();
            else {
                boolean isVisible = false;
                for (int i = 0; i < tanks.size(); i++) {
                    if (tanks.get(i).getLatitude() != null && tanks.get(i).getLongitude() != null) {
                        if (!tanks.get(i).getLatitude().isEmpty() && !tanks.get(i).getLongitude().isEmpty()) {
                            isVisible = true;
                            break;
                        }
                    }
                }
                view.loadTanksData(tanks, isVisible);
            }
        } else {
            if (tanks.isEmpty())
                view.showToastMessage(view.getActContext().getString(R.string.no_tanks_to_synchronize));
            else {
                view.showProgress();
                model.uploadTanksData(view.getActContext(), this, tanks);
            }
        }
    }

    @Override
    public void onRemoveTankObjectCalled(int flag, int position, long id) {
        if (flag > 0) {
            if (position == -1) {
                view.removeListItem(id);
            } else
                view.removeListItem(position);
        } else view.showToastMessage(view.getActContext().getString(R.string.failed_to_delete));
    }
}
