package com.seifoo.neomit.gisgathering.home.breaks.info.details;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.breaks.map.GetBreakMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.breaks.offline.details.OfflineBreakDetailsAdapter;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public class GetBreakInfoDetailsActivity extends AppCompatActivity implements GetBreakInfoDetailsMVP.View,OfflineBreakDetailsAdapter.OnListItemClicked{
    private RecyclerView recyclerView;
    private OfflineBreakDetailsAdapter adapter;

    private GetBreakInfoDetailsMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_break_info_details);
        getSupportActionBar().setTitle(getString(R.string.details));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new GetBreakInfoDetailsPresenter(this, new GetBreakInfoDetailsModel());

        recyclerView = (RecyclerView) findViewById(R.id.get_info_details_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineBreakDetailsAdapter(this, false,this);
        presenter.requestBreakDetails((BreakObject) getIntent().getExtras().getSerializable("GetInfo"));
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }


    @Override
    public void loadBreakMoreDetails(ArrayList<MoreDetails> list) {
        adapter.setDetailsList(list);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClickListener(String latitude, String longitude) {
        Intent intent = new Intent(this, GetBreakMapDetailsActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
