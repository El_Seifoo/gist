package com.seifoo.neomit.gisgathering.home.meter.add;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class FourthFragment extends Fragment implements Step, AddMeterMVP.MainView, AddMeterMVP.FourthView {

    private EditText streetNameEditText, sectorNameEditText, groundElevationEditText, elevationEditText, hcnEditText, meterAddressEditText;
    private Spinner subBuildingTypeSpinner, waterScheduleSpinner, newMeterBrandSpinner, districtNameSpinner;
    private RadioGroup sewerConnection;

    @Override
    public void showToastMessage(String message) {

    }

    @Override
    public void backToParent() {

    }

    @Override
    public String getCurrentDate() {
        return null;
    }

    private AddMeterMVP.Presenter presenter;

    public static FourthFragment newInstance(int position) {
        FourthFragment fragment = new FourthFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fourth, container, false);

        presenter = new AddMeterPresenter(this, this, new AddMeterModel());


        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);
        groundElevationEditText = (EditText) view.findViewById(R.id.ground_elevation);
        elevationEditText = (EditText) view.findViewById(R.id.elevation);
        hcnEditText = (EditText) view.findViewById(R.id.hcn);
        meterAddressEditText = (EditText) view.findViewById(R.id.meter_address);


        sewerConnection = (RadioGroup) view.findViewById(R.id.sewer_connection_radio_group);
        ((RadioButton) sewerConnection.getChildAt(0)).setChecked(true);

        subBuildingTypeSpinner = (Spinner) view.findViewById(R.id.sub_building_type_spinner);
        waterScheduleSpinner = (Spinner) view.findViewById(R.id.water_schedule_spinner);
        newMeterBrandSpinner = (Spinner) view.findViewById(R.id.new_meter_brand_spinner);
        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.hcn_qr)));
        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.meter_address_qr)));

        presenter.requestFourthSpinnersData();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFourthStepDataToActivity(new MeterObject(
                streetNameEditText.getText().toString().trim(),
                sectorNameEditText.getText().toString().trim(),
                getCheckedButton(sewerConnection).equals(getContext().getString(R.string.exist))?"1":"0",
                groundElevationEditText.getText().toString().trim(),
                elevationEditText.getText().toString().trim(),
                hcnEditText.getText().toString().trim(),
                meterAddressEditText.getText().toString().trim(),
                subBuildingTypeIds.get(subBuildingTypeSpinner.getSelectedItemPosition()),
                waterScheduleIds.get(waterScheduleSpinner.getSelectedItemPosition()),
                newMeterBrandIds.get(newMeterBrandSpinner.getSelectedItemPosition()),
                districtNameIds.get(districtNameSpinner.getSelectedItemPosition())));
        return null;
    }

    private String getCheckedButton(RadioGroup radioGroup) {
        int checkedButtonId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) getView().findViewById(checkedButtonId);
        return radioButton.getText().toString().trim();
    }


    @Override
    public void onSelected() {
    }

    @Override
    public void onError(@NonNull VerificationError error) {
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    ArrayList<Integer> subBuildingTypeIds, waterScheduleIds, newMeterBrandIds, districtNameIds;

    @Override
    public void loadSpinnersData(ArrayList<String> subBuildingType, ArrayList<Integer> subBuildingTypeIds,
                                 ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,
                                 ArrayList<String> newMeterBrand, ArrayList<Integer> newMeterBrandIds,
                                 ArrayList<String> districtName, ArrayList<Integer> districtNameIds) {


        this.subBuildingTypeIds = subBuildingTypeIds;
        this.waterScheduleIds = waterScheduleIds;
        this.newMeterBrandIds = newMeterBrandIds;
        this.districtNameIds = districtNameIds;

        subBuildingTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, subBuildingType));
        waterScheduleSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, waterSchedule));
        newMeterBrandSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, newMeterBrand));
        districtNameSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, districtName));

    }

    @Override
    public void setMeterAddress(String address) {
        meterAddressEditText.setText(address);
    }

    @Override
    public void setHCN(String hcn) {
        hcnEditText.setText(hcn);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator, int flag) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFourthViewActivityResult(requestCode, resultCode, data);
    }
}
