package com.seifoo.neomit.gisgathering.home.pump.rejected.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface EditRejectedPumpsMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex,
                              ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex,
                              ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds, int assetStatusIndex);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator);

        void setStationCode(String data);

        void navigateToTheMap(String latitude, String longitude, int requestCode);

        void setLocation(String location);

        void setStationName(String stationName);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> districtName, ArrayList<Integer> districtNameIds, int districtNameIndex,
                              ArrayList<String> subDistrict, ArrayList<Integer> subDistrictIds, int subDistrictIndex);

        void setLastUpdated(String date);

        void setData(String streetName, String dmaZone, String sectorName, String remarks);
    }

    interface Presenter {
        void requestFirstSpinnersData(PumpObject pump);

        void requestSecondSpinnersData(PumpObject pump);

        void requestPassFirstStepDataToActivity(PumpObject pump);

        void requestPassSecondStepDataToActivity(PumpObject pump);

        void requestEditPump(PumpObject pump, String createdAt, long id);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString);

    }
}
