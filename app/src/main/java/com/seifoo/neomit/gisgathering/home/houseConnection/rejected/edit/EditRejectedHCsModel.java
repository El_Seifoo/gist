package com.seifoo.neomit.gisgathering.home.houseConnection.rejected.edit;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class EditRejectedHCsModel {

    public void getGeoMasters(Context context, DBCallback callback, int[] types, int[] selectedIds, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types,
                MySingleton.getmInstance(context).getStringSharedPref(Constants.APP_LANGUAGE, context.getString(R.string.default_language_value))), selectedIds, index);
    }

    public void insertHcObj(Context context, DBCallback callback, HouseConnectionObject hcObject) {
        callback.onHcInsertionCalled(DataBaseHelper.getmInstance(context).insertHC(hcObject));
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> geoMastersList, int[] selectedIds, int index);

        void onHcInsertionCalled(long flag);
    }
}
