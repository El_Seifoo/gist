package com.seifoo.neomit.gisgathering.home.breaks.map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;

public interface GetBreakMapMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void loadBreakMap(String latitude, String longitude);
    }

    interface Presenter {

        void requestGetMap(String breakNumber);
    }
}
