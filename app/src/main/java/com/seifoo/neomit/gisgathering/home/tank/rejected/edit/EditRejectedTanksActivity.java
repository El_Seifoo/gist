package com.seifoo.neomit.gisgathering.home.tank.rejected.edit;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class EditRejectedTanksActivity extends AppCompatActivity implements StepperLayout.StepperListener, EditRejectedTanksMVP.View, EditRejectedTanksMVP.MainView {
    private StepperLayout mStepperLayout;
    private EditRejectedTanksMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_rejected_tanks);

        getSupportActionBar().setTitle(getString(R.string.edit_tank));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new EditRejectedTanksPresenter(this, this, new EditRejectedTanksModel());

        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new MyStepperAdapter(getSupportFragmentManager(), this));
        mStepperLayout.setListener(this);
    }

    private TankObject firstStepObj, secondStepObj;

    protected void passFirstObj(TankObject firstStepObj) {
        this.firstStepObj = firstStepObj;
    }

    protected void passSecondObj(TankObject secondStepObj) {
        this.secondStepObj = secondStepObj;
    }

    protected void changeStepperPosition(int position) {
        mStepperLayout.setCurrentStepPosition(position);
    }

    @Override
    public void onCompleted(View completeButton) {
        presenter.requestEditRejectedTank(new TankObject(firstStepObj, secondStepObj),
                ((TankObject) getIntent().getSerializableExtra("TankObj")).getCreatedAt(),
                ((TankObject) getIntent().getSerializableExtra("TankObj")).getTankId());
    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {

    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void backToParent() {
        Intent intent = getIntent();
        intent.putExtra("position", getIntent().getExtras().getInt("position"));
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class MyStepperAdapter extends AbstractFragmentStepAdapter {


        public MyStepperAdapter(FragmentManager fm, Context context) {
            super(fm, context);
        }

        @Override
        public Step createStep(int position) {
            switch (position) {
                case 0:
                    return FirstFragment.newInstance(((TankObject) ((EditRejectedTanksActivity) context).getIntent().getSerializableExtra("TankObj")).getFirstObject());
                case 1:
                default:
                    return SecondFragment.newInstance(((TankObject) ((EditRejectedTanksActivity) context).getIntent().getSerializableExtra("TankObj")).getSecondObject());
            }
        }

        @Override
        public int getCount() {
            return 2;
        }


        @NonNull
        @Override
        public StepViewModel getViewModel(@IntRange(from = 0) int position) {
            //Override this method to set Step title for the Tabs, not necessary for other stepper types
            return new StepViewModel.Builder(context)
                    .setTitle("") //can be a CharSequence instead
                    .create();
        }
    }

}
