package com.seifoo.neomit.gisgathering.home.chamber.offline;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;
import com.seifoo.neomit.gisgathering.utils.Urls;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class OfflineChambersModel {

    protected void uploadValvesData(final Context context, final VolleyCallback callback, final ArrayList<ChamberObject> chambers) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSyncingResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("parsing error ", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onSyncingError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""));
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            /*
                Json[data][0][Corrected_Id]:1 (optional but required for update)
                Json[data][0][X_MAP]:45.22523
                Json[data][0][Y_MAP]:24.255
                Json[data][0][MATERIAL_PIPE]:1
                Json[data][0][CH_NUMBER]:1
                Json[data][0][LENGTH]:1
                Json[data][0][WIDTH]:1
                Json[data][0][DEPTH]:1
                Json[data][0][DIAMETER]:1
                Json[data][0][DEPTH_UNDER_PIPE]:1
                Json[data][0][DEPTH_ABOVE_PIPE]:1
                Json[data][0][DISTRICT_NAME_ID]:1
                Json[data][0][DISTRICT_NO_ID]:1
                Json[data][0][DMA_ZONE_ID]:1
                Json[data][0][SUB_DISTRICT_NO]:1
                Json[data][0][STREET_NAME]:1
                Json[data][0][STREET_SER_NU]:1
                Json[data][0][SUB_NAME]:1
                Json[data][0][SECTOR_NAME]:1
                Json[data][0][DIAMETER_AIR_VALVE]:1
                Json[data][0][DIAMETER_WA_VALVE]:1
                Json[data][0][DIAMETER_ISO_VALVE]:1
                Json[data][0][DIAMETER_FLOWMETER]:1
                Json[data][0][IMAGE_NO]:1
                Json[data][0][F_TYPE_ID]:1
                Json[data][0][UPDATE_FIELD]:1
                Json[data][0][REMARKS]:1
                json[data][0][CreatedDate]:31-03-2019 22:20
                Json[data][0][RECORD_ID]:10
                Json[tableName]:chamberpoint
                Json[databaseName]:re_al_qwayah

             */
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                for (int i = 0; i < chambers.size(); i++) {
                    if (chambers.get(i).getChamberId() != 0.0d)
                        params.put("Json[data][" + i + "][Corrected_Id]", chambers.get(i).getChamberId() + "");
                    params.put("Json[data][" + i + "][RECORD_ID]", chambers.get(i).getId() + "");
                    params.put("Json[data][" + i + "][MATERIAL_PIPE]", chambers.get(i).getPipeMaterial() + "");
                    params.put("Json[data][" + i + "][CH_NUMBER]", chambers.get(i).getChNumber());
                    params.put("Json[data][" + i + "][LENGTH]", chambers.get(i).getLength() + "");
                    params.put("Json[data][" + i + "][WIDTH]", chambers.get(i).getWidth() + "");
                    params.put("Json[data][" + i + "][DEPTH]", chambers.get(i).getDepth() + "");
                    params.put("Json[data][" + i + "][DIAMETER]", chambers.get(i).getDiameter() + "");
                    params.put("Json[data][" + i + "][DEPTH_UNDER_PIPE]", chambers.get(i).getDepthUnderPipe() + "");
                    params.put("Json[data][" + i + "][DEPTH_ABOVE_PIPE]", chambers.get(i).getDepthAbovePipe() + "");
                    params.put("Json[data][" + i + "][DISTRICT_NAME_ID]", chambers.get(i).getDistrictName() + "");
                    params.put("Json[data][" + i + "][DMA_ZONE_ID]", chambers.get(i).getDmaZone());
                    params.put("Json[data][" + i + "][SUB_DISTRICT_NO]", chambers.get(i).getSubDistrictName() + "");
                    params.put("Json[data][" + i + "][STREET_NAME]", chambers.get(i).getStreetName());
                    params.put("Json[data][" + i + "][STREET_SER_NU]", chambers.get(i).getStreetNumber());
                    params.put("Json[data][" + i + "][SUB_NAME]", chambers.get(i).getSubName());
                    params.put("Json[data][" + i + "][SECTOR_NAME]", chambers.get(i).getSectorName());
                    params.put("Json[data][" + i + "][DIAMETER_AIR_VALVE]", chambers.get(i).getDiameterAirValve() + "");
                    params.put("Json[data][" + i + "][DIAMETER_WA_VALVE]", chambers.get(i).getDiameterWAValve() + "");
                    params.put("Json[data][" + i + "][DIAMETER_ISO_VALVE]", chambers.get(i).getDiameterISOValve() + "");
                    params.put("Json[data][" + i + "][DIAMETER_FLOWMETER]", chambers.get(i).getDiameterFlowMeter() + "");
                    params.put("Json[data][" + i + "][IMAGE_NO]", chambers.get(i).getImageNumber());
                    params.put("Json[data][" + i + "][F_TYPE_ID]", chambers.get(i).getType() + "");
                    params.put("Json[data][" + i + "][UPDATE_FIELD]", chambers.get(i).getUpdatedDate());
                    params.put("Json[data][" + i + "][REMARKS]", chambers.get(i).getRemarks() + "");
                    params.put("Json[data][" + i + "][CreatedDate]", chambers.get(i).getCreatedAt());
                    params.put("Json[data][" + i + "][X_MAP]", !chambers.get(i).getLongitude().isEmpty() ? chambers.get(i).getLongitude() : "360");
                    params.put("Json[data][" + i + "][Y_MAP]", !chambers.get(i).getLatitude().isEmpty() ? chambers.get(i).getLatitude() : "360");
                }
                params.put("Json[tableName]", context.getString(R.string.api_chamber_table_name));
                params.put("Json[databaseName]", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""));
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected void getChambersList(Context context, DBCallback callback, int type, int index) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetChambersListCalled(DataBaseHelper.getmInstance(context).getAllChambers(type), index);
    }

    protected void removeChamberObject(Context context, DBCallback callback, long id, int position) {
        callback.onRemoveChamberObjectCalled(DataBaseHelper.getmInstance(context).deleteChamber(id), position, id);
    }

    protected interface VolleyCallback {
        void onSyncingResponse(String response) throws JSONException;

        void onSyncingError(VolleyError error);
    }

    protected interface DBCallback {

        void onGetChambersListCalled(ArrayList<ChamberObject> chambers, int index);

        void onRemoveChamberObjectCalled(int flag, int position, long id);
    }
}
