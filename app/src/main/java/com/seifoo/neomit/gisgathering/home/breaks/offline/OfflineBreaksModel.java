package com.seifoo.neomit.gisgathering.home.breaks.offline;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;
import com.seifoo.neomit.gisgathering.utils.Urls;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class OfflineBreaksModel {

    protected void uploadBreaksData(final Context context, final VolleyCallback callback, final ArrayList<BreakObject> breaks) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSyncingResponse(response, breaks);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("parsing error ", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onSyncingError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""));
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                if (breaks.get(0).getBreakId() != 0.0d)
                    params.put("Json[data][0][Corrected_Id]", breaks.get(0).getBreakId() + "");
                params.put("Json[data][0][RECORD_ID]", breaks.get(0).getId() + "");
                params.put("Json[data][0][BREAK_NO]", breaks.get(0).getBreakNumber());
                params.put("Json[data][0][PIPE_TYPE]", breaks.get(0).getPipeType() + "");
                params.put("Json[data][0][PIPE_NUMBER]", breaks.get(0).getPipeNumber());
                params.put("Json[data][0][DISTRIC_NAME]", breaks.get(0).getDistrictName() + "");
                params.put("Json[data][0][SUB_ZONE]", breaks.get(0).getSubZone() + "");
                params.put("Json[data][0][BUILDING_NO]", breaks.get(0).getBuildingNumber());
                params.put("Json[data][0][STREET_NAME]", breaks.get(0).getStreetName());
                params.put("Json[data][0][BREAK_DATE]", breaks.get(0).getBreakDate());
                params.put("Json[data][0][DIAMETER]", breaks.get(0).getDiameter() + "");
                params.put("Json[data][0][PIPE_MATERIAL]", breaks.get(0).getPipeMaterial() + "");
                params.put("Json[data][0][Break_Details]", breaks.get(0).getBreakDetails());
                params.put("Json[data][0][EQUIPMENT_USED]", breaks.get(0).getUsedEquipment());
                params.put("Json[data][0][MAINTENANCE_COMPANY]", breaks.get(0).getMaintenanceCompany() + "");
                params.put("Json[data][0][MAINTENANCE_TYPE]", breaks.get(0).getMaintenanceType() + "");
                params.put("Json[data][0][SOIL_TYPE]", breaks.get(0).getSoilType() + "");
                params.put("Json[data][0][ASPHALT]", breaks.get(0).getAsphalt() + "");
                params.put("Json[data][0][DIMANTION_DRILLING]", breaks.get(0).getDimensionDrilling());
                params.put("Json[data][0][TYPE_DEVICE_MAINTANCE]", breaks.get(0).getMaintenanceDeviceType() + "");
                params.put("Json[data][0][PROCEDURE]", breaks.get(0).getProcedure() + "");
                params.put("Json[data][0][CreatedDate]", breaks.get(0).getCreatedAt());// created date ....
                params.put("Json[data][0][X_Map]", !breaks.get(0).getLongitude().isEmpty() ? breaks.get(0).getLongitude() : "360");
                params.put("Json[data][0][Y_Map]", !breaks.get(0).getLatitude().isEmpty() ? breaks.get(0).getLatitude() : "360");
                params.put("Json[tableName]", context.getString(R.string.api_break_table_name));
                params.put("Json[databaseName]", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""));
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void syncPics(final Context context, final UploadingPhotoCallback callback, final long acceptedId, final ArrayList<BreakObject> breaks) {

        try {
            UploadService.NAMESPACE = "com.seifoo.neomit.gisgathering";
            MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(context, UUID.randomUUID().toString(), Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_PHOTOS);

            final ArrayList<String> paths = new ArrayList<>();
            if (breaks.get(0).getPhotos().get("beforeImage") != null) {
                if (breaks.get(0).getPhotos().get("beforeImage").contains("//////")) {
                    Log.e("beforeImage", breaks.get(0).getPhotos().get("beforeImage").split("//////")[1]);
                    multipartUploadRequest.addFileToUpload(breaks.get(0).getPhotos().get("beforeImage").split("//////")[1], breaks.get(0).getBreakNumber() + "_before");
                    paths.add(breaks.get(0).getPhotos().get("beforeImage").split("//////")[1]);
                }
            }
            if (breaks.get(0).getPhotos().get("afterImage") != null) {
                if (breaks.get(0).getPhotos().get("afterImage").contains("//////")) {
                    Log.e("afterImage", breaks.get(0).getPhotos().get("afterImage").split("//////")[1]);
                    multipartUploadRequest.addFileToUpload(breaks.get(0).getPhotos().get("afterImage").split("//////")[1], breaks.get(0).getBreakNumber() + "_after");
                    paths.add(breaks.get(0).getPhotos().get("afterImage").split("//////")[1]);
                }

            }

            multipartUploadRequest
//                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .addHeader("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""))
                    .addParameter("databaseName", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""))
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(0)
                    .setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(Context context, UploadInfo uploadInfo) {

                        }

                        @Override
                        public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {

                        }


                        @Override
                        public void onCompleted(Context context1, UploadInfo uploadInfo, ServerResponse serverResponse) {
                            try {
                                JSONArray array = new JSONArray(new String(serverResponse.getBody(), "UTF-8"));
                                Log.e("server response", array.toString());
                                callback.onUploadSuccess(context, array, acceptedId, breaks, paths);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("erroooooooooooooooor 1", e.toString());
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                                Log.e("erroooooooooooooooor 2", e.toString());
                            }
                        }

                        @Override
                        public void onCancelled(Context context, UploadInfo uploadInfo) {
                            callback.onUploadFailed(context.getString(R.string.failed_to_sync_this_record));
                        }
                    })
                    .startUpload();
        } catch (Exception e) {
            Log.e("erroooooooooooooooor 3", e.toString());
            callback.onUploadFailed(context.getString(R.string.failed_to_sync_this_record_check_ur_photos));
        }
    }

    protected void getBreaksList(Context context, DBCallback callback, int type, int index) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetBreaksListCalled(DataBaseHelper.getmInstance(context).getAllBreaks(type), index);
    }

    protected void removeBreakObject(Context context, DBCallback callback, long id, int position) {
        callback.onRemoveBreakObjectCalled(DataBaseHelper.getmInstance(context).deleteBreak(id), position, id);
    }


    protected interface VolleyCallback {
        void onSyncingResponse(String response, ArrayList<BreakObject> breaks) throws JSONException;

        void onSyncingError(VolleyError error);
    }

    protected interface UploadingPhotoCallback {
        void onUploadSuccess(Context context, JSONArray response, long id, ArrayList<BreakObject> breaks, ArrayList<String> paths) throws JSONException;

        void onUploadFailed(String message);

    }

    protected interface DBCallback {

        void onGetBreaksListCalled(ArrayList<BreakObject> breaks, int index);

        void onRemoveBreakObjectCalled(int flag, int position, long id);
    }
}
