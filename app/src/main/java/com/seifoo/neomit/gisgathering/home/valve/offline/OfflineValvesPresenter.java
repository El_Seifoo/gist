package com.seifoo.neomit.gisgathering.home.valve.offline;

import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.home.valve.edit.EditValveActivity;
import com.seifoo.neomit.gisgathering.home.valve.offline.details.OfflineValvesDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class OfflineValvesPresenter implements OfflineValvesMVP.Presenter, OfflineValvesModel.VolleyCallback, OfflineValvesModel.DBCallback {
    private OfflineValvesMVP.View view;
    private OfflineValvesModel model;

    public OfflineValvesPresenter(OfflineValvesMVP.View view, OfflineValvesModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestValvesData(int type) {
        model.getValvesList(view.getAppContext(), this, type, 0);
    }

    @Override
    public void requestRemoveValveObjById(long id, int position) {
        model.removeValveObject(view.getActContext(), this, id, position);
    }

    private static final int SHOW_ALL_IN_MAP_REQUEST = 10;
    @Override
    public void OnListItemClickListener(int viewId, int position, ValveObject valve) {
        switch (viewId) {
            case R.id.more_details:
                view.navigateDestination("OfflineValveObj", valve, OfflineValvesDetailsActivity.class);
                break;
            case R.id.remove_item:
                this.requestRemoveValveObjById(valve.getId(), position);
                break;
            case R.id.edit_item:
                this.requestEditValveData(position, valve);
                break;
            case R.id.map:
                view.navigateToMap(SHOW_ALL_IN_MAP_REQUEST, valve, position);
        }
    }

    private static final int EDIT_VALVE_OBJECT_REQUEST = 1;

    @Override
    public void requestEditValveData(int position, ValveObject valve) {
        Intent intent = new Intent(view.getAppContext(), EditValveActivity.class);
        intent.putExtra("ValveObj", valve);
        intent.putExtra("position", position);
        view.navigateToEditValve(intent, EDIT_VALVE_OBJECT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_VALVE_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            view.updateListItem(data.getExtras().getInt("position"), (ValveObject) data.getSerializableExtra("editedValveObj"));
            return;
        }

        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            this.requestValvesData(view.getSpinnerSelectedPosition());
        }
    }

    @Override
    public void requestUploadData(int type) {
        model.getValvesList(view.getAppContext(), this, type, 1);
    }

    @Override
    public void showLocations(ArrayList<ValveObject> valves) {
        view.navigateToMap2(SHOW_ALL_IN_MAP_REQUEST,valves);
    }

    @Override
    public void onSyncingResponse(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        ArrayList<Long> accepted = new ArrayList<>(), rejected = new ArrayList<>();
        for (int i = 0; i < responseArray.length(); i++) {
            if (responseArray.getJSONObject(i).getString("status").equals("success"))
                accepted.add(responseArray.getJSONObject(i).getLong("id"));
            else rejected.add(responseArray.getJSONObject(i).getLong("id"));
        }

        if (!accepted.isEmpty()) {
            for (int i = 0; i < accepted.size(); i++) {
                model.removeValveObject(view.getActContext(), this, accepted.get(i), -1);
            }
        }
    }

    @Override
    public void onSyncingError(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onGetValvesListCalled(ArrayList<ValveObject> valves, int index) {
        if (index == 0) {
            if (valves.isEmpty())
                view.showEmptyListText();
            else {
                boolean isVisible = false;
                for (int i = 0; i < valves.size(); i++) {
                    if (valves.get(i).getLatitude() != null && valves.get(i).getLongitude() != null) {
                        if (!valves.get(i).getLatitude().isEmpty() && !valves.get(i).getLongitude().isEmpty()) {
                            isVisible = true;
                            break;
                        }
                    }
                }
                view.loadValvesData(valves,isVisible);
            }
        } else {
            if (valves.isEmpty())
                view.showToastMessage(view.getActContext().getString(R.string.no_valves_to_synchronize));
            else {
                view.showProgress();
                model.uploadValvesData(view.getActContext(), this, valves);
            }
        }
    }

    @Override
    public void onRemoveValveObjectCalled(int flag, int position, long id) {
        if (flag > 0) {
            if (position == -1) {
                view.removeListItem(id);
            } else
                view.removeListItem(position);
        } else view.showToastMessage(view.getActContext().getString(R.string.failed_to_delete));
    }
}
