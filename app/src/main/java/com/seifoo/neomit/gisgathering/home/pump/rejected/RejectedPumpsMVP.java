package com.seifoo.neomit.gisgathering.home.pump.rejected;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

import java.util.ArrayList;

public interface RejectedPumpsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showEmptyListText();

        void loadRejectedPumps(ArrayList<PumpObject> pumps);
    }

    interface Presenter {
        void requestPumps();
    }
}
