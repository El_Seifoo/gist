package com.seifoo.neomit.gisgathering.home.houseConnection.offline;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.home.houseConnection.rejected.RejectedHcsActivity;

import java.util.ArrayList;

public class OfflineHcsAdapter extends RecyclerView.Adapter<OfflineHcsAdapter.Holder> {
    private ArrayList<HouseConnectionObject> list;
    private final HcObjectListItemListener listItemListener;
    private boolean isOffline;

    public OfflineHcsAdapter(HcObjectListItemListener listItemListener, boolean isOffline) {
        this.listItemListener = listItemListener;
        this.isOffline = isOffline;
    }

    public interface HcObjectListItemListener {
        void onListItemClickListener(int viewId, int position, HouseConnectionObject HC);
    }

    public void setList(ArrayList<HouseConnectionObject> list) {
        this.list = list;
    }

    public void removeItem(Context context, int position) {
        list.remove(position);
        if (list.isEmpty()) {
            if (context instanceof OfflineHcsActivity)
                ((OfflineHcsActivity) context).showEmptyListText();
            else if (context instanceof RejectedHcsActivity)
                ((RejectedHcsActivity) context).showEmptyListText();
        }
        notifyDataSetChanged();
    }

    public void updateItem(int position, HouseConnectionObject editedObj) {
        list.set(position, editedObj);
        notifyDataSetChanged();
    }

    public void updateItem(Context context, long id) {
        for (int i = 0; i < list.size(); i++) {
            if (id == list.get(i).getId()) {
                list.remove(i);
                if (list.isEmpty()) {
                    if (context instanceof OfflineHcsActivity)
                        ((OfflineHcsActivity) context).showEmptyListText();
                    else if (context instanceof RejectedHcsActivity)
                        ((RejectedHcsActivity) context).showEmptyListText();
                }
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void clear() {
        if (list != null) {
            list.clear();
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offline_hc_list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        if (isOffline) {
            holder.removeItem.setVisibility(View.VISIBLE);
            holder.editItem.setVisibility(View.VISIBLE);
        } else {
            if (list.get(position).getReason() != null) {
                if (!list.get(position).getReason().isEmpty()) {
                    holder.reason.setVisibility(View.VISIBLE);
                    holder.reason.setText(
                            Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.reason) + ": </b></font>" + list.get(position).getReason()),
                            TextView.BufferType.SPANNABLE);
                } else holder.reason.setVisibility(View.GONE);
            } else holder.reason.setVisibility(View.GONE);

        }


        if (list.get(position).getLatitude() != null && list.get(position).getLongitude() != null) {
            if (!list.get(position).getLatitude().isEmpty() && !list.get(position).getLongitude().isEmpty()) {
                holder.map.setVisibility(View.VISIBLE);
            } else holder.map.setVisibility(View.GONE);
        } else holder.map.setVisibility(View.GONE);


        if (list.get(position).getSerialNumber() != null) {
            if (!list.get(position).getSerialNumber().isEmpty()) {
                holder.serialNumber.setVisibility(View.VISIBLE);
                holder.serialNumber.setText(
                        Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.serial_number) + ": </b></font>" + list.get(position).getSerialNumber()),
                        TextView.BufferType.SPANNABLE);

            } else holder.serialNumber.setVisibility(View.GONE);
        } else holder.serialNumber.setVisibility(View.GONE);


        if (list.get(position).getHCN() != null) {
            if (!list.get(position).getHCN().isEmpty()) {
                holder.HCN.setVisibility(View.VISIBLE);
                holder.HCN.setText(
                        Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.hcn) + ": </b></font>" + list.get(position).getHCN()),
                        TextView.BufferType.SPANNABLE);

            } else holder.HCN.setVisibility(View.GONE);
        } else holder.HCN.setVisibility(View.GONE);

            if (!list.get(position).getCreatedAt().isEmpty()) {
                holder.createdAt.setVisibility(View.VISIBLE);
                holder.createdAt.setText(
                        Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.created_at) + ": </b></font>" + list.get(position).getCreatedAt()),
                        TextView.BufferType.SPANNABLE);

            } else holder.createdAt.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView serialNumber, HCN, reason, moreDetails, map,createdAt;
        ImageView removeItem, editItem;

        public Holder(@NonNull View itemView) {
            super(itemView);
            serialNumber = (TextView) itemView.findViewById(R.id.serial_number);

            HCN = (TextView) itemView.findViewById(R.id.hcn);

            moreDetails = (TextView) itemView.findViewById(R.id.more_details);
            createdAt = (TextView) itemView.findViewById(R.id.created_at);
            map = (TextView) itemView.findViewById(R.id.map);

            map.setOnClickListener(this);
            moreDetails.setOnClickListener(this);
            if (isOffline) {
                removeItem = (ImageView) itemView.findViewById(R.id.remove_item);
                editItem = (ImageView) itemView.findViewById(R.id.edit_item);
                removeItem.setOnClickListener(this);
                editItem.setOnClickListener(this);
            } else {
                reason = (TextView) itemView.findViewById(R.id.reason);
            }
        }

        @Override
        public void onClick(View v) {
            listItemListener.onListItemClickListener(v.getId(), getAdapterPosition(), list.get(getAdapterPosition()));
        }
    }
}
