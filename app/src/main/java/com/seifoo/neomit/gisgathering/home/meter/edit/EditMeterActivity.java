package com.seifoo.neomit.gisgathering.home.meter.edit;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class EditMeterActivity extends AppCompatActivity implements StepperLayout.StepperListener, EditMeterMVP.MainView {
    private StepperLayout mStepperLayout;
    private EditMeterMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_meter);
        getSupportActionBar().setTitle(getString(R.string.edit_meter));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new EditMeterPresenter(this, new EditMeterModel());
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new MyStepperAdapter(getSupportFragmentManager(), this));
        mStepperLayout.setListener(this);

    }

    @Override
    public void onCompleted(View completeButton) {
        presenter.requestEditMeterObject(new MeterObject(firstStepMeterObj, secondStepMeterObj, thirdStepMeterObj, fourthStepMeterObj, fifthStepMeterObj),
                (MeterObject) getIntent().getSerializableExtra("meterObj"));
    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {

    }

    private MeterObject firstStepMeterObj, secondStepMeterObj, thirdStepMeterObj, fourthStepMeterObj, fifthStepMeterObj;

    protected void passFirstStepData(MeterObject meterObject) {
        firstStepMeterObj = meterObject;
    }

    protected void passSecondStepData(MeterObject meterObject) {
        secondStepMeterObj = meterObject;
    }

    protected void passThirdStepData(MeterObject meterObject) {
        thirdStepMeterObj = meterObject;
    }

    protected void passFourthStepData(MeterObject meterObject) {
        fourthStepMeterObj = meterObject;
    }

    protected void passFifthStepData(MeterObject meterObject) {
        fifthStepMeterObj = meterObject;
    }

    protected void changeStepperPosition(int position) {
        mStepperLayout.setCurrentStepPosition(position);
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void backToParent(MeterObject meterObject) {
        Intent intent = getIntent();
        intent.putExtra("position", getIntent().getExtras().getInt("position"));
        intent.putExtra("editedMeterObj", meterObject);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class MyStepperAdapter extends AbstractFragmentStepAdapter {


        public MyStepperAdapter(FragmentManager fm, Context context) {
            super(fm, context);
        }

        @Override
        public Step createStep(int position) {
            switch (position) {
                case 0:
                    return FirstFragment.newInstance(((MeterObject) ((EditMeterActivity) context).getIntent().getSerializableExtra("meterObj")).getFirstStepObj());
                case 1:
                    return SecondFragment.newInstance(((MeterObject) ((EditMeterActivity) context).getIntent().getSerializableExtra("meterObj")).getSecondStepObj());
                case 2:
                    return ThirdFragment.newInstance(((MeterObject) ((EditMeterActivity) context).getIntent().getSerializableExtra("meterObj")).getThirdStepObj());
                case 3:
                    return FourthFragment.newInstance(((MeterObject) ((EditMeterActivity) context).getIntent().getSerializableExtra("meterObj")).getFourthStepObj());
                case 4:
                default:
                    return FifthFragment.newInstance(((MeterObject) ((EditMeterActivity) context).getIntent().getSerializableExtra("meterObj")).getFifthStepObj());
            }
        }

        @Override
        public int getCount() {
            return 5;
        }


        @NonNull
        @Override
        public StepViewModel getViewModel(@IntRange(from = 0) int position) {
            //Override this method to set Step title for the Tabs, not necessary for other stepper types
            return new StepViewModel.Builder(context)
                    .setTitle("") //can be a CharSequence instead
                    .create();
        }
    }
}
