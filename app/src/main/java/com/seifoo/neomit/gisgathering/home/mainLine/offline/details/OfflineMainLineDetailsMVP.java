package com.seifoo.neomit.gisgathering.home.mainLine.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface OfflineMainLineDetailsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();


        void loadMainLineMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestMainLineDetails(MainLineObject mainLine);
    }
}
