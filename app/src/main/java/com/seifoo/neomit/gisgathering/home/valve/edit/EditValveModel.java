package com.seifoo.neomit.gisgathering.home.valve.edit;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class EditValveModel {

    public void getGeoMasters(Context context, DBCallback callback, int[] types, int[] selectedIds, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))), selectedIds, index);
    }

    public void updateValveObj(Context context, DBCallback callback, ValveObject valve) {
        callback.onValveUpdatingCalled(DataBaseHelper.getmInstance(context).updateValve(valve), valve);
    }

    protected void getValvesLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetValvesLocationCalled(DataBaseHelper.getmInstance(context).getValvesLocation(), prevLatLng);
    }
    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index);

        void onValveUpdatingCalled(int flag, ValveObject valve);

        void onGetValvesLocationCalled(ArrayList<HashMap<String,String>> valvesLocation, String prevLatLng);
    }
}
