package com.seifoo.neomit.gisgathering.home.breaks.info;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

public class GetBreakInfoPresenter implements GetBreakInfoMVP.Presenter, GetBreakInfoModel.VolleyCallback {
    private GetBreakInfoMVP.View view;
    private GetBreakInfoModel model;

    public GetBreakInfoPresenter(GetBreakInfoMVP.View view, GetBreakInfoModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestGetInfo(String breakNumber) {
        if (breakNumber.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.serial_number_is_required));
            return;
        }
        view.showProgress();
        model.getInfo(view.getActContext(), this, breakNumber);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {

                Map<String, String> photos = new LinkedHashMap<>();
                String before = "", after = "";
                if (data.getJSONObject(0).has("beforeImage"))
                    before = data.getJSONObject(0).getString("beforeImage");
                if (data.getJSONObject(0).has("afterImage"))
                    after = data.getJSONObject(0).getString("afterImage");

                photos.put("beforeImage", before);
                photos.put("afterImage", after);
                BreakObject breakObj = new BreakObject(
                        returnValidString(data.getJSONObject(0).getString("Y_Map")),
                        returnValidString(data.getJSONObject(0).getString("X_Map")),
                        returnValidString(data.getJSONObject(0).getString("BREAK_NO")),
                        returnValidInt(data.getJSONObject(0).getString("PIPE_TYPE")),
                        returnValidInt(data.getJSONObject(0).getString("DISTRIC_NAME")),
                        returnValidInt(data.getJSONObject(0).getString("SUB_ZONE")),
                        returnValidString(data.getJSONObject(0).getString("BUILDING_NO")),
                        returnValidString(data.getJSONObject(0).getString("PIPE_NUMBER")),
                        returnValidString(data.getJSONObject(0).getString("STREET_NAME")),
                        returnValidDate(data.getJSONObject(0).getString("BREAK_DATE")),
                        returnValidInt(data.getJSONObject(0).getString("DIAMETER")),
                        returnValidInt(data.getJSONObject(0).getString("PIPE_MATERIAL")),
                        returnValidString(data.getJSONObject(0).getString("Break_Details")),
                        returnValidString(data.getJSONObject(0).getString("EQUIPMENT_USED")),
                        returnValidInt(data.getJSONObject(0).getString("MAINTENANCE_COMPANY")),
                        returnValidInt(data.getJSONObject(0).getString("MAINTENANCE_TYPE")),
                        returnValidInt(data.getJSONObject(0).getString("SOIL_TYPE")),
                        returnValidInt(data.getJSONObject(0).getString("ASPHALT")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("ASPHALT")),
                        returnValidString(data.getJSONObject(0).getString("DIMANTION_DRILLING")),
                        returnValidInt(data.getJSONObject(0).getString("TYPE_DEVICE_MAINTANCE")),
                        returnValidInt(data.getJSONObject(0).getString("PROCEDURE")),
                        photos,
                        returnValidDate1(data.getJSONObject(0).getString("CreatedDate")));
                view.loadBreakInfo(breakObj);
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_break_not_exist));
        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_break_not_exist));
    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
