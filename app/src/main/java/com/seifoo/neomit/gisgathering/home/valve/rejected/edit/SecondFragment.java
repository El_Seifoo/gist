package com.seifoo.neomit.gisgathering.home.valve.rejected.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, EditRejectedValvesMVP.View, EditRejectedValvesMVP.SecondView {
    private Spinner coverStatusSpinner, enabledSpinner, districtsSpinner, subDistrictsSpinner, existInFieldSpinner, existInMapSpinner, waterScheduleSpinner, remarksSpinner;
    private EditText dmaZoneEditText, streetNameEditText, sectorNameEditText;

    private EditRejectedValvesMVP.Presenter presenter;

    public static SecondFragment newInstance(ValveObject valve) {
        SecondFragment fragment = new SecondFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("ValveObj", valve);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_valve_second_edit, container, false);

        presenter = new EditRejectedValvesPresenter(this, this, new EditRejectedValvesModel());

        coverStatusSpinner = (Spinner) view.findViewById(R.id.cover_status_spinner);
        enabledSpinner = (Spinner) view.findViewById(R.id.enabled_spinner);
        districtsSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);
        subDistrictsSpinner = (Spinner) view.findViewById(R.id.sub_district_spinner);
        existInFieldSpinner = (Spinner) view.findViewById(R.id.exist_in_field_spinner);
        existInMapSpinner = (Spinner) view.findViewById(R.id.exist_in_map_spinner);
        waterScheduleSpinner = (Spinner) view.findViewById(R.id.water_schedule_spinner);
        remarksSpinner = (Spinner) view.findViewById(R.id.remarks_spinner);

        dmaZoneEditText = (EditText) view.findViewById(R.id.dma_zone);
        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);

        presenter.requestSecondSpinnersData((ValveObject) getArguments().getSerializable("ValveObj"));
        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new ValveObject(coverStatusIds.get(coverStatusSpinner.getSelectedItemPosition()),
                enabledIds.get(enabledSpinner.getSelectedItemPosition()), dmaZoneEditText.getText().toString().trim(),
                districtsIds.get(districtsSpinner.getSelectedItemPosition()), subDistrictsIds.get(subDistrictsSpinner.getSelectedItemPosition()),
                streetNameEditText.getText().toString().trim(), sectorNameEditText.getText().toString().trim(),
                existInFieldIds.get(existInFieldSpinner.getSelectedItemPosition()), existInMapIds.get(existInMapSpinner.getSelectedItemPosition()),
                waterScheduleIds.get(waterScheduleSpinner.getSelectedItemPosition()), remarksIds.get(remarksSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> coverStatusIds, enabledIds, districtsIds, subDistrictsIds, existInFieldIds, existInMapIds, waterScheduleIds, remarksIds;

    @Override
    public void loadSpinnersData(ArrayList<String> coverStatus, ArrayList<Integer> coverStatusIds, int coverStatusIndex,
                                 ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex,
                                 ArrayList<String> districts, ArrayList<Integer> districtsIds, int districtsIndex,
                                 ArrayList<String> subDistricts, ArrayList<Integer> subDistrictsIds, int subDistrictsIndex,
                                 ArrayList<String> existInField, ArrayList<Integer> existInFieldIds, int existInFieldIndex,
                                 ArrayList<String> existInMap, ArrayList<Integer> existInMapIds, int existInMapIndex,
                                 ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds, int waterScheduleIndex,
                                 ArrayList<String> remarks, ArrayList<Integer> remarksIds, int remarksIndex) {

        this.coverStatusIds = coverStatusIds;
        this.enabledIds = enabledIds;
        this.districtsIds = districtsIds;
        this.subDistrictsIds = subDistrictsIds;
        this.existInFieldIds = existInFieldIds;
        this.existInMapIds = existInMapIds;
        this.waterScheduleIds = waterScheduleIds;
        this.remarksIds = remarksIds;


        coverStatusSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, coverStatus));
        coverStatusSpinner.setSelection(coverStatusIndex);
        enabledSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, enabled));
        enabledSpinner.setSelection(enabledIndex);
        districtsSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, districts));
        districtsSpinner.setSelection(districtsIndex);
        subDistrictsSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, subDistricts));
        subDistrictsSpinner.setSelection(subDistrictsIndex);
        existInFieldSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, existInField));
        existInFieldSpinner.setSelection(existInFieldIndex);
        existInMapSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, existInMap));
        existInMapSpinner.setSelection(existInMapIndex);
        waterScheduleSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, waterSchedule));
        waterScheduleSpinner.setSelection(waterScheduleIndex);
        remarksSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, remarks));
        remarksSpinner.setSelection(remarksIndex);

    }

    @Override
    public void setData(String dmaZone, String streetName, String sectorName) {
        dmaZoneEditText.setText(dmaZone);
        streetNameEditText.setText(streetName);
        sectorNameEditText.setText(sectorName);
    }
}
