package com.seifoo.neomit.gisgathering.home.mainLine.offline;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;
import com.seifoo.neomit.gisgathering.utils.Urls;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class OfflineMainLineModel {

    protected void uploadValvesData(final Context context, final VolleyCallback callback, final ArrayList<MainLineObject> mainLines) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Urls.HTTP + MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_IP, "") + Urls.SYNC_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSyncingResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("parsing error ", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onSyncingError(error);
            }
        }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", MySingleton.getmInstance(context).getStringSharedPref(Constants.USER_TOKEN, ""));
                return header;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            /*
Json[data][0][Corrected_Id]:1 (optional but required for update)
Json[data][0][X_MAP]:452.2258
Json[data][0][Y_MAP]:2454.585
Json[data][0][SERIAL_NO]:458500
Json[data][0][DIAMETER_ID]:1
Json[data][0][F_TYPE_ID]:1
Json[data][0][ENABLED_ID]:1
Json[data][0][MATERIAL_ID]:1
Json[data][0][STREET_NAME]:1
Json[data][0][STREET_SER_NU]:1
Json[data][0][DISTRICT_NAME_ID]:1
Json[data][0][ASSIGNED]:1
Json[data][0][SUP_DISTRICT_ID]:1
Json[data][0][DISTRICT_NO_ID]:1
Json[data][0][DMA_ZONE_ID]:1
Json[data][0][SUB_NAME]:1
Json[data][0][SECTOR_NAME]:1
Json[data][0][WATER_SCHEDULE_ID]:1
Json[data][0][ASSET_STATUS_ID]:1
Json[data][0][REMARKS]:1
json[data][0][CreatedDate]:31-03-2019 22:20 (same format as previous)
Json[data][0][RECORD_ID]:10
Json[tableName]:main_line
Json[databaseName]:re_al_qwayah
             */
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new LinkedHashMap<>();
                for (int i = 0; i < mainLines.size(); i++) {
                    if (mainLines.get(i).getMainLineId() != 0.0d)
                        params.put("Json[data][" + i + "][Corrected_Id]", mainLines.get(i).getMainLineId() + "");
                    params.put("Json[data][" + i + "][RECORD_ID]", mainLines.get(i).getId() + "");
                    params.put("Json[data][" + i + "][SERIAL_NO]", mainLines.get(i).getSerialNumber());
                    params.put("Json[data][" + i + "][DIAMETER_ID]", mainLines.get(i).getDiameter() + "");
                    params.put("Json[data][" + i + "][F_TYPE_ID]", mainLines.get(i).getType() + "");
                    params.put("Json[data][" + i + "][ENABLED_ID]", mainLines.get(i).getEnabled() + "");
                    params.put("Json[data][" + i + "][MATERIAL_ID]", mainLines.get(i).getMaterial() + "");
                    params.put("Json[data][" + i + "][STREET_NAME]", mainLines.get(i).getStreetName());
                    params.put("Json[data][" + i + "][STREET_SER_NU]", mainLines.get(i).getStreetNumber());
                    params.put("Json[data][" + i + "][DISTRICT_NAME_ID]", mainLines.get(i).getSubDistrict() + "");
                    params.put("Json[data][" + i + "][ASSIGNED]", mainLines.get(i).getAssigned());
                    params.put("Json[data][" + i + "][SUP_DISTRICT_ID]", mainLines.get(i).getSubDistrict() + "");
                    params.put("Json[data][" + i + "][DMA_ZONE_ID]", mainLines.get(i).getDmaZone());
                    params.put("Json[data][" + i + "][SUB_NAME]", mainLines.get(i).getSubName());
                    params.put("Json[data][" + i + "][SECTOR_NAME]", mainLines.get(i).getSectorName());
                    params.put("Json[data][" + i + "][WATER_SCHEDULE_ID]", mainLines.get(i).getWaterSchedule() + "");
                    params.put("Json[data][" + i + "][ASSET_STATUS_ID]", mainLines.get(i).getAssetStatus() + "");
                    params.put("Json[data][" + i + "][REMARKS]", mainLines.get(i).getRemarks() + "");
                    params.put("Json[data][" + i + "][CreatedDate]", mainLines.get(i).getCreatedAt());
                    params.put("Json[data][" + i + "][X_MAP]", !mainLines.get(i).getLongitude().isEmpty() ? mainLines.get(i).getLongitude() : "360");
                    params.put("Json[data][" + i + "][Y_MAP]", !mainLines.get(i).getLatitude().isEmpty() ? mainLines.get(i).getLatitude() : "360");
                }
                params.put("Json[tableName]", context.getString(R.string.api_main_line_table_name));
                params.put("Json[databaseName]", MySingleton.getmInstance(context).getStringSharedPref(Constants.API_DB_NAME, ""));
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected void getMainLinesList(Context context, DBCallback callback, int type, int index) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetMainLinesListCalled(DataBaseHelper.getmInstance(context).getAllMainLines(type), index);
    }

    protected void removeLineObject(Context context, DBCallback callback, long id, int position) {
        callback.onRemoveMainLineObjectCalled(DataBaseHelper.getmInstance(context).deleteMainLine(id), position, id);
    }

    protected interface VolleyCallback {
        void onSyncingResponse(String response) throws JSONException;

        void onSyncingError(VolleyError error);
    }

    protected interface DBCallback {

        void onGetMainLinesListCalled(ArrayList<MainLineObject> mainLines, int index);

        void onRemoveMainLineObjectCalled(int flag, int position, long id);
    }
}
