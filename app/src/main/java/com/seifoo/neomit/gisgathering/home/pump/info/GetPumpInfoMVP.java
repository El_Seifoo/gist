package com.seifoo.neomit.gisgathering.home.pump.info;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

public interface GetPumpInfoMVP {

    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void initializeScanner(IntentIntegrator integrator);

        void setCode(String code);

        void loadPumpInfo(PumpObject pumpObject);
    }

    interface Presenter {

        void requestQrCode(Activity activity);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestGetInfo(String code);
    }
}
