package com.seifoo.neomit.gisgathering.home.houseConnection.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, EditHcMVP.View, EditHcMVP.SecondView {
    private EditText subDistrictEditText, streetNameEditText, sectorNameEditText;
    private Spinner districtNameSpinner, waterScheduleSpinner, remarksSpinner;

    private EditHcMVP.Presenter presenter;

    public static SecondFragment newInstance(HouseConnectionObject HcObj) {
        SecondFragment fragment = new SecondFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("HcObj", HcObj);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hc_second_edit, container, false);

        presenter = new EditHcPresenter(this, this, new EditHcModel());

        subDistrictEditText = (EditText) view.findViewById(R.id.sub_district);
        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);

        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);
        waterScheduleSpinner = (Spinner) view.findViewById(R.id.water_schedule_spinner);
        remarksSpinner = (Spinner) view.findViewById(R.id.remarks_spinner);

        presenter.requestSecondSpinnersData((HouseConnectionObject) getArguments().getSerializable("HcObj"));
        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new HouseConnectionObject(districtsIds.get(districtNameSpinner.getSelectedItemPosition()),
                subDistrictEditText.getText().toString().trim(), streetNameEditText.getText().toString().trim(),
                sectorNameEditText.getText().toString().trim(), waterScheduleIds.get(waterScheduleSpinner.getSelectedItemPosition()),
                remarksIds.get(remarksSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    ArrayList<Integer> districtsIds, waterScheduleIds, remarksIds;

    @Override
    public void loadSpinnersData(ArrayList<String> districts, ArrayList<Integer> districtsIds, int districtsIndex, ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds, int waterScheduleIndex, ArrayList<String> remarks, ArrayList<Integer> remarksIds, int remarksIndex) {
        this.districtsIds = districtsIds;
        this.waterScheduleIds = waterScheduleIds;
        this.remarksIds = remarksIds;

        districtNameSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, districts));
        districtNameSpinner.setSelection(districtsIndex);
        waterScheduleSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, waterSchedule));
        waterScheduleSpinner.setSelection(waterScheduleIndex);
        remarksSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, remarks));
        remarksSpinner.setSelection(remarksIndex);

    }

    @Override
    public void setData(String subDistrict, String streetName, String sectorName) {
        subDistrictEditText.setText(subDistrict);
        streetNameEditText.setText(streetName);
        sectorNameEditText.setText(sectorName);
    }
}
