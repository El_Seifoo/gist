package com.seifoo.neomit.gisgathering.home.fireHydrant.rejected.edit;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class EditRejectedFiresPresenter implements EditRejectedFiresMVP.Presenter, EditRejectedFiresModel.DBCallback {
    private EditRejectedFiresMVP.View view;
    private EditRejectedFiresMVP.MainView mainView;
    private EditRejectedFiresMVP.FirstView firstView;
    private EditRejectedFiresMVP.SecondView secondView;
    private EditRejectedFiresModel model;

    public EditRejectedFiresPresenter(EditRejectedFiresMVP.View view, EditRejectedFiresMVP.MainView mainView, EditRejectedFiresModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public EditRejectedFiresPresenter(EditRejectedFiresMVP.View view, EditRejectedFiresMVP.FirstView firstView, EditRejectedFiresModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public EditRejectedFiresPresenter(EditRejectedFiresMVP.View view, EditRejectedFiresMVP.SecondView secondView, EditRejectedFiresModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }

    @Override
    public void requestFirstStepData(FireHydrantObject fireObject) {
        //barrelDiameterSpinner, materialSpinner, typeSpinner {0, 4, 24,25}
        model.getGeoMasters(view.getAppContext(), this, new int[]{0, 4, 24, 25},
                new int[]{fireObject.getFireBarrelDiameter(), fireObject.getFireMaterialId(), fireObject.getFireTypeId(),fireObject.getFireDistrictName()}, 1);
        firstView.setSerialNumber(fireObject.getFireSerialNumber());
        firstView.setFHNumber(fireObject.getFireNumber());
        firstView.setCommissionDate(fireObject.getFireCommissionDate());
        firstView.setHeight(fireObject.getFireHeight());

        if (!fireObject.getFireLatitude().isEmpty() && !fireObject.getFireLongitude().isEmpty())
            firstView.setLocation(fireObject.getFireLatitude() + "," + fireObject.getFireLongitude());
    }

    @Override
    public void requestSecondStepData(FireHydrantObject fireObject) {
        //fireHydrateStatusSpinner, remarksSpinner, valveTypeSpinner, fireBrandSpinner, waterScheduleSpinner, maintenanceAreaSpinner {3, 5, 20, 1, 16, 9}
        model.getGeoMasters(view.getAppContext(), this, new int[]{3, 5, 20, 1, 16, 9},
                new int[]{fireObject.getFireStatus(), fireObject.getFireRemarks(), fireObject.getFireValveType(), fireObject.getFireBrand(),
                        fireObject.getFireWaterSchedule(), fireObject.getFireMaintenanceArea()}, 2);
        secondView.setData(fireObject.getFireStreetName(), fireObject.getFireSectorName(), fireObject.getFireWithWater());
    }


    @Override
    public void requestPassFirstStepDataToActivity(FireHydrantObject fireHydrantObject) {
        if (!fireHydrantObject.getFireLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        fireHydrantObject.setFireLatitude(fireHydrantObject.getFireLatitude().split(",")[0]);
        fireHydrantObject.setFireLongitude(fireHydrantObject.getFireLongitude().split(",")[1]);
        if (view.getAppContext() instanceof EditRejectedFiresActivity) {
            ((EditRejectedFiresActivity) view.getAppContext()).passFirstObj(fireHydrantObject);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(FireHydrantObject fireHydrantObject) {
        if (view.getAppContext() instanceof EditRejectedFiresActivity) {
            ((EditRejectedFiresActivity) view.getAppContext()).passSecondObj(fireHydrantObject);
        }
    }

    @Override
    public void requestEditRejectedFireObject(FireHydrantObject editedFireObject, String createdAt, long id) {
        editedFireObject.setFireHydrantId(id);
        editedFireObject.setCreatedAt(returnValidNumbers(createdAt));
        model.insertFireObj(view.getAppContext(), this, editedFireObject);
    }

    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View clickedView) {
                switch (clickedView.getId()) {
                    case R.id.serial_num_qr:
                        requestQrCode(((EditRejectedFiresActivity) view.getAppContext()), view.getAppContext().getString(R.string.serial_number));
                        break;
                    case R.id.hcn_qr:
                        requestQrCode(((EditRejectedFiresActivity) view.getAppContext()), view.getAppContext().getString(R.string.fh_number));
                        break;
                }
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext, String flagString) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (flagString.equals(view.getAppContext().getString(R.string.fh_number))) {
            flag = 1;
        }

        firstView.initializeScanner(integrator, flag);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void handleQRScannerResult(String contents, int flag) {
        contents = contents.replaceAll("[^0-9]", "");
        if (flag == 0) {
            firstView.setSerialNumber(contents);
        } else if (flag == 1) {
            firstView.setFHNumber(contents);
        }
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", PICK_LOCATION_REQUEST);
        }
    }

    @Override
    public void requestDatePickerDialog(String dateString) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        view.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        firstView.setCommissionDate(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();
    }


    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> geoMastersList, int[] selectedIds, int index) {
        if (index == 1) handleFirstView(geoMastersList, selectedIds);
        else if (index == 2) handleSecondView(geoMastersList, selectedIds);
    }


    //{0, 4, 24,25}
    private void handleFirstView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String> barrelDiameter = new ArrayList<>(), material = new ArrayList<>(), type = new ArrayList<>(), districtsNames = new ArrayList<>();
        ArrayList<Integer> barrelDiameterIds = new ArrayList<>(), materialIds = new ArrayList<>(), typeIds = new ArrayList<>(), districtsNamesIds = new ArrayList<>();
        int barrelDiameterIndex, materialIndex, typeIndex, districtsNamesIndex;
        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 0) {
                barrelDiameter.add(masters.get(i).getName());
                barrelDiameterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 4) {
                material.add(masters.get(i).getName());
                materialIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 24) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 25) {
                districtsNames.add(masters.get(i).getName());
                districtsNamesIds.add(masters.get(i).getGeoMasterId());
            }
        }

        barrelDiameterIndex = returnIndex(barrelDiameterIds, selectedIds[0]);
        materialIndex = returnIndex(materialIds, selectedIds[1]);
        typeIndex = returnIndex(typeIds, selectedIds[2]);
        districtsNamesIndex = returnIndex(districtsNamesIds, selectedIds[3]);

        firstView.loadSpinnersData(barrelDiameter, barrelDiameterIds, barrelDiameterIndex,
                material, materialIds, materialIndex,
                type, typeIds, typeIndex,
                districtsNames, districtsNamesIds, districtsNamesIndex);
    }


    private int returnIndex(ArrayList<Integer> ids, int selectedId) {
        int index = 0;
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == selectedId)
                index = i;
        }

        return index;
    }

    //{3, 5, 20, 1, 16, 9}
    private void handleSecondView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String> fireHydrateStatus = new ArrayList<>(), remarks = new ArrayList<>(), fireValveType = new ArrayList<>(),
                fireHydrateBrand = new ArrayList<>(), waterSchedule = new ArrayList<>(), maintenanceArea = new ArrayList<>();
        ArrayList<Integer> fireHydrateStatusIds = new ArrayList<>(), remarksIds = new ArrayList<>(), fireValveTypeIds = new ArrayList<>(),
                fireHydrateBrandIds = new ArrayList<>(), waterScheduleIds = new ArrayList<>(), maintenanceAreaIds = new ArrayList<>();
        int fireHydrateStatusIndex, remarksIndex, fireValveTypeIndex, fireHydrateBrandIndex, waterScheduleIndex, maintenanceAreaIndex;
        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 3) {
                fireHydrateStatus.add(masters.get(i).getName());
                fireHydrateStatusIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 5) {
                remarks.add(masters.get(i).getName());
                remarksIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 20) {
                fireValveType.add(masters.get(i).getName());
                fireValveTypeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 1) {
                fireHydrateBrand.add(masters.get(i).getName());
                fireHydrateBrandIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 16) {
                waterSchedule.add(masters.get(i).getName());
                waterScheduleIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 9) {
                maintenanceArea.add(masters.get(i).getName());
                maintenanceAreaIds.add(masters.get(i).getGeoMasterId());
            }
        }


        fireHydrateStatusIndex = returnIndex(fireHydrateStatusIds, selectedIds[0]);
        remarksIndex = returnIndex(remarksIds, selectedIds[1]);
        fireValveTypeIndex = returnIndex(fireValveTypeIds, selectedIds[2]);
        fireHydrateBrandIndex = returnIndex(fireHydrateBrandIds, selectedIds[3]);
        waterScheduleIndex = returnIndex(waterScheduleIds, selectedIds[4]);
        maintenanceAreaIndex = returnIndex(maintenanceAreaIds, selectedIds[5]);

        secondView.loadSpinnersData(fireHydrateStatus, fireHydrateStatusIds, fireHydrateStatusIndex,
                remarks, remarksIds, remarksIndex,
                fireValveType, fireValveTypeIds, fireValveTypeIndex,
                fireHydrateBrand, fireHydrateBrandIds, fireHydrateBrandIndex,
                waterSchedule, waterScheduleIds, waterScheduleIndex,
                maintenanceArea, maintenanceAreaIds, maintenanceAreaIndex);
    }

    @Override
    public void onFireInsertionCalled(long flag) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.hc_edited_successfully));
            mainView.backToParent();
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_save));
        }
    }
}
