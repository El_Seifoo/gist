package com.seifoo.neomit.gisgathering.home.meter.offline;

import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.home.meter.edit.EditMeterActivity;
import com.seifoo.neomit.gisgathering.home.meter.offline.offlineMeterDetails.OfflineMeterDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class OfflineMeterPresenter implements OfflineMeterMVP.Presenter, OfflineMeterModel.VolleyCallback, OfflineMeterModel.DBCallback {
    private OfflineMeterMVP.View view;
    private OfflineMeterModel model;

    public OfflineMeterPresenter(OfflineMeterMVP.View view, OfflineMeterModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestMetersData(int type) {
        model.getMetersList(view.getAppContext(), this, type, 0);
    }

    @Override
    public void requestRemoveMeterObjById(long id, int position) {
        model.removeMeterObject(view.getActContext(), this, id, position);
    }

    private static final int SHOW_ALL_IN_MAP_REQUEST = 10;

    @Override
    public void OnListItemClickListener(int viewId, int position, MeterObject meterObject) {
        switch (viewId) {
            case R.id.more_details:
                view.navigateDestination("OfflineMeterObj", meterObject, OfflineMeterDetailsActivity.class);
                break;
            case R.id.remove_item:
                this.requestRemoveMeterObjById(meterObject.getId(), position);
                break;
            case R.id.edit_item:
                this.requestEditMeterData(position, meterObject);
                break;
            case R.id.map:
                view.navigateToMap(SHOW_ALL_IN_MAP_REQUEST, meterObject, position);
        }
    }

    private static final int EDIT_METER_OBJECT_REQUEST = 1;

    @Override
    public void requestEditMeterData(int position, MeterObject meterObject) {
        Intent intent = new Intent(view.getAppContext(), EditMeterActivity.class);
        intent.putExtra("meterObj", meterObject);
        intent.putExtra("position", position);
        view.navigateToEditMeter(intent, EDIT_METER_OBJECT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_METER_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            view.updateListItem(data.getExtras().getInt("position"), (MeterObject) data.getSerializableExtra("editedMeterObj"));
            return;
        }

        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            this.requestMetersData(view.getSpinnerSelectedPosition());
        }
    }

    @Override
    public void requestUploadData(int type) {
        model.getMetersList(view.getAppContext(), this, type, 1);
    }

    @Override
    public void showLocations(ArrayList<MeterObject> meters) {
        view.navigateToMap2(SHOW_ALL_IN_MAP_REQUEST, meters);
    }

    @Override
    public void onGetMetersListCalled(ArrayList<MeterObject> meterObjectList, int index) {
        if (index == 0) {
            if (meterObjectList.isEmpty())
                view.showEmptyListText();
            else {
                boolean isVisible = false;
                for (int i = 0; i < meterObjectList.size(); i++) {
                    if (meterObjectList.get(i).getMeterLatitude() != null && meterObjectList.get(i).getMeterLongitude() != null) {
                        if (!meterObjectList.get(i).getMeterLatitude().isEmpty() && !meterObjectList.get(i).getMeterLongitude().isEmpty()) {
                            isVisible = true;
                            break;
                        }
                    }
                }
                view.loadMetersData(meterObjectList, isVisible);
            }
        } else {
            if (meterObjectList.isEmpty())
                view.showToastMessage(view.getActContext().getString(R.string.no_meters_to_synchronize));
            else {
                view.showProgress();
                model.uploadMeterData(view.getActContext(), this, meterObjectList);
            }
        }

    }

    @Override
    public void onRemoveMeterObjectCalled(int flag, int position, long id) {
        if (flag > 0) {
            if (position == -1) {
                view.removeListItem(id);
            } else
                view.removeListItem(position);
        } else view.showToastMessage(view.getActContext().getString(R.string.failed_to_delete));
    }

    @Override
    public void onSyncingResponse(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        ArrayList<Long> accepted = new ArrayList<>(), rejected = new ArrayList<>();
        for (int i = 0; i < responseArray.length(); i++) {
            if (responseArray.getJSONObject(i).getString("status").equals("success"))
                accepted.add(responseArray.getJSONObject(i).getLong("id"));
            else rejected.add(responseArray.getJSONObject(i).getLong("id"));
        }

        if (!accepted.isEmpty()) {
            for (int i = 0; i < accepted.size(); i++) {
                model.removeMeterObject(view.getActContext(), this, accepted.get(i), -1);
            }
        }
    }

    @Override
    public void onSyncingError(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? view.getActContext().getString(R.string.invalid_username_or_password) :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
