package com.seifoo.neomit.gisgathering.home.houseConnection.getInfo.details;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.home.houseConnection.getMap.GetHcMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.meter.offline.offlineMeterDetails.OfflineDetailsAdapter;

import java.util.ArrayList;

public class GetHcInfoDetailsActivity extends AppCompatActivity implements GetHcInfoDetailsMVP.View ,OfflineDetailsAdapter.OnListItemClicked{
    private RecyclerView recyclerView;
    private OfflineDetailsAdapter adapter;

    private GetHcInfoDetailsMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_hc_info_details);

        getSupportActionBar().setTitle(getString(R.string.details));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new GetHcInfoDetailsPresenter(this, new GetHcInfoDetailsModel());

        recyclerView = (RecyclerView) findViewById(R.id.get_info_details_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineDetailsAdapter(false, this);
        presenter.requestHcData((HouseConnectionObject) getIntent().getExtras().getSerializable("GetInfo"));
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }


    @Override
    public void loadHcMoreDetails(ArrayList<MoreDetails> list) {
        adapter.setDetailsList(list);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClickListener(String latitude, String longitude) {
        Intent intent = new Intent(this, GetHcMapDetailsActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
