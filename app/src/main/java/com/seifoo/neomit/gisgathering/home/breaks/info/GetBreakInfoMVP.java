package com.seifoo.neomit.gisgathering.home.breaks.info;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;

public interface GetBreakInfoMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showToastMessage(String message);

        void showProgress();

        void hideProgress();

        void loadBreakInfo(BreakObject breakObject);
    }

    interface Presenter {
        void requestGetInfo(String breakNumber);
    }
}
