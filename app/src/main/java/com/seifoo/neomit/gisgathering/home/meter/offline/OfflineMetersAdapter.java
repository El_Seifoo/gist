package com.seifoo.neomit.gisgathering.home.meter.offline;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.home.meter.rejected.RejectedMetersActivity;

import java.util.ArrayList;

public class OfflineMetersAdapter extends RecyclerView.Adapter<OfflineMetersAdapter.Holder> {
    private ArrayList<MeterObject> metersList;
    private final MeterObjectListItemListener listItemListener;
    private boolean isOffline;

    public OfflineMetersAdapter(MeterObjectListItemListener listItemListener, boolean isOffline) {
        this.listItemListener = listItemListener;
        this.isOffline = isOffline;
    }

    public void setMetersList(ArrayList<MeterObject> metersList) {
        this.metersList = metersList;
        notifyDataSetChanged();
    }

    public void removeItem(Context context, int position) {
        metersList.remove(position);
        if (metersList.isEmpty()) {
            if (context instanceof OfflineMetersActivity)
                ((OfflineMetersActivity) context).showEmptyListText();
            else if (context instanceof RejectedMetersActivity)
                ((RejectedMetersActivity) context).showEmptyListText();
        }
        notifyDataSetChanged();
    }

    public void updateItem(int position, MeterObject editedMeterObj) {
        metersList.set(position, editedMeterObj);
        notifyDataSetChanged();
    }

    public void updateItem(Context context, long id) {
        for (int i = 0; i < metersList.size(); i++) {
            if (id == metersList.get(i).getId()) {
                metersList.remove(i);
                if (metersList.isEmpty()) {
                    if (context instanceof OfflineMetersActivity)
                        ((OfflineMetersActivity) context).showEmptyListText();
                    else if (context instanceof RejectedMetersActivity)
                        ((RejectedMetersActivity) context).showEmptyListText();
                }
                notifyDataSetChanged();
                break;
            }
        }
    }

    public void clear() {
        if (metersList != null) {
            metersList.clear();
            notifyDataSetChanged();
        }
    }

    public interface MeterObjectListItemListener {
        void onListItemClickListener(int viewId, int position, MeterObject meterObject);
    }

    @NonNull
    @Override
    public OfflineMetersAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.offline_meter_list_item, viewGroup, false));
    }

    /*
    numbers += "<font color = '#0070C0'>" + holder.itemView.getContext().getString(R.string.plate_number) + ": </font>" + dataList.get(position).getPlateNumber() + "<br>";
    holder.numbers.setText(Html.fromHtml(ss), TextView.BufferType.SPANNABLE);
     */
    @Override
    public void onBindViewHolder(@NonNull OfflineMetersAdapter.Holder holder, int position) {
        if (isOffline) {
            holder.removeItem.setVisibility(View.VISIBLE);
            holder.editItem.setVisibility(View.VISIBLE);
        } else {
            if (metersList.get(position).getReason() != null) {
                if (!metersList.get(position).getReason().isEmpty()) {
                    holder.reason.setVisibility(View.VISIBLE);
                    holder.reason.setText(
                            Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.reason) + ": </b></font>" + metersList.get(position).getReason()),
                            TextView.BufferType.SPANNABLE);
                } else holder.reason.setVisibility(View.GONE);
            } else holder.reason.setVisibility(View.GONE);


        }


        if (metersList.get(position).getMeterLatitude() != null && metersList.get(position).getMeterLongitude() != null) {
            if (!metersList.get(position).getMeterLatitude().isEmpty() && !metersList.get(position).getMeterLongitude().isEmpty()) {
                holder.map.setVisibility(View.VISIBLE);
            } else holder.map.setVisibility(View.GONE);
        } else holder.map.setVisibility(View.GONE);

        if (metersList.get(position).getMeterSerialNumber() != null) {
            if (!metersList.get(position).getMeterSerialNumber().isEmpty()) {
                holder.serialNumber.setVisibility(View.VISIBLE);
                holder.serialNumber.setText(
                        Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.serial_number) + ": </b></font>" + metersList.get(position).getMeterSerialNumber()),
                        TextView.BufferType.SPANNABLE);

            } else holder.serialNumber.setVisibility(View.GONE);
        } else holder.serialNumber.setVisibility(View.GONE);


        if (metersList.get(position).getMeterPlateNumber() != null) {
            if (!metersList.get(position).getMeterPlateNumber().isEmpty()) {
                holder.plateNumber.setVisibility(View.VISIBLE);
                holder.plateNumber.setText(
                        Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.plate_number) + ": </b></font>" + metersList.get(position).getMeterPlateNumber()),
                        TextView.BufferType.SPANNABLE);

            } else holder.plateNumber.setVisibility(View.GONE);
        } else holder.plateNumber.setVisibility(View.GONE);

        if (metersList.get(position).getMeterHcn() != null) {
            if (!metersList.get(position).getMeterHcn().isEmpty()) {
                holder.hcn.setVisibility(View.VISIBLE);
                holder.hcn.setText(
                        Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.hcn) + ": </b></font>" + metersList.get(position).getMeterHcn()),
                        TextView.BufferType.SPANNABLE);
            } else holder.hcn.setVisibility(View.GONE);
        } else holder.hcn.setVisibility(View.GONE);

        if (!metersList.get(position).getCreatedAt().isEmpty()) {
            holder.createdAt.setVisibility(View.VISIBLE);
            holder.createdAt.setText(
                    Html.fromHtml("<font color = '#0070C0'><b>" + holder.itemView.getContext().getString(R.string.created_at) + ": </b></font>" + metersList.get(position).getCreatedAt()),
                    TextView.BufferType.SPANNABLE);
        } else holder.createdAt.setVisibility(View.GONE);


    }

    @Override
    public int getItemCount() {
        return metersList != null ? metersList.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView serialNumber, plateNumber, hcn, reason, moreDetails, map, createdAt;
        ImageView removeItem, editItem;

        public Holder(@NonNull View itemView) {
            super(itemView);
            serialNumber = (TextView) itemView.findViewById(R.id.serial_number);
            plateNumber = (TextView) itemView.findViewById(R.id.plate_number);
            hcn = (TextView) itemView.findViewById(R.id.hcn);
            moreDetails = (TextView) itemView.findViewById(R.id.more_details);
            createdAt = (TextView) itemView.findViewById(R.id.created_at);
            map = (TextView) itemView.findViewById(R.id.map);

            map.setOnClickListener(this);
            moreDetails.setOnClickListener(this);
            if (isOffline) {
                removeItem = (ImageView) itemView.findViewById(R.id.remove_item);
                editItem = (ImageView) itemView.findViewById(R.id.edit_item);
                removeItem.setOnClickListener(this);
                editItem.setOnClickListener(this);
            } else {
                reason = (TextView) itemView.findViewById(R.id.reason);
            }

        }

        @Override
        public void onClick(View view) {
            listItemListener.onListItemClickListener(view.getId(), getAdapterPosition(), metersList.get(getAdapterPosition()));
        }
    }
}
