package com.seifoo.neomit.gisgathering.home.houseConnection.getInfo;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class GetHcInfoPresenter implements GetHcInfoMVP.Presenter, GetHcInfoModel.VolleyCallback {
    private GetHcInfoMVP.View view;
    private GetHcInfoModel model;

    public GetHcInfoPresenter(GetHcInfoMVP.View view, GetHcInfoModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void whichQRClicked(int id) {
        switch (id) {
            case R.id.serial_num_qr:
                requestQrCode(((GetHcInfoActivity) view.getAppContext()), view.getAppContext().getString(R.string.serial_number));
                break;
            case R.id.hcn_qr:
                requestQrCode(((GetHcInfoActivity) view.getAppContext()), view.getAppContext().getString(R.string.hcn));
                break;
        }
    }

    @Override
    public void requestQrCode(Activity activity, String flagString) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;

        if (flagString.equals(view.getAppContext().getString(R.string.hcn))) {
            flag = 1;
        }
        view.initializeScanner(integrator, flag);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents(), requestCode);
        }
    }

    @Override
    public void handleQRScannerResult(String contents, int flag) {
        contents = contents.replaceAll("[^0-9]", "");
        if (flag == 0) {
            view.setSerialNumber(contents);
        } else if (flag == 1) {
            view.setHCN(contents);
        }
    }

    @Override
    public void requestGetInfo(String serialNumber, String HCN) {
        if (serialNumber.isEmpty() && HCN.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.fill_at_least_one_field_to_continue));
            return;
        }
        view.showProgress();
        model.getInfo(view.getActContext(), this, serialNumber, HCN);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {

                HouseConnectionObject fire = new HouseConnectionObject(returnValidString(
                        data.getJSONObject(0).getString("SERIAL_NO")),
                        returnValidString(data.getJSONObject(0).getString("HCN")),
                        returnValidInt(data.getJSONObject(0).getString("F_TYPE_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("F_TYPE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("DIAMETER_ID")),
                        returnValidInt(data.getJSONObject(0).getString("MATERIAL_ID")),
                        returnValidInt(data.getJSONObject(0).getString("ASSET_STATUS_ID")),
                        returnValidInt(data.getJSONObject(0).getString("DISTRICT_NAME_ID")),
                        returnValidString(data.getJSONObject(0).getString("SUP_DISTRICT_ID")),
                        returnValidString(data.getJSONObject(0).getString("STREET_NAME")),
                        returnValidString(data.getJSONObject(0).getString("SECTOR_NAME")),
                        returnValidInt(data.getJSONObject(0).getString("WATER_SCHEDULE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("REMARKS")),
                        returnValidString(data.getJSONObject(0).getString("Y_Map")),
                        returnValidString(data.getJSONObject(0).getString("X_Map")),
                        returnValidDate1(data.getJSONObject(0).getString("CreatedDate"))
                );
                view.loadHcInfo(fire);
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_hc_not_exist));
        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_hc_not_exist));
    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
