package com.seifoo.neomit.gisgathering.home.breaks.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, EditBreakMVP.View, EditBreakMVP.SecondView {
    private EditText breakDetailsEditText, usedEquipmentEditText, dimensionDrillingEditText;
    private Spinner pipeMaterialSpinner, maintenanceCompanySpinner, maintenanceTypeSpinner, soilTypeSpinner, asphaltSpinner, maintenanceDeviceTypeSpinner, procedureSpinner;

    private EditBreakMVP.Presenter presenter;

    public static SecondFragment newInstance(BreakObject breakObj) {
        SecondFragment fragment = new SecondFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("BreakObj", breakObj);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_break_second_edit, container, false);

        presenter = new EditBreakPresenter(this, this, new EditBreakModel());

        pipeMaterialSpinner = (Spinner) view.findViewById(R.id.pipe_material_spinner);
        maintenanceCompanySpinner = (Spinner) view.findViewById(R.id.maintenance_company_spinner);
        maintenanceTypeSpinner = (Spinner) view.findViewById(R.id.maintenance_type_spinner);
        soilTypeSpinner = (Spinner) view.findViewById(R.id.soil_type_spinner);
        asphaltSpinner = (Spinner) view.findViewById(R.id.asphalt_spinner);
        maintenanceDeviceTypeSpinner = (Spinner) view.findViewById(R.id.maintenance_device_type_spinner);
        procedureSpinner = (Spinner) view.findViewById(R.id.procedure_spinner);


        breakDetailsEditText = (EditText) view.findViewById(R.id.break_details);
        usedEquipmentEditText = (EditText) view.findViewById(R.id.used_equipment);
        dimensionDrillingEditText = (EditText) view.findViewById(R.id.dimension_drilling);

        presenter.requestSecondSpinnersData((BreakObject) getArguments().getSerializable("BreakObj"));

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new BreakObject(
                pipeMaterialIds.get(pipeMaterialSpinner.getSelectedItemPosition()), breakDetailsEditText.getText().toString().trim(),
                usedEquipmentEditText.getText().toString().trim(), maintenanceCompanyIds.get(maintenanceCompanySpinner.getSelectedItemPosition()),
                maintenanceTypeIds.get(maintenanceTypeSpinner.getSelectedItemPosition()),
                soilTypeIds.get(soilTypeSpinner.getSelectedItemPosition()), asphaltIds.get(asphaltSpinner.getSelectedItemPosition()),
                dimensionDrillingEditText.getText().toString().trim(), maintenanceDeviceTypeIds.get(maintenanceDeviceTypeSpinner.getSelectedItemPosition()),
                procedureIds.get(procedureSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> pipeMaterialIds, maintenanceCompanyIds, maintenanceTypeIds, soilTypeIds, asphaltIds, maintenanceDeviceTypeIds, procedureIds;

    @Override
    public void loadSpinnersData(ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds, int pipeMaterialIndex, ArrayList<String> maintenanceCompany, ArrayList<Integer> maintenanceCompanyIds, int maintenanceCompanyIndex, ArrayList<String> maintenanceType, ArrayList<Integer> maintenanceTypeIds, int maintenanceTypeIndex, ArrayList<String> soilType, ArrayList<Integer> soilTypeIds, int soilTypeIndex, ArrayList<String> asphalt, ArrayList<Integer> asphaltIds, int asphaltIndex, ArrayList<String> maintenanceDeviceType, ArrayList<Integer> maintenanceDeviceTypeIds, int maintenanceDeviceTypeIndex, ArrayList<String> procedure, ArrayList<Integer> procedureIds, int procedureIndex) {
        this.pipeMaterialIds = pipeMaterialIds;
        this.maintenanceCompanyIds = maintenanceCompanyIds;
        this.maintenanceTypeIds = maintenanceTypeIds;
        this.soilTypeIds = soilTypeIds;
        this.asphaltIds = asphaltIds;
        this.maintenanceDeviceTypeIds = maintenanceDeviceTypeIds;
        this.procedureIds = procedureIds;

        pipeMaterialSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, pipeMaterial));
        pipeMaterialSpinner.setSelection(pipeMaterialIndex);
        maintenanceCompanySpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, maintenanceCompany));
        maintenanceCompanySpinner.setSelection(maintenanceCompanyIndex);
        maintenanceTypeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, maintenanceType));
        maintenanceTypeSpinner.setSelection(maintenanceTypeIndex);
        soilTypeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, soilType));
        soilTypeSpinner.setSelection(soilTypeIndex);
        asphaltSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, asphalt));
        asphaltSpinner.setSelection(asphaltIndex);
        maintenanceDeviceTypeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, maintenanceDeviceType));
        maintenanceDeviceTypeSpinner.setSelection(maintenanceDeviceTypeIndex);
        procedureSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, procedure));
        procedureSpinner.setSelection(procedureIndex);

    }

    @Override
    public void setData(String breakDetails, String usedEquipment, String dimensionDrilling) {
        breakDetailsEditText.setText(breakDetails);
        usedEquipmentEditText.setText(usedEquipment);
        dimensionDrillingEditText.setText(dimensionDrilling);
    }
}
