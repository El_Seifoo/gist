package com.seifoo.neomit.gisgathering.home.valve.offline;

import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

import java.util.ArrayList;

public interface OfflineValvesMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void showToastMessage(String message);

        void loadValvesData(ArrayList<ValveObject> valve, boolean isVisible);

        void removeListItem(int position);

        void navigateDestination(String key, ValveObject valve, Class destination);

        void navigateToEditValve(Intent intent, int requestCode);

        void updateListItem(int position, ValveObject valve);

        void removeListItem(long id);

        void navigateToMap(int RequestCode, ValveObject valve, int position);

        int getSpinnerSelectedPosition();

        void navigateToMap2(int requestCode, ArrayList<ValveObject> valves);
    }

    interface Presenter {
        void requestValvesData(int type);

        void requestRemoveValveObjById(long id, int position);

        void OnListItemClickListener(int viewId, int position, ValveObject valve);

        void requestEditValveData(int position, ValveObject valve);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestUploadData(int type);

        void showLocations(ArrayList<ValveObject> valves);
    }
}
