package com.seifoo.neomit.gisgathering.home.tank.info;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class GetTankInfoPresenter implements GetTankInfoMVP.Presenter, GetTankInfoModel.VolleyCallback {
    private GetTankInfoMVP.View view;
    private GetTankInfoModel model;

    public GetTankInfoPresenter(GetTankInfoMVP.View view, GetTankInfoModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestQrCode(Activity activity) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        view.initializeScanner(integrator);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                view.setSerialNumber(result.getContents().replaceAll("[^0-9]", ""));
        }
    }

    @Override
    public void requestGetInfo(String serialNumber) {
        if (serialNumber.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.serial_number_is_required));
            return;
        }
        view.showProgress();
        model.getInfo(view.getActContext(), this, serialNumber);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                /*
                "X_Map": 45.2502,
            "Y_Map": 24.2525,
            "SERIAL_NO": "1500",
            "TANK_NAME": "tanks 1",
            "FEATURE_TYPE_ID": "1",
            "ENABLED_ID": 1,
            "COMMISSION_DATE": "2019-03-31T22:20:00",
            "TANK_DIAMETER": 1,
            "ACTIVE_VOLUME": 1,
            "INACTIVE_VOLUME": 1,
            "LOW_LEVEL": 1,
            "HIGH_LEVEL": 1,
            "ASSET_STATUS_ID": 1,
            "STREET_NAME": "xyz",
            "DISTRICT_NAME_ID": 1,
            "SECTOR_NAME": "xyz",
            "REGION_ID": "1",
            "REMARKS": "qwerty",
            "LAST_UPDATED": "2019-03-31T22:20:00",
            "Reason": null,
            "CreatedDate": "2019-03-31T22:20:00"
                 */

                TankObject tank = new TankObject(
                        returnValidString(data.getJSONObject(0).getString("Y_Map")),
                        returnValidString(data.getJSONObject(0).getString("X_Map")),
                        returnValidString(data.getJSONObject(0).getString("SERIAL_NO")),
                        returnValidString(data.getJSONObject(0).getString("TANK_NAME")),
                        returnValidInt(data.getJSONObject(0).getString("FEATURE_TYPE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("ENABLED_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(0).getString("ENABLED_ID")),
                        returnValidDate(data.getJSONObject(0).getString("COMMISSION_DATE")),
                        returnValidInt(data.getJSONObject(0).getString("TANK_DIAMETER")),
                        returnValidString(data.getJSONObject(0).getString("ACTIVE_VOLUME")),
                        returnValidString(data.getJSONObject(0).getString("INACTIVE_VOLUME")),
                        returnValidString(data.getJSONObject(0).getString("LOW_LEVEL")),
                        returnValidString(data.getJSONObject(0).getString("HIGH_LEVEL")),
                        returnValidInt(data.getJSONObject(0).getString("ASSET_STATUS_ID")),
                        returnValidString(data.getJSONObject(0).getString("STREET_NAME")),
                        returnValidInt(data.getJSONObject(0).getString("DISTRICT_NAME_ID")),
                        returnValidString(data.getJSONObject(0).getString("SECTOR_NAME")),
                        returnValidString(data.getJSONObject(0).getString("REGION_ID")),
                        returnValidString(data.getJSONObject(0).getString("REMARKS")),
                        returnValidDate(data.getJSONObject(0).getString("LAST_UPDATED")),
                        returnValidDate1(data.getJSONObject(0).getString("CreatedDate")));
                view.loadInfo(tank);
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_tank_not_exist));
        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_tank_not_exist));
    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

}
