package com.seifoo.neomit.gisgathering.home.pump.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface AddPumpMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();

        String getCurrentDate();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> enabled, ArrayList<Integer> enabledIds,
                              ArrayList<String> type, ArrayList<Integer> typeIds,
                              ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator);

        void setStationCode(String data);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);
    }

    interface SecondView {
        void loadSpinnersData(ArrayList<String> districtName, ArrayList<Integer> districtNameIds,
                              ArrayList<String> subDistrict, ArrayList<Integer> subDistrictIds);

        void setLastUpdated(String date);
    }

    interface Presenter {
        void requestFirstSpinnersData();

        void requestSecondSpinnersData();

        void requestPassFirstStepDataToActivity(PumpObject pump);

        void requestPassSecondStepDataToActivity(PumpObject pump);

        void requestAddPump(PumpObject pump);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString);

    }
}
