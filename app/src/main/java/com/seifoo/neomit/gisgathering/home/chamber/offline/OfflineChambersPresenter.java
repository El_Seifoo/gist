package com.seifoo.neomit.gisgathering.home.chamber.offline;

import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.home.chamber.edit.EditChamberActivity;
import com.seifoo.neomit.gisgathering.home.chamber.offline.details.OfflineChambersDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class OfflineChambersPresenter implements OfflineChambersMVP.Presenter, OfflineChambersModel.VolleyCallback, OfflineChambersModel.DBCallback {
    private OfflineChambersMVP.View view;
    private OfflineChambersModel model;

    public OfflineChambersPresenter(OfflineChambersMVP.View view, OfflineChambersModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestChambersData(int type) {
        model.getChambersList(view.getAppContext(), this, type, 0);
    }

    @Override
    public void requestRemoveChamberObjById(long id, int position) {
        model.removeChamberObject(view.getActContext(), this, id, position);
    }

    private static final int SHOW_ALL_IN_MAP_REQUEST = 10;

    @Override
    public void OnListItemClickListener(int viewId, int position, ChamberObject chamber) {
        switch (viewId) {
            case R.id.more_details:
                view.navigateDestination("OfflineChamberObj", chamber, OfflineChambersDetailsActivity.class);
                break;
            case R.id.remove_item:
                this.requestRemoveChamberObjById(chamber.getId(), position);
                break;
            case R.id.edit_item:
                this.requestEditChamberData(position, chamber);
                break;
            case R.id.map:
                view.navigateToMap(SHOW_ALL_IN_MAP_REQUEST, chamber, position);
        }
    }

    private static final int EDIT_CHAMBER_OBJECT_REQUEST = 1;

    @Override
    public void requestEditChamberData(int position, ChamberObject chamber) {
        Intent intent = new Intent(view.getAppContext(), EditChamberActivity.class);
        intent.putExtra("ChamberObj", chamber);
        intent.putExtra("position", position);
        view.navigateToEditChamber(intent, EDIT_CHAMBER_OBJECT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_CHAMBER_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            view.updateListItem(data.getExtras().getInt("position"), (ChamberObject) data.getSerializableExtra("editedChamberObj"));
            return;
        }

        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            this.requestChambersData(view.getSpinnerSelectedPosition());
        }
    }

    @Override
    public void requestUploadData(int type) {
        model.getChambersList(view.getAppContext(), this, type, 1);
    }

    @Override
    public void showLocations(ArrayList<ChamberObject> chambers) {
        view.navigateToMap2(SHOW_ALL_IN_MAP_REQUEST, chambers);
    }

    @Override
    public void onSyncingResponse(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        JSONArray responseArray = response.getJSONArray("response");
        ArrayList<Long> accepted = new ArrayList<>(), rejected = new ArrayList<>();
        for (int i = 0; i < responseArray.length(); i++) {
            if (responseArray.getJSONObject(i).getString("status").equals("success"))
                accepted.add(responseArray.getJSONObject(i).getLong("id"));
            else rejected.add(responseArray.getJSONObject(i).getLong("id"));
        }

        if (!accepted.isEmpty()) {
            for (int i = 0; i < accepted.size(); i++) {
                model.removeChamberObject(view.getActContext(), this, accepted.get(i), -1);
            }
        }
    }

    @Override
    public void onSyncingError(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onGetChambersListCalled(ArrayList<ChamberObject> chambers, int index) {
        if (index == 0) {
            if (chambers.isEmpty())
                view.showEmptyListText();
            else {
                boolean isVisible = false;
                for (int i = 0; i < chambers.size(); i++) {
                    if (chambers.get(i).getLatitude() != null && chambers.get(i).getLongitude() != null) {
                        if (!chambers.get(i).getLatitude().isEmpty() && !chambers.get(i).getLongitude().isEmpty()) {
                            isVisible = true;
                            break;
                        }
                    }
                }
                view.loadChambersData(chambers,isVisible);
            }
        } else {
            if (chambers.isEmpty())
                view.showToastMessage(view.getActContext().getString(R.string.no_chambers_to_synchronize));
            else {
                view.showProgress();
                model.uploadValvesData(view.getActContext(), this, chambers);
            }
        }
    }

    @Override
    public void onRemoveChamberObjectCalled(int flag, int position, long id) {
        if (flag > 0) {
            if (position == -1) {
                view.removeListItem(id);
            } else
                view.removeListItem(position);
        } else view.showToastMessage(view.getActContext().getString(R.string.failed_to_delete));
    }
}
