package com.seifoo.neomit.gisgathering.home.mainLine.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.HashMap;

public class FirstFragment extends Fragment implements Step, EditMainLineMVP.View, EditMainLineMVP.FirstView {
    private Spinner diameterSpinner, typeSpinner, enabledSpinner, materialSpinner;
    private EditText latLngEditText, serialNumberEditText, streetNameEditText, streetNumberEditText;

    private EditMainLineMVP.Presenter presenter;

    public static FirstFragment newInstance(MainLineObject mainLine) {
        FirstFragment fragment = new FirstFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("MainLineObj", mainLine);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_line_first_edit, container, false);

        presenter = new EditMainLinePresenter(this, this, new EditMainLineModel());

        latLngEditText = (EditText) view.findViewById(R.id.location);
        serialNumberEditText = (EditText) view.findViewById(R.id.serial_num);
        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        streetNumberEditText = (EditText) view.findViewById(R.id.street_num);

        diameterSpinner = (Spinner) view.findViewById(R.id.diameter_spinner);
        typeSpinner = (Spinner) view.findViewById(R.id.type_spinner);
        enabledSpinner = (Spinner) view.findViewById(R.id.enabled_spinner);
        materialSpinner = (Spinner) view.findViewById(R.id.material_spinner);

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(latLngEditText.getText().toString().trim());
            }
        });

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.serial_num_qr)));

        presenter.requestFirstSpinnersData((MainLineObject) getArguments().getSerializable("MainLineObj"));

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFirstStepDataToActivity(new MainLineObject(latLngEditText.getText().toString().trim(), latLngEditText.getText().toString().trim(),
                serialNumberEditText.getText().toString().trim(), diameterIds.get(diameterSpinner.getSelectedItemPosition()),
                typeIds.get(typeSpinner.getSelectedItemPosition()), enabledIds.get(enabledSpinner.getSelectedItemPosition()),
                materialIds.get(materialSpinner.getSelectedItemPosition()), streetNameEditText.getText().toString().trim(),
                streetNumberEditText.getText().toString().trim()));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    ArrayList<Integer> diameterIds, typeIds, enabledIds, materialIds;

    @Override
    public void loadSpinnersData(ArrayList<String> diameter, ArrayList<Integer> diameterIds, int diameterIndex, ArrayList<String> type, ArrayList<Integer> typeIds, int typeIndex, ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex, ArrayList<String> material, ArrayList<Integer> materialIds, int materialIndex) {
        this.diameterIds = diameterIds;
        this.typeIds = typeIds;
        this.enabledIds = enabledIds;
        this.materialIds = materialIds;

        diameterSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameter));
        diameterSpinner.setSelection(diameterIndex);
        typeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, type));
        typeSpinner.setSelection(typeIndex);
        enabledSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, enabled));
        enabledSpinner.setSelection(enabledIndex);
        materialSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, material));
        materialSpinner.setSelection(materialIndex);

    }

    @Override
    public void showLocationError(String errorMessage) {
        latLngEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((EditMainLineActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, 1);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("latLng", latLngs);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void setLocation(String location) {
        latLngEditText.setText(location);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFirstViewActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setData(String streetName, String streetNumber) {
        streetNameEditText.setText(streetName);
        streetNumberEditText.setText(streetNumber);
    }
}

