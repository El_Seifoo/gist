package com.seifoo.neomit.gisgathering.subHome;

import android.view.MenuItem;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.add.AddBreakActivity;
import com.seifoo.neomit.gisgathering.home.breaks.info.GetBreakInfoActivity;
import com.seifoo.neomit.gisgathering.home.breaks.map.GetBreakMapActivity;
import com.seifoo.neomit.gisgathering.home.breaks.offline.OfflineBreaksActivity;
import com.seifoo.neomit.gisgathering.home.breaks.rejected.RejectedBreaksActivity;
import com.seifoo.neomit.gisgathering.home.chamber.add.AddChamberActivity;
import com.seifoo.neomit.gisgathering.home.chamber.info.GetChamberInfoActivity;
import com.seifoo.neomit.gisgathering.home.chamber.map.GetChamberMapActivity;
import com.seifoo.neomit.gisgathering.home.chamber.offline.OfflineChambersActivity;
import com.seifoo.neomit.gisgathering.home.chamber.rejected.RejectedChambersActivity;
import com.seifoo.neomit.gisgathering.home.fireHydrant.add.AddFireHydrantActivity;
import com.seifoo.neomit.gisgathering.home.fireHydrant.getInfo.GetFireInfoActivity;
import com.seifoo.neomit.gisgathering.home.fireHydrant.getMap.GetFireMapActivity;
import com.seifoo.neomit.gisgathering.home.fireHydrant.offline.OfflineFireHydrantActivity;
import com.seifoo.neomit.gisgathering.home.fireHydrant.rejected.RejectedFireHydrantActivity;
import com.seifoo.neomit.gisgathering.home.houseConnection.add.AddHCActivity;
import com.seifoo.neomit.gisgathering.home.houseConnection.edit.EditHCActivity;
import com.seifoo.neomit.gisgathering.home.houseConnection.getInfo.GetHcInfoActivity;
import com.seifoo.neomit.gisgathering.home.houseConnection.getMap.GetHcMapActivity;
import com.seifoo.neomit.gisgathering.home.houseConnection.offline.OfflineHcsActivity;
import com.seifoo.neomit.gisgathering.home.houseConnection.rejected.RejectedHcsActivity;
import com.seifoo.neomit.gisgathering.home.mainLine.add.AddMainLineActivity;
import com.seifoo.neomit.gisgathering.home.mainLine.info.GetMainLineInfoActivity;
import com.seifoo.neomit.gisgathering.home.mainLine.map.GetMainLineMapActivity;
import com.seifoo.neomit.gisgathering.home.mainLine.offline.OfflineMainLineActivity;
import com.seifoo.neomit.gisgathering.home.mainLine.rejected.RejectedMainLinesActivity;
import com.seifoo.neomit.gisgathering.home.meter.add.AddMeterActivity;
import com.seifoo.neomit.gisgathering.home.meter.getInfo.GetMeterInfoActivity;
import com.seifoo.neomit.gisgathering.home.meter.getMap.GetMeterMapActivity;
import com.seifoo.neomit.gisgathering.home.meter.offline.OfflineMetersActivity;
import com.seifoo.neomit.gisgathering.home.meter.rejected.RejectedMetersActivity;
import com.seifoo.neomit.gisgathering.home.pump.add.AddPumpActivity;
import com.seifoo.neomit.gisgathering.home.pump.info.GetPumpInfoActivity;
import com.seifoo.neomit.gisgathering.home.pump.map.GetPumpMapActivity;
import com.seifoo.neomit.gisgathering.home.pump.offline.OfflinePumpsActivity;
import com.seifoo.neomit.gisgathering.home.pump.rejected.RejectedPumpsActivity;
import com.seifoo.neomit.gisgathering.home.sensors.add.AddSensorActivity;
import com.seifoo.neomit.gisgathering.home.sensors.info.GetSensorInfoActivity;
import com.seifoo.neomit.gisgathering.home.sensors.map.GetSensorMapActivity;
import com.seifoo.neomit.gisgathering.home.sensors.offline.OfflineSensorsActivity;
import com.seifoo.neomit.gisgathering.home.sensors.rejected.RejectedSensorsActivity;
import com.seifoo.neomit.gisgathering.home.tank.add.AddTankActivity;
import com.seifoo.neomit.gisgathering.home.tank.info.GetTankInfoActivity;
import com.seifoo.neomit.gisgathering.home.tank.map.GetTankMapActivity;
import com.seifoo.neomit.gisgathering.home.tank.offline.OfflineTanksActivity;
import com.seifoo.neomit.gisgathering.home.tank.rejected.RejectedTanksActivity;
import com.seifoo.neomit.gisgathering.home.valve.add.AddValveActivity;
import com.seifoo.neomit.gisgathering.home.valve.info.GetValveInfoActivity;
import com.seifoo.neomit.gisgathering.home.valve.map.GetValveMapActivity;
import com.seifoo.neomit.gisgathering.home.valve.offline.OfflineValvesActivity;
import com.seifoo.neomit.gisgathering.home.valve.rejected.RejectedValvesActivity;

import java.util.ArrayList;

public class SubHomePresenter implements SubHomeMVP.Presenter {
    private SubHomeMVP.View view;

    public SubHomePresenter(SubHomeMVP.View view) {
        this.view = view;
    }

    @Override
    public void requestListData() {
        ArrayList<Integer> imgs = new ArrayList<>();
        imgs.add(R.mipmap.add);
        imgs.add(R.mipmap.upload);
        imgs.add(R.mipmap.get_info);
        imgs.add(R.mipmap.map);
        imgs.add(R.mipmap.rejected);
        imgs.add(R.mipmap.home);

        String[] titles = view.getBContext().getResources().getStringArray(R.array.sub_home_list_titles);

        view.loadListData(imgs, titles);
    }

    @Override
    public void onItemClicked(int position, int whichFeature) {
        switch (position) {
            case 0:
                if (whichFeature == 0) {
                    view.navigateToDestination(AddMeterActivity.class);
                } else if (whichFeature == 1) {
                    view.navigateToDestination(AddMainLineActivity.class);
                } else if (whichFeature == 2) {
                    view.navigateToDestination(AddHCActivity.class);
                }  else if (whichFeature == 3) {
                    view.navigateToDestination(AddSensorActivity.class);
                } else if (whichFeature == 4) {
                    view.navigateToDestination(AddValveActivity.class);
                } else if (whichFeature == 5) {
                    view.navigateToDestination(AddFireHydrantActivity.class);
                }else if (whichFeature == 6) {
                    view.navigateToDestination(AddChamberActivity.class);
                }else if (whichFeature == 7) {
                    view.navigateToDestination(AddTankActivity.class);
                }else if (whichFeature == 8) {
                    view.navigateToDestination(AddPumpActivity.class);
                }else if (whichFeature == 9) {
                    view.navigateToDestination(AddBreakActivity.class);
                }
                break;
            case 1:
                if (whichFeature == 0) {
                    view.navigateToDestination(OfflineMetersActivity.class);
                } else if (whichFeature == 1) {
                    view.navigateToDestination(OfflineMainLineActivity.class);
                } else if (whichFeature == 2) {
                    view.navigateToDestination(OfflineHcsActivity.class);
                }  else if (whichFeature == 3) {
                    view.navigateToDestination(OfflineSensorsActivity.class);
                } else if (whichFeature == 4) {
                    view.navigateToDestination(OfflineValvesActivity.class);
                } else if (whichFeature == 5) {
                    view.navigateToDestination(OfflineFireHydrantActivity.class);
                }else if (whichFeature == 6) {
                    view.navigateToDestination(OfflineChambersActivity.class);
                }else if (whichFeature == 7) {
                    view.navigateToDestination(OfflineTanksActivity.class);
                }else if (whichFeature == 8) {
                    view.navigateToDestination(OfflinePumpsActivity.class);
                }else if (whichFeature == 9) {
                    view.navigateToDestination(OfflineBreaksActivity.class);
                }
                break;
            case 2:
                if (whichFeature == 0) {
                    view.navigateToDestination(GetMeterInfoActivity.class);
                } else if (whichFeature == 1) {
                    view.navigateToDestination(GetMainLineInfoActivity.class);
                } else if (whichFeature == 2) {
                    view.navigateToDestination(GetHcInfoActivity.class);
                }else if (whichFeature == 3) {
                    view.navigateToDestination(GetSensorInfoActivity.class);
                } else if (whichFeature == 4) {
                    view.navigateToDestination(GetValveInfoActivity.class);
                } else if (whichFeature == 5) {
                    view.navigateToDestination(GetFireInfoActivity.class);
                }else if (whichFeature == 6) {
                    view.navigateToDestination(GetChamberInfoActivity.class);
                }else if (whichFeature == 7) {
                    view.navigateToDestination(GetTankInfoActivity.class);
                }else if (whichFeature == 8) {
                    view.navigateToDestination(GetPumpInfoActivity.class);
                }else if (whichFeature == 9) {
                    view.navigateToDestination(GetBreakInfoActivity.class);
                }
                break;
            case 3:
                if (whichFeature == 0) {
                    view.navigateToDestination(GetMeterMapActivity.class);
                } else if (whichFeature == 1) {
                    view.navigateToDestination(GetMainLineMapActivity.class);
                } else if (whichFeature == 2) {
                    view.navigateToDestination(GetHcMapActivity.class);
                }else if (whichFeature == 3) {
                    view.navigateToDestination(GetSensorMapActivity.class);
                } else if (whichFeature == 4) {
                    view.navigateToDestination(GetValveMapActivity.class);
                } else if (whichFeature == 5) {
                    view.navigateToDestination(GetFireMapActivity.class);
                }else if (whichFeature == 6) {
                    view.navigateToDestination(GetChamberMapActivity.class);
                } else if (whichFeature == 7) {
                    view.navigateToDestination(GetTankMapActivity.class);
                }else if (whichFeature == 8) {
                    view.navigateToDestination(GetPumpMapActivity.class);
                }else if (whichFeature == 9) {
                    view.navigateToDestination(GetBreakMapActivity.class);
                }
                break;
            case 4:
                if (whichFeature == 0) {
                    view.navigateToDestination(RejectedMetersActivity.class);
                } else if (whichFeature == 1) {
                    view.navigateToDestination(RejectedMainLinesActivity.class);
                } else if (whichFeature == 2) {
                    view.navigateToDestination(RejectedHcsActivity.class);
                } else if (whichFeature == 3) {
                    view.navigateToDestination(RejectedSensorsActivity.class);
                } else if (whichFeature == 4) {
                    view.navigateToDestination(RejectedValvesActivity.class);
                } else if (whichFeature == 5) {
                    view.navigateToDestination(RejectedFireHydrantActivity.class);
                }else if (whichFeature == 6) {
                    view.navigateToDestination(RejectedChambersActivity.class);
                }else if (whichFeature == 7) {
                    view.navigateToDestination(RejectedTanksActivity.class);
                }else if (whichFeature == 8) {
                    view.navigateToDestination(RejectedPumpsActivity.class);
                }else if (whichFeature == 9) {
                    view.navigateToDestination(RejectedBreaksActivity.class);
                }
                break;
            case 5:
                view.backHome();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            return view.backHome();
        }
        return ((SubHomeActivity) view.getBContext()).onOptionsItemSelected(item);
    }
}
