package com.seifoo.neomit.gisgathering.home.breaks.add;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, AddBreakMVP.View, AddBreakMVP.SecondView {
    private EditText breakDetailsEditText, usedEquipmentEditText, dimensionDrillingEditText;
    private Spinner pipeMaterialSpinner, maintenanceCompanySpinner, maintenanceTypeSpinner, soilTypeSpinner, asphaltSpinner, maintenanceDeviceTypeSpinner, procedureSpinner;

    private AddBreakMVP.Presenter presenter;

    public static SecondFragment newInstance() {
        return new SecondFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_break_second, container, false);

        presenter = new AddBreakPresenter(this, this, new AddBreakModel());

        pipeMaterialSpinner = (Spinner) view.findViewById(R.id.pipe_material_spinner);
        maintenanceCompanySpinner = (Spinner) view.findViewById(R.id.maintenance_company_spinner);
        maintenanceTypeSpinner = (Spinner) view.findViewById(R.id.maintenance_type_spinner);
        soilTypeSpinner = (Spinner) view.findViewById(R.id.soil_type_spinner);
        asphaltSpinner = (Spinner) view.findViewById(R.id.asphalt_spinner);
        maintenanceDeviceTypeSpinner = (Spinner) view.findViewById(R.id.maintenance_device_type_spinner);
        procedureSpinner = (Spinner) view.findViewById(R.id.procedure_spinner);


        breakDetailsEditText = (EditText) view.findViewById(R.id.break_details);
        usedEquipmentEditText = (EditText) view.findViewById(R.id.used_equipment);
        dimensionDrillingEditText = (EditText) view.findViewById(R.id.dimension_drilling);

        presenter.requestSecondSpinnersData();

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new BreakObject(
                pipeMaterialIds.get(pipeMaterialSpinner.getSelectedItemPosition()), breakDetailsEditText.getText().toString().trim(),
                usedEquipmentEditText.getText().toString().trim(), maintenanceCompanyIds.get(maintenanceCompanySpinner.getSelectedItemPosition()),
                maintenanceTypeIds.get(maintenanceTypeSpinner.getSelectedItemPosition()),
                soilTypeIds.get(soilTypeSpinner.getSelectedItemPosition()), asphaltIds.get(asphaltSpinner.getSelectedItemPosition()),
                dimensionDrillingEditText.getText().toString().trim(), maintenanceDeviceTypeIds.get(maintenanceDeviceTypeSpinner.getSelectedItemPosition()),
                procedureIds.get(procedureSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> pipeMaterialIds, maintenanceCompanyIds, maintenanceTypeIds, soilTypeIds, asphaltIds, maintenanceDeviceTypeIds, procedureIds;

    @Override
    public void loadSpinnersData(ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds,
                                 ArrayList<String> maintenanceCompany, ArrayList<Integer> maintenanceCompanyIds,
                                 ArrayList<String> maintenanceType, ArrayList<Integer> maintenanceTypeIds,
                                 ArrayList<String> soilType, ArrayList<Integer> soilTypeIds,
                                 ArrayList<String> asphalt, ArrayList<Integer> asphaltIds,
                                 ArrayList<String> maintenanceDeviceType, ArrayList<Integer> maintenanceDeviceTypeIds,
                                 ArrayList<String> procedure, ArrayList<Integer> procedureIds) {
        this.pipeMaterialIds = pipeMaterialIds;
        this.maintenanceCompanyIds = maintenanceCompanyIds;
        this.maintenanceTypeIds = maintenanceTypeIds;
        this.soilTypeIds = soilTypeIds;
        this.asphaltIds = asphaltIds;
        this.maintenanceDeviceTypeIds = maintenanceDeviceTypeIds;
        this.procedureIds = procedureIds;

        pipeMaterialSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, pipeMaterial));
        maintenanceCompanySpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, maintenanceCompany));
        maintenanceTypeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, maintenanceType));
        soilTypeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, soilType));
        asphaltSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, asphalt));
        maintenanceDeviceTypeSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, maintenanceDeviceType));
        procedureSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, procedure));

    }
}
