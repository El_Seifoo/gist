package com.seifoo.neomit.gisgathering.home.breaks.info.details;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class GetBreakInfoDetailsPresenter implements GetBreakInfoDetailsMVP.Presenter,GetBreakInfoDetailsModel.DBCallback{
    private GetBreakInfoDetailsMVP.View view;
    private GetBreakInfoDetailsModel model;

    public GetBreakInfoDetailsPresenter(GetBreakInfoDetailsMVP.View view, GetBreakInfoDetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestBreakDetails(BreakObject breakObj) {
        // pipeType 30,  districtName 25 ,  subZone 28 ,  diameter 0
        // pipeMaterial 18,  maintenanceCompany 35,  maintenanceType 31 , soilType 32 ,  asphalt 36,  maintenanceDeviceType 33, procedure 34,
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();

        ids.add(breakObj.getPipeType());
        types.add(30);
        ids.add(breakObj.getDistrictName());
        types.add(25);
        ids.add(breakObj.getSubZone());
        types.add(28);
        ids.add(breakObj.getDiameter());
        types.add(0);
        ids.add(breakObj.getPipeMaterial());
        types.add(18);
        ids.add(breakObj.getMaintenanceCompany());
        types.add(35);
        ids.add(breakObj.getMaintenanceType());
        types.add(31);
        ids.add(breakObj.getSoilType());
        types.add(32);
        ids.add(breakObj.getAsphalt());
        types.add(36);
        ids.add(breakObj.getMaintenanceDeviceType());
        types.add(33);
        ids.add(breakObj.getProcedure());
        types.add(34);

        model.returnValidGeoMaster(view.getAppContext(), this, breakObj, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));
    }

    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, BreakObject breakObj) {
        ArrayList<MoreDetails> moreDetails = new ArrayList<>();

        if (!breakObj.getLatitude().isEmpty() && !breakObj.getLongitude().isEmpty())
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), breakObj.getLatitude() + "," + breakObj.getLongitude()));
        else
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), ""));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.break_number), breakObj.getBreakNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.pipe_type), strings.get(0)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(1)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sub_district), strings.get(2)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.building_num), breakObj.getBuildingNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.pipe_number), breakObj.getPipeNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_name), breakObj.getStreetName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.break_date), breakObj.getBreakDate()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.diameter), strings.get(3)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.pipe_material), strings.get(4)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.break_details), breakObj.getBreakDetails()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.used_equipment), breakObj.getUsedEquipment()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.maintenance_company), strings.get(5)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.maintenance_type), strings.get(6)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.soil_type), strings.get(7)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.asphalt), strings.get(8)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.dimension_drilling), breakObj.getDimensionDrilling()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.maintenance_device_type), strings.get(9)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.procedure), strings.get(10)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.created_at), breakObj.getCreatedAt()));
        if (breakObj.getPhotos() != null) {
            if (breakObj.getPhotos().containsKey("beforeImage") && breakObj.getPhotos().get("beforeImage") != null)
                moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.before_breaking), breakObj.getPhotos().get("beforeImage")));
            if (breakObj.getPhotos().containsKey("afterImage") && breakObj.getPhotos().get("afterImage") != null)
                moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.after_breaking), breakObj.getPhotos().get("afterImage")));
        }

        view.loadBreakMoreDetails(moreDetails);
    }
}
