package com.seifoo.neomit.gisgathering.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.login.LoginActivity;
import com.seifoo.neomit.gisgathering.subHome.SubHomeActivity;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity implements HomeMVP.View {
    private GridView gridView;
    private CustomArrayAdapter adapter;

    private HomeMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MySingleton.getmInstance(getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, getString(R.string.default_language_value))
                .equals(getString(R.string.arabic_language_value)))
            setLocale("ar", this);
        else setLocale("en", this);
        setContentView(R.layout.activity_home);

        getSupportActionBar().setTitle(getString(R.string.home) + " (" + MySingleton.getmInstance(this).getStringSharedPref(Constants.API_DB_NAME, "") + ")");

        presenter = new HomePresenter(this);

        gridView = (GridView) findViewById(R.id.grid_view);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                presenter.onItemClicked(position);
            }
        });

//        Log.e("db name", MySingleton.getmInstance(this).getStringSharedPref(Constants.API_DB_NAME, "aaaa"));
        presenter.requestListData();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void loadListData(ArrayList<Integer> imgs, String[] titles) {
        adapter = new CustomArrayAdapter(HomeActivity.this, imgs, titles);
        gridView.setAdapter(adapter);

    }

    @Override
    public void navigateToDestination(int position) {
        Intent subHomeIntent = new Intent(HomeActivity.this, SubHomeActivity.class);
        subHomeIntent.putExtra("Position", position);
        startActivity(subHomeIntent);
    }

    @Override
    public void navigateLogin() {
        Intent loginIntent = new Intent(getAppContext(), LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_language) {
            if (MySingleton.getmInstance(getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, getString(R.string.default_language_value))
                    .equals(getString(R.string.arabic_language_value)))
                MySingleton.getmInstance(this).saveStringSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_language_value));
            else
                MySingleton.getmInstance(this).saveStringSharedPref(Constants.APP_LANGUAGE, getString(R.string.arabic_language_value));
            finish();
            startActivity(getIntent());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setLocale(String lang, Context context) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            getApplicationContext().getResources().updateConfiguration(config, null);
        } else {
            config.locale = locale;
            getApplicationContext().getResources().updateConfiguration(config, null);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale);
            config.setLayoutDirection(locale);
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            context.createConfigurationContext(config);
        } else {
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
    }

}
