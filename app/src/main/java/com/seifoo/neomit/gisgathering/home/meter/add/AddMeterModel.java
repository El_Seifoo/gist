package com.seifoo.neomit.gisgathering.home.meter.add;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class AddMeterModel {

    public void getGeoMasters(Context context, DBCallback callback, int[] types, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))), index);
    }

    public void insertMeterObj(Context context, DBCallback callback, MeterObject meterObject) {
        callback.onMeterInsertionCalled(DataBaseHelper.getmInstance(context).insertMeterObject(meterObject));
    }

    protected void getMetersLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetMetersLocationCalled(DataBaseHelper.getmInstance(context).getMetersLocation(), prevLatLng);
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> geoMastersList, int index);

        void onMeterInsertionCalled(long flag);

        void onGetMetersLocationCalled(ArrayList<HashMap<String,String>> metersLocation, String prevLatLng);
    }
}
