package com.seifoo.neomit.gisgathering.home.tank.info.details;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.meter.offline.offlineMeterDetails.OfflineDetailsAdapter;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.home.tank.map.GetTankMapDetailsActivity;

import java.util.ArrayList;

public class GetTankInfoDetailsActivity extends AppCompatActivity implements GetTankInfoDetailsMVP.View, OfflineDetailsAdapter.OnListItemClicked {
    private RecyclerView recyclerView;
    private OfflineDetailsAdapter adapter;

    private GetTankInfoDetailsMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_tank_info_details);

        getSupportActionBar().setTitle(getString(R.string.details));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new GetTankInfoDetailsPresenter(this, new GetTankInfoDetailsModel());

        recyclerView = (RecyclerView) findViewById(R.id.get_info_details_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineDetailsAdapter(false, this);
        presenter.requestData((TankObject) getIntent().getExtras().getSerializable("GetInfo"));
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }


    @Override
    public void loadMoreDetails(ArrayList<MoreDetails> list) {
        adapter.setDetailsList(list);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onItemClickListener(String latitude, String longitude) {
        Intent intent = new Intent(this, GetTankMapDetailsActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
