package com.seifoo.neomit.gisgathering.home.chamber.add;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.home.meter.map.PickLocationActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.HashMap;

public class FirstFragment extends Fragment implements Step, AddChamberMVP.View, AddChamberMVP.FirstView {
    private EditText latLngEditText, chNumberEditText, dmaZoneEditText, lengthEditText, widthEditText, depthEditText, depthUnderPipeEditText, depthAbovePipeEditText;
    private Spinner pipeMaterialSpinner, diameterSpinner, districtNameSpinner;

    private AddChamberMVP.Presenter presenter;

    public static FirstFragment newInstance() {
        return new FirstFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chamber_first, container, false);

        presenter = new AddChamberPresenter(this, this, new AddChamberModel());

        pipeMaterialSpinner = (Spinner) view.findViewById(R.id.pipe_material_spinner);
        diameterSpinner = (Spinner) view.findViewById(R.id.diameter_spinner);
        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);

        latLngEditText = (EditText) view.findViewById(R.id.location);
        chNumberEditText = (EditText) view.findViewById(R.id.ch_num);
        dmaZoneEditText = (EditText) view.findViewById(R.id.dma_zone);
        lengthEditText = (EditText) view.findViewById(R.id.length);
        widthEditText = (EditText) view.findViewById(R.id.width);
        depthEditText = (EditText) view.findViewById(R.id.depth);
        depthUnderPipeEditText = (EditText) view.findViewById(R.id.depth_under_pipe);
        depthAbovePipeEditText = (EditText) view.findViewById(R.id.depth_above_pipe);

        Button pickLocationBtn = (Button) view.findViewById(R.id.location_btn);
        pickLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestPickLocation(latLngEditText.getText().toString().trim());
            }
        });

        presenter.handleQRCodeImages(((ImageView) view.findViewById(R.id.ch_num_qr)));


        presenter.requestFirstSpinnersData();

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassFirstStepDataToActivity(new ChamberObject(latLngEditText.getText().toString().trim(),
                latLngEditText.getText().toString().trim(), pipeMaterialIds.get(pipeMaterialSpinner.getSelectedItemPosition()),
                chNumberEditText.getText().toString().trim(), lengthEditText.getText().toString().trim(),
                widthEditText.getText().toString().trim(), depthEditText.getText().toString().trim(),
                diameterIds.get(diameterSpinner.getSelectedItemPosition()), depthUnderPipeEditText.getText().toString().trim(),
                depthAbovePipeEditText.getText().toString().trim(),
                districtNameIds.get(districtNameSpinner.getSelectedItemPosition()), dmaZoneEditText.getText().toString().trim()));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> pipeMaterialIds, diameterIds, districtNameIds;

    @Override
    public void loadSpinnersData(ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds,
                                 ArrayList<String> diameter, ArrayList<Integer> diameterIds,
                                 ArrayList<String> districtName, ArrayList<Integer> districtNameIds) {

        this.pipeMaterialIds = pipeMaterialIds;
        this.diameterIds = diameterIds;
        this.districtNameIds = districtNameIds;

        pipeMaterialSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, pipeMaterial));
        diameterSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, diameter));
        districtNameSpinner.setAdapter(new ArrayAdapter<>(getAppContext(), android.R.layout.simple_spinner_item, districtName));

    }

    @Override
    public void showLocationError(String errorMessage) {
        latLngEditText.setError(errorMessage);
        Toast.makeText(getAppContext(), errorMessage, Toast.LENGTH_LONG).show();
        ((AddChamberActivity) getAppContext()).changeStepperPosition(-1);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, 1);
    }

    @Override
    public void setCHN(String data) {
        chNumberEditText.setText(data);
    }

    @Override
    public void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode) {
        Intent intent = new Intent(getAppContext(), PickLocationActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        intent.putExtra("latLng", latLngs);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void setLocation(String location) {
        latLngEditText.setText(location);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onFirstViewActivityResult(requestCode, resultCode, data);
    }
}
