package com.seifoo.neomit.gisgathering.home.chamber.rejected.edit;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class EditRejectedChambersPresenter implements EditRejectedChambersMVP.Presenter, EditRejectedChambersModel.DBCallback {
    private EditRejectedChambersMVP.View view;
    private EditRejectedChambersMVP.MainView mainView;
    private EditRejectedChambersMVP.FirstView firstView;
    private EditRejectedChambersMVP.SecondView secondView;
    private EditRejectedChambersModel model;

    public EditRejectedChambersPresenter(EditRejectedChambersMVP.View view, EditRejectedChambersMVP.MainView mainView, EditRejectedChambersModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public EditRejectedChambersPresenter(EditRejectedChambersMVP.View view, EditRejectedChambersMVP.FirstView firstView, EditRejectedChambersModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public EditRejectedChambersPresenter(EditRejectedChambersMVP.View view, EditRejectedChambersMVP.SecondView secondView, EditRejectedChambersModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }


    @Override
    public void requestFirstSpinnersData(ChamberObject chamber) {
        // pipeMaterial 4, diameter 0 , districtName 25
        model.getGeoMasters(view.getAppContext(), this, new int[]{4, 0, 25},
                new int[]{chamber.getPipeMaterial(), chamber.getDiameter(), chamber.getDistrictName()},
                1);

        firstView.setCHN(chamber.getChNumber());
        firstView.setData(chamber.getLength(), chamber.getWidth(), chamber.getDepth(), chamber.getDepthUnderPipe(), chamber.getDepthAbovePipe(), chamber.getDmaZone());
        firstView.setLocation(chamber.getLatitude() + "," + chamber.getLongitude());
    }

    @Override
    public void requestSecondSpinnersData(ChamberObject chamber) {
        //  subDistrictName 28 , diameterAirValve 0 ,  diameterWAValve 0 , diameterISOValve 0 ,  diameterFlowMeter 0 , type 24,  remarks 5
        model.getGeoMasters(view.getAppContext(), this, new int[]{28, 0, 0, 0, 0, 24, 5},
                new int[]{chamber.getSubDistrictName(), chamber.getDiameterAirValve(), chamber.getDiameterWAValve(), chamber.getDiameterISOValve(), chamber.getDiameterFlowMeter(), chamber.getType(), chamber.getRemarks()},
                2);

        secondView.setLastUpdated(chamber.getUpdatedDate());
        secondView.setData(chamber.getStreetName(), chamber.getStreetNumber(), chamber.getSubName(), chamber.getSectorName(), chamber.getImageNumber());
    }

    @Override
    public void requestPassFirstStepDataToActivity(ChamberObject chamber) {
        if (!chamber.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        chamber.setLatitude(chamber.getLatitude().split(",")[0]);
        chamber.setLongitude(chamber.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof EditRejectedChambersActivity) {
            ((EditRejectedChambersActivity) view.getAppContext()).passFirstObj(chamber);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(ChamberObject chamber) {
        if (view.getAppContext() instanceof EditRejectedChambersActivity) {
            ((EditRejectedChambersActivity) view.getAppContext()).passSecondObj(chamber);
        }
    }

    @Override
    public void requestEditRejectedChamber(ChamberObject chamber, String createdAt, long id) {
        chamber.setCreatedAt(returnValidNumbers(createdAt));
        chamber.setChamberId(id);
        model.insertChamberObj(view.getAppContext(), this, chamber);
    }

    private String returnValidNumbers(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
                    break;
            }
        }

        return rslt;
    }

    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQrCode(((EditRejectedChambersActivity) view.getAppContext()));
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        firstView.initializeScanner(integrator);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents());
        }
    }

    @Override
    public void handleQRScannerResult(String contents) {
        firstView.setCHN(contents.replaceAll("[^0-9]", ""));
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", PICK_LOCATION_REQUEST);
        }
    }

    public void requestDatePickerDialog(String dateString) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        view.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        secondView.setLastUpdated(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();

    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index) {
        if (index == 1) handleFirstView(masters, selectedIds);
        else if (index == 2) handleSecondView(masters, selectedIds);
    }

    private void handleFirstView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                pipeMaterial = new ArrayList<>(),
                diameter = new ArrayList<>(),
                districtName = new ArrayList<>();


        ArrayList<Integer>
                pipeMaterialIds = new ArrayList<>(),
                diameterIds = new ArrayList<>(),
                districtNameIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 4) {
                pipeMaterial.add(masters.get(i).getName());
                pipeMaterialIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                diameter.add(masters.get(i).getName());
                diameterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 25) {
                districtName.add(masters.get(i).getName());
                districtNameIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int pipeMaterialIndex = returnIndex(pipeMaterialIds, selectedIds[0]);
        int diameterIndex = returnIndex(diameterIds, selectedIds[1]);
        int districtNameIndex = returnIndex(districtNameIds, selectedIds[2]);


        firstView.loadSpinnersData(pipeMaterial, pipeMaterialIds, pipeMaterialIndex,
                diameter, diameterIds, diameterIndex,
                districtName, districtNameIds, districtNameIndex);
    }

    private void handleSecondView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                subDistrictName = new ArrayList<>(),
                diameterAirValve = new ArrayList<>(),
                diameterWAValve = new ArrayList<>(),
                diameterISOValve = new ArrayList<>(),
                diameterFlowMeter = new ArrayList<>(),
                type = new ArrayList<>(),
                remarks = new ArrayList<>();


        ArrayList<Integer>
                subDistrictNameIds = new ArrayList<>(),
                diameterAirValveIds = new ArrayList<>(),
                diameterWAValveIds = new ArrayList<>(),
                diameterISOValveIds = new ArrayList<>(),
                diameterFlowMeterIds = new ArrayList<>(),
                typeIds = new ArrayList<>(),
                remarksIds = new ArrayList<>();


        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 28) {
                subDistrictName.add(masters.get(i).getName());
                subDistrictNameIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                diameterAirValve.add(masters.get(i).getName());
                diameterAirValveIds.add(masters.get(i).getGeoMasterId());

                diameterWAValve.add(masters.get(i).getName());
                diameterWAValveIds.add(masters.get(i).getGeoMasterId());

                diameterISOValve.add(masters.get(i).getName());
                diameterISOValveIds.add(masters.get(i).getGeoMasterId());

                diameterFlowMeter.add(masters.get(i).getName());
                diameterFlowMeterIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 24) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 5) {
                remarks.add(masters.get(i).getName());
                remarksIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int subDistrictNameIndex = returnIndex(subDistrictNameIds, selectedIds[0]);
        int diameterAirValveIndex = returnIndex(diameterAirValveIds, selectedIds[1]);
        int diameterWAValveIndex = returnIndex(diameterWAValveIds, selectedIds[2]);
        int diameterISOValveIndex = returnIndex(diameterISOValveIds, selectedIds[3]);
        int diameterFlowMeterIndex = returnIndex(diameterFlowMeterIds, selectedIds[4]);
        int typeIndex = returnIndex(typeIds, selectedIds[5]);
        int remarksIndex = returnIndex(remarksIds, selectedIds[6]);


        secondView.loadSpinnersData(subDistrictName, subDistrictNameIds, subDistrictNameIndex,
                diameterAirValve, diameterAirValveIds, diameterAirValveIndex,
                diameterWAValve, diameterWAValveIds, diameterWAValveIndex,
                diameterISOValve, diameterISOValveIds, diameterISOValveIndex,
                diameterFlowMeter, diameterFlowMeterIds, diameterFlowMeterIndex,
                type, typeIds, typeIndex,
                remarks, remarksIds, remarksIndex);
    }

    private int returnIndex(ArrayList<Integer> ids, int selectedId) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == selectedId)
                return i;
        }
        return 0;
    }

    @Override
    public void onChamberInsertionCalled(long flag) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.edit_chamber_done_successfully));
            mainView.backToParent();
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_edit));
        }
    }
}
