package com.seifoo.neomit.gisgathering.home.mainLine.rejected;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;
import com.seifoo.neomit.gisgathering.home.mainLine.map.GetMainLineMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.mainLine.offline.OfflineMainLineAdapter;
import com.seifoo.neomit.gisgathering.home.mainLine.rejected.edit.EditRejectedMainLinesActivity;
import com.seifoo.neomit.gisgathering.home.mainLine.rejected.map.RejectedMainLinesMapActivity;

import java.util.ArrayList;

public class RejectedMainLinesActivity extends AppCompatActivity implements RejectedMainLinesMVP.View, OfflineMainLineAdapter.MainLineObjectListItemListener {
    private static final int EDIT_REJECTED_MAIN_LINE_REQUEST = 1;
    private static final int SHOW_ALL_IN_MAP_REQUEST = 2;
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private OfflineMainLineAdapter adapter;
    private ProgressBar progressBar;
    private Button showLocations;

    private RejectedMainLinesMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_main_lines);

        getSupportActionBar().setTitle(getString(R.string.rejected_main_lines));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new RejectedMainLinesPresenter(this, new RejectedMainLinesModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        showLocations = (Button) findViewById(R.id.show_locations);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.rejected_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineMainLineAdapter(this, false);

        presenter.requestMainLines();
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyListText() {
        showLocations.setVisibility(View.GONE);
        emptyListTextView.setText(getString(R.string.no_main_lines_available));
        adapter.clear();
    }

    @Override
    public void loadRejectedMainLines(ArrayList<MainLineObject> mainLines) {
        emptyListTextView.setText("");
        adapter.setList(mainLines);
        recyclerView.setAdapter(adapter);
        handleMapButton(mainLines);
    }

    private void handleMapButton(final ArrayList<MainLineObject> mainLines) {
        for (int i = 0; i < mainLines.size(); i++) {
            if (mainLines.get(i).getLatitude() != null && mainLines.get(i).getLongitude() != null) {
                if (!mainLines.get(i).getLatitude().isEmpty() && !mainLines.get(i).getLongitude().isEmpty()) {
                    showLocations.setVisibility(View.VISIBLE);
                    showLocations.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(RejectedMainLinesActivity.this, RejectedMainLinesMapActivity.class);
                            intent.putExtra("MainLines", mainLines);
                            startActivityForResult(intent, SHOW_ALL_IN_MAP_REQUEST);
                        }
                    });
                    break;
                }
            }
        }
    }

    @Override
    public void onListItemClickListener(int viewId, int position, MainLineObject mainLine) {
        switch (viewId) {
            case R.id.more_details:
                Intent intent = new Intent(this, EditRejectedMainLinesActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("MainLineObj", mainLine);
                startActivityForResult(intent, EDIT_REJECTED_MAIN_LINE_REQUEST);
                break;
            case R.id.map:
                Intent intent1 = new Intent(this, GetMainLineMapDetailsActivity.class);
                intent1.putExtra("MainLine", mainLine);
                startActivityForResult(intent1, SHOW_ALL_IN_MAP_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_REJECTED_MAIN_LINE_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.removeItem(this, data.getExtras().getInt("position"));
            return;
        }
        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            presenter.requestMainLines();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
