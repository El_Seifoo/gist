package com.seifoo.neomit.gisgathering.home.meter.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface AddMeterMVP {
    interface MainView {
        Context getAppContext();

        void showToastMessage(String message);

        void backToParent();

        String getCurrentDate();
    }

    interface FirstView {

        void loadSpinnersData(ArrayList<String> meterDiameter, ArrayList<Integer> meterDiameterIds, ArrayList<String> meterBrand,
                              ArrayList<Integer> meterBrandIds, ArrayList<String> meterType, ArrayList<Integer> meterTypeIds,
                              ArrayList<String> meterStatus, ArrayList<Integer> meterStatusIds, ArrayList<String> meterMaterial,
                              ArrayList<Integer> meterMaterialIds, ArrayList<String> meterRemarks, ArrayList<Integer> meterRemarksIds,
                              ArrayList<String> readType, ArrayList<Integer> readTypeIds);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator, int flag);

        void setSerialNumber(String data);

        void setPlateNumber(String data);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);
    }

    interface SecondView {

        void loadSpinnersData(ArrayList<String> pipeAfterMeter, ArrayList<Integer> pipeAfterMeterIds,
                              ArrayList<String> pipeSize, ArrayList<Integer> pipeSizeIds,
                              ArrayList<String> meterMaintenanceArea, ArrayList<Integer> meterMaintenanceAreaIds,
                              ArrayList<String> meterBoxPosition, ArrayList<Integer> meterBoxPositionIds,
                              ArrayList<String> meterBoxType, ArrayList<Integer> meterBoxTypeIds,
                              ArrayList<String> coverStatus, ArrayList<Integer> coverStatusIds,
                              ArrayList<String> location, ArrayList<Integer> locationIds,
                              ArrayList<String> buildingUsage, ArrayList<Integer> buildingUsageIds,
                              ArrayList<String> waterConnectionType, ArrayList<Integer> waterConnectionTypeIds,
                              ArrayList<String> pipeMaterial, ArrayList<Integer> pipeMaterialIds);
    }

    interface ThirdView {

        void loadSpinnersData(ArrayList<String> valveType, ArrayList<Integer> valveTypeIds,
                              ArrayList<String> valveStatus, ArrayList<Integer> valveStatusIds,
                              ArrayList<String> enabled, ArrayList<Integer> enabledIds,
                              ArrayList<String> reducerDiameter, ArrayList<Integer> reducerDiameterIds);

        void setDate(String date);
    }

    interface FourthView {

        void loadSpinnersData(ArrayList<String> subBuildingType, ArrayList<Integer> subBuildingTypeIds,
                              ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,
                              ArrayList<String> newMeterBrand, ArrayList<Integer> newMeterBrandIds,
                              ArrayList<String> districtName, ArrayList<Integer> districtNameIds);

        void setMeterAddress(String address);

        void setHCN(String hcn);

        void initializeScanner(IntentIntegrator integrator, int flag);
    }

    interface FifthView {

    }

    interface Presenter {

        void requestFirstSpinnersData();

        void requestSecondSpinnersData();

        void requestThirdSpinnersData();

        void requestFourthSpinnersData();

        void requestPassFirstStepDataToActivity(MeterObject meterObject);

        void requestPassSecondStepDataToActivity(MeterObject meterObject);

        void requestPassThirdStepDataToActivity(MeterObject meterObject);

        void requestPassFourthStepDataToActivity(MeterObject meterObject);

        void requestPassFifthStepDataToActivity(MeterObject meterObject);

        void requestAddMeterObject(MeterObject meterObject);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext, String flagString);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void onFourthViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents, int flag);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String date);
    }
}
