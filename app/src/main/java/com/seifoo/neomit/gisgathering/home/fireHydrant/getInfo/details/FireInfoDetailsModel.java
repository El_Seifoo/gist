package com.seifoo.neomit.gisgathering.home.fireHydrant.getInfo.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;

import java.util.ArrayList;

public class FireInfoDetailsModel {

    public void returnValidGeoMaster(Context context, DBCallback callback, FireHydrantObject fireObject, ArrayList<Integer> ids, ArrayList<Integer> types, String lang) {
        callback.onConvertingIdsCalled(DataBaseHelper.getmInstance(context).returnConvertedGeoMastersIds(ids, types, lang), fireObject);
    }

    protected interface DBCallback {
        void onConvertingIdsCalled(ArrayList<String> strings, FireHydrantObject fireObject);
    }
}
