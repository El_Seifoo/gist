package com.seifoo.neomit.gisgathering.home.pump.rejected;


import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.home.valve.rejected.RejectedValvesMVP;
import com.seifoo.neomit.gisgathering.home.valve.rejected.RejectedValvesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RejectedPumpsPresenter implements RejectedPumpsMVP.Presenter, RejectedPumpsModel.VolleyCallback, RejectedPumpsModel.DBCallback {
    private RejectedPumpsMVP.View view;
    private RejectedPumpsModel model;

    public RejectedPumpsPresenter(RejectedPumpsMVP.View view, RejectedPumpsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestPumps() {
        view.showProgress();
        model.getRejected(view.getAppContext(), this);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ArrayList<PumpObject> rejectedPumps = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {
                    /*
                    Json[data][0][CODE]:1
                    Json[data][0][STATION_NAME]:1
                    Json[data][0][ENABLED_ID]:1
                    Json[data][0][F_TYPE]:1
                    Json[data][0][ASSET_STATUS_ID]:1
                    Json[data][0][STREET_NAME]:a
                    Json[data][0][DISTRICT_NAME_ID]:1
                    Json[data][0][SUP_DISTRICT_ID]:1
                    Json[data][0][DMA_ZONE_ID]:1
                    Json[data][0][SUB_NAME]:aa
                    Json[data][0][SECTOR_NAME]:az
                    Json[data][0][LAST_UPDATED]:31-03-2019 22:20
                    Json[data][0][CreatedDate]:31-03-2019 22:20
                    Json[data][0][REMARKS]:xyz
                    Json[data][0][RECORD_ID]:10
                    Json[tableName]:pump
                    Json[databaseName]:re_al_qwayah
                     */
                    rejectedPumps.add(new PumpObject(
                            data.getJSONObject(i).getLong("Corrected_Id"),
                            returnValidString(data.getJSONObject(i).getString("Y_Map")),
                            returnValidString(data.getJSONObject(i).getString("X_Map")),
                            returnValidString(data.getJSONObject(i).getString("CODE")),
                            returnValidString(data.getJSONObject(i).getString("STATION_NAME")),
                            returnValidInt(data.getJSONObject(i).getString("ENABLED_ID")) == -1 ? 1 : returnValidInt(data.getJSONObject(i).getString("ENABLED_ID")),
                            returnValidInt(data.getJSONObject(i).getString("F_TYPE")),
                            returnValidInt(data.getJSONObject(i).getString("ASSET_STATUS_ID")),
                            returnValidString(data.getJSONObject(i).getString("STREET_NAME")),
                            returnValidInt(data.getJSONObject(i).getString("DISTRICT_NAME_ID")),
                            returnValidInt(data.getJSONObject(i).getString("SUP_DISTRICT_ID")),
                            returnValidString(data.getJSONObject(i).getString("DMA_ZONE_ID")),
                            returnValidString(data.getJSONObject(i).getString("SECTOR_NAME")),
                            returnValidDate(data.getJSONObject(i).getString("LAST_UPDATED")),
                            returnValidString(data.getJSONObject(i).getString("REMARKS")),
                            returnValidString(data.getJSONObject(i).getString("Reason")),
                            returnValidDate1(data.getJSONObject(i).getString("CreatedDate"))));
//x -> longitude , y -> latitude
                }
                model.checkDatabasePumps(view.getAppContext(), this, rejectedPumps);
            } else {
                view.hideProgress();
                view.showEmptyListText();
            }
        } else {
            view.hideProgress();
            view.showEmptyListText();
        }
    }

    private String returnValidString(String data) {
        return data.isEmpty() || data.toLowerCase().equals("null") ? "" : data;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }

    @Override
    public void onCheckingPumpsCalled(ArrayList<PumpObject> pumps) {
        view.hideProgress();
        if (pumps.isEmpty())
            view.showEmptyListText();
        else
            view.loadRejectedPumps(pumps);
    }
}
