package com.seifoo.neomit.gisgathering.home.tank.edit;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.FixedHoloDatePickerDialog;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;
import com.seifoo.neomit.gisgathering.home.tank.add.AddTankActivity;
import com.seifoo.neomit.gisgathering.home.tank.add.AddTankMVP;
import com.seifoo.neomit.gisgathering.home.tank.add.AddTankModel;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class EditTankPresenter implements EditTankMVP.Presenter, EditTankModel.DBCallback {
    private EditTankMVP.View view;
    private EditTankMVP.MainView mainView;
    private EditTankMVP.FirstView firstView;
    private EditTankMVP.SecondView secondView;
    private EditTankModel model;

    public EditTankPresenter(EditTankMVP.View view, EditTankMVP.MainView mainView, EditTankModel model) {
        this.view = view;
        this.mainView = mainView;
        this.model = model;
    }

    public EditTankPresenter(EditTankMVP.View view, EditTankMVP.FirstView firstView, EditTankModel model) {
        this.view = view;
        this.firstView = firstView;
        this.model = model;
    }

    public EditTankPresenter(EditTankMVP.View view, EditTankMVP.SecondView secondView, EditTankModel model) {
        this.view = view;
        this.secondView = secondView;
        this.model = model;
    }


    /*
        0 -> meter diameter , 1 -> meter brand , 2 - > meter type , 3 -> meter status , 4 -> meter material , 5 -> meter remarks
        6 -> read type , 7 -> pipe after meter , 8 -> pipe size , 9 -> maintenance area , 10 -> meter box Position , 11 -> meter box type
        12 -> cover status , 13 -> location , 14 -> building usage , 15 -> sub-building type , 16 -> water schedule , 17 -> water connection type
        18 -> pipe material , 19 -> new meter brand , 20 ->tank type  , 21 -> tank status , 22 -> yes_no , 23 -> reducer diameter
        24 -> FireType , 25 -> districts , 26 -> houseConnectionType , 27 -> tankJob , 28 -> subDistrict , 29 -> tankType,
     */
    @Override
    public void requestFirstSpinnersData(TankObject tank) {
        // type 29 , enabled 22 , tankDiameter 0
        model.getGeoMasters(view.getAppContext(), this, new int[]{29, 22, 0},
                new int[]{tank.getType(), tank.getEnabled(), tank.getTankDiameter()},
                1);
        firstView.setCommissionDate(tank.getCommissionDate());
        firstView.setLocation(tank.getLatitude() + "," + tank.getLongitude());
        firstView.setSerialNumber(tank.getSerialNumber());
        firstView.setData(tank.getTankName(), tank.getActiveVolume());
    }

    @Override
    public void requestSecondSpinnersData(TankObject tank) {
        // assetStatus 3,districtName 25
        model.getGeoMasters(view.getAppContext(), this, new int[]{3, 25},
                new int[]{tank.getAssetStatus(), tank.getDistrictName()},
                2);
        secondView.setLastUpdated(tank.getLastUpdated());
        secondView.setData(tank.getInActiveVolume(), tank.getLowLevel(), tank.getHighLevel(), tank.getStreetName(), tank.getRegion(), tank.getSectorName(), tank.getRemarks());
    }

    @Override
    public void requestPassFirstStepDataToActivity(TankObject tank) {
        if (!tank.getLatitude().contains(",")) {
            firstView.showLocationError(view.getAppContext().getString(R.string.location_is_required));
            return;
        }

        tank.setLatitude(tank.getLatitude().split(",")[0]);
        tank.setLongitude(tank.getLongitude().split(",")[1]);
        if (view.getAppContext() instanceof EditTankActivity) {
            ((EditTankActivity) view.getAppContext()).passFirstObj(tank);
        }
    }

    @Override
    public void requestPassSecondStepDataToActivity(TankObject tank) {
        if (view.getAppContext() instanceof EditTankActivity) {
            ((EditTankActivity) view.getAppContext()).passSecondObj(tank);
        }
    }

    @Override
    public void requestEditTank(TankObject tank, String createdAt, long id) {
        tank.setId(id);
        tank.setCreatedAt(createdAt);
        model.updateTank(view.getAppContext(), this, tank);
    }


    @Override
    public void handleQRCodeImages(ImageView clickedImage) {
        clickedImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestQrCode(((AddTankActivity) view.getAppContext()));
            }
        });
    }

    @Override
    public void requestQrCode(Activity appContext) {
        IntentIntegrator integrator = new IntentIntegrator(appContext);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        firstView.initializeScanner(integrator);
    }

    private static final int PICK_LOCATION_REQUEST = 10;

    @Override
    public void onFirstViewActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_LOCATION_REQUEST && resultCode == RESULT_OK && data != null) {
            firstView.setLocation(data.getExtras().getString("pickedLocation"));
            return;
        }
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                this.handleQRScannerResult(result.getContents());
        }
    }

    @Override
    public void handleQRScannerResult(String contents) {
        firstView.setSerialNumber(contents.replaceAll("[^0-9]", ""));
    }

    @Override
    public void requestPickLocation(String prevLatLng) {
        model.getTanksLocation(view.getAppContext(), this, prevLatLng);
    }

    @Override
    public void requestDatePickerDialog(String dateString, final int index) {
        Calendar calendar = Calendar.getInstance();
        if (!dateString.isEmpty())
            calendar.set(
                    Integer.valueOf(dateString.split("/")[2]),
                    (Integer.valueOf(dateString.split("/")[1]) - 1),
                    Integer.valueOf(dateString.split("/")[0]));


        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        view.getAppContext(),
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        if (index == 0)
                            firstView.setCommissionDate(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                        else
                            secondView.setLastUpdated(new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(birthDay.getTime()));
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();
    }

    @Override
    public void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index) {
        if (index == 1) handleFirstView(masters, selectedIds);
        else if (index == 2) handleSecondView(masters, selectedIds);
    }

    // type 29 , enabled 22 , tankDiameter 0
    private void handleFirstView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                type = new ArrayList<>(),
                enabled = new ArrayList<>(),
                tankDiameter = new ArrayList<>();

        ArrayList<Integer>
                typeIds = new ArrayList<>(),
                enabledIds = new ArrayList<>(),
                tankDiameterIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 29) {
                type.add(masters.get(i).getName());
                typeIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 22) {
                enabled.add(masters.get(i).getName());
                enabledIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 0) {
                tankDiameter.add(masters.get(i).getName());
                tankDiameterIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int typeIndex = returnIndex(typeIds, selectedIds[0]);
        int enabledIndex = returnIndex(enabledIds, selectedIds[1]);
        int tankDiameterIndex = returnIndex(tankDiameterIds, selectedIds[2]);

        firstView.loadSpinnersData(type, typeIds, typeIndex,
                enabled, enabledIds, enabledIndex,
                tankDiameter, tankDiameterIds, tankDiameterIndex);
    }

    // assetStatus 3,districtName 25
    private void handleSecondView(ArrayList<GeoMasterObj> masters, int[] selectedIds) {
        ArrayList<String>
                assetStatus = new ArrayList<>(),
                districtName = new ArrayList<>();

        ArrayList<Integer>
                assetStatusIds = new ArrayList<>(),
                districtNameIds = new ArrayList<>();

        for (int i = 0; i < masters.size(); i++) {
            if (masters.get(i).getType() == 3) {
                assetStatus.add(masters.get(i).getName());
                assetStatusIds.add(masters.get(i).getGeoMasterId());
            } else if (masters.get(i).getType() == 25) {
                districtName.add(masters.get(i).getName());
                districtNameIds.add(masters.get(i).getGeoMasterId());
            }
        }

        int assetStatusIndex = returnIndex(assetStatusIds, selectedIds[0]);
        int districtNameIndex = returnIndex(districtNameIds, selectedIds[1]);

        secondView.loadSpinnersData(assetStatus, assetStatusIds, assetStatusIndex,
                districtName, districtNameIds, districtNameIndex);
    }


    private int returnIndex(ArrayList<Integer> ids, int selectedId) {
        for (int i = 0; i < ids.size(); i++) {
            if (ids.get(i) == selectedId)
                return i;
        }
        return 0;
    }

    @Override
    public void onTankUpdateCalled(long flag, TankObject tank) {
        if (flag > 0) {
            mainView.showToastMessage(view.getAppContext().getString(R.string.edit_tank_done_successfully));
            mainView.backToParent(tank);
        } else {
            mainView.showToastMessage(view.getAppContext().getString(R.string.failed_to_edit));
        }
    }

    @Override
    public void onGetTanksLocationCalled(ArrayList<HashMap<String, String>> tanksLocation, String prevLatLng) {
        if (prevLatLng.contains(",")) {
            firstView.navigateToTheMap(prevLatLng.split(",")[0], prevLatLng.split(",")[1], tanksLocation, PICK_LOCATION_REQUEST);
        } else {
            firstView.navigateToTheMap("", "", tanksLocation, PICK_LOCATION_REQUEST);
        }
    }
}
