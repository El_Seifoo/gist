package com.seifoo.neomit.gisgathering.home.chamber.info;

import android.app.Activity;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

public class GetChamberInfoPresenter implements GetChamberInfoMVP.Presenter,GetChamberInfoModel.VolleyCallback{
    private GetChamberInfoMVP.View view;
    private GetChamberInfoModel model;

    public GetChamberInfoPresenter(GetChamberInfoMVP.View view, GetChamberInfoModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestQrCode(Activity activity) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        view.initializeScanner(integrator);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                view.setSerialNumber(result.getContents().replaceAll("[^0-9]", ""));
        }
    }

    @Override
    public void requestGetInfo(String chNumber) {
        if (chNumber.isEmpty()) {
            view.showToastMessage(view.getActContext().getString(R.string.ch_number_is_required));
            return;
        }
        view.showProgress();
        model.getInfo(view.getActContext(), this, chNumber);
    }

    @Override
    public void onRequestSucceeded(String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("status").equals("Success")) {
            JSONArray data = response.getJSONArray("data");
            if (data.length() > 0) {
                ChamberObject valve = new ChamberObject(
                        returnValidString(data.getJSONObject(0).getString("Y_Map")),
                        returnValidString(data.getJSONObject(0).getString("X_Map")),
                        returnValidInt(data.getJSONObject(0).getString("MATERIAL_PIPE")),
                        returnValidString(data.getJSONObject(0).getString("CH_NUMBER")),
                        returnValidString(data.getJSONObject(0).getString("LENGTH")),
                        returnValidString(data.getJSONObject(0).getString("WIDTH")),
                        returnValidString(data.getJSONObject(0).getString("DEPTH")),
                        returnValidInt(data.getJSONObject(0).getString("DIAMETER")),
                        returnValidString(data.getJSONObject(0).getString("DEPTH_UNDER_PIPE")),
                        returnValidString(data.getJSONObject(0).getString("DEPTH_ABOVE_PIPE")),
                        returnValidInt(data.getJSONObject(0).getString("DISTRICT_NAME_ID")),
                        returnValidString(data.getJSONObject(0).getString("DMA_ZONE_ID")),
                        returnValidInt(data.getJSONObject(0).getString("SUB_DISTRICT_NO")),
                        returnValidString(data.getJSONObject(0).getString("STREET_NAME")),
                        returnValidString(data.getJSONObject(0).getString("STREET_SER_NU")),
                        returnValidString(data.getJSONObject(0).getString("SUB_NAME")),
                        returnValidString(data.getJSONObject(0).getString("SECTOR_NAME")),
                        returnValidInt(data.getJSONObject(0).getString("DIAMETER_AIR_VALVE")),
                        returnValidInt(data.getJSONObject(0).getString("DIAMETER_WA_VALVE")),
                        returnValidInt(data.getJSONObject(0).getString("DIAMETER_ISO_VALVE")),
                        returnValidInt(data.getJSONObject(0).getString("DIAMETER_FLOWMETER")),
                        returnValidString(data.getJSONObject(0).getString("IMAGE_NO")),
                        returnValidInt(data.getJSONObject(0).getString("F_TYPE_ID")),
                        returnValidDate(data.getJSONObject(0).getString("UPDATE_FIELD")),
                        returnValidInt(data.getJSONObject(0).getString("REMARKS")),
                        returnValidDate1(data.getJSONObject(0).getString("CreatedDate")));
                view.loadChamberInfo(valve);
            } else
                view.showToastMessage(view.getActContext().getString(R.string.this_chamber_not_exist));
        } else
            view.showToastMessage(view.getActContext().getString(R.string.this_chamber_not_exist));
    }

    private String returnValidString(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? "" : string;
    }

    private int returnValidInt(String string) {
        return string.isEmpty() || string.toLowerCase().equals("null") ? -1 :
                !isNumeric(string) ? -1 :
                        Integer.parseInt(string) < -1 ? -1 : Integer.parseInt(string);
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }

    private String returnValidDate(String date) {
        String rslt = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) rslt += date.split("T")[0];
        return rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0];
    }

    private String returnValidDate1(String date) {
        String rslt = "";
        String rslt1 = "";
        if (date.isEmpty() || date.toLowerCase().equals("null")) return "";
        if (!date.contains("-")) return "";
        if (date.contains("T")) {
            rslt += date.split("T")[0];
            rslt1 += date.split("T")[1];
        }
        if (rslt1.contains(":"))
            return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0])
                    .concat(" ").concat(rslt1.split(":")[0] + ":" + rslt1.split(":")[1]);

        return (rslt.split("-")[2] + "/" + rslt.split("-")[1] + "/" + rslt.split("-")[0]).concat(" ").concat(rslt1);
    }

    @Override
    public void onRequestFailed(VolleyError error) {
        view.hideProgress();
        view.showToastMessage(error instanceof AuthFailureError ? "" :
                error instanceof NoConnectionError ? view.getActContext().getString(R.string.no_internet_connection) :
                        error instanceof TimeoutError ? view.getActContext().getString(R.string.time_out_error_message) :
                                error instanceof ServerError ? view.getActContext().getString(R.string.server_error) :
                                        view.getActContext().getString(R.string.wrong_message));
    }
}
