package com.seifoo.neomit.gisgathering.home.meter.offline.offlineMeterDetails;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface OfflineMeterDetailsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();


        void loadMeterMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestMeterDetails(MeterObject offlineMeterObj);
    }
}
