package com.seifoo.neomit.gisgathering.home.fireHydrant.getInfo;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.seifoo.neomit.gisgathering.home.fireHydrant.getInfo.details.GetFireInfoDetailsActivity;

public class GetFireInfoActivity extends AppCompatActivity implements GetFireInfoMVP.View {
    private EditText serialNumberEditText, fhNumberEditText;
    private Button getInfoButton;
    private ProgressBar progressBar;

    private GetFireInfoMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_fire_info);

        getSupportActionBar().setTitle(getString(R.string.get_info));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new GetFireInfoPresenter(this, new GetFireInfoModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        serialNumberEditText = (EditText) findViewById(R.id.serial_num);
        fhNumberEditText = (EditText) findViewById(R.id.fh_num);

        getInfoButton = (Button) findViewById(R.id.get_info_button);
        getInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestGetInfo(serialNumberEditText.getText().toString().trim(),
                        fhNumberEditText.getText().toString().trim());
            }
        });
    }

    public void scanQR(View view) {
        presenter.whichQRClicked(view.getId());
    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void initializeScanner(IntentIntegrator integrator, int flag) {
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void setSerialNumber(String data) {
        serialNumberEditText.setText(data);
    }

    @Override
    public void setFHNumber(String data) {
        fhNumberEditText.setText(data);
    }

    @Override
    public void loadFireInfo(FireHydrantObject moreDetails) {
        Intent intent = new Intent(this, GetFireInfoDetailsActivity.class);
        intent.putExtra("GetInfo", moreDetails);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
