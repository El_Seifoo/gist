package com.seifoo.neomit.gisgathering.home.meter.add;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class ThirdFragment extends Fragment implements Step, AddMeterMVP.MainView, AddMeterMVP.ThirdView {
    private EditText lastUpdate, postCodeEditText, scecoNumberEditText, numberOfElectricMetersEditText, lastReadingEditText, numberOfFloorsEditText;
    private Spinner valveTypeSpinner, valveStatusSpinner, enabledSpinner, reducerDiameterSpinner;

    private AddMeterMVP.Presenter presenter;

    public static ThirdFragment newInstance(int position) {
        ThirdFragment fragment = new ThirdFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putInt("position", position);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third, container, false);
        presenter = new AddMeterPresenter(this, this, new AddMeterModel());


        lastUpdate = (EditText) view.findViewById(R.id.last_updated);
        postCodeEditText = (EditText) view.findViewById(R.id.post_code);
        scecoNumberEditText = (EditText) view.findViewById(R.id.sceco_num);
        numberOfElectricMetersEditText = (EditText) view.findViewById(R.id.number_of_electric_meters);
        lastReadingEditText = (EditText) view.findViewById(R.id.last_reading);
        numberOfFloorsEditText = (EditText) view.findViewById(R.id.number_of_floors);


        valveTypeSpinner = (Spinner) view.findViewById(R.id.valve_type_spinner);
        valveStatusSpinner = (Spinner) view.findViewById(R.id.valve_status_spinner);
        enabledSpinner = (Spinner) view.findViewById(R.id.enabled_spinner);
        reducerDiameterSpinner = (Spinner) view.findViewById(R.id.reducer_diameter_spinner);

        lastUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestDatePickerDialog(lastUpdate.getText().toString().trim());
            }
        });

        presenter.requestThirdSpinnersData();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
//        Log.e("verfStep", "step");
        /*
        int meterValveTypeId, int meterValveStatusId, int meterEnabledId, int meterReducerDiameterId,
                       String meterLastUpdated, String meterPostCode, String meterScecoNumber,
                       String meterNumberOfElectricMeters, String meterLastReading, String meterNumberOfFloors
         */
        presenter.requestPassThirdStepDataToActivity(new MeterObject(
                valveTypeIds.get(valveTypeSpinner.getSelectedItemPosition()),
                valveStatusIds.get(valveStatusSpinner.getSelectedItemPosition()),
                enabledIds.get(enabledSpinner.getSelectedItemPosition()),
                reducerDiameterIds.get(reducerDiameterSpinner.getSelectedItemPosition()),
                lastUpdate.getText().toString().trim(), postCodeEditText.getText().toString().trim(),
                scecoNumberEditText.getText().toString().trim(),
                numberOfElectricMetersEditText.getText().toString().trim(),
                lastReadingEditText.getText().toString().trim(),
                numberOfFloorsEditText.getText().toString().trim()));
        return null;
    }

    @Override
    public void onSelected() {
//        Log.e("selected", "selected");
    }

    @Override
    public void onError(@NonNull VerificationError error) {
//        Log.e("error", error.toString());
    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    @Override
    public void showToastMessage(String message) {

    }

    @Override
    public void backToParent() {

    }

    @Override
    public String getCurrentDate() {
        return null;
    }

    private ArrayList<Integer>  valveTypeIds, valveStatusIds, enabledIds, reducerDiameterIds;

    @Override
    public void loadSpinnersData(ArrayList<String> valveType, ArrayList<Integer> valveTypeIds,
                                 ArrayList<String> valveStatus, ArrayList<Integer> valveStatusIds,
                                 ArrayList<String> enabled, ArrayList<Integer> enabledIds,
                                 ArrayList<String> reducerDiameter, ArrayList<Integer> reducerDiameterIds) {


        this.valveTypeIds = valveTypeIds;
        this.valveStatusIds = valveStatusIds;
        this.enabledIds = enabledIds;
        this.reducerDiameterIds = reducerDiameterIds;

        valveTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, valveType));
        valveStatusSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, valveStatus));
        enabledSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, enabled));
        reducerDiameterSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, reducerDiameter));

    }

    @Override
    public void setDate(String date) {
        lastUpdate.setText(date);
    }
}
