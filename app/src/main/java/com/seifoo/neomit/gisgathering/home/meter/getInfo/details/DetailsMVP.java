package com.seifoo.neomit.gisgathering.home.meter.getInfo.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;

import java.util.ArrayList;

public interface DetailsMVP {
    interface View {
        Context getAppContext();

        Context getActContext();

        void loadMeterMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {
        void requestMeterData(MeterObject meterObject);
    }
}
