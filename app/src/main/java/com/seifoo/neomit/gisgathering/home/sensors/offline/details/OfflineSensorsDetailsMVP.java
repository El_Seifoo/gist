package com.seifoo.neomit.gisgathering.home.sensors.offline.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.sensors.SensorObject;

import java.util.ArrayList;

public interface OfflineSensorsDetailsMVP {
    interface View {
        Context getActContext();

        Context getAppContext();


        void loadSensorMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {

        void requestSensorDetails(SensorObject sensor);
    }
}
