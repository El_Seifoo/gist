package com.seifoo.neomit.gisgathering.home.fireHydrant.add;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, AddFiresMVP.View, AddFiresMVP.SecondView {
    private EditText streetNameEditText, sectorNameEditText, withWaterEditText;
    private Spinner fireHydrateStatusSpinner, remarksSpinner, valveTypeSpinner, fireBrandSpinner, waterScheduleSpinner, maintenanceAreaSpinner;

    private AddFiresMVP.Presenter presenter;

    public static SecondFragment newInstance() {
        return new SecondFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fire_second, container, false);

        presenter = new AddFiresPresenter(this, this, new AddFiresModel());

        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);
        withWaterEditText = (EditText) view.findViewById(R.id.with_water);


        fireHydrateStatusSpinner = (Spinner) view.findViewById(R.id.fire_hydrant_status_spinner);
        remarksSpinner = (Spinner) view.findViewById(R.id.remarks_spinner);
        valveTypeSpinner = (Spinner) view.findViewById(R.id.fire_hydrant_valve_type_spinner);
        fireBrandSpinner = (Spinner) view.findViewById(R.id.fire_hydrant_brand_spinner);
        waterScheduleSpinner = (Spinner) view.findViewById(R.id.water_schedule_spinner);
        maintenanceAreaSpinner = (Spinner) view.findViewById(R.id.maintenance_area_spinner);

        presenter.requestSecondSpinnersData();

        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassSecondStepDataToActivity(new FireHydrantObject(streetNameEditText.getText().toString().trim(), sectorNameEditText.getText().toString().trim(),
                fireHydrateStatusIds.get(fireHydrateStatusSpinner.getSelectedItemPosition()), remarksIds.get(remarksSpinner.getSelectedItemPosition()),
                withWaterEditText.getText().toString().trim(), fireValveTypeIds.get(valveTypeSpinner.getSelectedItemPosition()),
                fireHydrateBrandIds.get(fireBrandSpinner.getSelectedItemPosition()), waterScheduleIds.get(waterScheduleSpinner.getSelectedItemPosition()),
                maintenanceAreaIds.get(maintenanceAreaSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    private ArrayList<Integer> fireHydrateStatusIds, remarksIds, fireValveTypeIds, fireHydrateBrandIds, waterScheduleIds, maintenanceAreaIds;

    @Override
    public void loadSpinnersData(ArrayList<String> fireHydrateStatus, ArrayList<Integer> fireHydrateStatusIds,
                                 ArrayList<String> remarks, ArrayList<Integer> remarksIds,
                                 ArrayList<String> fireValveType, ArrayList<Integer> fireValveTypeIds,
                                 ArrayList<String> fireHydrateBrand, ArrayList<Integer> fireHydrateBrandIds,
                                 ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,
                                 ArrayList<String> maintenanceArea, ArrayList<Integer> maintenanceAreaIds) {
        this.fireHydrateStatusIds = fireHydrateStatusIds;
        this.remarksIds = remarksIds;
        this.fireValveTypeIds = fireValveTypeIds;
        this.fireHydrateBrandIds = fireHydrateBrandIds;
        this.waterScheduleIds = waterScheduleIds;
        this.maintenanceAreaIds = maintenanceAreaIds;

        fireHydrateStatusSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, fireHydrateStatus));
        remarksSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, remarks));
        valveTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, fireValveType));
        fireBrandSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, fireHydrateBrand));
        waterScheduleSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, waterSchedule));
        maintenanceAreaSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, maintenanceArea));
    }
}
