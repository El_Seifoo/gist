package com.seifoo.neomit.gisgathering.home.valve.rejected;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.home.valve.map.GetValveMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.valve.offline.OfflineValvesAdapter;
import com.seifoo.neomit.gisgathering.home.valve.rejected.edit.EditRejectedValvesActivity;
import com.seifoo.neomit.gisgathering.home.valve.rejected.map.RejectedValvesMapActivity;

import java.util.ArrayList;

public class RejectedValvesActivity extends AppCompatActivity implements RejectedValvesMVP.View, OfflineValvesAdapter.ValveObjectListItemListener {
    private static final int EDIT_REJECTED_VALVE_REQUEST = 1;
    private static final int SHOW_ALL_IN_MAP_REQUEST = 2;
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private OfflineValvesAdapter adapter;
    private ProgressBar progressBar;
    private Button showLocations;

    private RejectedValvesMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_valves);

        getSupportActionBar().setTitle(getString(R.string.rejected_valves));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new RejectedValvesPresenter(this, new RejectedValvesModel());
        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);


        showLocations = (Button) findViewById(R.id.show_locations);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.rejected_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflineValvesAdapter(this, false);

        presenter.requestValves();

    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyListText() {
        showLocations.setVisibility(View.GONE);
        emptyListTextView.setText(getString(R.string.no_valves_available));
        adapter.clear();
    }

    @Override
    public void loadRejectedValves(ArrayList<ValveObject> valves) {
        emptyListTextView.setText("");
        adapter.setList(valves);
        recyclerView.setAdapter(adapter);
        handleMapButton(valves);
    }

    private void handleMapButton(final ArrayList<ValveObject> Valves) {
        for (int i = 0; i < Valves.size(); i++) {
            if (Valves.get(i).getLatitude() != null && Valves.get(i).getLongitude() != null) {
                if (!Valves.get(i).getLatitude().isEmpty() && !Valves.get(i).getLongitude().isEmpty()) {
                    showLocations.setVisibility(View.VISIBLE);
                    showLocations.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(RejectedValvesActivity.this, RejectedValvesMapActivity.class);
                            intent.putExtra("Valves", Valves);
                            startActivityForResult(intent, SHOW_ALL_IN_MAP_REQUEST);
                        }
                    });
                    break;
                }
            }
        }
    }

    @Override
    public void onListItemClickListener(int viewId, int position, ValveObject valve) {
        switch (viewId) {
            case R.id.more_details:
                Intent intent = new Intent(this, EditRejectedValvesActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("ValveObj", valve);
                startActivityForResult(intent, EDIT_REJECTED_VALVE_REQUEST);
                break;
            case R.id.map:
                Intent intent1 = new Intent(this, GetValveMapDetailsActivity.class);
                intent1.putExtra("Valve", valve);
                startActivityForResult(intent1, SHOW_ALL_IN_MAP_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_REJECTED_VALVE_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.removeItem(this, data.getExtras().getInt("position"));
            return;
        }
        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            presenter.requestValves();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
