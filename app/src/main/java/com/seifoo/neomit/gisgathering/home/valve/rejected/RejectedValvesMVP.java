package com.seifoo.neomit.gisgathering.home.valve.rejected;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.valve.ValveObject;

import java.util.ArrayList;

public interface RejectedValvesMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void showEmptyListText();

        void loadRejectedValves(ArrayList<ValveObject> valves);
    }

    interface Presenter {
        void requestValves();
    }
}
