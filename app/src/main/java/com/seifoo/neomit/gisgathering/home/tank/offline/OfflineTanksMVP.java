package com.seifoo.neomit.gisgathering.home.tank.offline;

import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.tank.TankObject;

import java.util.ArrayList;

public interface OfflineTanksMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void showToastMessage(String message);

        void loadTanksData(ArrayList<TankObject> tanks, boolean isVisible);

        void removeListItem(int position);

        void navigateDestination(String key, TankObject tank, Class destination);

        void navigateToEditTank(Intent intent, int requestCode);

        void updateListItem(int position, TankObject tank);

        void removeListItem(long id);

        void navigateToMap(int RequestCode, TankObject tank, int position);

        int getSpinnerSelectedPosition();

        void navigateToMap2(int requestCode, ArrayList<TankObject> tanks);
    }

    interface Presenter {
        void requestTanksData(int type);

        void requestRemoveTankObjById(long id, int position);

        void OnListItemClickListener(int viewId, int position, TankObject tank);

        void requestEditTankData(int position, TankObject tank);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestUploadData(int type);

        void showLocations(ArrayList<TankObject> tanks);
    }
}
