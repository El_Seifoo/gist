package com.seifoo.neomit.gisgathering.home.houseConnection.add;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class SecondFragment extends Fragment implements Step, AddHcMVP.View, AddHcMVP.SecondView {
    private EditText subDistrictEditText, streetNameEditText, sectorNameEditText;
    private Spinner districtNameSpinner, waterScheduleSpinner, remarksSpinner;

    private AddHcMVP.Presenter presenter;

    public static SecondFragment newInstance() {
        return new SecondFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hc_second, container, false);

        presenter = new AddHcPresenter(this, this, new AddHcModel());

        subDistrictEditText = (EditText) view.findViewById(R.id.sub_district);
        streetNameEditText = (EditText) view.findViewById(R.id.street_name);
        sectorNameEditText = (EditText) view.findViewById(R.id.sector_name);

        districtNameSpinner = (Spinner) view.findViewById(R.id.district_name_spinner);
        waterScheduleSpinner = (Spinner) view.findViewById(R.id.water_schedule_spinner);
        remarksSpinner = (Spinner) view.findViewById(R.id.remarks_spinner);

        presenter.requestSecondSpinnersData();
        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        /*
        (int districtName, String subDistrict, String streetName,
                                 String sectorName, int waterSchedule, int remarks)
         */

        presenter.requestPassSecondStepDataToActivity(new HouseConnectionObject(districtsIds.get(districtNameSpinner.getSelectedItemPosition()),
                subDistrictEditText.getText().toString().trim(), streetNameEditText.getText().toString().trim(),
                sectorNameEditText.getText().toString().trim(), waterScheduleIds.get(waterScheduleSpinner.getSelectedItemPosition()),
                remarksIds.get(remarksSpinner.getSelectedItemPosition())));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }

    ArrayList<Integer> districtsIds, waterScheduleIds, remarksIds;

    @Override
    public void loadSpinnersData(ArrayList<String> districts, ArrayList<Integer> districtsIds,
                                 ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,
                                 ArrayList<String> remarks, ArrayList<Integer> remarksIds) {

        this.districtsIds = districtsIds;
        this.waterScheduleIds = waterScheduleIds;
        this.remarksIds = remarksIds;

        districtNameSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, districts));
        waterScheduleSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, waterSchedule));
        remarksSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, remarks));

    }
}
