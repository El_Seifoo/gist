package com.seifoo.neomit.gisgathering.home.breaks.offline;

import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.breaks.BreakObject;

import java.util.ArrayList;

public interface OfflineBreaksMVP {
    interface View {
        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void showToastMessage(String message);

        void loadBreaksData(ArrayList<BreakObject> breakObj, boolean isVisible);

        void removeListItem(int position);

        void navigateDestination(String key, BreakObject breakObj, Class destination);

        void navigateToEditBreak(Intent intent, int requestCode);

        void updateListItem(int position, BreakObject breakObj);

        void removeListItem(long id);

        void navigateToMap(int RequestCode, BreakObject breakObj, int position);

        int getSpinnerSelectedPosition();

        void navigateToMap2(int requestCode, ArrayList<BreakObject> breaks);

        void sendBroadCast(Intent intent);
    }

    interface Presenter {
        void requestBreaksData(int type);

        void requestRemoveBreakObjById(long id, int position);

        void OnListItemClickListener(int viewId, int position, BreakObject breakObj);

        void requestEditBreakData(int position, BreakObject breakObj);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestUploadData(int type);

        void showLocations(ArrayList<BreakObject> breaks);
    }
}

