package com.seifoo.neomit.gisgathering.home.tank.info.details;

import android.content.Context;

import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.tank.TankObject;

import java.util.ArrayList;

public interface GetTankInfoDetailsMVP {

    interface View {
        Context getAppContext();

        Context getActContext();

        void loadMoreDetails(ArrayList<MoreDetails> list);
    }

    interface Presenter {
        void requestData(TankObject tank);
    }
}
