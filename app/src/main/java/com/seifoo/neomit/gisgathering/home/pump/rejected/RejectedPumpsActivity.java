package com.seifoo.neomit.gisgathering.home.pump.rejected;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.pump.PumpObject;
import com.seifoo.neomit.gisgathering.home.pump.map.GetPumpMapDetailsActivity;
import com.seifoo.neomit.gisgathering.home.pump.offline.OfflinePumpsAdapter;
import com.seifoo.neomit.gisgathering.home.pump.rejected.edit.EditRejectedPumpsActivity;
import com.seifoo.neomit.gisgathering.home.pump.rejected.map.RejectedPumpsMapActivity;

import java.util.ArrayList;

public class RejectedPumpsActivity extends AppCompatActivity implements RejectedPumpsMVP.View, OfflinePumpsAdapter.PumpObjectListItemListener {
    private static final int EDIT_REJECTED_PUMPS_REQUEST = 1;
    private static final int SHOW_ALL_IN_MAP_REQUEST = 2;
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private OfflinePumpsAdapter adapter;
    private ProgressBar progressBar;
    private Button showLocations;

    private RejectedPumpsMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected_pumps);

        getSupportActionBar().setTitle(getString(R.string.rejected_pumps));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new RejectedPumpsPresenter(this, new RejectedPumpsModel());
        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);


        showLocations = (Button) findViewById(R.id.show_locations);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        recyclerView = (RecyclerView) findViewById(R.id.rejected_recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new OfflinePumpsAdapter(this, false);

        presenter.requestPumps();

    }

    @Override
    public Context getActContext() {
        return getBaseContext();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyListText() {
        showLocations.setVisibility(View.GONE);
        emptyListTextView.setText(getString(R.string.no_pumps_available));
        adapter.clear();
    }

    @Override
    public void loadRejectedPumps(ArrayList<PumpObject> pumps) {
        emptyListTextView.setText("");
        adapter.setList(pumps);
        recyclerView.setAdapter(adapter);
        handleMapButton(pumps);
    }

    private void handleMapButton(final ArrayList<PumpObject> pumps) {
        for (int i = 0; i < pumps.size(); i++) {
            if (pumps.get(i).getLatitude() != null && pumps.get(i).getLongitude() != null) {
                if (!pumps.get(i).getLatitude().isEmpty() && !pumps.get(i).getLongitude().isEmpty()) {
                    showLocations.setVisibility(View.VISIBLE);
                    showLocations.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(RejectedPumpsActivity.this, RejectedPumpsMapActivity.class);
                            intent.putExtra("Pumps", pumps);
                            startActivityForResult(intent, SHOW_ALL_IN_MAP_REQUEST);
                        }
                    });
                    break;
                }
            }
        }
    }


    @Override
    public void onListItemClickListener(int viewId, int position, PumpObject pump) {
        switch (viewId) {
            case R.id.more_details:
                Intent intent = new Intent(this, EditRejectedPumpsActivity.class);
                intent.putExtra("position", position);
                intent.putExtra("PumpObj", pump);
                startActivityForResult(intent, EDIT_REJECTED_PUMPS_REQUEST);
                break;
            case R.id.map:
                Intent intent1 = new Intent(this, GetPumpMapDetailsActivity.class);
                intent1.putExtra("Pump", pump);
                startActivityForResult(intent1, SHOW_ALL_IN_MAP_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == EDIT_REJECTED_PUMPS_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.removeItem(this, data.getExtras().getInt("position"));
            return;
        }
        if (requestCode == SHOW_ALL_IN_MAP_REQUEST) {
            presenter.requestPumps();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
