package com.seifoo.neomit.gisgathering.home.fireHydrant.offline;

import android.content.Context;
import android.content.Intent;

import com.seifoo.neomit.gisgathering.home.fireHydrant.FireHydrantObject;

import java.util.ArrayList;

public interface OfflineFiresMVP {

    interface View {

        Context getActContext();

        Context getAppContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void showToastMessage(String message);

        void loadFiresData(ArrayList<FireHydrantObject> fireHydrantObjects, boolean isVisible);

        void removeListItem(int position);

        void navigateDestination(String key, FireHydrantObject fireHydrantObject, Class destination);

        void navigateToEditFire(Intent intent, int requestCode);

        void updateListItem(int position, FireHydrantObject editedFireObj);

        void removeListItem(long id);

        void navigateToMap(int requestCode, FireHydrantObject fireHydrantObject, int position);

        int getSpinnerSelectedPosition();

        void navigateToMap2(int request, ArrayList<FireHydrantObject> fires);
    }

    interface Presenter {
        void requestFiresData(int type);

        void requestRemoveFireObjById(long id, int position);

        void OnListItemClickListener(int viewId, int position, FireHydrantObject fireHydrantObject);

        void requestEditFireData(int position, FireHydrantObject fireHydrantObject);

        void onActivityResult(int requestCode, int resultCode, Intent data);

        void requestUploadData(int type);

        void showLocations(ArrayList<FireHydrantObject> fires);
    }
}
