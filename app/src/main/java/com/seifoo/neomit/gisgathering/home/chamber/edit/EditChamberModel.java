package com.seifoo.neomit.gisgathering.home.chamber.edit;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class EditChamberModel {

    public void getGeoMasters(Context context, DBCallback callback, int[] types, int[] selectedIds, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))), selectedIds, index);
    }

    public void updateChamberObj(Context context, DBCallback callback, ChamberObject chamber) {
        callback.onValveUpdatingCalled(DataBaseHelper.getmInstance(context).updateChamber(chamber), chamber);
    }

    protected void getChambersLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetChambersLocationCalled(DataBaseHelper.getmInstance(context).getChambersLocation(), prevLatLng);
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index);

        void onValveUpdatingCalled(int flag, ChamberObject chamber);

        void onGetChambersLocationCalled(ArrayList<HashMap<String,String>> chambersLocation, String prevLatLng);
    }
}
