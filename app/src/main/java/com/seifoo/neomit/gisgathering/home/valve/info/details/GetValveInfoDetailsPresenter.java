package com.seifoo.neomit.gisgathering.home.valve.info.details;

import android.util.Log;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MoreDetails;
import com.seifoo.neomit.gisgathering.home.valve.ValveObject;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class GetValveInfoDetailsPresenter implements GetValveInfoDetailsMVP.Presenter, GetValveInfoDetailsModel.DBCallback {
    private GetValveInfoDetailsMVP.View view;
    private GetValveInfoDetailsModel model;

    public GetValveInfoDetailsPresenter(GetValveInfoDetailsMVP.View view, GetValveInfoDetailsModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestValveData(ValveObject valve) {
// valveJob,  material, diameter,  type, lock,   status,{27, 4, 0, 20, 22, 21}
        //  coverStatus,  enabled, districts, subDistricts,   existInField,  existInMap,   waterSchedule,  remarks{12, 22, 25, 28, 22, 22, 16, 5}
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Integer> types = new ArrayList<>();

        ids.add(valve.getDiameter());
        types.add(0);
        ids.add(valve.getMaterial());
        types.add(4);
        ids.add(valve.getRemarks());
        types.add(5);
        ids.add(valve.getCoverStatus());
        types.add(12);
        ids.add(valve.getWaterSchedule());
        types.add(16);
        ids.add(valve.getType());
        types.add(20);
        ids.add(valve.getStatus());
        types.add(21);
        ids.add(valve.getDistrictName());
        types.add(25);
        ids.add(valve.getValveJob());
        types.add(27);
        ids.add(valve.getSubDistrict());
        types.add(28);

        model.returnValidGeoMaster(view.getAppContext(), this, valve, ids, types,
                MySingleton.getmInstance(view.getAppContext()).getStringSharedPref(Constants.APP_LANGUAGE, view.getAppContext().getString(R.string.default_language_value)));
    }

    @Override
    public void onConvertingIdsCalled(ArrayList<String> strings, ValveObject valve) {
        // 0 , 4,  5 , 12 , 16, 20 , 21 , , 25 , 27, 28
        // 0 , 1,  2 , 3 ,  4 , 5 ,  6 ,  , 7 , 8, 9
        ArrayList<MoreDetails> moreDetails = new ArrayList<>();
        if (!valve.getLatitude().isEmpty() && !valve.getLongitude().isEmpty())
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), valve.getLatitude() + "," + valve.getLongitude()));
        else
            moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.location_1), ""));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.valve_job), strings.get(8)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.serial_number), valve.getSerialNumber()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.material), strings.get(1)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.diameter), strings.get(0)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.valve_type), strings.get(5)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.full_number_of_turns), valve.getFullNumberOfTurns()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.number_of_turns), valve.getNumberOfTurns()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.lock), valve.getLock() == 1 ? view.getActContext().getString(R.string.yes) : view.getActContext().getString(R.string.no)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.valve_status), strings.get(6)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.elevation), valve.getElevation()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.ground_elevation), valve.getGroundElevation()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.cover_status), strings.get(3)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.enabled), valve.getEnabled() == 1 ? view.getActContext().getString(R.string.yes) : view.getActContext().getString(R.string.no)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.dma_zone), valve.getDmaZone()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.district_name), strings.get(7)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sub_district), strings.get(9)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.street_name), valve.getStreetName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.sector_name), valve.getSectorName()));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.exist_in_field), valve.getExistInField() == 1 ? view.getActContext().getString(R.string.yes) : view.getActContext().getString(R.string.no)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.exist_in_map), valve.getExistInMap() == 1 ? view.getActContext().getString(R.string.yes) : view.getActContext().getString(R.string.no)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.water_schedule), strings.get(4)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.remarks), strings.get(2)));
        moreDetails.add(new MoreDetails(view.getActContext().getString(R.string.created_at), valve.getCreatedAt()));

        view.loadValveMoreDetails(moreDetails);
    }
}
