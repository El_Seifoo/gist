package com.seifoo.neomit.gisgathering.home.meter.edit;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class EditMeterModel {

    public void getGeoMasters(Context context, DBCallback callback, int[] types, int[] selectedIds, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))), selectedIds, index);
    }

    public void updateMeterObj(Context context, DBCallback callback, MeterObject meterObject) {
        callback.onMeterUpdatingCalled(DataBaseHelper.getmInstance(context).updateMeterObject(meterObject), meterObject);
    }

    protected void getMetersLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetMetersLocationCalled(DataBaseHelper.getmInstance(context).getMetersLocation(), prevLatLng);
    }
    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> geoMastersList, int[] selectedIds, int index);

        void onMeterUpdatingCalled(long flag, MeterObject meterObject);

        void onGetMetersLocationCalled(ArrayList<HashMap<String,String>> metersLocation, String prevLatLng);
    }
}
