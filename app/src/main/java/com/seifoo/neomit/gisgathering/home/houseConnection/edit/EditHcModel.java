package com.seifoo.neomit.gisgathering.home.houseConnection.edit;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.houseConnection.HouseConnectionObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class EditHcModel {

    public void getGeoMasters(Context context, DBCallback callback, int[] types, int[] selectedIds, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE,context.getString(R.string.default_language_value))), selectedIds, index);
    }

    public void updateHcObj(Context context, DBCallback callback, HouseConnectionObject houseConnection) {
        callback.onHcUpdatingCalled(DataBaseHelper.getmInstance(context).updateHC(houseConnection), houseConnection);
    }

    protected void getHCsLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetHCsLocationCalled(DataBaseHelper.getmInstance(context).getHCsLocation(), prevLatLng);
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int[] selectedIds, int index);

        void onHcUpdatingCalled(int flag, HouseConnectionObject houseConnection);

        void onGetHCsLocationCalled(ArrayList<HashMap<String,String>> hCsLocation, String prevLatLng);
    }
}
