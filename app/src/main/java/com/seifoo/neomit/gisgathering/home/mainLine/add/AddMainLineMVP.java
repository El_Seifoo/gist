package com.seifoo.neomit.gisgathering.home.mainLine.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.seifoo.neomit.gisgathering.home.mainLine.MainLineObject;

import java.util.ArrayList;
import java.util.HashMap;

public interface AddMainLineMVP {
    // common
    interface View {
        Context getAppContext();
    }

    // activity
    interface MainView {
        Context getActContext();

        void showToastMessage(String message);

        void backToParent();

        String getCurrentDate();
    }

    interface FirstView {
        void loadSpinnersData(ArrayList<String> diameter, ArrayList<Integer> diameterIds,
                              ArrayList<String> type, ArrayList<Integer> typeIds,
                              ArrayList<String> enabled, ArrayList<Integer> enabledIds,
                              ArrayList<String> material, ArrayList<Integer> materialIds);

        void showLocationError(String errorMessage);

        void initializeScanner(IntentIntegrator integrator);

        void setSerialNumber(String data);

        void navigateToTheMap(String latitude, String longitude, ArrayList<HashMap<String, String>> latLngs, int requestCode);

        void setLocation(String location);
    }


    interface SecondView {
        void loadSpinnersData(ArrayList<String> districtName, ArrayList<Integer> districtNameIds,
                              ArrayList<String> subDistrict, ArrayList<Integer> subDistrictIds,
                              ArrayList<String> waterSchedule, ArrayList<Integer> waterScheduleIds,
                              ArrayList<String> assetStatus, ArrayList<Integer> assetStatusIds,
                              ArrayList<String> remarks, ArrayList<Integer> remarksIds);

        void setLastUpdated(String date);

        void setAssigned(String date);
    }

    interface Presenter {
        void requestFirstSpinnersData();

        void requestSecondSpinnersData();

        void requestPassFirstStepDataToActivity(MainLineObject mainLine);

        void requestPassSecondStepDataToActivity(MainLineObject mainLine);

        void requestAddMainLine(MainLineObject mainLine);

        void handleQRCodeImages(ImageView clickedImage);

        void requestQrCode(Activity appContext);

        void onFirstViewActivityResult(int requestCode, int resultCode, Intent data);

        void handleQRScannerResult(String contents);

        void requestPickLocation(String prevLatLng);

        void requestDatePickerDialog(String dateString, int index);
    }

}
