package com.seifoo.neomit.gisgathering.home.chamber.add;

import android.content.Context;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.chamber.ChamberObject;
import com.seifoo.neomit.gisgathering.login.GeoMasterObj;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;
import java.util.HashMap;

public class AddChamberModel {

    public void getGeoMasters(Context context, DBCallback callback, int[] types, int index) {
        callback.onGetGeoMastersCalled(DataBaseHelper.getmInstance(context).getGeoMastersByType(types, MySingleton.getmInstance(context).
                getStringSharedPref(Constants.APP_LANGUAGE, context.getString(R.string.default_language_value))), index);
    }

    public void insertChamber(Context context, DBCallback callback, ChamberObject chamber) {
        callback.onChamberInsertionCalled(DataBaseHelper.getmInstance(context).insertChamber(chamber));
    }

    protected void getChambersLocation(Context context, DBCallback callback, String prevLatLng) {
        //index ... 0-> all , 1 -> offline , 2 -> rejected
        callback.onGetChambersLocationCalled(DataBaseHelper.getmInstance(context).getChambersLocation(), prevLatLng);
    }

    protected interface DBCallback {

        void onGetGeoMastersCalled(ArrayList<GeoMasterObj> masters, int index);

        void onChamberInsertionCalled(long flag);

        void onGetChambersLocationCalled(ArrayList<HashMap<String, String>> latLngs, String prevLatLng);
    }
}
