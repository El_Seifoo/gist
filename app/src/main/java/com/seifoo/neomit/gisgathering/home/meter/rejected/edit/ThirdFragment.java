package com.seifoo.neomit.gisgathering.home.meter.rejected.edit;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.home.meter.MeterObject;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

public class ThirdFragment extends Fragment implements Step, EditRejectedMeterMVP.MainView, EditRejectedMeterMVP.ThirdView {
    private EditText lastUpdate, postCodeEditText, scecoNumberEditText, numberOfElectricMetersEditText, lastReadingEditText, numberOfFloorsEditText;
    private Spinner valveTypeSpinner, valveStatusSpinner, enabledSpinner, reducerDiameterSpinner;

    private EditRejectedMeterMVP.Presenter presenter;

    public static ThirdFragment newInstance(MeterObject meterObject) {
        ThirdFragment fragment = new ThirdFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("meterObj", meterObject);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third_edit, container, false);
        presenter = new EditRejectedMeterPresenter(this, this, new EditRejectedMeterModel());

        lastUpdate = (EditText) view.findViewById(R.id.last_updated);
        postCodeEditText = (EditText) view.findViewById(R.id.post_code);
        scecoNumberEditText = (EditText) view.findViewById(R.id.sceco_num);
        numberOfElectricMetersEditText = (EditText) view.findViewById(R.id.number_of_electric_meters);
        lastReadingEditText = (EditText) view.findViewById(R.id.last_reading);
        numberOfFloorsEditText = (EditText) view.findViewById(R.id.number_of_floors);


        valveTypeSpinner = (Spinner) view.findViewById(R.id.valve_type_spinner);
        valveStatusSpinner = (Spinner) view.findViewById(R.id.valve_status_spinner);
        enabledSpinner = (Spinner) view.findViewById(R.id.enabled_spinner);
        reducerDiameterSpinner = (Spinner) view.findViewById(R.id.reducer_diameter_spinner);

        lastUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestDatePickerDialog(lastUpdate.getText().toString().trim());
            }
        });


        presenter.requestThirdStepData((MeterObject) getArguments().getSerializable("meterObj"));
        return view;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        presenter.requestPassThirdStepDataToActivity(new MeterObject(
                valveTypeIds.get(valveTypeSpinner.getSelectedItemPosition()),
                valveStatusIds.get(valveStatusSpinner.getSelectedItemPosition()),
                enabledIds.get(enabledSpinner.getSelectedItemPosition()),
                reducerDiameterIds.get(reducerDiameterSpinner.getSelectedItemPosition()),
                lastUpdate.getText().toString().trim(), postCodeEditText.getText().toString().trim(),
                scecoNumberEditText.getText().toString().trim(),
                numberOfElectricMetersEditText.getText().toString().trim(),
                lastReadingEditText.getText().toString().trim(),
                numberOfFloorsEditText.getText().toString().trim()));
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public Context getAppContext() {
        return getContext();
    }


    private ArrayList<Integer> valveTypeIds, valveStatusIds, enabledIds, reducerDiameterIds;

    @Override
    public void loadSpinnersData(ArrayList<String> valveType, ArrayList<Integer> valveTypeIds, int valveTypeIndex,
                                 ArrayList<String> valveStatus, ArrayList<Integer> valveStatusIds, int valveStatusIndex,
                                 ArrayList<String> enabled, ArrayList<Integer> enabledIds, int enabledIndex,
                                 ArrayList<String> reducerDiameter, ArrayList<Integer> reducerDiameterIds, int reducerDiameterIndex) {


        this.valveTypeIds = valveTypeIds;
        this.valveStatusIds = valveStatusIds;
        this.enabledIds = enabledIds;
        this.reducerDiameterIds = reducerDiameterIds;

        valveTypeSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, valveType));
        valveTypeSpinner.setSelection(valveTypeIndex);
        valveStatusSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, valveStatus));
        valveStatusSpinner.setSelection(valveStatusIndex);
        enabledSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, enabled));
        enabledSpinner.setSelection(enabledIndex);
        reducerDiameterSpinner.setAdapter(new ArrayAdapter<String>(getAppContext(), android.R.layout.simple_spinner_item, reducerDiameter));
        reducerDiameterSpinner.setSelection(reducerDiameterIndex);

    }

    @Override
    public void setDate(String date) {
        lastUpdate.setText(date);
    }

    @Override
    public void setData(String postCode, String scecoNumber, String numberOfElectricMeters, String lastReading, String numberOfFloors) {
        postCodeEditText.setText(postCode);
        scecoNumberEditText.setText(scecoNumber);
        numberOfElectricMetersEditText.setText(numberOfElectricMeters);
        lastReadingEditText.setText(lastReading);
        numberOfFloorsEditText.setText(numberOfFloors);
    }
}
