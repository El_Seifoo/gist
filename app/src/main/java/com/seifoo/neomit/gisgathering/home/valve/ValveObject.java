package com.seifoo.neomit.gisgathering.home.valve;

import java.io.Serializable;

public class ValveObject implements Serializable {
    private long id, valveId;
    private String latitude, longitude;
    private int valveJob;
    private String serialNumber;
    private int material, diameter, type;
    private String fullNumberOfTurns, numberOfTurns;
    private int lock, status;
    private String elevation, groundElevation;
    private int coverStatus, enabled;
    private String dmaZone;
    private int districtName, subDistrict;
    private String streetName, sectorName;
    private int existInField, existInMap, waterSchedule, remarks;
    private String reason, createdAt;

    // first step const.
    public ValveObject(String latitude, String longitude, int valveJob, String serialNumber, int material, int diameter,
                       int type, String fullNumberOfTurns, String numberOfTurns, int lock, int status, String elevation, String groundElevation) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.valveJob = valveJob;
        this.serialNumber = serialNumber;
        this.material = material;
        this.diameter = diameter;
        this.type = type;
        this.fullNumberOfTurns = fullNumberOfTurns;
        this.numberOfTurns = numberOfTurns;
        this.lock = lock;
        this.status = status;
        this.elevation = elevation;
        this.groundElevation = groundElevation;
    }

    public ValveObject getFirstObject() {
        return new ValveObject(latitude, longitude, valveJob, serialNumber, material, diameter,
                type, fullNumberOfTurns, numberOfTurns, lock, status, elevation, groundElevation);
    }

    // second step const.
    public ValveObject(int coverStatus, int enabled, String dmaZone, int districtName, int subDistrict,
                       String streetName, String sectorName, int existInField, int existInMap, int waterSchedule, int remarks) {
        this.coverStatus = coverStatus;
        this.enabled = enabled;
        this.dmaZone = dmaZone;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.streetName = streetName;
        this.sectorName = sectorName;
        this.existInField = existInField;
        this.existInMap = existInMap;
        this.waterSchedule = waterSchedule;
        this.remarks = remarks;
    }

    public ValveObject getSecondObject() {
        return new ValveObject(coverStatus, enabled, dmaZone, districtName, subDistrict,
                streetName, sectorName, existInField, existInMap, waterSchedule, remarks);
    }


    public ValveObject(ValveObject firstObj, ValveObject secondObj) {
        this.latitude = firstObj.getLatitude();
        this.longitude = firstObj.getLongitude();
        this.valveJob = firstObj.getValveJob();
        this.serialNumber = firstObj.getSerialNumber();
        this.material = firstObj.getMaterial();
        this.diameter = firstObj.getDiameter();
        this.type = firstObj.getType();
        this.fullNumberOfTurns = firstObj.getFullNumberOfTurns();
        this.numberOfTurns = firstObj.getNumberOfTurns();
        this.lock = firstObj.getLock();
        this.status = firstObj.getStatus();
        this.elevation = firstObj.getElevation();
        this.groundElevation = firstObj.getGroundElevation();

        this.coverStatus = secondObj.getCoverStatus();
        this.enabled = secondObj.getEnabled();
        this.dmaZone = secondObj.getDmaZone();
        this.districtName = secondObj.getDistrictName();
        this.subDistrict = secondObj.getSubDistrict();
        this.streetName = secondObj.getStreetName();
        this.sectorName = secondObj.getSectorName();
        this.existInField = secondObj.getExistInField();
        this.existInMap = secondObj.getExistInMap();
        this.waterSchedule = secondObj.getWaterSchedule();
        this.remarks = secondObj.getRemarks();
    }

    public ValveObject(long id, long valveId, String latitude, String longitude, int valveJob,
                       String serialNumber, int material, int diameter, int type, String fullNumberOfTurns,
                       String numberOfTurns, int lock, int status, String elevation, String groundElevation,
                       int coverStatus, int enabled, String dmaZone, int districtName, int subDistrict, String streetName,
                       String sectorName, int existInField, int existInMap, int waterSchedule, int remarks, String createdAt) {
        this.id = id;
        this.valveId = valveId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.valveJob = valveJob;
        this.serialNumber = serialNumber;
        this.material = material;
        this.diameter = diameter;
        this.type = type;
        this.fullNumberOfTurns = fullNumberOfTurns;
        this.numberOfTurns = numberOfTurns;
        this.lock = lock;
        this.status = status;
        this.elevation = elevation;
        this.groundElevation = groundElevation;
        this.coverStatus = coverStatus;
        this.enabled = enabled;
        this.dmaZone = dmaZone;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.streetName = streetName;
        this.sectorName = sectorName;
        this.existInField = existInField;
        this.existInMap = existInMap;
        this.waterSchedule = waterSchedule;
        this.remarks = remarks;
        this.createdAt = createdAt;
    }

    public ValveObject(String latitude, String longitude, int valveJob, String serialNumber, int material,
                       int diameter, int type, String fullNumberOfTurns, String numberOfTurns, int lock,
                       int status, String elevation, String groundElevation, int coverStatus, int enabled,
                       String dmaZone, int districtName, int subDistrict, String streetName, String sectorName,
                       int existInField, int existInMap, int waterSchedule, int remarks, String createdAt) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.valveJob = valveJob;
        this.serialNumber = serialNumber;
        this.material = material;
        this.diameter = diameter;
        this.type = type;
        this.fullNumberOfTurns = fullNumberOfTurns;
        this.numberOfTurns = numberOfTurns;
        this.lock = lock;
        this.status = status;
        this.elevation = elevation;
        this.groundElevation = groundElevation;
        this.coverStatus = coverStatus;
        this.enabled = enabled;
        this.dmaZone = dmaZone;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.streetName = streetName;
        this.sectorName = sectorName;
        this.existInField = existInField;
        this.existInMap = existInMap;
        this.waterSchedule = waterSchedule;
        this.remarks = remarks;
        this.createdAt = createdAt;
    }

    public long getId() {
        return id;
    }

    public long getValveId() {
        return valveId;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public int getValveJob() {
        return valveJob;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public int getMaterial() {
        return material;
    }

    public int getDiameter() {
        return diameter;
    }

    public int getType() {
        return type;
    }

    public String getFullNumberOfTurns() {
        return fullNumberOfTurns;
    }

    public String getNumberOfTurns() {
        return numberOfTurns;
    }

    public int getLock() {
        return lock;
    }

    public int getStatus() {
        return status;
    }

    public String getElevation() {
        return elevation;
    }

    public String getGroundElevation() {
        return groundElevation;
    }

    public int getCoverStatus() {
        return coverStatus;
    }

    public int getEnabled() {
        return enabled;
    }

    public String getDmaZone() {
        return dmaZone;
    }

    public int getDistrictName() {
        return districtName;
    }

    public int getSubDistrict() {
        return subDistrict;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getSectorName() {
        return sectorName;
    }

    public int getExistInField() {
        return existInField;
    }

    public int getExistInMap() {
        return existInMap;
    }

    public int getWaterSchedule() {
        return waterSchedule;
    }

    public int getRemarks() {
        return remarks;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setValveId(long valveId) {
        this.valveId = valveId;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    // rejected const.
    public ValveObject(long valveId, String latitude, String longitude, int valveJob, String serialNumber,
                       int material, int diameter, int type, String fullNumberOfTurns, String numberOfTurns,
                       int lock, int status, String elevation, String groundElevation, int coverStatus,
                       int enabled, String dmaZone, int districtName, int subDistrict, String streetName,
                       String sectorName, int existInField, int existInMap, int waterSchedule, int remarks, String reason, String createdAt) {
        this.valveId = valveId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.valveJob = valveJob;
        this.serialNumber = serialNumber;
        this.material = material;
        this.diameter = diameter;
        this.type = type;
        this.fullNumberOfTurns = fullNumberOfTurns;
        this.numberOfTurns = numberOfTurns;
        this.lock = lock;
        this.status = status;
        this.elevation = elevation;
        this.groundElevation = groundElevation;
        this.coverStatus = coverStatus;
        this.enabled = enabled;
        this.dmaZone = dmaZone;
        this.districtName = districtName;
        this.subDistrict = subDistrict;
        this.streetName = streetName;
        this.sectorName = sectorName;
        this.existInField = existInField;
        this.existInMap = existInMap;
        this.waterSchedule = waterSchedule;
        this.remarks = remarks;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
