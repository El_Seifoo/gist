package com.seifoo.neomit.gisgathering.home;

import com.seifoo.neomit.gisgathering.R;
import com.seifoo.neomit.gisgathering.utils.Constants;
import com.seifoo.neomit.gisgathering.utils.DataBaseHelper;
import com.seifoo.neomit.gisgathering.utils.MySingleton;

import java.util.ArrayList;

public class HomePresenter implements HomeMVP.Presenter {
    private HomeMVP.View view;

    public HomePresenter(HomeMVP.View view) {
        this.view = view;
    }

    @Override
    public void requestListData() {
        ArrayList<Integer> imgs = new ArrayList<>();
        imgs.add(R.mipmap.meter);
        imgs.add(R.mipmap.main_line);
        imgs.add(R.mipmap.house_connection);
        imgs.add(R.mipmap.metering_point);
        imgs.add(R.mipmap.valve);
        imgs.add(R.mipmap.fire_hydrant);
        imgs.add(R.mipmap.chamber);
        imgs.add(R.mipmap.tanks);
        imgs.add(R.mipmap.pump);
        imgs.add(R.mipmap.leaks);
        imgs.add(R.mipmap.about);
        imgs.add(R.mipmap.log_out);

        String[] titles = view.getActContext().getResources().getStringArray(R.array.home_list_titles);

        view.loadListData(imgs, titles);
    }

    @Override
    public void onItemClicked(int position) {
        switch (position) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                view.navigateToDestination(position);
                break;
            case 10:
                //how to use
                break;
            default:
                //log out
                String username = MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.USER_NAME, "");
                String ip = MySingleton.getmInstance(view.getActContext()).getStringSharedPref(Constants.USER_IP, "");
                DataBaseHelper.getmInstance(view.getActContext()).deleteAllGeoMasters();
                MySingleton.getmInstance(view.getActContext()).logout();
                MySingleton.getmInstance(view.getActContext()).saveStringSharedPref(Constants.USER_IP, ip);
                MySingleton.getmInstance(view.getActContext()).saveStringSharedPref(Constants.USER_NAME, username);
                view.navigateLogin();

        }
    }
}
